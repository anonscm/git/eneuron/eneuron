#!/bin/sh
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

prefix=""

while getopts p: x
do
  case $x in
      p)  
	  prefix=${OPTARG}_ 
	  ;;
      
      '?') 
	  echo "Invalid option $x"
	  echo Usage $0 '[-p <prefix>]' '[<version file>]'
	  exit 1
	  ;;
  esac
done

shift $(($OPTIND-1))

if [ -z "$1" ] ; then
    source VERSION
else
    source $1
fi

eval MAJOR=\$${prefix}MAJOR
eval MINOR=\$${prefix}MINOR
eval PATCH=\$${prefix}PATCH

sed -e "s/__${prefix}MAJOR__/$MAJOR/g" \
    -e "s/__${prefix}MINOR__/$MINOR/g" \
    -e "s/__${prefix}PATCH__/$PATCH/g"
