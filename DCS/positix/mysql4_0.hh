/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : mysql4.0.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_dcs_mysql4_0_hh__
#define __positix_dcs_mysql4_0_hh__

#include <mysql.h>
#include <positix/multithreading.hh>

#include "dcs_driver.hh"

namespace positix_dcs {
namespace mysql4_0 {

class MySql;

struct MySqlConn : public SqlConnection
{
    inline MySqlConn(MySql& mysql_, unsigned id) throw ()
    : SqlConnection(id),
      mysql(mysql_)
    {
	// empty
    }

    MySql&     mysql;
    MYSQL      handle;
};


///////////////////////////////////////////////////////////////////////////////

class MySqlRow : public SqlRow
{
public:
    virtual ~MySqlRow();

    virtual const char* get   (unsigned index) const;
    virtual size_t      length(unsigned index) const;

    MYSQL_ROW      row;
    unsigned long* lengths;
}; 

///////////////////////////////////////////////////////////////////////////////

class MySqlResult : public SqlResult
{
public:
    inline MySqlResult(MYSQL_RES* myresult)
    : _result(myresult),
      _conn(0)
    {
	// empty
    }

    inline 
    MySqlResult(
	MYSQL_RES*   myresult, 
	MySqlConn*   conn)
    : _result(myresult),
      _conn(conn)
    {
	// empty
    }

    ~MySqlResult();

    virtual unsigned numberOfColumns() throw ();
    virtual SqlColumn getColumn(unsigned i);
    virtual const MySqlRow* fetchRow();    
    virtual SqlSerialNumber numberOfLocalRows() throw ();

private:
    MYSQL_RES*   _result;
    MySqlConn*   _conn;
    MySqlRow     _current;
};

///////////////////////////////////////////////////////////////////////////////

class MySql : public SqlDriver
{
public:
    static const positix_types::Version Version;
    static const char*                  DriverName;

    MySql(
	const std::string& address,
	const std::string& db,
	const std::string& user,
	const std::string& password);

    virtual const char* driverName() const throw ();
    virtual positix_types::Version version() const throw ();

    virtual
    MySqlResult*
    execute(
	const std::string& query,
	SqlModInfo* const  mod);

    virtual
    MySqlResult*
    execute(
	const char*       query,
	SqlModInfo* const mod);

//     virtual
//     SqlSerialNumber
//     makeRow(
// 	const SqlRowMaker&     rm,
// 	const SqlRequestParam* params,
// 	unsigned               nParams);

//     virtual
//     SqlResult*
//     execute(
// 	const SqlRequestPattern& rp,
// 	const SqlRequestParam*   params,
// 	unsigned                 nParams,
// 	bool                     fetchAll);


    MySqlResult*
    execute(
	const char*       query,
	unsigned long     length,
	SqlModInfo* const mod);

    virtual MySqlConn* newConnection(unsigned id);

private:
    friend class MySqlResult;
    SqlConnectionManager _idleConnections;
    std::string          _db;
    std::string          _pw;
};

///////////////////////////////////////////////////////////////////////////////

} // namespace mysql4_0
} // namespace positix_dcs

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: mysql4.0.hh
