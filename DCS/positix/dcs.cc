/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : dcs.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <positix/dl.hh>

#include "dcs.hh"
#include "interface.hh"
#include "dcs_driver.hh"
#include "msg.hh"

using namespace std;
using namespace positix;

inline const char* StrDup(const char* s)
{
    size_t len = strlen(s)+1;
    char* ns = new char[len];
    memcpy(ns, s, len);
    return ns;
}

namespace positix_dcs {

///////////////////////////////////////////////////////////////////////////////

positix::LogStream* TheLogStream = 0;
static LibraryCollection<SqlDriverFactory> TheSqlInterfaces;

SqlModInfo _SqlStoreLocal;
SqlModInfo _SqlStoreRemote;

///////////////////////////////////////////////////////////////////////////////

static const char* SqlInsQuote(ostream& os, const char* s)
{    
    char e = *s++;

    while ( *s )
    {
	if ( *s == e )
	{
	    os << e;
	    return s + 1;
	}
	
	if ( *s == '\\' )
	{
	    os << *s++;
	    if ( *s )
		os << *s++;
	}
	else
	{
	    os << *s++;
	}
    }

    // Enforce closing quote
    os << e;

    return s;
}

///////////////////////////////////////////////////////////////////////////////

ostream& SqlQuote(ostream& os, const char* s, size_t len)
{
    if ( len== 0 )
    {
	os << "''";
	return os;
    }
 
    os << '\'';
    do
    {
	switch ( *s )
	{
	default: 
	    os << *s++;
	    break;

	case '\'':
	case '\"':
	case '\\':
	    os << '\\' << *s++;
	    break;

	case 0   : ++s; os << "\\0"; break;
	case '\r': ++s; os << "\\r"; break;
	case '\t': ++s; os << "\\t"; break;
	}

    } while ( --len != 0 );

    return os << '\'';
}

///////////////////////////////////////////////////////////////////////////////

SqlState::SqlState(const char* stringcode) throw ()
{
    CodeValue code = 0;
    if ( stringcode )
    {
	unsigned cnt = 5;
	do
	{
	    char digit;
	    if ( *stringcode>='0' && *stringcode<='9' )
		digit = *stringcode - '0';
	    else if ( *stringcode>='A' && *stringcode<='Z' )
		digit = *stringcode - 'A';
	    else if ( *stringcode>='a' && *stringcode<='z' )
		digit = *stringcode - 'a';
	    else
		break;
	    code = code * CodeValue(36) + (unsigned char)digit;
	} while ( --cnt > 0 );
    }

    _code = code;        
}

///////////////////////////////////////////////////////////////////////////////

void SqlState::codestring(char* buf, unsigned len) const throw ()
{
    if ( !len || !buf )
	return;

    CodeValue code = _code;

    buf[len-1] = 0;

    while ( --len > 0 )
    {
	char digit = char(code % CodeValue(36));
	code /= CodeValue(36);
	if ( digit < 10 )
	    buf[len-1] = digit + '0';
	else
	    buf[len-1] = digit + 'A';	
    }
}

///////////////////////////////////////////////////////////////////////////////

std::string SqlState::codestring() const
{
    char str[6];
    codestring(str, sizeof(str));
    return std::string(str);
}

///////////////////////////////////////////////////////////////////////////////

ostream& operator<< (ostream& os, SqlState state)
{
    char str[6];
    state.codestring(str, sizeof(str));
    return os << str;
}

///////////////////////////////////////////////////////////////////////////////

SqlSerialNumber SqlResult::numberOfLocalRows() throw ()
{
    return 0;
}

///////////////////////////////////////////////////////////////////////////////

SqlConnector::~SqlConnector()
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

SqlResult*
SqlConnector::execute(
    const SqlRequestPattern& rp,
    const SqlRequestParam*   params,
    unsigned                 nParams,
    SqlModInfo* const        mod)
{
    const char* s = rp.pattern();
    ostringstream out;
    unsigned      i = 0;

    while ( *s )
    {
	if ( *s == '?' )
	{
	    if ( i >= nParams )
	    {
		throw std::logic_error(
		    TXT("Developer error: Not enough SqlRequestParam values"));
	    }

	    ++s;
	    out << params[i++];
	}
	else if ( *s == '\'' || *s == '\"' )
	    s = SqlInsQuote(out, s);
	else
	    out << *s++;
    }

    if ( i < nParams )
    {
	throw std::logic_error(
	    TXT("Developer error: Too many SqlRequestParam values"));
    }

    return execute(out.str(), mod);
}

///////////////////////////////////////////////////////////////////////////////

SqlSerialNumber
SqlConnector::makeRow(
    const SqlRowMaker&     rm,
    const SqlRequestParam* params,
    unsigned               nParams)
{
    const char** cols    = rm.cols();
    unsigned     nCols   = rm.nCols();
    unsigned     nKeys   = rm.nKeys();
    SqlModInfo   mod;
    auto_ptr<SqlResult> res;

    if ( nParams!=nCols )
    {
	throw std::logic_error(
	    TXT("Developer error: Bad number of SqlRequestParams passed"));
    }

    try
    {
	ostringstream out;
	out << "INSERT INTO " << rm.table() 
	    << " (" << cols[0];
	for (unsigned i = 1; i < nCols; ++i)
	    out << ',' << cols[i];
	out << ") VALUES (" << params[0];
	for (unsigned i = 1; i < nCols; ++i)
	    out << ',' << params[i];
	out << ')';
	res.reset( execute(out.str(), &mod) );
	return mod.lastSerial ? mod.lastSerial : ~SqlSerialNumber(0);
    }
    catch ( DatabaseError& e )
    {	
	if ( SqlState(e.code()).errClass()!=75  )
	    throw;
    }    
    catch ( ... )
    {
	throw;
    }

    if ( nKeys==nCols )
	return 0;

    { // Update

	ostringstream out;
	out << "UPDATE " << rm.table() 
	    << " SET " << cols[nKeys] << '=' << params[nKeys];
	for (unsigned i = nKeys+1; i < nCols; ++i)
	    out << ',' << cols[i] << '=' << params[i];
	if ( nKeys )
	{
	    out << " WHERE " << cols[0] << '=' << params[0];
	    for (unsigned i = 1; i < nKeys; ++i)
		out << " AND " << cols[i] << '=' << params[i];
	}
	res.reset( execute(out.str(), &mod) );
	return 0;
    }
}

///////////////////////////////////////////////////////////////////////////////

SqlRequestPattern::SqlRequestPattern(
    bool        copyStrings,
    const char* name,
    const char* pattern,
    bool        dontStore)
: _id(0),
  _name(0),
  _pattern(0),
  _stringsCopied(false),
  _dontStore(dontStore)
{
    if ( !copyStrings )
    {
	_name    = name;
	_pattern = pattern;
    }
    else
    {
	if ( name )
	{
	    _name = strdup(name);
	    if ( !_name )
		throw std::bad_alloc();
	}
	_pattern = strdup(pattern);
	if ( !_pattern )
	{
	    if ( _name )
		free(const_cast<char*>(_name));
	    throw::bad_alloc();
	}
	_stringsCopied = true;
    }
}

///////////////////////////////////////////////////////////////////////////////

SqlRequestPattern::~SqlRequestPattern()
{
    if ( _stringsCopied )
    {
	if (_name)    free(const_cast<char*>(_name));
	if (_pattern) free(const_cast<char*>(_pattern));
    }
}

///////////////////////////////////////////////////////////////////////////////

SqlRowMaker::SqlRowMaker(
    bool        copyStrings,
    const char* name,
    const char* table,
    const char* cols[],
    unsigned    nCols,
    unsigned    nKeys,
    bool        dontStore)
: _id(0),
  _name(0),
  _table(0),
  _cols(0),
  _nCols(0),
  _nKeys(nKeys),
  _stringsCopied(false),
  _dontStore(dontStore)
{
    if ( nCols==0 || nKeys==0 || nKeys>nCols )
    {
	throw std::logic_error(
	    TXT("Developer Error: Bad value passed to RowMaker constructor!"));
    }

    if ( !copyStrings )
    {
	_name  = name;
	_table = table;
	_cols  = cols;
	_nCols = nCols;
    }
    else
    {
	try
	{
	    _name = StrDup(name);
	    _cols  = new const char* [nCols];
	    for (unsigned i = 0; i < nCols; ++i)
	    {
		_cols[i] = StrDup(cols[i]);
		++_nCols;
	    }
	}
	catch ( ... )
	{
	    for (unsigned i = 0; i < _nCols; ++i)
		delete[] _cols[i];
	    delete[] _cols;
	    delete[] _name;
	    throw;
	}
	_stringsCopied = true;
    }
}

///////////////////////////////////////////////////////////////////////////////

SqlRowMaker::~SqlRowMaker()
{
    if ( _stringsCopied )
    {
	for (unsigned i = 0; i < _nCols; ++i)
	    delete[] _cols[i];
	delete[] _cols;
	delete[] _name;	
    }
}

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator << (std::ostream& os, const SqlRequestParam& p)
{
    if ( !p.size )
    {
	return os << " NULL ";
    }

    switch ( p.typecode )
    {
    case SqlTypeNone:
	os << " NULL ";
	break;

    case SqlTypeText:
    case SqlTypeVarChar:
    case SqlTypeChar:
    case SqlTypeBlob:
	SqlQuote(os, (const char*)p.ptr, p.size);
	break;

    case SqlTypeUnsignedTinyInt : os << unsigned(p.u8); break;
    case SqlTypeUnsignedSmallInt: os << p.u16;  break;
    case SqlTypeUnsignedInteger : os << p.u32;  break;
    case SqlTypeUnsignedBigInt  : os << p.u64;  break;

    case SqlTypeTinyInt : os << int(p.i8);   break;
    case SqlTypeSmallInt: os << p.i16;  break;
    case SqlTypeInteger : os << p.i32;  break;
    case SqlTypeBigInt  : os << p.i64;  break;

    case SqlTypeFloat   : os << p.flt;  break;
    case SqlTypeDouble  : os << p.dbl;  break;
    case SqlTypeReal    : os << p.ldbl; break;

    case SqlTypeBoolean : os << unsigned(p.u8); break;

    case SqlTypeTime:
      {
	const positix_types::Time* T = (const positix_types::Time*)p.time;
	os << "TIME\'" 
	   << T->hour << ':' 
	   << T->min  << ':'
	   << T->sec  << '\'';
	break;	    
      }

    case SqlTypeDate:
      {
	const positix_types::Date* D = (const positix_types::Date*)p.date;
	char c = os.fill('0');
	os << "DATE\'" 
	   << D->year  << '-' 
	   << setw(2) << D->month << '-'
	   << setw(2) << D->day   << '\'';
	os.fill(c);
	break;	    
      }

    case SqlTypeTimeStamp:
      {
	const positix_types::DateTime* DT
	    = (const positix_types::DateTime*)p.dt;
	char c = os.fill('0');
	os << "TIMESTAMP\'" 
	   << DT->year  << '-' 
	   << setw(2) << DT->month << '-'
	   << setw(2) << DT->day   << ' '
	   << setw(2) << DT->hour  << ':' 
	   << setw(2) << DT->min   << ':'
	   << setw(2) << DT->sec   << '\'';
	os.fill(c);
	break;	    
      }

    case SqlTypeDecimal:
    case SqlTypeNumeric:
    case SqlTypeBit:
    case SqlTypeVarBit:
    case SqlTypeInterval:
	throw std::logic_error("Requested SQL type not yet supported");

    case SqlTypeProprietary:
	throw std::logic_error("Cannot handle proprietary SQL types");
    }//switch

    return os;
}

///////////////////////////////////////////////////////////////////////////////

#define AsDriver(p) (static_cast<SqlDriver*>(p))

void SqlDB::connect(
    const std::string& interface,
    const std::string& address,
    const std::string& db,
    const std::string& user,
    const std::string& password)
{
    if ( _db )
    {
	AsDriver(_db)->unlink();
	_db = 0;
    }
    SqlDriverFactory& df = TheSqlInterfaces.load(interface, "positix_dcs_");
    _db = df.create(address, db, user, password);
}

///////////////////////////////////////////////////////////////////////////////

SqlDB::SqlDB(const SqlDB& db) throw ()
: _db(db._db)
{
    if ( _db )
	AsDriver(_db)->link();
}

///////////////////////////////////////////////////////////////////////////////

SqlDB& SqlDB::operator = (const SqlDB& db) throw ()
{
    if ( _db != db._db )
    {
	if ( _db )
	    AsDriver(_db)->unlink();
	_db = db._db;
	if ( _db )
	    AsDriver(_db)->link();
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

void SqlRow::get(unsigned index, positix::ByteBuf& bb) const
{
    const char* s = get(index);
    size_t len    = length(index);
    bb.use(len);
    CopyBytes(bb, ConstBytePtr(s), len);
}

///////////////////////////////////////////////////////////////////////////////

void SqlRow::TestOk(unsigned index, std::istringstream& in)
{
    if ( !in || in.get()!=istringstream::traits_type::eof() )
    {
      throw positix::Error(
	 Msg(TXT("SqlRow::get called for incompatible data format (index %0)"),
	     index));
    }
}

///////////////////////////////////////////////////////////////////////////////

SqlDB::~SqlDB() throw ()
{
    if ( _db )
	AsDriver(_db)->unlink();
}

#undef AsDriver

///////////////////////////////////////////////////////////////////////////////

SqlDriverFactory::SqlDriverFactory(
    positix::Library&  lib, 
    const std::string& libname)
{
    Library::Loader loader(lib, libname);
    create  = Creator  (loader("PxDcsCreate"));
    destroy = Destroyer(loader("PxDcsDestroy"));
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_dcs

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: dcs.cc

