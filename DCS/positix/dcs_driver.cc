/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : dcs_driver.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "dcs_driver.hh"
#include "msg.hh"

using namespace positix;

namespace positix_dcs {

///////////////////////////////////////////////////////////////////////////////

void SqlDriver::destroy() throw ()
{
    if ( _destroy )
	_destroy(this);
}

///////////////////////////////////////////////////////////////////////////////

SqlConnectionManager::SqlConnectionManager(unsigned mConnections) throw ()
: _idleConnections(0),
  _nConnections(0),
  _mConnections(mConnections)
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

SqlConnectionManager::~SqlConnectionManager() throw ()
{
    Mutex::Lock x(_mutex);
    SqlConnection* conn = _idleConnections;

    while ( conn )
    {
	SqlConnection* next = conn->_next;
	try
	{
	    delete conn;
	}
	catch ( ... )
	{
	    // Ignore exceptions
	}
	conn = next;
    }

    _idleConnections = 0;
    _mConnections    = 0;
}

///////////////////////////////////////////////////////////////////////////////

SqlConnection* SqlConnectionManager::seize(SqlDriver* db)
{
    Mutex::Lock x(_mutex);
    SqlConnection* conn;

    if ( !_idleConnections )
    {
	if ( _nConnections < _mConnections )
	{
	    conn = db->newConnection(_nConnections);
	    if ( conn )
		++_nConnections;
	    return conn;
	}

	if ( !_mConnections )
	{
	    throw DatabaseError(
		SqlState::ConnectionException,
		Msg(TXT("Shutting down database connections")));
	}

	while ( !_idleConnections )
	    _cond.wait(_mutex);
    }

    conn = _idleConnections;
    _idleConnections = conn->_next;
    
    return conn;
}

///////////////////////////////////////////////////////////////////////////////

void SqlConnectionManager::release(SqlConnection* conn) throw ()
{
    if ( conn )
    {
	Mutex::Lock x(_mutex);
	conn->_next = _idleConnections;
	_idleConnections = conn;
	_cond.broadcast();
    }
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_dcs

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: dcs_driver.cc

