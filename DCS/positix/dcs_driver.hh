/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : dcs_driver.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_dcs_driver_hh__
#define __positix_dcs_driver_hh__

#include <positix/multithreading.hh>
#include "dcs.hh"
#include "interface.hh"

namespace positix_dcs {

///////////////////////////////////////////////////////////////////////////////

class SqlConnectionManager;

class SqlConnection
{
public:
    inline SqlConnection(unsigned id) throw ()
    : _id(id),
      _next(0)
    {
	// empty
    }

    virtual ~SqlConnection() {};

    inline unsigned id() const throw ()
    {
	return _id;
    }

private:
    friend class SqlConnectionManager;
    unsigned       _id;
    SqlConnection* _next;
};

///////////////////////////////////////////////////////////////////////////////

class SqlDriver : public SqlConnector
{
public:
    inline SqlDriver(
	const std::string&          address,
	const std::string&          user,
	SqlDriverFactory::Destroyer destroyer = 0)
    : SqlConnector(address, user),
      _refCount(1),
      _destroy(destroyer)
    {
	// empty
    }

    virtual SqlConnection* newConnection(unsigned id) = 0;

    inline void link() throw ()
    {
	positix::Mutex::Lock x(_refMutex);
	++_refCount;
    }

    inline void unlink() throw ()
    {
	positix::Mutex::Lock x(_refMutex);
	if ( --_refCount == 0 )
	{
	    destroy();
	}
    }

protected:
    positix::FastMutex _refMutex;

private:
    unsigned                    _refCount;
    SqlDriverFactory::Destroyer _destroy;

    void destroy() throw ();
};

///////////////////////////////////////////////////////////////////////////////

class SqlConnectionManager
{
public:
    explicit SqlConnectionManager(unsigned mConnections) throw ();
    ~SqlConnectionManager() throw ();

    SqlConnection* seize(SqlDriver* db);
    void release(SqlConnection* conn) throw ();

private:
    positix::FastMutex _mutex;
    positix::CondVar   _cond;
    SqlConnection*     _idleConnections;
    unsigned           _nConnections;
    unsigned           _mConnections;
};

///////////////////////////////////////////////////////////////////////////////

class SqlActiveConnectionBase
{
public:
    inline SqlActiveConnectionBase(SqlDriver* db, SqlConnectionManager& scm)
    : _db(db),
      _scm(scm),
      _conn(scm.seize(db))
    {
	// empty
    }

    inline ~SqlActiveConnectionBase() throw ()
    {
	_scm.release(_conn);
    }

    inline SqlConnection* get() throw ()
    {
	return _conn;
    }

    inline SqlConnection* release() throw ()
    {
	SqlConnection* conn = _conn;
	_conn = 0;
	return conn;
    }

protected:
    SqlDriver*            _db;
    SqlConnectionManager& _scm;
    SqlConnection*        _conn;
};

///////////////////////////////////////////////////////////////////////////////

template <class Conn_>
class SqlActiveConnection : public SqlActiveConnectionBase
{
public:
    typedef Conn_ Conn;

    inline SqlActiveConnection(
	SqlDriver*            db, 
	SqlConnectionManager& scm)
    : SqlActiveConnectionBase(db, scm)
    {
	// empty
    }

    inline Conn* operator-> () const throw ()
    {
	return static_cast<Conn*>(_conn);
    }

    inline Conn* get() throw ()
    {
	return static_cast<Conn*>(_conn);
    }

    inline Conn* release() throw ()
    {
	SqlConnection* conn = _conn;
	_conn = 0;
	return static_cast<Conn*>(conn);
    }
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_dcs

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: dcs_driver.hh
