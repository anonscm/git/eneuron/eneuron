/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : mysql4.0.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <memory>
#include <stdexcept>
#include <mysql/mysqld_error.h>
#include "mysql4_0.hh"
#include "helpers.hh"
#include "msg.hh"

// If actually compiled with MySQL 4.1
#ifndef ER_TABLE_CANT_HANDLE_FULLTEXT 
#define ER_TABLE_CANT_HANDLE_FULLTEXT ER_TABLE_CANT_HANDLE_FT
#endif

#define STATE_00000 SqlState::SuccessfulCompletion
#define STATE_08004 SqlState(  373252)
#define STATE_08S01 SqlState(  409537)
#define STATE_21000 SqlState( 3405888)
#define STATE_21S01 SqlState( 3442177)
#define STATE_21S02 SqlState( 3442178)
#define STATE_23000 SqlState( 3499200)
#define STATE_25000 SqlState( 3592512)
#define STATE_40001 SqlState( 6718465)
#define STATE_42000 SqlState( 6811776)
#define STATE_42S01 SqlState( 6848065)
#define STATE_42S02 SqlState( 6848066)
#define STATE_42S12 SqlState( 6848102)
#define STATE_42S21 SqlState( 6848137)
#define STATE_42S22 SqlState( 6848138)
#define STATE_HY000 SqlState(30139776)
#define STATE_HY001 SqlState(30139777)

using namespace std;
using namespace positix;

namespace positix_dcs {
namespace mysql4_0 {

const positix_types::Version MySql::Version(1,0,0);
const char*                  MySql::DriverName = "mysql4.0";

///////////////////////////////////////////////////////////////////////////////

extern "C" SqlDriver* PxDcsCreate(
    	const std::string& address,
	const std::string& db,
	const std::string& user,
	const std::string& password)
{
    return new MySql(address, db, user, password);
}

///////////////////////////////////////////////////////////////////////////////

extern "C" void PxDcsDestroy(SqlDriver* d)
{
    delete d;
}

///////////////////////////////////////////////////////////////////////////////

static SqlState MySqlErrToSqlState(unsigned myerr) throw ()
{
    switch ( myerr )
    {
    default: 
	return SqlState::SystemError;

    case ER_HASHCHK                                   : return STATE_HY000;
    case ER_NISAMCHK                                  : return STATE_HY000;
    case ER_NO                                        : return STATE_HY000;
    case ER_YES                                       : return STATE_HY000;
    case ER_CANT_CREATE_FILE                          : return STATE_HY000;
    case ER_CANT_CREATE_TABLE                         : return STATE_HY000;
    case ER_CANT_CREATE_DB                            : return STATE_HY000;
    case ER_DB_CREATE_EXISTS                          : return STATE_HY000;
    case ER_DB_DROP_EXISTS                            : return STATE_HY000;
    case ER_DB_DROP_DELETE                            : return STATE_HY000;
    case ER_DB_DROP_RMDIR                             : return STATE_HY000;
    case ER_CANT_DELETE_FILE                          : return STATE_HY000;
    case ER_CANT_FIND_SYSTEM_REC                      : return STATE_HY000;
    case ER_CANT_GET_STAT                             : return STATE_HY000;
    case ER_CANT_GET_WD                               : return STATE_HY000;
    case ER_CANT_LOCK                                 : return STATE_HY000;
    case ER_CANT_OPEN_FILE                            : return STATE_HY000;
    case ER_FILE_NOT_FOUND                            : return STATE_HY000;
    case ER_CANT_READ_DIR                             : return STATE_HY000;
    case ER_CANT_SET_WD                               : return STATE_HY000;
    case ER_CHECKREAD                                 : return STATE_HY000;
    case ER_DISK_FULL                                 : return STATE_HY000;
    case ER_DUP_KEY                                   : return STATE_23000;
    case ER_ERROR_ON_CLOSE                            : return STATE_HY000;
    case ER_ERROR_ON_READ                             : return STATE_HY000;
    case ER_ERROR_ON_RENAME                           : return STATE_HY000;
    case ER_ERROR_ON_WRITE                            : return STATE_HY000;
    case ER_FILE_USED                                 : return STATE_HY000;
    case ER_FILSORT_ABORT                             : return STATE_HY000;
    case ER_FORM_NOT_FOUND                            : return STATE_HY000;
    case ER_GET_ERRNO                                 : return STATE_HY000;
    case ER_ILLEGAL_HA                                : return STATE_HY000;
    case ER_KEY_NOT_FOUND                             : return STATE_HY000;
    case ER_NOT_FORM_FILE                             : return STATE_HY000;
    case ER_NOT_KEYFILE                               : return STATE_HY000;
    case ER_OLD_KEYFILE                               : return STATE_HY000;
    case ER_OPEN_AS_READONLY                          : return STATE_HY000;
    case ER_OUTOFMEMORY                               : return STATE_HY001;
    case ER_OUT_OF_SORTMEMORY                         : return STATE_HY001;
    case ER_UNEXPECTED_EOF                            : return STATE_HY000;
    case ER_CON_COUNT_ERROR                           : return STATE_08004;
    case ER_OUT_OF_RESOURCES                          : return STATE_08004;
    case ER_BAD_HOST_ERROR                            : return STATE_08S01;
    case ER_HANDSHAKE_ERROR                           : return STATE_08S01;
    case ER_DBACCESS_DENIED_ERROR                     : return STATE_42000;
    case ER_ACCESS_DENIED_ERROR                       : return STATE_42000;
    case ER_NO_DB_ERROR                               : return STATE_42000;
    case ER_UNKNOWN_COM_ERROR                         : return STATE_08S01;
    case ER_BAD_NULL_ERROR                            : return STATE_23000;
    case ER_BAD_DB_ERROR                              : return STATE_42000;
    case ER_TABLE_EXISTS_ERROR                        : return STATE_42S01;
    case ER_BAD_TABLE_ERROR                           : return STATE_42S02;
    case ER_NON_UNIQ_ERROR                            : return STATE_23000;
    case ER_SERVER_SHUTDOWN                           : return STATE_08S01;
    case ER_BAD_FIELD_ERROR                           : return STATE_42S22;
    case ER_WRONG_FIELD_WITH_GROUP                    : return STATE_42000;
    case ER_WRONG_GROUP_FIELD                         : return STATE_42000;
    case ER_WRONG_SUM_SELECT                          : return STATE_42000;
    case ER_WRONG_VALUE_COUNT                         : return STATE_21S01;
    case ER_TOO_LONG_IDENT                            : return STATE_42000;
    case ER_DUP_FIELDNAME                             : return STATE_42S21;
    case ER_DUP_KEYNAME                               : return STATE_42000;
    case ER_DUP_ENTRY                                 : return STATE_23000;
    case ER_WRONG_FIELD_SPEC                          : return STATE_42000;
    case ER_PARSE_ERROR                               : return STATE_42000;
    case ER_EMPTY_QUERY                               : return STATE_42000;
    case ER_NONUNIQ_TABLE                             : return STATE_42000;
    case ER_INVALID_DEFAULT                           : return STATE_42000;
    case ER_MULTIPLE_PRI_KEY                          : return STATE_42000;
    case ER_TOO_MANY_KEYS                             : return STATE_42000;
    case ER_TOO_MANY_KEY_PARTS                        : return STATE_42000;
    case ER_TOO_LONG_KEY                              : return STATE_42000;
    case ER_KEY_COLUMN_DOES_NOT_EXITS                 : return STATE_42000;
    case ER_BLOB_USED_AS_KEY                          : return STATE_42000;
    case ER_TOO_BIG_FIELDLENGTH                       : return STATE_42000;
    case ER_WRONG_AUTO_KEY                            : return STATE_42000;
    case ER_READY                                     : return STATE_00000;
    case ER_NORMAL_SHUTDOWN                           : return STATE_00000;
    case ER_GOT_SIGNAL                                : return STATE_00000;
    case ER_SHUTDOWN_COMPLETE                         : return STATE_00000;
    case ER_FORCING_CLOSE                             : return STATE_08S01;
    case ER_IPSOCK_ERROR                              : return STATE_08S01;
    case ER_NO_SUCH_INDEX                             : return STATE_42S12;
    case ER_WRONG_FIELD_TERMINATORS                   : return STATE_42000;
    case ER_BLOBS_AND_NO_TERMINATED                   : return STATE_42000;
    case ER_TEXTFILE_NOT_READABLE                     : return STATE_HY000;
    case ER_FILE_EXISTS_ERROR                         : return STATE_HY000;
    case ER_LOAD_INFO                                 : return STATE_HY000;
    case ER_ALTER_INFO                                : return STATE_HY000;
    case ER_WRONG_SUB_KEY                             : return STATE_HY000;
    case ER_CANT_REMOVE_ALL_FIELDS                    : return STATE_42000;
    case ER_CANT_DROP_FIELD_OR_KEY                    : return STATE_42000;
    case ER_INSERT_INFO                               : return STATE_HY000;
//    case ER_UPDATE_TABLE_USED                         : return STATE_HY000;
    case ER_NO_SUCH_THREAD                            : return STATE_HY000;
    case ER_KILL_DENIED_ERROR                         : return STATE_HY000;
    case ER_NO_TABLES_USED                            : return STATE_HY000;
    case ER_TOO_BIG_SET                               : return STATE_HY000;
    case ER_NO_UNIQUE_LOGFILE                         : return STATE_HY000;
    case ER_TABLE_NOT_LOCKED_FOR_WRITE                : return STATE_HY000;
    case ER_TABLE_NOT_LOCKED                          : return STATE_HY000;
    case ER_BLOB_CANT_HAVE_DEFAULT                    : return STATE_42000;
    case ER_WRONG_DB_NAME                             : return STATE_42000;
    case ER_WRONG_TABLE_NAME                          : return STATE_42000;
    case ER_TOO_BIG_SELECT                            : return STATE_42000;
    case ER_UNKNOWN_ERROR                             : return STATE_HY000;
    case ER_UNKNOWN_PROCEDURE                         : return STATE_42000;
    case ER_WRONG_PARAMCOUNT_TO_PROCEDURE             : return STATE_42000;
    case ER_WRONG_PARAMETERS_TO_PROCEDURE             : return STATE_HY000;
    case ER_UNKNOWN_TABLE                             : return STATE_42S02;
    case ER_FIELD_SPECIFIED_TWICE                     : return STATE_42000;
    case ER_INVALID_GROUP_FUNC_USE                    : return STATE_42000;
    case ER_UNSUPPORTED_EXTENSION                     : return STATE_42000;
    case ER_TABLE_MUST_HAVE_COLUMNS                   : return STATE_42000;
    case ER_RECORD_FILE_FULL                          : return STATE_HY000;
    case ER_UNKNOWN_CHARACTER_SET                     : return STATE_42000;
    case ER_TOO_MANY_TABLES                           : return STATE_HY000;
    case ER_TOO_MANY_FIELDS                           : return STATE_HY000;
    case ER_TOO_BIG_ROWSIZE                           : return STATE_42000;
    case ER_STACK_OVERRUN                             : return STATE_HY000;
    case ER_WRONG_OUTER_JOIN                          : return STATE_42000;
    case ER_NULL_COLUMN_IN_INDEX                      : return STATE_42000;
    case ER_CANT_FIND_UDF                             : return STATE_HY000;
    case ER_CANT_INITIALIZE_UDF                       : return STATE_HY000;
    case ER_UDF_NO_PATHS                              : return STATE_HY000;
    case ER_UDF_EXISTS                                : return STATE_HY000;
    case ER_CANT_OPEN_LIBRARY                         : return STATE_HY000;
    case ER_CANT_FIND_DL_ENTRY                        : return STATE_HY000;
    case ER_FUNCTION_NOT_DEFINED                      : return STATE_HY000;
    case ER_HOST_IS_BLOCKED                           : return STATE_HY000;
    case ER_HOST_NOT_PRIVILEGED                       : return STATE_HY000;
    case ER_PASSWORD_ANONYMOUS_USER                   : return STATE_42000;
    case ER_PASSWORD_NOT_ALLOWED                      : return STATE_42000;
    case ER_PASSWORD_NO_MATCH                         : return STATE_42000;
    case ER_UPDATE_INFO                               : return STATE_HY000;
    case ER_CANT_CREATE_THREAD                        : return STATE_HY000;
    case ER_WRONG_VALUE_COUNT_ON_ROW                  : return STATE_21S01;
    case ER_CANT_REOPEN_TABLE                         : return STATE_HY000;
    case ER_INVALID_USE_OF_NULL                       : return STATE_42000;
    case ER_REGEXP_ERROR                              : return STATE_42000;
    case ER_MIX_OF_GROUP_FUNC_AND_FIELDS              : return STATE_42000;
    case ER_NONEXISTING_GRANT                         : return STATE_42000;
    case ER_TABLEACCESS_DENIED_ERROR                  : return STATE_42000;
    case ER_COLUMNACCESS_DENIED_ERROR                 : return STATE_42000;
    case ER_ILLEGAL_GRANT_FOR_TABLE                   : return STATE_42000;
    case ER_GRANT_WRONG_HOST_OR_USER                  : return STATE_42000;
    case ER_NO_SUCH_TABLE                             : return STATE_42S02;
    case ER_NONEXISTING_TABLE_GRANT                   : return STATE_42000;
    case ER_NOT_ALLOWED_COMMAND                       : return STATE_42000;
    case ER_SYNTAX_ERROR                              : return STATE_42000;
    case ER_DELAYED_CANT_CHANGE_LOCK                  : return STATE_HY000;
    case ER_TOO_MANY_DELAYED_THREADS                  : return STATE_HY000;
    case ER_ABORTING_CONNECTION                       : return STATE_08S01;
    case ER_NET_PACKET_TOO_LARGE                      : return STATE_08S01;
    case ER_NET_READ_ERROR_FROM_PIPE                  : return STATE_08S01;
    case ER_NET_FCNTL_ERROR                           : return STATE_08S01;
    case ER_NET_PACKETS_OUT_OF_ORDER                  : return STATE_08S01;
    case ER_NET_UNCOMPRESS_ERROR                      : return STATE_08S01;
    case ER_NET_READ_ERROR                            : return STATE_08S01;
    case ER_NET_READ_INTERRUPTED                      : return STATE_08S01;
    case ER_NET_ERROR_ON_WRITE                        : return STATE_08S01;
    case ER_NET_WRITE_INTERRUPTED                     : return STATE_08S01;
    case ER_TOO_LONG_STRING                           : return STATE_42000;
    case ER_TABLE_CANT_HANDLE_BLOB                    : return STATE_42000;
    case ER_TABLE_CANT_HANDLE_AUTO_INCREMENT          : return STATE_42000;
    case ER_DELAYED_INSERT_TABLE_LOCKED               : return STATE_HY000;
    case ER_WRONG_COLUMN_NAME                         : return STATE_42000;
    case ER_WRONG_KEY_COLUMN                          : return STATE_42000;
    case ER_WRONG_MRG_TABLE                           : return STATE_HY000;
    case ER_DUP_UNIQUE                                : return STATE_23000;
    case ER_BLOB_KEY_WITHOUT_LENGTH                   : return STATE_42000;
    case ER_PRIMARY_CANT_HAVE_NULL                    : return STATE_42000;
    case ER_TOO_MANY_ROWS                             : return STATE_42000;
    case ER_REQUIRES_PRIMARY_KEY                      : return STATE_42000;
    case ER_NO_RAID_COMPILED                          : return STATE_HY000;
    case ER_UPDATE_WITHOUT_KEY_IN_SAFE_MODE           : return STATE_HY000;
    case ER_KEY_DOES_NOT_EXITS                        : return STATE_HY000;
    case ER_CHECK_NO_SUCH_TABLE                       : return STATE_42000;
    case ER_CHECK_NOT_IMPLEMENTED                     : return STATE_42000;
    case ER_CANT_DO_THIS_DURING_AN_TRANSACTION        : return STATE_25000;
    case ER_ERROR_DURING_COMMIT                       : return STATE_HY000;
    case ER_ERROR_DURING_ROLLBACK                     : return STATE_HY000;
    case ER_ERROR_DURING_FLUSH_LOGS                   : return STATE_HY000;
    case ER_ERROR_DURING_CHECKPOINT                   : return STATE_HY000;
    case ER_NEW_ABORTING_CONNECTION                   : return STATE_08S01;
    case ER_DUMP_NOT_IMPLEMENTED                      : return STATE_HY000;
    case ER_FLUSH_MASTER_BINLOG_CLOSED                : return STATE_HY000;
    case ER_INDEX_REBUILD                             : return STATE_HY000;
    case ER_MASTER                                    : return STATE_HY000;
    case ER_MASTER_NET_READ                           : return STATE_08S01;
    case ER_MASTER_NET_WRITE                          : return STATE_08S01;
    case ER_FT_MATCHING_KEY_NOT_FOUND                 : return STATE_HY000;
    case ER_LOCK_OR_ACTIVE_TRANSACTION                : return STATE_HY000;
    case ER_UNKNOWN_SYSTEM_VARIABLE                   : return STATE_HY000;
    case ER_CRASHED_ON_USAGE                          : return STATE_HY000;
    case ER_CRASHED_ON_REPAIR                         : return STATE_HY000;
    case ER_WARNING_NOT_COMPLETE_ROLLBACK             : return STATE_HY000;
    case ER_TRANS_CACHE_FULL                          : return STATE_HY000;
    case ER_SLAVE_MUST_STOP                           : return STATE_HY000;
    case ER_SLAVE_NOT_RUNNING                         : return STATE_HY000;
    case ER_BAD_SLAVE                                 : return STATE_HY000;
    case ER_MASTER_INFO                               : return STATE_HY000;
    case ER_SLAVE_THREAD                              : return STATE_HY000;
    case ER_TOO_MANY_USER_CONNECTIONS                 : return STATE_42000;
    case ER_SET_CONSTANTS_ONLY                        : return STATE_HY000;
    case ER_LOCK_WAIT_TIMEOUT                         : return STATE_HY000;
    case ER_LOCK_TABLE_FULL                           : return STATE_HY000;
    case ER_READ_ONLY_TRANSACTION                     : return STATE_25000;
    case ER_DROP_DB_WITH_READ_LOCK                    : return STATE_HY000;
    case ER_CREATE_DB_WITH_READ_LOCK                  : return STATE_HY000;
    case ER_WRONG_ARGUMENTS                           : return STATE_HY000;
    case ER_NO_PERMISSION_TO_CREATE_USER              : return STATE_42000;
    case ER_UNION_TABLES_IN_DIFFERENT_DIR             : return STATE_HY000;
    case ER_LOCK_DEADLOCK                             : return STATE_40001;
    case ER_TABLE_CANT_HANDLE_FULLTEXT                : return STATE_HY000;
    case ER_CANNOT_ADD_FOREIGN                        : return STATE_HY000;
    case ER_NO_REFERENCED_ROW                         : return STATE_23000;
    case ER_ROW_IS_REFERENCED                         : return STATE_23000;
#if MYSQL_VERSION_ID>=40000
    case ER_CONNECT_TO_MASTER                         : return STATE_08S01;
    case ER_QUERY_ON_MASTER                           : return STATE_HY000;
    case ER_ERROR_WHEN_EXECUTING_COMMAND              : return STATE_HY000;
    case ER_WRONG_USAGE                               : return STATE_HY000;
    case ER_WRONG_NUMBER_OF_COLUMNS_IN_SELECT         : return STATE_21000;
    case ER_CANT_UPDATE_WITH_READLOCK                 : return STATE_HY000;
    case ER_MIXING_NOT_ALLOWED                        : return STATE_HY000;
    case ER_DUP_ARGUMENT                              : return STATE_HY000;
    case ER_USER_LIMIT_REACHED                        : return STATE_42000;
    case ER_SPECIFIC_ACCESS_DENIED_ERROR              : return STATE_HY000;
    case ER_LOCAL_VARIABLE                            : return STATE_HY000;
    case ER_GLOBAL_VARIABLE                           : return STATE_HY000;
    case ER_NO_DEFAULT                                : return STATE_42000;
    case ER_WRONG_VALUE_FOR_VAR                       : return STATE_42000;
    case ER_WRONG_TYPE_FOR_VAR                        : return STATE_42000;
    case ER_VAR_CANT_BE_READ                          : return STATE_HY000;
    case ER_CANT_USE_OPTION_HERE                      : return STATE_42000;
    case ER_NOT_SUPPORTED_YET                         : return STATE_42000;
    case ER_MASTER_FATAL_ERROR_READING_BINLOG         : return STATE_HY000;
    case ER_SLAVE_IGNORED_TABLE                       : return STATE_HY000;
#endif
//    case ER_WRONG_FK_DEF                              : return STATE_42000;
//    case ER_KEY_REF_DO_NOT_MATCH_TABLE_REF            : return STATE_HY000;
//    case ER_CARDINALITY_COL                           : return STATE_21000;
//    case ER_SUBSELECT_NO_1_ROW                        : return STATE_21000;
//    case ER_UNKNOWN_STMT_HANDLER                      : return STATE_HY000;
//    case ER_CORRUPT_HELP_DB                           : return STATE_HY000;
//    case ER_CYCLIC_REFERENCE                          : return STATE_HY000;
//    case ER_AUTO_CONVERT                              : return STATE_HY000;
//    case ER_ILLEGAL_REFERENCE                         : return STATE_42S22;
//    case ER_DERIVED_MUST_HAVE_ALIAS                   : return STATE_42000;
//    case ER_SELECT_REDUCED                            : return STATE_01000;
//    case ER_TABLENAME_NOT_ALLOWED_HERE                : return STATE_42000;
//    case ER_NOT_SUPPORTED_AUTH_MODE                   : return STATE_08004;
//    case ER_SPATIAL_CANT_HAVE_NULL                    : return STATE_42000;
//    case ER_COLLATION_CHARSET_MISMATCH                : return STATE_42000;
//    case ER_SLAVE_WAS_RUNNING                         : return STATE_HY000;
//    case ER_SLAVE_WAS_NOT_RUNNING                     : return STATE_HY000;
//    case ER_TOO_BIG_FOR_UNCOMPRESS                    : return STATE_HY000;
//    case ER_ZLIB_Z_MEM_ERROR                          : return STATE_HY000;
//    case ER_ZLIB_Z_BUF_ERROR                          : return STATE_HY000;
//    case ER_ZLIB_Z_DATA_ERROR                         : return STATE_HY000;
//    case ER_CUT_VALUE_GROUP_CONCAT                    : return STATE_HY000;
//    case ER_WARN_TOO_FEW_RECORDS                      : return STATE_01000;
//    case ER_WARN_TOO_MANY_RECORDS                     : return STATE_01000;
//    case ER_WARN_NULL_TO_NOTNULL                      : return STATE_01000;
//    case ER_WARN_DATA_OUT_OF_RANGE                    : return STATE_01000;
//    case ER_WARN_DATA_TRUNCATED                       : return STATE_01000;
//    case ER_WARN_USING_OTHER_HANDLER                  : return STATE_01000;
//    case ER_CANT_AGGREGATE_COLLATIONS                 : return STATE_42000;
//    case ER_DROP_USER                                 : return STATE_42000;
//    case ER_REVOKE_GRANTS                             : return STATE_42000;
//    case ER_CANT_AGGREGATE_3COLLATIONS                : return STATE_42000;
//    case ER_CANT_AGGREGATE_NCOLLATIONS                : return STATE_42000;
//    case ER_VARIABLE_IS_NOT_STRUCT                    : return STATE_HY000;
//    case ER_UNKNOWN_COLLATION                         : return STATE_HY000;
//    case ER_SLAVE_IGNORED_SSL_PARAMS                  : return STATE_HY000;
//    case ER_SERVER_IS_IN_SECURE_AUTH_MODE             : return STATE_HY000;
//    case ER_WARN_FIELD_RESOLVED                       : return STATE_HY000;
//    case ER_BAD_SLAVE_UNTIL_COND                      : return STATE_HY000;
//    case ER_MISSING_SKIP_SLAVE                        : return STATE_HY000;
//    case ER_UNTIL_COND_IGNORED                        : return STATE_HY000;
    }
}

///////////////////////////////////////////////////////////////////////////////

static unsigned MySqlToDcsTypeCode(const MYSQL_FIELD* field) throw ()
{
    bool u = field->flags & UNSIGNED_FLAG;

    switch ( field->type )
    {
    default: 
	return SqlTypeProprietary;

    case FIELD_TYPE_TINY:
	if ( field->length == 1 )
	    return ((SqlTypeTinyInt+u)<<1);
	return SqlTypeProprietary<<1;

    case FIELD_TYPE_INT24     : return ((SqlTypeBigInt+u)<<1)|1;
    case FIELD_TYPE_YEAR      : return (SqlTypeSmallInt<<1)|1;

    case FIELD_TYPE_SHORT     : return (SqlTypeSmallInt+u)<<1;
    case FIELD_TYPE_LONG      : return (SqlTypeInteger+u)<<1;
    case FIELD_TYPE_LONGLONG  : return (SqlTypeBigInt+u)<<1;
    case FIELD_TYPE_DECIMAL   : return SqlTypeDecimal<<1;
    case FIELD_TYPE_FLOAT     : return SqlTypeFloat<<1;
    case FIELD_TYPE_DOUBLE    : return SqlTypeDouble<<1;
    case FIELD_TYPE_STRING    : return SqlTypeChar<<1;
    case FIELD_TYPE_VAR_STRING: return SqlTypeVarChar<<1;
    case FIELD_TYPE_BLOB      : return SqlTypeBlob<<1;
    case FIELD_TYPE_DATE      : return SqlTypeDate<<1;
    case FIELD_TYPE_TIME      : return SqlTypeTime<<1;
    case FIELD_TYPE_TIMESTAMP : return SqlTypeTimeStamp<<1;
    }
}

///////////////////////////////////////////////////////////////////////////////

MySql::MySql(
    const std::string& address,
    const std::string& db,
    const std::string& user,
    const std::string& password)
: SqlDriver(address, user),
  _idleConnections(1),
  _db(db),
  _pw(password)
{
    _idleConnections.release(newConnection(0));
}

///////////////////////////////////////////////////////////////////////////////

const char* MySql::driverName() const throw ()
{
    return MySql::DriverName;
}

///////////////////////////////////////////////////////////////////////////////

positix_types::Version MySql::version() const throw ()
{
    return MySql::Version;
}

///////////////////////////////////////////////////////////////////////////////

MySqlConn* MySql::newConnection(unsigned id)
{
    auto_ptr<MySqlConn> aconn(new MySqlConn(*this, id));

    std::string host, pipe;
    unsigned int port;

    if ( TheLogStream )
    {
	try
	{
	    Format(LogDebug, *TheLogStream, 
		   "New connection MySQL %0,%1,%2", 
		   _address, _user, id);
	}
	catch ( ... )
	{
	    // Ignore exceptions
	}
    }

    SqlDecomposeAddress(_address, host, port, pipe);

    if ( !mysql_init(&aconn->handle) )
	throw std::bad_alloc();

    // TODO: Set appropriate options

    if ( mysql_real_connect(
	     &aconn->handle, 
	     host.c_str(), 
	     _user.c_str(), 
	     _pw.c_str(),
	     _db.empty() ? (const char*)0 : _db.c_str(),
	     port,
	     pipe.empty() ? (const char*)0 : pipe.c_str(),
	     0) // TODO: Check for nice flags to set 
	)
    {
	return aconn.release();
    }

    unsigned    myerr = mysql_errno(&aconn->handle);
    const char* mymsg = mysql_error(&aconn->handle);
    throw DatabaseError(
	MySqlErrToSqlState(myerr),
	myerr,
	Msg(TXT("Cannot connect to MySQL server at %0 as user %1: %2"), 
	    _address, _user, mymsg));
}

///////////////////////////////////////////////////////////////////////////////

MySqlResult*
MySql::execute(
    const char*       query,
    unsigned long     length,
    SqlModInfo* const mod)
{
    SqlActiveConnection<MySqlConn> conn(this, _idleConnections);
    MYSQL_RES*  myresult = 0;
    const char* msg      = 0;

    if ( TheLogStream )
    {
	try
	{
	    Format(LogDebug, *TheLogStream, "[MySQL %0,%1,%2] %3", 
		   _address, _user, conn->id(), query);
	}
	catch ( ... )
	{
	    // Ignore exceptions
	}
    }

    if ( mysql_real_query(&conn->handle, query, length)==0 )
    {
	try
	{
	    if ( mod==SqlStoreLocal )
	    {
		myresult = mysql_store_result(&conn->handle);
		if ( myresult || mysql_errno(&conn->handle)==0 )
		    return new MySqlResult(myresult);
		msg = TXT("Cannot store result for query %0: %1 [%2]");
	    }
	    else if ( mod==SqlStoreRemote )
	    {
		myresult = mysql_use_result(&conn->handle);
		if ( myresult )
		    return new MySqlResult(myresult, conn.release());
		msg = TXT("Cannot use result for query %0: %1 [%2]");
	    }
	    else
	    {
		myresult = mysql_use_result(&conn->handle);
		if ( myresult )
		    mysql_free_result(myresult);
		if ( mod )
		{
		    mod->lastSerial   = mysql_insert_id    (&conn->handle);
		    mod->affectedRows = mysql_affected_rows(&conn->handle);
		}
		return 0;
	    }
	}
	catch ( ... )
	{
	    if ( myresult )
		mysql_free_result(myresult);
	    throw;
	}
    }
    else
    {
	msg = TXT("Query %0 failed: %1 [%2]");
    }

    unsigned    myerr = mysql_errno(&conn->handle);
    const char* mymsg = mysql_error(&conn->handle);
    throw DatabaseError(MySqlErrToSqlState(myerr),
			myerr,
			Msg(msg, query, mymsg, myerr));
}

///////////////////////////////////////////////////////////////////////////////

MySqlResult*
MySql::execute(
    const std::string& query,
    SqlModInfo* const  mod)
{
    return MySql::execute(query.c_str(), query.length(), mod);
}

///////////////////////////////////////////////////////////////////////////////

MySqlResult*
MySql::execute(
    const char*       query,
    SqlModInfo* const mod)
{
    return MySql::execute(query, strlen(query), mod);
}

///////////////////////////////////////////////////////////////////////////////

// SqlSerialNumber
// MySql::makeRow(
//     const SqlRowMaker&     rm,
//     const SqlRequestParam* params,
//     unsigned               nParams)
// {
//     unsigned      nCols = rm.nCols();
//     const char**  cols  = rm.cols();
//     ostringstream os;

//     if ( nParams!=nCols )
//     {
// 	throw std::logic_error(
// 	    TXT("Developer error: Bad number of SqlRequestParams passed"));
//     }

//     os << "REPLACE " << rm.table() << " (" << cols[0];
//     for (unsigned i = 1; i < nCols; ++i)
// 	os << ',' << cols[i];
//     os << ") VALUES (" << params[0];
//     for (unsigned i = 1; i < nCols; ++i)
// 	os << ',' << params[i];
//     os << ")";

//     string       cmd    = os.str();
//     MySqlResult* result = execute(cmd.c_str(), cmd.length(), true);
//     SqlSerialNumber sn  = result->affectedRows()==1 ? result->lastSerial() : 0;
//     delete result;
//     return sn;
// }

///////////////////////////////////////////////////////////////////////////////

// SqlResult*
// MySql::execute(
//     const SqlRequestPattern& rp,
//     const SqlRequestParam*   params,
//     unsigned                 nParams,
//     bool                     fetchAll)
// {
//     MySqlConn* conn = getConnection();

//     // TODO: Make more portable (C99 may not supported by all compilers):
//     MYSQL_BIND bind[nParams];

//     return 0;
// }

///////////////////////////////////////////////////////////////////////////////

MySqlResult::~MySqlResult()
{
    if ( _result )
	mysql_free_result(_result);

    if ( _conn )
	_conn->mysql._idleConnections.release(_conn);
}

///////////////////////////////////////////////////////////////////////////////

unsigned
MySqlResult::numberOfColumns() throw ()
{
    return _result ? mysql_num_fields(_result) : 0;
}

///////////////////////////////////////////////////////////////////////////////

SqlColumn
MySqlResult::getColumn(unsigned i)
{
    SqlColumn col;
    memset(&col, 0, sizeof(col));

    if ( _result )
    {
	MYSQL_FIELD* field = mysql_fetch_field_direct(_result, i);
	if ( field )
	{
	    unsigned flags = field->flags;
	    unsigned tc    = MySqlToDcsTypeCode(field);

	    col.name             = field->name;
	    col.typeCode         = SqlTypeCode(tc>>1);
	    col.defaultValue     = field->def;
	    col.width            = field->length;
	    col.maxWidth         = field->max_length;
	    col.fApproximateType = (tc & 1);
	    col.fNotNull         = ((flags & NOT_NULL_FLAG)   != 0);
	    col.fPrimaryKey      = ((flags & PRI_KEY_FLAG)    != 0);
	    col.fUnique          = ((flags & UNIQUE_KEY_FLAG) != 0);

	    col.fNotNullValid    = 1;
	    col.fPrimaryKeyValid = 1;
	    col.fUniqueValid     = 1;
	}
    }
    
    return col;
}

///////////////////////////////////////////////////////////////////////////////

const MySqlRow*
MySqlResult::fetchRow()
{
    _current.row = mysql_fetch_row(_result);
    if ( _current.row )
    {
	_current.lengths = mysql_fetch_lengths(_result);
	return &_current;
    }
    else
	return 0;
}

///////////////////////////////////////////////////////////////////////////////

SqlSerialNumber MySqlResult::numberOfLocalRows() throw ()
{
    return _conn ? 0 : mysql_num_rows(_result);
}

///////////////////////////////////////////////////////////////////////////////

MySqlRow::~MySqlRow()
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

const char*
MySqlRow::get(unsigned index) const
{
    return row ? row[index] : 0;
}

///////////////////////////////////////////////////////////////////////////////

size_t
MySqlRow::length(unsigned index) const
{
    return row ? lengths[index] : 0;
}


///////////////////////////////////////////////////////////////////////////////

} // namespace mysql4_0
} // namespace positix_dcs


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: mysql4.0.cc
