/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : postgresql7.4.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <memory>
#include "postgresql7_4.hh"
#include "postgresql7_4_oids.h"
#include "helpers.hh"
#include "msg.hh"

using namespace std;
using namespace positix;

namespace positix_dcs {
namespace postgresql7_4 {

const positix_types::Version Postgres::Version(1,0,0);
const char*                  Postgres::DriverName = "PostgreSQL7.4";

///////////////////////////////////////////////////////////////////////////////

extern "C" SqlDriver* PxDcsCreate(
    	const std::string& address,
	const std::string& db,
	const std::string& user,
	const std::string& password)
{
    return new Postgres(address, db, user, password);
}

///////////////////////////////////////////////////////////////////////////////

extern "C" void PxDcsDestroy(SqlDriver* d)
{
    delete d;
}

///////////////////////////////////////////////////////////////////////////////

static unsigned PostgresToDcsTypeCode(Oid oid) throw ()
{
    switch ( oid )
    {
    default: 
	return SqlTypeProprietary<<1;

    case BOOLOID     : return (SqlTypeBoolean<<1);   // boolean
    case INT2OID     : return (SqlTypeSmallInt<<1);  // smallint
    case INT4OID     : return (SqlTypeInteger<<1);   // integer
    case INT8OID     : return (SqlTypeBigInt<<1);    // bigint
    case NUMERICOID  : return (SqlTypeNumeric<<1);   // numeric
    case FLOAT4OID   : return (SqlTypeReal<<1);      // real
    case FLOAT8OID   : return (SqlTypeReal<<1);      // double precision
    case CHAROID     : return (SqlTypeChar<<1);      // char
    case VARCHAROID  : return (SqlTypeVarChar<<1);   // varchar
    case TEXTOID     : return (SqlTypeText<<1);      // text
    case BITOID      : return (SqlTypeBit<<1);       // bit
    case VARBITOID   : return (SqlTypeVarBit<<1);    // bit varying
    case BYTEAOID    : return (SqlTypeBlob<<1)|1;    // bytea
    case DATEOID     : return (SqlTypeDate<<1);      // date
    case TIMEOID     : return (SqlTypeTime<<1);      // time
    case TIMESTAMPOID: return (SqlTypeTimeStamp<<1); // timestamp
    case INTERVALOID : return (SqlTypeInterval<<1);   // interval
    }
}

///////////////////////////////////////////////////////////////////////////////

Postgres::Postgres(
    const std::string& address,
    const std::string& db,
    const std::string& user,
    const std::string& password)
: SqlDriver(address, user),
  _idleConnections(1),
  _db(db),
  _pw(password)
{
    _idleConnections.release(newConnection(0));
}

///////////////////////////////////////////////////////////////////////////////

const char* Postgres::driverName() const throw ()
{
    return Postgres::DriverName;
}

///////////////////////////////////////////////////////////////////////////////

positix_types::Version Postgres::version() const throw ()
{
    return Postgres::Version;
}

///////////////////////////////////////////////////////////////////////////////

PostgresConn* Postgres::newConnection(unsigned id)
{
    std::string host, pipe;
    unsigned port;
    char cport[6];
    const char* sport;

    SqlDecomposeAddress(_address, host, port, pipe);

    if ( pipe.length() > 0 )
	sport = pipe.c_str();
    else if ( port!=0 )
    {
	sprintf(cport, "%u", port & 0xFFFF);
	sport = cport;
    }
    else
	sport = 0; 


   if ( TheLogStream )
   {
	try
	{
	    Format(LogDebug, *TheLogStream, 
		   "New connection PostgreSQL %0,%1,%2", 
		   _address, _user, id);
	}
	catch ( ... )
	{
	    // Ignore exceptions
	}
   }

   PGconn* conn = PQsetdbLogin(host.c_str(),  //pghost
			       sport,         //pgport
			       0,             //pgoptions
			       0,             //pgtty
			       _db.c_str(),   //dbName
			       _user.c_str(), //login
			       _pw.c_str()    //pwd
       );

   if ( !conn )
	throw std::bad_alloc();

    if ( PQstatus(conn)!=CONNECTION_OK )
    {
	string msg(PQerrorMessage(conn));
	PQfinish(conn);
	throw DatabaseError(SqlState::UnableToEstablishConnection, msg);    
    }

    try
    {
	return new PostgresConn(*this, id, conn);
    }
    catch ( ... )
    {
	PQfinish(conn);
	throw;
    }
}

///////////////////////////////////////////////////////////////////////////////

PostgresResult*
Postgres::execute(
    const char*   query,
    bool          /*fetchAll*/)
{
    SqlActiveConnection<PostgresConn> conn(this, _idleConnections);

    if ( TheLogStream )
    {
	try
	{
	    Format(LogDebug, *TheLogStream, "[PostgreSQL %0,%1,%2] %3", 
		   _address, _user, conn->id(), query);
	}
	catch ( ... )
	{
	    // Ignore exceptions
	}
    }


    PGresult* pgres = PQexec(conn->handle, query);
    try
    {
	ExecStatusType est = pgres ? PQresultStatus(pgres) : PGRES_FATAL_ERROR;

	if ( est!=PGRES_COMMAND_OK && est!=PGRES_TUPLES_OK )
	{
	    SqlState state(
		pgres
		? SqlState(PQresultErrorField(pgres, PG_DIAG_SQLSTATE))
		: SqlState::SystemError);

	    throw DatabaseError(
		state,
		Msg(TXT("Database %0,%1,%2: Query %3 failed: %4 [%5]"),
		    _address, _user, conn->id(),
		    query, PQerrorMessage(conn->handle), state));
	}

	return new PostgresResult(pgres);
    }
    catch ( ... )
    {
	if ( pgres )
	    PQclear(pgres);
	throw;
    }
}

///////////////////////////////////////////////////////////////////////////////

PostgresResult*
Postgres::execute(
    const std::string& query,
    bool               fetchAll)
{
    return Postgres::execute(query.c_str(), fetchAll);
}

///////////////////////////////////////////////////////////////////////////////

// SqlResult*
// Postgres::execute(
//     const SqlRequestPattern& rp,
//     const SqlRequestParam*   params,
//     unsigned                 nParams,
//     bool                     fetchAll)
// {
//     PostgresConn* conn = getConnection();

//     // TODO: Make more portable (C99 may not supported by all compilers):
//     POSTGRES_BIND bind[nParams];

//     return 0;
// }

///////////////////////////////////////////////////////////////////////////////

PostgresResult::~PostgresResult()
{
    if ( _result )
	PQclear(_result);
}

///////////////////////////////////////////////////////////////////////////////

unsigned
PostgresResult::numberOfColumns() throw ()
{
    return _result ? PQnfields(_result) : 0;
}

///////////////////////////////////////////////////////////////////////////////

SqlColumn
PostgresResult::getColumn(unsigned i)
{
    SqlColumn col;
    memset(&col, 0, sizeof(col));

    if ( _result )
    {
	unsigned tc = PostgresToDcsTypeCode(PQftype(_result, i));

	col.name             = PQfname(_result, i);
	col.typeCode         = SqlTypeCode(tc>>1);
	col.defaultValue     = 0;
	col.width            = PQfsize(_result, i);
	col.maxWidth         = PQfmod(_result, i);
	col.fApproximateType = (tc & 1);
	col.fNotNull         = 0;
	col.fPrimaryKey      = 0;
	col.fUnique          = 0;

	col.fNotNullValid    = 0;
	col.fPrimaryKeyValid = 0;
	col.fUniqueValid     = 0;
    }
    
    return col;
}

///////////////////////////////////////////////////////////////////////////////

const PostgresRow*
PostgresResult::fetchRow()
{
    if ( _result && _current.rowNumber+1 < PQntuples(_result) )
    {
	++_current.rowNumber;
	return &_current;
    }
    else
	return 0;
}

///////////////////////////////////////////////////////////////////////////////

PostgresRow::~PostgresRow()
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

const char*
PostgresRow::get(unsigned index) const
{
    return pgres ? PQgetvalue(pgres, rowNumber, index) : 0;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace postgresql7_4
} // namespace positix_dcs


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: postgresql7.4.cc
