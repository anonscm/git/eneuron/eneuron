/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : dcs.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_dcs_interface_hh__
#define __positix_dcs_interface_hh__

#include <positix/dl.hh>

namespace positix_dcs {

class SqlDriver;

///////////////////////////////////////////////////////////////////////////////

class SqlDriverFactory
{
public:
    typedef SqlDriver* (*Creator)(
	const std::string& address,
	const std::string& db,
	const std::string& user,
	const std::string& password);

    typedef void (*Destroyer)(SqlDriver* d);

    Creator   create;
    Destroyer destroy;

    inline SqlDriverFactory() throw () {}
    SqlDriverFactory(positix::Library& lib, const std::string& name);
};

///////////////////////////////////////////////////////////////////////////////

} //namespace positix_dcs

#endif
