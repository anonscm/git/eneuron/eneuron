/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : helpers.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_dcs_helpers_hh__
#define __positix_dcs_helpers_hh__

#include <string>

namespace positix_dcs {

///////////////////////////////////////////////////////////////////////////////

extern
void
SqlDecomposeAddress(
    const std::string& addr,
    std::string&       host,
    unsigned&          port,
    std::string&       file);

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_dcs

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: helpers.hh
