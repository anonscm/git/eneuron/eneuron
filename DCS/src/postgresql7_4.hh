/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : postgresql7.4.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_dcs_postgres7_4_hh__
#define __positix_dcs_postgres7_4_hh__

#include <pgsql/libpq-fe.h>
#include <positix/multithreading.hh>

#include "dcs.hh"
#include "dcs_driver.hh"

namespace positix_dcs {
namespace postgresql7_4 {

class Postgres;

struct PostgresConn : public SqlConnection
{
    PostgresConn(Postgres& postgres_, unsigned id, PGconn* handle_)
    : SqlConnection(id),
      postgres(postgres_),
      handle(handle_)
    {
	// empty
    }

    Postgres& postgres;
    PGconn*   handle;
};


///////////////////////////////////////////////////////////////////////////////

class PostgresRow : public SqlRow
{
public:
    PostgresRow(PGresult* pgres_, int rowNumber_)
    : pgres(pgres_),
      rowNumber(rowNumber_)
    {
	// empty
    }

    virtual ~PostgresRow();

    virtual const char* get(unsigned index) const;

    PGresult* pgres;
    int       rowNumber;
}; 

///////////////////////////////////////////////////////////////////////////////

class PostgresResult : public SqlResult
{
public:
    explicit inline PostgresResult(PGresult* result)
    : _result(result),
      _current(result, -1)
    {
	// empty
    }

    ~PostgresResult();

    virtual unsigned numberOfColumns() throw ();
    virtual SqlColumn getColumn(unsigned i);
    virtual const PostgresRow* fetchRow();

private:
    PGresult*   _result;
    PostgresRow _current;
};

///////////////////////////////////////////////////////////////////////////////

class Postgres : public SqlDriver
{
public:
    static const positix_types::Version Version;
    static const char*                  DriverName;

    Postgres(
	const std::string& address,
	const std::string& db,
	const std::string& user,
	const std::string& password);

    virtual const char* driverName() const throw ();
    virtual positix_types::Version version() const throw ();

    virtual
    PostgresResult*
    execute(
	const std::string& query,
	bool               fetchAll);

    virtual
    PostgresResult*
    execute(
	const char* query,
	bool        fetchAll);	

//     virtual
//     SqlResult*
//     execute(
// 	const SqlRequestPattern& rp,
// 	const SqlRequestParam*   params,
// 	unsigned                 nParams,
// 	bool                     fetchAll);


    virtual PostgresConn* newConnection(unsigned id);

private:
    SqlConnectionManager _idleConnections;
    std::string          _db;
    std::string          _pw;
};

///////////////////////////////////////////////////////////////////////////////

} // namespace postgresql7_4
} // namespace positix_dcs

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: postgresql7.4.hh
