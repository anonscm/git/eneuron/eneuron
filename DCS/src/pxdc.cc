/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : pxdc.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include "dcs.hh"
#include "msg.hh"

using namespace std;
using namespace positix_dcs;

string Interface("mysql");
string Address("localhost");
string DbName("pxmf");
string User("root");
string Password("");
bool   FetchAll = false;

positix::LogStream TheLog;

///////////////////////////////////////////////////////////////////////////////

static void PrintResult(SqlResult* result)
{
    unsigned nColumns = result->numberOfColumns();
    unsigned count = 0;

    if ( nColumns>0 )
    {
	for (unsigned i = 0;;)
	{
	    SqlColumn col = result->getColumn(i);
	    
	    cout << col.name;
	    if ( ++i == nColumns )
	    {
		cout << endl;
		break;
	    }
	    cout << '|';
	}

	// Print results
	for(const SqlRow* row=result->fetchRow(); row; row=result->fetchRow())
	{
	    unsigned i = 0;
	    ++count;
	    for(;;)
	    {
		cout << row->get(i);
		if ( ++i == nColumns )
		{
		    cout << endl;
		    break;
		}
		cout << '|';
	    }
	}
    }

    cout << "\n#" << count << endl;
}

///////////////////////////////////////////////////////////////////////////////

static void Execute(SqlDB db, const char* query)
{
    // Execute query
    cerr << "BEGIN\n";
    auto_ptr<SqlResult> result(db->execute(query, SqlStoreLocal));
    cerr << "END\n";

    PrintResult(result.get());
}

///////////////////////////////////////////////////////////////////////////////

static int Main(int argc, char* argv[])
{
    SqlDB db(Interface, Address, DbName, User, Password);

    positix_dcs::TheLogStream = &TheLog;

    for (int i = 1; i < argc; ++i)
    {
	Execute(db, argv[i]);
    }

//     cout << "\n-----------------\n";
//     SqlRequestPattern P(false, 0, 
// 			"select * from Users where UserId=? or UserName=?");
//     SqlRequestParams<2> params;
//     int         x = 0;
//     const char* s = "roland";
//     params[0] = SqlRequestParam(&x);
//     params[1] = SqlRequestParam(s, (size_t) strlen(s));
//     auto_ptr<SqlResult> result(db->execute(P, params));
//     PrintResult(result.get());

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    try
    {
	return Main(argc, argv);
    }
    catch ( positix::Error& e )
    {
	cerr << e.msg() << endl;
    }
    catch ( std::exception& e )
    {
	cerr << TXT("A runtime exception was raised: ") << e.what() << endl;
    }
    catch ( ... )
    {
	cerr << TXT("An unknown exception was raised") << endl;
    }

    return 2;
}

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: pxcd.cc

