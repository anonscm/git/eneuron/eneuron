/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix DCS
// File  : dcs.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_dcs_hh__
#define __positix_dcs_hh__

#include <sstream>
#include <positix/bytes.hh>
#include <positix/error.hh>
#include <positix/log.hh>
#include <positix/time.hh>
#include <positix/def.hh>

namespace positix_dcs {

extern positix::LogStream* TheLogStream;

typedef positix_types::fu64 SqlSerialNumber;

///////////////////////////////////////////////////////////////////////////////

class SqlState
{
public:
    typedef positix_types::fu32 CodeValue;

    enum Code
    {
	// Class 00
	SuccessfulCompletion                               =        0, // 00000

	// Class 01
	Warning                                            =    46656, // 01000
	WarningCursorOperationConflict                     =    46657, // 01001
	WarningDisconnectError                             =    46658, // 01002
	WarningNullValueEliminatedInSetFunction            =    46659, // 01003
	WarningStringDataRightTruncation                   =    46660, // 01004
	WarningInsufficientItemDescriptorAreas             =    46661, // 01005
	WarningPrivilegeNotRevoked                         =    46662, // 01006
	WarningPrivilegeNotGranted                         =    46663, // 01007
	WarningImplicitZeroBitPadding                      =    46664, // 01008
	WarningSearchConditionTooLong                      =    46665, // 01009
	WarningQueryExpressionTooLong                      =    46666, // 0100A
	WarningDynamicResultSetsReturned                   =    46667, // 0100C
	

	// Class 02
	NoData                                             =    93312, // 02000
	NoAdditionDynamicResultSetsReturned                =    93313, // 02001

	// Class 03
	SqlStatementNotYetComplete                         =   139968, // 03000

	// Class 08
	ConnectionException                                =   373248, // 08000
	UnableToEstablishConnection                        =   373249, // 08001
	ConnectionNameInUse                                =   373250, // 08002
	ConnectionDoesNotExist                             =   373251, // 08003
	ServerRejectedConnection                           =   373252, // 08004
	// ?                                               =   373253  // 08005
	ConnectionFailure                                  =   373254, // 08006
	TransactionResolutionUnknown                       =   373255, // 08007

	// Class 09
	TriggeredActionException                           =   419904, // 09000

	// Class 0A
	FeatureNotSupported                                =   466560, // 0A000

	// Class 0B
	InvalidTransactionInitiation                       =   513216, // 0B000

	// Class 0F
	LocatorException                                   =   699840, // 0F000
	
	// Class 21
	CardinalityViolation                               =  3405888, // 21000

	// Class 22
	DataException                                      =  3452544, // 22000

	// Class 23
	IntegrityConstraintViolation                       =  3499200, // 23000

	// Class 24
	InvalidCursorState                                 =  3545856, // 24000

	// Class 25
	InvalidTransactionState                            =  3592512, // 25000

	// Class 26
	InvalidSqlStatementName                            =  3639168, // 26000

	// Class 27
	TriggeredDataChangeViolation                       =  3685824, // 27000

	// Class 28
	InvalidAuthorizationSpecification                  =  3732480, // 28000

	// Class 2A
	DirectSqlSyntaxErrorOrAccessRuleViolation          =  3825792, // 2A000

	// Class 2B
	DependentPrivilegeDescriptorsStillExists           =  3872448, // 2B000

	// Class 2C
	InvalidCharacterSetName                            =  3919104, // 2C000

	// Class 2D
	InvalidTransactionTermination                      =  3965760, // 2D000
	
	// Class 2E
	InvalidConnectionName                              =  4012416, // 2E000

	// Class 2F
	SqlRoutineException                                =  4059072, // 2F000

	// Class 33
	InvalidSqlDescriptorName                           =  5178816, // 33000

	// Class 34
	InvalidCursorName                                  =  5225472, // 34000

	// Class 35
	InvalidConditionNumber                             =  5272128, // 35000

	// Class 37
	DynamicSqlSyntaxErrorOrAccessRuleViolation         =  5365440, // 37000

	// Class 38
	ExternalRoutineException                           =  5412096, // 38000

	// Class 39
	ExternalRoutineInvocationException                 =  5458752, // 39000

	// Class 3C
	AmbiguousCursorName                                =  5598720, // 3C000

	// Class 3D
	InvalidCatalogName                                 =  5645376, // 3D000

	// Class 3F
	InvalidSchemaName                                  =  5738688, // 3F000
	
	// Class 40
	TransactionRollback                                =  6718464, // 40000

	// Class 42
	SyntaxErrorOrAccessRuleViolation                   =  6811776, // 42000

	// Class 44
	WithCheckOptionViolation                           =  6905088, // 44000
	
	// Class 53
	InsufficientResources                              =  8538048, // 53000

	// Class 54
	ProgramLimitExceeded                               =  8584704, // 54000

	// Class 55
	ObjectNotInPrerequisiteState                       =  8631360, // 55000

	// Class 57
	OperatorIntervention                               =  8724672, // 57000

	// Class 58
	SystemError                                        =  8771328, // 58000

	// For correct value size, cf. C++ enum sizes:
	_MaxValue                                          = 60466175  // ZZZZZ
    };

    inline SqlState() throw ()
    : _code(0)
    {
	// empty
    }

    inline explicit SqlState(CodeValue cv) throw ()
    : _code(cv)
    {
	// empty
    }

    inline SqlState(Code code) throw ()
    : _code(CodeValue(code))
    {
	// empty
    }

    explicit SqlState(const char* stringcode) throw ();

    inline SqlState& operator = (Code code) throw ()
    {
	_code = CodeValue(code);
	return *this;
    }

    inline CodeValue codevalue() const throw ()
    {
	return _code;
    }

    inline Code code() const throw ()
    {
	return Code(_code);
    }

    inline unsigned errClass() const throw ()
    {
	return _code / (36*36*36);
    }

    std::string codestring() const;

    void codestring(char* buf, unsigned len) const throw ();

private:
    CodeValue _code;
};

extern std::ostream& operator<< (std::ostream& os, SqlState state);

///////////////////////////////////////////////////////////////////////////////

class DatabaseError : public positix::Error
{
public:
    typedef unsigned InterfaceErrorCode;

    explicit DatabaseError(
	SqlState                    state,
	const positix::TextMessage& message)
    : positix::Error(state.code(), message),      
      _iec(0)
    {
	// empty
    }

    explicit DatabaseError(
	SqlState                    state,
	InterfaceErrorCode          iec,
	const positix::TextMessage& message)
    : positix::Error(state.code(), message),
      _iec(iec)
    {
	// empty
    }

    inline InterfaceErrorCode interfaceErrorCode() const throw ()
    {
	return _iec;
    }

protected:
    InterfaceErrorCode _iec;
};

///////////////////////////////////////////////////////////////////////////////

enum SqlTypeCode
{
    SqlTypeNone,
    SqlTypeProprietary,

    SqlTypeBoolean,

    SqlTypeTinyInt,
    SqlTypeUnsignedTinyInt,
    SqlTypeSmallInt,
    SqlTypeUnsignedSmallInt,
    SqlTypeInteger,
    SqlTypeUnsignedInteger,
    SqlTypeBigInt,
    SqlTypeUnsignedBigInt,
    
    SqlTypeDecimal,
    SqlTypeNumeric,

    SqlTypeFloat,
    SqlTypeReal,
    SqlTypeDouble,

    SqlTypeChar,
    SqlTypeVarChar,
    SqlTypeText,

    SqlTypeBit,
    SqlTypeVarBit,
    SqlTypeBlob,

    SqlTypeDate,
    SqlTypeTime,
    SqlTypeTimeStamp,
    SqlTypeInterval
};

///////////////////////////////////////////////////////////////////////////////

struct SqlColumn
{
    const char* name;
    SqlTypeCode typeCode;    
    const char* defaultValue;
    unsigned    width;
    unsigned    maxWidth;
    unsigned    fApproximateType : 1;
    unsigned    fNotNull         : 1;
    unsigned    fNotNullValid    : 1;
    unsigned    fPrimaryKey      : 1;
    unsigned    fPrimaryKeyValid : 1;
    unsigned    fUnique          : 1;
    unsigned    fUniqueValid     : 1;
};

///////////////////////////////////////////////////////////////////////////////

class SqlRow
{
public:
    virtual ~SqlRow()
    {
	// empty
    }

    virtual const char* get   (unsigned index) const = 0;
    virtual size_t      length(unsigned index) const = 0;

    inline 
    void get(unsigned index, std::string& value) const
    {
	const char* s = get(index);
	if ( s )
	    value = s;
	else
	    value.clear();
    }

    void get(unsigned index, positix::ByteBuf& bb) const;

    template <class Value>
    void get(unsigned index, Value& value) const
    {
	const char* s = get(index);
	if ( !s )
	    value = Value();
	else
	{
	    std::istringstream in(s);
	    in >> value;
	    TestOk(index, in);
	}
    }

protected:
    inline SqlRow() throw ()
    {
	// empty
    }

    static void TestOk(unsigned index, std::istringstream& in);

private:
    POSITIX_CLASS_NOCOPY(SqlRow);
};

///////////////////////////////////////////////////////////////////////////////

template <class Value>
inline void Get(const SqlRow* row, unsigned i, Value& val)
{
    row->get(i, val);
}

///////////////////////////////////////////////////////////////////////////////

class SqlResult
{
public:
    virtual ~SqlResult()
    {
	// empty
    }

    virtual unsigned numberOfColumns() throw () = 0;
    virtual SqlColumn getColumn(unsigned i) = 0;
    virtual const SqlRow* fetchRow() = 0;

    virtual SqlSerialNumber numberOfLocalRows() throw ();

protected:
    inline SqlResult() throw ()
    {
	// empty
    }

private:
    POSITIX_CLASS_NOCOPY(SqlResult);
};

///////////////////////////////////////////////////////////////////////////////

class SqlRequestPattern
{
public:
    SqlRequestPattern(
	bool        copyStrings,
	const char* name,
	const char* pattern,
	bool        dontStore = false);

    ~SqlRequestPattern();

    inline const char* pattern() const throw ()
    {
	return _pattern;
    }

    inline const char* name() const throw ()
    {
	return _name;
    }

private:
    positix_types::fu32 _id;
    const char*         _name;
    const char*         _pattern;
    bool                _stringsCopied : 1;
    bool                _dontStore     : 1;

private:
    POSITIX_CLASS_NOCOPY(SqlRequestPattern);
};

///////////////////////////////////////////////////////////////////////////////

class SqlRowMaker
{
public:
    SqlRowMaker(
	bool        copyStrings,
	const char* name,
	const char* table,
	const char* cols[],
	unsigned    nCols,
	unsigned    nKeys,
	bool        dontStore = false);
    ~SqlRowMaker();

    inline const char* table() const throw ()
    {
	return _table;
    }

    inline const char** cols() const throw ()
    {
	return _cols;
    }

    inline unsigned nCols() const throw ()
    {
	return _nCols;
    }

    inline unsigned nKeys() const throw ()
    {
	return _nKeys;
    }
    
private:
    positix_types::fu32 _id;
    const char*         _name;
    const char*         _table;
    const char**        _cols;
    unsigned            _nCols;
    unsigned            _nKeys;
    bool                _stringsCopied : 1;
    bool                _dontStore     : 1;

private:
    POSITIX_CLASS_NOCOPY(SqlRowMaker);
};

///////////////////////////////////////////////////////////////////////////////

class SqlRequestParam
{
public:
    inline SqlRequestParam() throw ()
    : typecode(SqlTypeNone),
      size(0)
    {}

    inline SqlRequestParam(const char* s, size_t length) throw ()
    : typecode(SqlTypeChar),
      size(length)
    {
	ptr = s;
    }

    inline SqlRequestParam(const std::string& s) throw ()
    : typecode(SqlTypeChar),
      size(s.length())
    {
	ptr = s.c_str();
    }

//     inline SqlRequestParam(const std::string* s) throw ()
//     : typecode(SqlTypeChar),
//       value(s->c_str()),
//       size(s->length())
//     {}

//     inline SqlRequestParam(const char* const* s) throw ()
//     : typecode(SqlTypeChar),
//       value(*s),
//       size(strlen(*s))
//     {}

    template <unsigned n>
    inline SqlRequestParam(const char (*s)[n]) throw ()
    : typecode(SqlTypeChar),
      size(n-1)
    {
	ptr = s;
    }

    inline SqlRequestParam(const positix::ByteBuf& s) throw ()
    : typecode(SqlTypeBlob),
      size(s.size())
    {
	ptr = (positix::ConstBytePtr) s;
    }

    inline SqlRequestParam(const positix_types::si8& s) throw ()
    : typecode(SqlTypeTinyInt),
      size(sizeof(s))
    {
	i8 = s;
    }

    inline SqlRequestParam(const positix_types::su8& s) throw ()
    : typecode(SqlTypeUnsignedTinyInt),
      size(sizeof(s))
    {
	u8 = s;
    }

    inline SqlRequestParam(const positix_types::si16& s) throw ()
    : typecode(SqlTypeSmallInt),
      size(sizeof(s))
    {
	i16 = s;
    }

    inline SqlRequestParam(const positix_types::su16& s) throw ()
    : typecode(SqlTypeUnsignedSmallInt),
      size(sizeof(s))
    {
	u16 = s;
    }

    inline SqlRequestParam(const positix_types::si32& s) throw ()
    : typecode(SqlTypeInteger),
      size(sizeof(s))
    {
	i32 = s;
    }

    inline SqlRequestParam(const positix_types::su32& s) throw ()
    : typecode(SqlTypeUnsignedInteger),
      size(sizeof(s))
    {
	u32 = s;
    }

    inline SqlRequestParam(const positix_types::si64& s) throw ()
    : typecode(SqlTypeBigInt),
      size(sizeof(s))
    {
	i64 = s;
    }

    inline SqlRequestParam(const positix_types::su64& s) throw ()
    : typecode(SqlTypeUnsignedBigInt),
      size(sizeof(s))
    {
	u64 = s;
    }

    inline SqlRequestParam(const float& s) throw ()
    : typecode(SqlTypeFloat),
      size(sizeof(s))
    {
	flt = s;
    }

    inline SqlRequestParam(const double& s) throw ()
    : typecode(SqlTypeDouble),
      size(sizeof(s))
    {
	dbl = s;
    }

    inline SqlRequestParam(const long double& s) throw ()
    : typecode(SqlTypeReal),
      size(sizeof(s))
    {
	ldbl = s;
    }

    inline SqlRequestParam(const bool& s) throw ()
    : typecode(SqlTypeBoolean),
      size(sizeof(s))
    {
	u8 = s;
    }

    inline SqlRequestParam(const positix_types::Time& s) throw ()
    : typecode(SqlTypeTime),
      size(sizeof(s))
    {
	new (&time) positix_types::Time(s);
    }

    inline SqlRequestParam(const positix_types::Date& s) throw ()
    : typecode(SqlTypeDate),
      size(sizeof(s))
    {
	new (&date) positix_types::Date(s);
    }

    inline SqlRequestParam(const positix_types::DateTime& s) throw ()
    : typecode(SqlTypeTimeStamp),
      size(sizeof(s))
    {
	new (&dt) positix_types::DateTime(s);
    }

    SqlTypeCode typecode;
    size_t	size;
    union
    {
	const void*             ptr;
	positix_types::si8      i8;
	positix_types::si16     i16;
	positix_types::si32     i32;
	positix_types::si64     i64;
	positix_types::su8      u8;
	positix_types::su16     u16;
	positix_types::su32     u32;
	positix_types::su64     u64;
	char                    time[sizeof(positix_types::Time)];
	char                    date[sizeof(positix_types::Date)];
	char                    dt  [sizeof(positix_types::DateTime)];
	float                   flt;
	double                  dbl;
	long double             ldbl;	
    };
};

extern std::ostream& operator << (std::ostream& os, const SqlRequestParam& p);

///////////////////////////////////////////////////////////////////////////////

template <unsigned Len>
struct SqlRequestParams
{
public:
    typedef       SqlRequestParam* pointer;
    typedef const SqlRequestParam* const_pointer;
    typedef       SqlRequestParam* iterator;
    typedef const SqlRequestParam* const_iterator;
    typedef       SqlRequestParam& reference;
    typedef const SqlRequestParam& const_reference;

    typedef SqlRequestParam value_type;
    typedef ptrdiff_t       difference_type;
    typedef size_t          size_type;

    static const unsigned Length = Len;

    template <class Type>
    inline SqlRequestParam& operator[](Type index) throw ()
    {
	return array[index];
    }

    template <class Type>
    inline const SqlRequestParam& operator[](Type index) const throw ()
    {
	return array[index];
    }

    inline SqlRequestParam* begin() throw ()
    {
	return array;
    }

    inline SqlRequestParam* end() throw ()
    {
	return array + Len;
    }

    inline const SqlRequestParam* begin() const throw ()
    {
	return array;
    }

    inline SqlRequestParam* end() const throw ()
    {
	return array + Len;
    }

    SqlRequestParam array[Len];
};

///////////////////////////////////////////////////////////////////////////////

struct SqlModInfo
{
    SqlSerialNumber lastSerial;
    SqlSerialNumber affectedRows;
};

extern SqlModInfo _SqlStoreLocal;
extern SqlModInfo _SqlStoreRemote;

SqlModInfo* const SqlStoreLocal  = &_SqlStoreLocal;
SqlModInfo* const SqlStoreRemote = &_SqlStoreRemote;

class SqlConnector
{
public:
    virtual ~SqlConnector();

    virtual const char* driverName() const throw () = 0;
    virtual positix_types::Version version() const throw () = 0;

    virtual
    SqlResult*
    execute(
	const std::string& query,
	SqlModInfo* const  mod)
    = 0;

    virtual
    SqlResult*
    execute(
	const char*       query,
	SqlModInfo* const mod)
    = 0;

    virtual
    SqlResult*
    execute(
	const SqlRequestPattern& rp,
	const SqlRequestParam*   params,
	unsigned                 nParams,
	SqlModInfo* const        mod);

    template <unsigned Len>
    inline 
    SqlResult*
    execute(
	const SqlRequestPattern&     rp,
	const SqlRequestParams<Len>& params,
	SqlModInfo* const            mod)
    {
	return execute(rp, params.array, Len, mod);
    }

    /////////////////////////////////////////////////////////////////////
    // Special INSERT or UPDATE handling
    /////////////////////////////////////////////////////////////////////

    virtual
    SqlSerialNumber
    makeRow(
	const SqlRowMaker&     rm,
	const SqlRequestParam* params,
	unsigned               nParams);

    template <unsigned Len>
    inline
    SqlSerialNumber
    makeRow(
	const SqlRowMaker&           rm,
	const SqlRequestParams<Len>& params)
    {
	return makeRow(rm, params.array, Len);
    }

    /////////////////////////////////////////////////////////////////////
    // Access functions
    /////////////////////////////////////////////////////////////////////

    inline const std::string& address() const throw ()
    {
	return _address;
    }

    inline const std::string& user() const throw ()
    {
	return _user;
    }

protected:
    inline SqlConnector(
	const std::string& address,
	const std::string& user)
    : _address(address),
      _user(user)
    {
	// empty
    }

    std::string _address;
    std::string _user;
};

///////////////////////////////////////////////////////////////////////////////

class SqlDB
{
public:
    inline SqlDB() throw ()
    : _db(0)
    {
	// empty
    }

    inline SqlDB(    
	const std::string& interface,
	const std::string& address,
	const std::string& db,
	const std::string& user,
	const std::string& password)
    : _db(0)
    {
	connect(interface, address, db, user, password);
    }

    SqlDB(const SqlDB& db) throw ();

    SqlDB& operator = (const SqlDB& db) throw ();

    ~SqlDB() throw ();

    inline SqlConnector* operator -> () throw ()
    {
	return _db;
    }

    inline const SqlConnector* operator -> () const throw ()
    {
	return _db;
    }

    inline operator SqlConnector* () throw ()
    {
	return _db;
    }

    inline operator const SqlConnector* () const throw ()
    {
	return _db;
    }

    void connect(
	const std::string& interface,
	const std::string& address,
	const std::string& db,
	const std::string& user,
	const std::string& password);

private:
    mutable SqlConnector* _db;
};

///////////////////////////////////////////////////////////////////////////////

inline
SqlResult*
ExecuteV(
    SqlConnector*            db,
    SqlModInfo* const        mod,
    const SqlRequestPattern& rp,
    const SqlRequestParam*   params,
    unsigned                 nParams)
{
    return db->execute(rp, params, nParams, mod);
}

#define _POSITIX_FORMAT_            Execute
#define _POSITIX_FMT_               const SqlRequestPattern&
#define _POSITIX_FORMARG_           SqlRequestParam
#define _POSITIX_FORMARG_CONS_(V,v) SqlRequestParam(v)
#define _POSITIX_FORMAT_FUN_        ExecuteV
#define _POSITIX_FORMAT_RESULT_     SqlResult*
#define _POSITIX_FORMAT_PARAM_      SqlConnector* db, SqlModInfo* const mod,
#define _POSITIX_FORMAT_ARG_        db, mod,
#include <positix/format.hh>

///////////////////////////////////////////////////////////////////////////////

inline
SqlSerialNumber
MakeRowV(
    SqlConnector*          db,
    const SqlRowMaker&     rm,
    const SqlRequestParam* params,
    unsigned               nParams)
{
    return db->makeRow(rm, params, nParams);
}

#define _POSITIX_FORMAT_            MakeRow
#define _POSITIX_FMT_               const SqlRowMaker&
#define _POSITIX_FORMARG_           SqlRequestParam
#define _POSITIX_FORMARG_CONS_(V,v) SqlRequestParam(v)
#define _POSITIX_FORMAT_FUN_        MakeRowV
#define _POSITIX_FORMAT_RESULT_     bool
#define _POSITIX_FORMAT_PARAM_      SqlConnector* db,
#define _POSITIX_FORMAT_ARG_        db,
#include <positix/format.hh>

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_dcs

#include <positix/undef.hh>

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix DCS: dcs.hh

