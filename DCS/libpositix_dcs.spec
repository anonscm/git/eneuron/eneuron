#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.
%define platform %{_vendor}/%{_arch}
%define _topdir %(echo $HOME)/out

Summary: RDBMS interface library in C++
Name: libpositix_dcs
Version: %(source VERSION; echo $DCS_MAJOR.$DCS_MINOR.$DCS_PATCH)
Release: 2
Copyright: commercial
Group: Development/Libraries
URL: http://www.positix.com
Vendor: positix GmbH
Packager: positix GmbH
Requires: %(sh ../rpmplatform.sh)
Prefix: /opt/positix

%define v_mysql %(source VERSION; echo $mysql4_0_MAJOR.$mysql4_0_MINOR.$mysql4_0_PATCH)
%define v_postgresql %(source VERSION; echo $mysql_MAJOR.$mysql_MINOR.$mysql_PATCH)

%description 
This library acts as an encapsulation layer for relational database management
systems. Currently, following RDBMS are supported:
- MySQL
- PostgreSQL (in preparation)

This package contains the release built for %{platform}.

%prep
mkdir $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{prefix}/lib
cp ~/out/release/lib/libpositix_dcs.so.%{version} \
   ~/out/release/lib/libpositix_dcs_mysql4_0.so.%{v_mysql} \
   $RPM_BUILD_ROOT/%{prefix}/lib
strip $RPM_BUILD_ROOT/%{prefix}/lib/*
cd $RPM_BUILD_ROOT/%{prefix}/lib; \
   ln -sf libpositix_dcs.so.%{version} libpositix_dcs.so; \
   ln -sf libpositix_dcs_mysql4_0.so.%{v_mysql} libpositix_dcs_mysql4_0.so; \

%install
echo Install: %{prefix}

%files
%defattr(-, root, root)
%attr(755, root, root) %dir %{prefix}
%attr(755, root, root) %dir %{prefix}/lib
%attr(755, root, root) %{prefix}/lib/libpositix_dcs.so.%{version}
%attr(755, root, root) %{prefix}/lib/libpositix_dcs_mysql4_0.so.%{v_mysql}
%{prefix}/lib/libpositix_dcs.so
%{prefix}/lib/libpositix_dcs_mysql4_0.so

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ $RPM_INSTALL_PREFIX/lib != /usr/lib -a $RPM_INSTALL_PREFIX/lib != //lib ] ; then
    if type -p grep > /dev/null ; then
	if ! grep $RPM_INSTALL_PREFIX/lib /etc/ld.so.conf > /dev/null ; then
	    echo $RPM_INSTALL_PREFIX/lib >> /etc/ld.so.conf
	fi
    else
	echo You must include $RPM_INSTALL_PREFIX/lib to your library paths
    fi
fi
/sbin/ldconfig $RPM_INSTALL_PREFIX/lib

%postun
/sbin/ldconfig $RPM_INSTALL_PREFIX/lib
if rmdir $RPM_INSTALL_PREFIX/lib 2> /dev/null ; then
    cp -p /etc/ld.so.conf /etc/ld.so.conf~
    grep -v $RPM_INSTALL_PREFIX/lib /etc/ld.so.conf~ > /etc/ld.so.conf
    /sbin/ldconfig
fi
rmdir $RPM_INSTALL_PREFIX 2> /dev/null || true

