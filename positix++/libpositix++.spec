#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.
%define platform %{_vendor}/%{_arch}
%define _topdir %(echo $HOME)/out

Summary: C++ utility and encapsulation library
Name: libpositix++
Version: %(source VERSION; echo $MAJOR.$MINOR.$PATCH)
Release: 1
Copyright: commercial
Group: Development/Libraries
URL: http://www.positix.com
Vendor: positix GmbH
Packager: positix GmbH
Prefix: /opt/positix
Requires: %(sh ../rpmplatform.sh)

%define pxlibdir ~/out/release/lib

%description 

This library is the basis of most positix software written in C++. It offers
reusable software components for common procedures and tasks. It also serves
as an abstraction and encapsulation layer for system libraries.
Currently, libpositix++ covers following aspects:
+ System user accounting (read only)
+ Configuration file handling
+ Command line parsing
+ Error handling
+ Logging
+ Multithreading
+ String utilities
+ Message formatting
+ Simple Tokenizing

This package contains the release built for %{platform}.

%prep
mkdir -p $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{prefix}/lib
cp %{pxlibdir}/libpositix++.so.%{version} $RPM_BUILD_ROOT/%{prefix}/lib
strip $RPM_BUILD_ROOT/%{prefix}/lib/*
cd $RPM_BUILD_ROOT/%{prefix}/lib; \
   ln -sf libpositix++.so.%{version} libpositix++.so

%install
echo Install: %{prefix}

%files
%defattr(-, root, root)
%attr(755, root, root) %dir %{prefix}
%attr(755, root, root) %dir %{prefix}/lib
%attr(755, root, root) %{prefix}/lib/libpositix++.so.%{version}
%{prefix}/lib/libpositix++.so

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ $RPM_INSTALL_PREFIX/lib != /usr/lib -a $RPM_INSTALL_PREFIX/lib != //lib ] ; then
    if type -p grep > /dev/null ; then
	if ! grep $RPM_INSTALL_PREFIX/lib /etc/ld.so.conf > /dev/null ; then
	    echo $RPM_INSTALL_PREFIX/lib >> /etc/ld.so.conf
	fi
    else
	echo You must include $RPM_INSTALL_PREFIX/lib to your library paths
    fi
fi
/sbin/ldconfig $RPM_INSTALL_PREFIX/lib

%postun
/sbin/ldconfig $RPM_INSTALL_PREFIX/lib
if rmdir $RPM_INSTALL_PREFIX/lib 2> /dev/null ; then
    cp -p /etc/ld.so.conf /etc/ld.so.conf~
    grep -v $RPM_INSTALL_PREFIX/lib /etc/ld.so.conf~ > /etc/ld.so.conf
    /sbin/ldconfig
fi
rmdir $RPM_INSTALL_PREFIX 2> /dev/null || true
