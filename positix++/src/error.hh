/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : error.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_error_hh__
#define __positix_error_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <exception>

#include "text.hh"

namespace positix {
    
class Error;

///////////////////////////////////////////////////////////////////////////////
    
class Error : public std::exception
{
public:
    inline explicit Error(const TextMessage& msg)
    : _message(msg),
      _code(-1)
    {
	// empty
    }

    inline Error(int code, const TextMessage& msg)
    : _message(msg),
      _code(code)
    {
	// empty
    }
    
    virtual ~Error() throw ()
    {
	// empty
    }

    const char* what() const throw ();

    inline const TextMessage& msg() const throw ()
    {
	return _message;
    }

    inline int code() const throw ()
    {
	return _code;
    }

protected:
    inline Error() throw ()
    : _code(-1)
    {
        // empty
    }
    
    inline Error(int code) throw ()
    : _code(code)
    {
       // empty
    }
    
protected:
    TextMessage _message;
    int         _code;
};

///////////////////////////////////////////////////////////////////////////////

class SystemError : public Error
{
public:
    inline explicit SystemError(
	SysErrCode code, 
	const char* prefix)
    : Error(code, SystemError::Message(prefix, code))
    {
	// empty
    }

    inline explicit SystemError(
	SysErrCode code, 
	const TextMessage& prefix)
    : Error(code, SystemError::Message(prefix, code))
    {
	// empty
    }

    static TextMessage Message(SysErrCode code);
    static TextMessage Message(const char* prefix, SysErrCode code);
    static TextMessage Message(const TextMessage& prefix, SysErrCode code);
}; 

///////////////////////////////////////////////////////////////////////////////

extern TextMessage StrError(SysErrCode e);

///////////////////////////////////////////////////////////////////////////////

class SyntaxError : public Error
{
public:
    inline SyntaxError(const TextMessage& msg)
    : Error(msg)
    {
	// empty
    }
};

///////////////////////////////////////////////////////////////////////////////
    
} // namespace positix

#endif

// end of positix++: error.hh
