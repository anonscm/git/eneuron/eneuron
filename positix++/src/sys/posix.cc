/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : posix.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>
#include <syslog.h>
#include <dlfcn.h>
#include <sstream>
#include <vector>

#include "../time.hh"
#include "../arrays.hh"
#include "../strings.hh"
#include "../log.hh"
#include "../system.hh"
#include "../files.hh"
#include "../accounting.hh"
#include "../error.hh"
#include "../cmdopt.hh"
#include "../sockets.hh"
#include "../multithreading.hh"
#include "../error.hh"
#include "../text.hh"
#include "../dl.hh"
#include "../msg.hh"

using namespace std;

namespace positix {

const size_t MemoryPageSize = size_t(sysconf(_SC_PAGESIZE));

///////////////////////////////////////////////////////////////////////////////

static std::vector<char*> TheFileNameMap;
static FastMutex TheFileNameMutex;

bool PxUseFileNameMap = false;

void PxAddToFileNameMap(SysFile fd, const char* name)
{
    if ( fd.value==-1 )
	return;
    Mutex::Lock x(TheFileNameMutex);
    if ( fd.value >= int(TheFileNameMap.size()) )
	TheFileNameMap.resize(TheFileNameMap.size() + 256);
    TheFileNameMap[fd.value] = ::strdup(name);
}

void PxRemoveFromFileNameMap(SysFile fd)
{
    if ( fd.value==-1 )
	return;
    Mutex::Lock x(TheFileNameMutex);
    if ( fd.value < int(TheFileNameMap.size()) )
    {
	::free(TheFileNameMap[fd.value]);
	TheFileNameMap[fd.value] = 0;
    }             
}

extern const char* PxGetFromFileNameMap(SysFile fd)
{
    if ( fd.value==-1 )
	return PxNullStr;

    Mutex::Lock x(TheFileNameMutex);

    if ( fd.value < int(TheFileNameMap.size()) )
    {
	const char* s = TheFileNameMap[fd.value];
	return s ? s : PxNullStr;
    }

    return PxNullStr;
}

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream& os, const SysFile& fd)
{
    return os << fd.value;
}

///////////////////////////////////////////////////////////////////////////////

int SysFileStatType(const SysFileStat& st) throw ()
{
    mode_t mode = st.value.st_mode;

    if ( S_ISREG(mode) )
	return File::Regular;	
    if ( S_ISDIR(mode) )
	return File::Directory;
    if ( S_ISLNK(mode) )
	return File::SymLink;
    if ( S_ISFIFO(mode) )
	 return File::Pipe;
    if ( S_ISSOCK(mode) )
	return File::Socket;
    if ( S_ISBLK(mode) )
	return File::BlockDev;
    if ( S_ISCHR(mode) )
	return File::CharDev;

    return File::Unknown;
}

///////////////////////////////////////////////////////////////////////////////

bool SysReadLink(const char* link, std::string& contents) throw ()
{
    unsigned mlen = PATH_MAX;
    char*    buf;
    int	     n;

    for(;;)
    {
	buf = (char*) malloc(mlen+1);
	if ( !buf )
	{
	    errno = ENOMEM;
	    return false;
	}

	n = readlink(link, buf, mlen);
	if ( n<0 )
	{
	    free(buf);
	    return false;
	}

	if ( unsigned(n)<mlen )
	    break;
	free(buf);
	mlen += PATH_MAX;
    }

    buf[n] = 0;

    try
    {
	contents.reserve(n);
	contents = buf;
	free(buf);
	return true;
    }
    catch ( ... )
    {
	free(buf);
	errno = ENOMEM;
	return false;
    }
}

///////////////////////////////////////////////////////////////////////////////

extern std::string CurrentDirectory()
{
    // TODO: something like in SysReadLink
    char buf[PATH_MAX];
    getcwd(buf, sizeof(buf));
    return buf;
}

///////////////////////////////////////////////////////////////////////////////

extern bool FileExists(const std::string& filename) throw ()
{
    return 0==access(filename.c_str(), F_OK);
}

///////////////////////////////////////////////////////////////////////////////

extern int CompareFilenames(const char* a, const char* b) throw ()
{
    return strcmp(a, b);
}

///////////////////////////////////////////////////////////////////////////////

extern void SysOpenLog(const char* ident, LogFacility facility)
{
    int fac;
    switch ( facility )
    {
    default       :
    case LogUser  : fac = LOG_USER;   break;
    case LogDaemon: fac = LOG_DAEMON; break;
    case LogMail  : fac = LOG_MAIL;   break;
    }

    openlog(ident, LOG_CONS | LOG_NOWAIT | LOG_PID, fac);
}

///////////////////////////////////////////////////////////////////////////////

extern void SysCloseLog()
{
    closelog();
}

///////////////////////////////////////////////////////////////////////////////

extern void SysLog(LogLevel level, const std::string& msg)
{
    std::string::const_iterator e = msg.end();
    unsigned numberOfPercentSigns = 0;

    for (std::string::const_iterator p = msg.begin(); p!=e; ++p)
    {
	if ( *p == '%' )
	    ++numberOfPercentSigns;
    }

    if ( numberOfPercentSigns == 0 )
    {
	syslog(level, msg.c_str());
    }
    else
    {
	std::string copy;
	copy.reserve(msg.length() + numberOfPercentSigns);
	for (std::string::const_iterator p = msg.begin(); p!=e; ++p)
	{
	    if ( *p == '%' )
		copy += *p;
	    copy += *p;
	}
	syslog(level, msg.c_str());
    }
}

///////////////////////////////////////////////////////////////////////////////

void PidFile::create(const std::string& name)
{
    remove();
    int fd = ::open(name.c_str(), O_RDWR | O_CREAT | O_TRUNC | O_EXCL, 0600);
    if ( fd == -1 )
    {
	int e = errno;
	throw SystemError(e, Msg(TXT("Cannot create PID file %0"), name));
    }
    pid_t pid = getpid();
    ostringstream os;
    os << pid << '\n';
    string spid = os.str();
    ::write(fd, spid.c_str(), spid.length());
    ::close(fd);
    _name = name;
}

///////////////////////////////////////////////////////////////////////////////

void PidFile::remove()
{
    if ( !_name.empty() )
    {
	// Ignore any error
	::unlink(_name.c_str());
    }
}

///////////////////////////////////////////////////////////////////////////////

std::string Hostname() throw ()
{
    long buflen = sysconf(_SC_HOST_NAME_MAX) + 1;
    char* buf = new char[buflen];
    
    try
    {
	if ( -1 == gethostname(buf, buflen) )	
	{
	    delete[] buf;
	    return std::string();
	}
	else
	{
	    std::string name(buf);
	    delete[] buf;
	    return name;
	}
    }
    catch ( ... )
    {
	return std::string();
    }
}

///////////////////////////////////////////////////////////////////////////////

std::string PxLocateCfgFile(
    const char* vendor, 
    const char* filename,
    bool        lookAtHome)
{
    std::string name;
    name.reserve(strlen(vendor) + strlen(filename) + 64);

    if ( lookAtHome )
    {
	// Look for ~/.VENDOR/FILENAME
	const char* home = getenv("HOME");
	if ( home )
	{
	    name  = home;
	    name += "/.";
	    name += vendor;
	    name += '/';
	    name += filename;

	    if ( 0==access(name.c_str(), F_OK) )
		return name;
	}
    }

    // Look for /etc/VENDOR/FILENAME
    name  = "/etc";
    name += vendor;
    name += '/';
    name += filename;

    if ( 0==access(name.c_str(), F_OK) )
	return name;

    // Look for /etc/FILENAME
    name  = "/etc/";
    name += filename;

    if ( 0==access(name.c_str(), F_OK) )
	return name;

    // Look for /opt/VENDOR/HOSTNAME.etc/FILENAME
    name  = "/opt/";
    name += vendor;
    name += '/';
    name += Hostname();
    name += ".etc/";
    name += filename;

    if ( 0==access(name.c_str(), F_OK) )
	return name;

    // Look for /opt/VENDOR/etc/FILENAME
    name  = "/opt/";
    name += vendor;
    name += "/etc/";
    name += filename;

    if ( 0==access(name.c_str(), F_OK) )
	return name;

    return std::string();
}

///////////////////////////////////////////////////////////////////////////////

SysUserId User::CurrentUserId()
{
    return SysUserId(geteuid());
}

///////////////////////////////////////////////////////////////////////////////

User::User(const char* name)
{
    long buflen = sysconf(_SC_GETPW_R_SIZE_MAX);
    AutoArrayPtr<char> Buf(new char[buflen]);
    char* buf = Buf.get();
	
    struct passwd  pw;
    struct passwd* ppw;
    int e;

    for(;;)
    {
	e = getpwnam_r(name, &pw, buf, buflen, &ppw);
	if ( e!=ERANGE || buflen >= 128*1024 )
	    break;
	Buf.reset();
	buflen += 1024;
	Buf.reset(new char[buflen]);
	buf = Buf.get();	
    }

    if ( e!=0 )
    {
	throw SystemError(
	    e,
	    Msg(TXT("Failure retrieving data of user %0"), name));
    }

    if ( !ppw )
	throw Error(Msg(TXT("There is no user with name %0"), name));

    _name      = pw.pw_name;
    _password  = pw.pw_passwd;
    _home      = pw.pw_dir;
    _uid.value = pw.pw_uid;
    _gid.value = pw.pw_gid;
}

///////////////////////////////////////////////////////////////////////////////

User::User(const SysUserId& uid)
{
    long buflen = sysconf(_SC_GETPW_R_SIZE_MAX);
    AutoArrayPtr<char> Buf(new char[buflen]);
    char* buf = Buf.get();

    struct passwd  pw;
    struct passwd* ppw;
    int e;

    for(;;)
    {
	e = getpwuid_r(uid.value, &pw, buf, buflen, &ppw);
	if ( e!=ERANGE || buflen >= 128*1024 )
	    break;
	Buf.reset();
	buflen += 1024;
	Buf.reset(new char[buflen]);
	buf = Buf.get();	
    }

    if ( e!=0 )
    {
	throw SystemError(
	    e, 
	    Msg(TXT("Failure retrieving data of user with id %0"), uid.value));
    }

    if ( !ppw )
	throw Error(Msg(TXT("There is no user with id %0"), uid.value));

    _name      = pw.pw_name;
    _password  = pw.pw_passwd;
    _home      = pw.pw_dir;
    _uid.value = pw.pw_uid;
    _gid.value = pw.pw_gid;
}

///////////////////////////////////////////////////////////////////////////////

User::User(const SysUserId& uid, MissingIgnorance mi)
{
    long buflen = sysconf(_SC_GETPW_R_SIZE_MAX);
    AutoArrayPtr<char> Buf(new char[buflen]);
    char* buf = Buf.get();

    struct passwd  pw;
    struct passwd* ppw;
    int e;

    for(;;)
    {
	e = getpwuid_r(uid.value, &pw, buf, buflen, &ppw);
	if ( e!=ERANGE || buflen >= 128*1024 )
	    break;
	Buf.reset();
	buflen += 1024;
	Buf.reset(new char[buflen]);
	buf = Buf.get();	
    }

    if ( e!=0 )
    {
	throw SystemError(
	    e, 
	    Msg(TXT("Failure retrieving data of user with id %0"), uid.value));
    }

    if ( !ppw )
    {
	if ( mi==DontIgnoreMissing )
	    throw Error(Msg(TXT("There is no user with id %0"), uid.value));
	_name             = ToString(uid.value);
	_password.erase();
	_home.erase();
	_uid.value        = uid.value;
	_gid.value        = gid_t(-1);
    }
    else
    {
	_name      = pw.pw_name;
	_password  = pw.pw_passwd;
	_home      = pw.pw_dir;
	_uid.value = pw.pw_uid;
	_gid.value = pw.pw_gid;
    }
}

///////////////////////////////////////////////////////////////////////////////

void SwitchPersonality(const SysUserId& uid)
{
    if ( -1 == setuid(uid.value) )
    {
	int e = errno;
	throw SystemError(
	    e,
	    Msg(TXT("Cannot switch process personality to user with id %0"), 
		uid.value));
    }
}

///////////////////////////////////////////////////////////////////////////////

Group::Group(const char* name)
{
    long buflen = sysconf(_SC_GETGR_R_SIZE_MAX);
    AutoArrayPtr<char> Buf(new char[buflen]);
    char* buf = Buf.get();
    struct group  gr;
    struct group* pgr;

    int e;
    for(;;)
    {
	e  = getgrnam_r(name, &gr, buf, buflen, &pgr);
	if ( e!=ERANGE || buflen >= 128*1024 )
	    break;
	Buf.reset();
	if ( buflen < 4096 )
	    buflen = 4096;
	else
	    buflen += 4096;
	Buf.reset(new char[buflen]);
	buf = Buf.get();	
    }

    if ( e!=0 )
    {
	throw SystemError(
	    e,
	    Msg(TXT("Failure in retrieving data of group %0"), name));
    }

    if ( !pgr )
	throw Error(Msg(TXT("There is no group with name %0"), name));

    _name      = gr.gr_name;
    _gid.value = gr.gr_gid;
}

///////////////////////////////////////////////////////////////////////////////

Group::Group(const SysGroupId& gid)
{
    long buflen = sysconf(_SC_GETGR_R_SIZE_MAX);
    AutoArrayPtr<char> Buf(new char[buflen]);
    char* buf = Buf.get();
    
    struct group  gr;
    struct group* pgr;
    int e = getgrgid_r(gid.value, &gr, buf, buflen, &pgr);
    for(;;)
    {
	e = getgrgid_r(gid.value, &gr, buf, buflen, &pgr);
	if ( e!=ERANGE || buflen >= 128*1024 )
	    break;
	Buf.reset();
	if ( buflen < 4096 )
	    buflen = 4096;
	else
	    buflen += 4096;
	Buf.reset(new char[buflen]);
	buf = Buf.get();	
    }

    if ( e!=0 )
    {
	throw SystemError(
	    e,
	    Msg(TXT("Failure retrieving data of group with id %0"),gid.value));
    }

    if ( !pgr )
	throw Error(Msg(TXT("There is no group with id %0"), gid.value));

    _name      = gr.gr_name;
    _gid.value = gr.gr_gid;
}

///////////////////////////////////////////////////////////////////////////////

Group::Group(const SysGroupId& gid, MissingIgnorance mi)
{
    long buflen = sysconf(_SC_GETGR_R_SIZE_MAX);
    AutoArrayPtr<char> Buf(new char[buflen]);
    char* buf = Buf.get();
    
    struct group  gr;
    struct group* pgr;
    int e = getgrgid_r(gid.value, &gr, buf, buflen, &pgr);
    for(;;)
    {
	e = getgrgid_r(gid.value, &gr, buf, buflen, &pgr);
	if ( e!=ERANGE || buflen >= 128*1024 )
	    break;
	Buf.reset();
	if ( buflen < 4096 )
	    buflen = 4096;
	else
	    buflen += 4096;
	Buf.reset(new char[buflen]);
	buf = Buf.get();	
    }

    if ( e!=0 )
    {
	throw SystemError(
	    e,
	    Msg(TXT("Failure retrieving data of group with id %0"),gid.value));
    }

    if ( !pgr )
    {
	if ( mi==DontIgnoreMissing )
	    throw Error(Msg(TXT("There is no group with id %0"), gid.value));
	_name      = ToString(gid.value);
	_gid.value = gid.value;
    }
    else
    {
	_name      = gr.gr_name;
	_gid.value = gr.gr_gid;
    }
}

///////////////////////////////////////////////////////////////////////////////

void SwitchGroup(const SysGroupId& gid)
{
    if ( -1 == setegid(gid.value) )
    {
	int e = errno;
	throw SystemError(
	    e,
	    Msg(TXT("Cannot switch to group with id %0"), gid.value));
    }
}

///////////////////////////////////////////////////////////////////////////////

bool MemoryMap::trymap(
    SysFile file, 
    off_t   off, 
    size_t  length, 
    Access  acc, 
    Type    type,
    void*   start) 
throw ()
{
    if ( _map.ptr )
	unmap();

    int prot;
    switch ( acc )
    {
    default       :
    case NoAccess : prot = PROT_NONE;            break;
    case Read     : prot = PROT_READ;            break;
    case Write    : prot = PROT_WRITE;           break;
    case ReadWrite: prot = PROT_READ|PROT_WRITE; break;
    case Execute  : prot = PROT_EXEC;            break;
    }

    int flags = (type==Private) ? MAP_PRIVATE : MAP_SHARED;
    if ( start )
	flags |= MAP_FIXED;

    _map.ptr = (char*) mmap(start, length, prot, flags, file.value, off);

    if ( _map.ptr==MAP_FAILED )
	return false;

    _map.len = length;

    return true;
}

///////////////////////////////////////////////////////////////////////////////

void MemoryMap::map(
    SysFile file, 
    off_t   off, 
    size_t  length, 
    Access  acc,
    Type    type,
    void*   start)
{
    if ( !trymap(file, off, length, acc, type, start) )
    {
	int e = errno;
	throw SystemError(e, TXT("Cannot create memory map"));
    }
}

///////////////////////////////////////////////////////////////////////////////

bool MemoryMap::tryremap(
    size_t           newlength, 
    RemapAddressMode ram)
throw ()
{
#ifdef __linux__
    void* p = mremap(_map.ptr, 
		     _map.len, 
		     newlength, 
		     ram==AddressMayMove ? MREMAP_MAYMOVE : 0);
    if ( p == MAP_FAILED )
	return false;
    _map.ptr = (char*) p;
    _map.len = newlength;
    return true;
#else
#error No mremap (?)
#endif    
}

///////////////////////////////////////////////////////////////////////////////

void MemoryMap::remap(
    size_t           newlength, 
    RemapAddressMode ram)
{
    if ( !tryremap(newlength, ram) )
    {
	int e = errno;
	throw SystemError(e, TXT("MemoryMap::remap failed"));
    }
}

///////////////////////////////////////////////////////////////////////////////

void MemoryMap::sync(SyncTime st, SyncMode sm)
{
    int flags = (st==SyncWait) ? MS_SYNC : MS_ASYNC;
    if ( sm!=KeepOtherMaps )
	flags |= MS_INVALIDATE;
    msync(_map.ptr, _map.len, flags);
}

///////////////////////////////////////////////////////////////////////////////

void MemoryMap::unmap()
{
    if ( _map.ptr )
	munmap(_map.ptr, _map.len);
}

///////////////////////////////////////////////////////////////////////////////

static FastMutex TheLibraryMutex;

const void* const SysLibrary::Null = 0;

std::string Library::open(const char* name, const char* directory)
{
    if ( _handle.value )
	close();
    std::string xname;
    xname.reserve(strlen(name) + (directory ? strlen(directory) : 0) + 6);
    if ( directory )
    {
	xname = directory;
	xname += POSITIX_DIR_SEPC;
    }
    xname += "lib";
    xname += name;
    xname += ".so";

    Mutex::Lock x(TheLibraryMutex);

    _handle.value = dlopen(xname.c_str(), RTLD_NOW | RTLD_GLOBAL);
    if ( !_handle.value )
	throw Error(Msg(TXT("Cannot dynamically load library %0: %1"), 
			xname, dlerror()));

    return xname;
}

///////////////////////////////////////////////////////////////////////////////

void Library::close()
{
    if ( _handle.value )
    {
	Mutex::Lock x(TheLibraryMutex);
	dlclose(_handle.value);
    }
}

///////////////////////////////////////////////////////////////////////////////

void* Library::address(const char* symbol, std::string& errmsg) throw ()
{
    Mutex::Lock x(TheLibraryMutex);

    errmsg.erase();
    dlerror();
    void* addr = dlsym(_handle.value, symbol);
    char* err = dlerror();
    if ( err )
	errmsg = err;
    return addr;
}

///////////////////////////////////////////////////////////////////////////////

void DirScanner::open(const char* path)
{
    _dir = opendir(path);
    if ( !_dir )
    {
	SysLastErr e;
	throw SystemError(e, Msg(TXT("Cannot open directory %0"), path));
    }
}

///////////////////////////////////////////////////////////////////////////////

const char* DirScanner::next()
{
    struct dirent* res;

    do
    {
	int rc = readdir_r(_dir, &_u.ent, &res);
	if ( rc!=0 )
	    throw SystemError(rc, TXT("Cannot read directory entry"));
    } while ( res 
	      && _u.ent.d_name[0]=='.' 
	      && (_u.ent.d_name[1]==0
		  || (_u.ent.d_name[1]=='.' && _u.ent.d_name[2]==0)) );

    return res ? _u.ent.d_name : 0;
}

///////////////////////////////////////////////////////////////////////////////
} // namespace positix
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
namespace positix_types {
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////

Date Today() throw ()
{
    time_t    t = time(0);
    struct tm T;
    localtime_r(&t, &T);
    return Date(T.tm_year+1900, T.tm_mon+1, T.tm_mday);
}

///////////////////////////////////////////////////////////////////////////////

Time Clock() throw ()
{
    struct timeval t;
    struct tm      T;
    gettimeofday(&t, 0);
    localtime_r(&t.tv_sec, &T);
    return Time(T.tm_hour, T.tm_min, T.tm_sec, t.tv_usec/1000);
}

///////////////////////////////////////////////////////////////////////////////

DateTime Now() throw ()
{
    struct timeval t;
    struct tm      T;
    gettimeofday(&t, 0);
    localtime_r(&t.tv_sec, &T);
    return DateTime(T.tm_year+1900, T.tm_mon+1, T.tm_mday,
		    T.tm_hour, T.tm_min, T.tm_sec, t.tv_usec/1000);    
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_types


// Local Variables: ***
// mode:c++ ***
// End: ***


// end of positix++: posix.cc
