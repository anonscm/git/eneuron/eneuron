/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : pthread.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_pthread_hh__
#define __positix_pthread_hh__

#include <pthread.h>
#include <semaphore.h>

#define POSITIX_SYS_USE_PTHREAD

namespace positix {

POSITIX_SYS_HANDLE(Thread, pthread_t, 0)
POSITIX_SYS_VALUE(Mutex, pthread_mutex_t)
POSITIX_SYS_VALUE(CondVar, pthread_cond_t)
POSITIX_SYS_VALUE(Semaphore, sem_t)

} // namespace positix_types

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: pthread.hh
