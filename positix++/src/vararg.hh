/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : vararg.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_vararg_hh__
#define __positix_vararg_hh__

///////////////////////////////////////////////////////////////////////////////

namespace positix {

///////////////////////////////////////////////////////////////////////////////

template <class VarFun, unsigned Index>
struct VarArg
{
    typedef typename VarFun::Arg    Arg;
    typedef typename VarFun::Result Result;

    Arg                           arg;
    const VarArg<VarFun,Index-1>& prev;

    template <class Value>
    inline VarArg(
	const Value&                  value, 
	const VarArg<VarFun,Index-1>& prev_) 
    throw ()
    : arg(value),
      prev(prev_)
    {
	// empty
    }

    template <class Value>
    inline 
    VarArg<VarFun,Index+1> operator () (
	const Value& value) 
    const 
    throw ()
    {
	return VarArg<VarFun,Index+1>(value, *this);
    }

    inline
    Result 
    call(
	const Arg*     args[], 
	const unsigned n)
    const
    {
	args[Index] = &arg;
	return prev.call(args, n);
    }
    
    inline
    operator Result () const
    {
	const Arg* args[Index+1];
	return call(args, Index+1);
    }

    template <class Fun>
    inline
    void
    call(
	const Fun&     fun,
	const Arg*     args[], 
	const unsigned n)
    const
    {
	args[Index] = &arg;
	prev.call(fun, args, n);
    }

    template <class Fun>
    inline
    void
    call(
	const Fun& fun)
    const
    {
	const Arg* args[Index+1];
	return call(fun, args, Index+1);
    }	
};

template <class VarFun>
struct VarArg<VarFun,0>
{
    typedef typename VarFun::Arg    Arg;
    typedef typename VarFun::Result Result;

    Arg           arg;    
    const VarFun& fun;

    template <class Value>
    inline VarArg(
	const Value&  value, 
	const VarFun& fun_)
    throw ()
    : arg(value),
      fun(fun_)
    {
	// empty
    }

    template <class Value>
    inline VarArg<VarFun,1> operator () (const Value& value) const throw ()
    {
	return VarArg<VarFun,1>(value, *this);
    }

    inline
    Result call(const Arg* args[], const unsigned n) const
    {
	args[0] = &arg;
	return fun(args, n);
    }
    
    inline
    operator Result () const
    {
	const Arg* args[1];
	args[0] = &arg;
	return fun(args, 1);
    }

    template <class Fun>
    inline
    void
    call(
	const Fun&     afun,
	const Arg*     args[], 
	const unsigned n)
    const
    {
	args[0] = &arg;
	afun(fun, args, n);
    }

    template <class Fun>
    inline
    void
    call(
	const Fun& fun)
    const
    {
	const Arg* args[1];
	args[0] = &arg;
	fun(args, 1);
    }
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

///////////////////////////////////////////////////////////////////////////////

#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: vararg.hh
