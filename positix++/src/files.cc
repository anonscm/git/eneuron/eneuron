/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : files.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "error.hh"
#include "files.hh"
#include "arrays.hh"
#include "msg.hh"

#include <iostream>
using namespace std;

namespace positix {

///////////////////////////////////////////////////////////////////////////////

void File::postOpenTest(const char* name, int flags)
{
    if ( !_fd.valid() )
    {
	SysLastErr sle;
	if ( flags & O_CREAT )
	    throw SystemError(sle, Msg(TXT("Cannot create file %0"), name));
	else
	    throw SystemError(sle, Msg(TXT("Cannot open file %0"), name));
    }
}

///////////////////////////////////////////////////////////////////////////////

bool File::close() throw ()
{
    if ( !_fd.valid() )
	return true;
    if ( SysFileClose(_fd) )
    {
	_fd.value = SysFile::Null;
	return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////

FileError::FileError(SysFile fd, SysErrCode code, const char* op)
: SystemError(code, Msg(TXT("File %0 handle %1: %2"), 
			PxPassStr(PxGetFromFileNameMap(fd)),
			fd,
			op))
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

FileError::FileError(SysFile fd, const char* what, const char* op)
: SystemError(EINVAL, Msg(TXT("File %0 handle %1: %2: %3"), 
			  PxPassStr(PxGetFromFileNameMap(fd)),		  
			  fd,
			  what,
			  op))
{
    // empty
}
				     
///////////////////////////////////////////////////////////////////////////////

void File::open(const char* name, int flags)
{
    close();
    _fd = SysFileOpen(name, flags, 0);
    postOpenTest(name, flags);
}

///////////////////////////////////////////////////////////////////////////////

void File::open(const char* name, int flags, mode_t mode)
{
    close();
    _fd = SysFileOpen(name, flags, mode);
    postOpenTest(name, flags);
}

///////////////////////////////////////////////////////////////////////////////

size_t File::read(void* buf, size_t count)
{
    ssize_t s = SysFileRead(_fd, buf, count);
    if ( s==ssize_t(-1) )
    {
	SysLastErr sle;
	throw FileError(_fd, sle, TXT("read"));
    }
    return size_t(s);
}

///////////////////////////////////////////////////////////////////////////////

void File::readn(void* buf, size_t count)
{
    size_t r = read(buf, count);
    if ( r!=count )
    {
	SysLastErr sle;
	throw FileError(_fd, TXT("underflow"), TXT("readn"));
    }
}

///////////////////////////////////////////////////////////////////////////////

size_t File::write(const void* buf, size_t count)
{
    ssize_t s = SysFileWrite(_fd, buf, count);
    if ( s==ssize_t(-1) )
    {
	SysLastErr sle;
	throw FileError(_fd, sle, TXT("write"));
    }
    return size_t(s);
}

///////////////////////////////////////////////////////////////////////////////

void File::writen(const void* buf, size_t count)
{
    size_t w = write(buf, count);
    if ( w!=count )
    {
	SysLastErr sle;
	throw FileError(_fd, TXT("overflow"), TXT("writen"));
    }
}

///////////////////////////////////////////////////////////////////////////////

size_t File::read(File::Offset offset, void* buf, size_t count)
{
    ssize_t s = SysFileRead(_fd, offset, buf, count);
    if ( s==ssize_t(-1) )
    {
	SysLastErr sle;
	throw FileError(_fd, sle, TXT("position+read"));
    }
    return size_t(s);
}

///////////////////////////////////////////////////////////////////////////////

void File::readn(File::Offset offset, void* buf, size_t count)
{
    size_t r = read(offset, buf, count);
    if ( r!=count )
    {
	throw FileError(_fd, TXT("underflow"), TXT("position+readn"));	
    }
}

///////////////////////////////////////////////////////////////////////////////

size_t File::write(File::Offset offset, const void* buf, size_t count)
{
    ssize_t s = SysFileWrite(_fd, offset, buf, count);
    if ( s==ssize_t(-1) )
    {
	SysLastErr sle;
	throw FileError(_fd, sle, TXT("position+write"));
    }
    return size_t(s);
}

///////////////////////////////////////////////////////////////////////////////

void File::writen(File::Offset offset, const void* buf, size_t count)
{
    size_t w = write(offset, buf, count);
    if ( w!=count )
    {
	throw FileError(_fd, TXT("overflow"), TXT("position+writen"));
    }
}

///////////////////////////////////////////////////////////////////////////////

File::Offset File::seek(File::Offset offset, int whence)
{
    File::Offset o = SysFileSeek(_fd, offset, whence);
    if ( o==File::Offset(-1) )
    {
	SysLastErr sle;
	throw FileError(_fd, sle, TXT("seek"));
    }
    return o;
}

///////////////////////////////////////////////////////////////////////////////

void File::resize(Offset newlength)
{
    if ( !SysFileResize(_fd, newlength) )
    {
	SysLastErr sle;
	throw FileError(_fd, sle, TXT("resize"));
    }
}

///////////////////////////////////////////////////////////////////////////////

bool File::sync(bool required)
{
    if ( SysFileSync(_fd) )
	return true;
    if ( !required )
	return false;
    SysLastErr sle;
    throw FileError(_fd, sle, TXT("sync"));
}

///////////////////////////////////////////////////////////////////////////////

bool File::datasync(bool required)
{
    if ( SysFileDataSync(_fd) )
	return true;
    if ( !required )
	return false;
    SysLastErr sle;
    throw FileError(_fd, sle, TXT("datasync"));
}

///////////////////////////////////////////////////////////////////////////////

File::Stat& File::Stat::get(const char* filename)
{
    if ( !SysFileGetStat(filename, *this) )
    {
	SysLastErr sle;
	throw SystemError(sle, Msg(TXT("Cannot read information on file %0"),
				   filename));
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

File::Stat& File::Stat::get(SysFile fd)
{
    if ( !SysFileGetStat(fd, *this) )
    {
	SysLastErr sle;
	throw FileError(fd, sle, TXT("stat"));
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

void File::getStat(Stat& stat) const
{
    if ( !SysFileGetStat(_fd, stat) )
    {
	SysLastErr sle;
	throw FileError(_fd, sle, TXT("stat"));
    }
}

///////////////////////////////////////////////////////////////////////////////

int File::appendTo(SysFile fd, unsigned blksize, char* buf)
{
    AutoArrayPtr<char> abuf;

    if ( !blksize )
	blksize = 4096;

    if ( !buf )
    {
	abuf.reset(new char[blksize]);
	buf = abuf.get();
    }

    ssize_t rd, wr;
    for(;;)
    {
	rd = SysFileRead(_fd, buf, blksize);
	if ( rd==0 )
	    return 0; // EOF met
	if ( rd<0 )
	    return -1;
	wr = SysFileWrite(fd, buf, rd);
	if ( wr<0 )
	    return 1;
    }
}

///////////////////////////////////////////////////////////////////////////////

int File::copyTo(
    const char* name, int flags, mode_t mode, unsigned blksize, char* buf)
{
    File file(name, flags, mode);
    return appendTo(file, blksize, buf);
}

///////////////////////////////////////////////////////////////////////////////

void File::copyCompletelyTo(
    const char* name, int flags, mode_t mode, unsigned blksize, char* buf)
{
    int rc = copyTo(name, flags, mode, blksize, buf);
    if ( rc==0 )
	return;
    SysLastErr e;
    const char* umsg = PxNullStr;
    int         u    = ::unlink(name);
    if ( u!=0 && errno!=ENOENT )
	umsg = TXT(". File remains.");

    if ( rc<0 )
    {
	throw SystemError(e, Msg(TXT("Read error: cannot copy to %0%1"), 
				 name, umsg));
    }
    else
    {
	throw SystemError(e, Msg(TXT("Write error: cannot copy to %0%1"), 
				 name, umsg));
    }
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

// end of positix++: files.cc
