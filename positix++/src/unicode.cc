/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : unicode.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "error.hh"
#include "unicode.hh"
#include "bytes.hh"
#include "msg.hh"

#include <iostream>

using namespace std;
using namespace positix_types;

///////////////////////////////////////////////////////////////////////////////
namespace positix {
///////////////////////////////////////////////////////////////////////////////

UnicodeChar BadUnicodeInsertHandlerThrowError(UnicodeChar c)
{
    throw Error(Msg(TXT("Output of illegal unicode char U%0"),
		    FmtHex(c)));
}

UnicodeChar BadUnicodeInsertHandlerIgnore(UnicodeChar /*c*/)
{
    return UnicodeEof;
}

BadUnicodeCharInsertHandlerFunction TheBadUnicodeCharInsertHandler
= BadUnicodeInsertHandlerThrowError;

///////////////////////////////////////////////////////////////////////////////

// 1c98 7654 321b 9876 5432 1a98 7654 3210  SigBits  Number
// 0000 0000 0000 0000 0000 0000 0aaa aaaa     7      127
// 0000 0000 0000 0000 110b bbbb 10aa aaaa    11     2048
// 0000 0000 1110 cccc 10bb bbbb 10aa aaaa    16    65536
// 1111 0ddd 10cc cccc 10bb bbbb 10aa aaaa    21  2097152 
//                                         Total: 2164864

// 1b 9876 5432 1a98 7654 3210
// 00 0000 0000 0000 0aaa aaaa
// 00 0000 0000 0bbb bbaa aaaa
// 00 0000 cccc bbbb bbaa aaaa
// 0d ddcc cccc bbbb bbaa aaaa

extern fu32 EncodeUTF8(UnicodeChar c) throw ()
{
    fu32 u = c;

    if ( u<=0x7f )
	return c;
    if ( u<=0x7ff )
    {
	return ((u & 0x7C0)<<2) // b
	    |  ((u & 0x03F))    // a
	    | 0xC080;           // marker bits
    }
    if ( u<=0xFFFF )
    {
	if ( c>=0xD800 && c<=0xDFFF )
	    return 0;
	return ((u & 0xF000)<<4) // c
	    |  ((u & 0x0FC0)<<2) // b
	    |   (u & 0x003F)     // a
	    | 0xE08080;          // marker bits
    }
    if ( u<=0x10FFFF )
    {
	
	return ((u & 0x1C0000)<<6) // d
	    |  ((u & 0x03F000)<<4) // c
	    |  ((u & 0x000FC0)<<2) // b
	    |   (u & 0x00003F)     // a
	    |  0xF0808080;         // marker bits
    }
    return 0;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar DecodeUTF8(fu32 u) throw ()
{
    if ( u<=0x7f )
	return UnicodeChar(u);
    if ( u<=0xDFFF )
    {
	u = (u & 0x3F) | ((u>>2) & 0x7C0);
    }
    else if ( u<=0xEFFFFF )
    {
	u = (u & 0x3F) | ((u>>2) & 0xFC0) | ((u>>4) & 0xF000);
    }
    else if ( u<=0xF7FFFFFF )
    {
	u = (u & 0x3F) | ((u>>2) & 0xFC0) | ((u>>4) & 0x3F000) 
	    | ((u>>6) & 0x1C0000);
    }
    else
    {
	return 0;
    }

    return (u>=0xD800 && u<=0xDFFF) ? 0 : u;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUTF8(std::istream& in)
{
    fi32 add;
    fi32 i = in.get();

    if ( i==std::istream::traits_type::eof() )
	return UnicodeEof;
    else if ( i <= 0x7F )
	return UnicodeChar(i);
    else if ( i <= 0xDF )
    {
	add = 1;
	i  &= 0x1F;
    }
    else if ( i <= 0xEF )
    {
	add = 2;
	i  &= 0x0F;
    }
    else if ( i <= 0xF7 )
    {
	add = 3;
	i  &= 0x07;
    }
    else
    {
	goto bad;
    }

    do
    {
	std::istream::int_type j = in.get();
	if ( (j & 0xC0)!=0x80 || j==std::istream::traits_type::eof() )
	    goto bad;
	i = (i<<6) | (j & 0x3F);	
    } while ( --add );

    if ( (i<0xD800 || (i>0xDFFF && i<=UnicodeMax)) )
	return UnicodeChar(i);

  bad:
    in.setstate(ios_base::badbit);
    return UnicodeEof;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUTF8c(std::istream& in)
{
    fi32 add, min;
    fi32 i = in.get();

    if ( i==std::istream::traits_type::eof() )
	return UnicodeEof;
    else if ( i <= 0x7F )
	return UnicodeChar(i);
    else if ( i <= 0xDF )
    {
	add = 1;
	min = 0x0080;
	i  &= 0x1F;
    }
    else if ( i <= 0xEF )
    {
	add = 2;
	min = 0x0800;
	i  &= 0x0F;
    }
    else if ( i <= 0xF7 )
    {
	add = 3;
	min = 0x10000;
	i  &= 0x07;
    }
    else
    {
	goto bad;
    }

    do
    {
	std::istream::int_type j = in.get();
	if ( (j & 0xC0)!=0x80 || j==std::istream::traits_type::eof() )
	    goto bad;
	i = (i<<6) | (j & 0x3F);	
    } while ( --add );

    if ( (i>=min) && (i<0xD800 || (i>0xDFFF && i<=UnicodeMax)) )
	return UnicodeChar(i);

  bad:
    in.setstate(ios_base::badbit);
    return UnicodeEof;
}

///////////////////////////////////////////////////////////////////////////////

extern bool IsValidUTF8(fu32 u) throw ()
{
    if ( u<=0x7f )
	return true;
    if ( u<=0xDFFF )
	return u>=0xC200 && (u&0xE0C0)==0xC080;
    else if ( u<=0xEFFFFF )
	return u>=0xE0A000 && (u&0xF0C0C0)==0xE08080 
	    && (u<0xEDA080 || u>0xEDBFBF); // surrogates
    else if ( u<=0xF48FBFBF ) // Encoding of UnicodeMax==0x10FFFF
	return u>=0xF0900000 && (u&0xF8C0C0C0)==0xF0808080;
    else
	return false;
}

///////////////////////////////////////////////////////////////////////////////

fu32 EncodeUTF16(UnicodeChar c) throw ()
{
    fu32 u = c;
    if ( u < 0xD800 )
	return c;
    if ( u < 0xE000 )
	return 0;
    if ( u <= 0xFFFF )
	return u;
    if ( u <= fu32(UnicodeMax) )
    {
	u -= 0x10000;
	return (u & 0x03FF)|((u & 0xFFC00)<<6)|0xD800DC00;
    }
    return 0;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar DecodeUTF16(fu32 u) throw ()
{
    if ( u < 0xD800 )
	return UnicodeChar(u);
    if ( u < 0xE000 )
	return 0;
    if ( u <= 0xFFFF )
	return UnicodeChar(u);
    if ( (u&0xFC00FC00)!=0xD800DC00 )
	return 0;
    if ( u<=0xDBFFDFFF )
	return UnicodeChar( ((u & 0x3FF) | ((u>>6) & 0xFFC00)) + 0x10000);
    return 0;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUTF16LE(std::istream& in)
{
    PX_BYTE_ARRAY(buf, 4);
    in.read((char*)buf, 2);
    if ( in.gcount()!=2 )
	return UnicodeEof;
    UnicodeChar c = GetLE16(buf);
    if ( c>=0xD800 && c<=0xDFFF )
    {
	in.read((char*)(buf+2), 2);
	if ( in.gcount()==2 )
	{
	    c = DecodeUTF16( (fu32(c)<<16) | GetLE16(buf+2) );
	    if ( c )
		return c;
	}
	in.setstate(ios_base::badbit);
	return UnicodeEof;
    }
    return c;   
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUTF16BE(std::istream& in)
{
    PX_BYTE_ARRAY(buf, 4);
    in.read((char*)buf, 2);
    if ( in.gcount()!=2 )
	return UnicodeEof;
    UnicodeChar c = GetBE16(buf);
    if ( c>=0xD800 && c<=0xDFFF )
    {
	in.read((char*)(buf+2), 2);
	if ( in.gcount()==2 )
	{
	    c = DecodeUTF16( (fu32(c)<<16) | GetBE16(buf+2) );
	    if ( c )
		return c;
	}
	in.setstate(ios_base::badbit);
	return UnicodeEof;
    }
    return c;   
}

///////////////////////////////////////////////////////////////////////////////

extern bool IsValidUTF16(fu32 u) throw ()
{
    if ( u < 0xD800 )
	return true;
    if ( u < 0xE000 )
	return false;
    if ( u <= 0xFFFF )
	return true;
    if ( (u&0xFC00FC00)!=0xD800DC00 )
	return false;;
    if ( u<=0xDBFFDFFF )
	return true;
    return false;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUCS2BE(std::istream& in)
{
    PX_BYTE_ARRAY(buf, 2);
    in.read((char*)buf, 2);
    if ( in.gcount()!=2 )
	return UnicodeEof;
    UnicodeChar c = GetBE16(buf);
    if ( c>=0xD800 && c<=0xDFFF )
    {
	in.setstate(ios_base::badbit);
	c = 0;
    }
    return c;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUCS2LE(std::istream& in)
{
    PX_BYTE_ARRAY(buf, 2);
    in.read((char*)buf, 2);
    if ( in.gcount()!=2 )
	return UnicodeEof;
    UnicodeChar c = GetLE16(buf);
    if ( c>=0xD800 && c<=0xDFFF )
    {
	in.setstate(ios_base::badbit);
	c = 0;
    }
    return c;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUCS4BE(std::istream& in)
{
    PX_BYTE_ARRAY(buf, 4);
    in.read((char*)buf, 4);
    if ( in.gcount()!=4 )
	return UnicodeEof;
    UnicodeChar c = GetBE32(buf);
    if ( (c>=0xD800 && c<=0xDFFF) || c>=0x10FFFF )
    {
	in.setstate(ios_base::badbit);
	c = 0;
    }
    return c;
}

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUCS4LE(std::istream& in)
{
    PX_BYTE_ARRAY(buf, 4);
    in.read((char*)buf, 4);
    if ( in.gcount()!=4 )
	return UnicodeEof;
    UnicodeChar c = GetLE32(buf);
    if ( (c>=0xD800 && c<=0xDFFF) || c>=0x10FFFF )
    {
	in.setstate(ios_base::badbit);
	c = UnicodeMax+1;
    }
    return c;
}

///////////////////////////////////////////////////////////////////////////////
} // namespace positix
///////////////////////////////////////////////////////////////////////////////


// end of positix++: unicode.cc
