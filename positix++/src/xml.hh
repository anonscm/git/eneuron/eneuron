/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : xml.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_xml_hh__
#define __positix_xml_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <iostream>
#include <vector>
#include <stack>
#include "types.hh"
#include "error.hh"
#include "unicode.hh"

namespace positix {

///////////////////////////////////////////////////////////////////////////////

// TODO: May be only 16 bits. But Unicode is 21 bits.
typedef wchar_t XmlChar; 
extern bool IsXmlNameStartChar(XmlChar c) throw ();
extern bool IsXmlNameChar(XmlChar c) throw ();
extern bool IsXmlSpace(XmlChar c) throw ();

typedef unsigned XmlSymbolId;
class XmlBasicParser;
class XmlParser;
class XmlTokenizer;

///////////////////////////////////////////////////////////////////////////////

struct XmlVersion
{
    int major;
    int minor;
};

///////////////////////////////////////////////////////////////////////////////

struct XmlFileInfo
{
    XmlVersion          version;
    positix::TextCoding coding;
    bool		standAlone;
};

///////////////////////////////////////////////////////////////////////////////

class XmlError : public Error
{
public:
    inline explicit XmlError(const TextMessage& msg)
    : Error(msg)
    {
	// empty
    }
};

///////////////////////////////////////////////////////////////////////////////

class XmlSyntaxError : public XmlError
{
public:
    inline explicit XmlSyntaxError(const TextMessage& msg)
    : XmlError(msg)
    {
	// empty
    }
};

///////////////////////////////////////////////////////////////////////////////

class XmlParserError : public XmlError
{
public:
    inline explicit XmlParserError(const TextMessage& msg)
    : XmlError(msg)
    {
	// empty
    }
};

///////////////////////////////////////////////////////////////////////////////

struct XmlAttr
{
    XmlAttr(XmlSymbolId id, const std::string& val)
    : sid(id),
      value(val)
    {
	// empty
    }

    XmlSymbolId sid;
    std::string   value;
};

typedef std::vector<XmlAttr> XmlAttrList;

///////////////////////////////////////////////////////////////////////////////

struct XmlNode
{
    enum Type
    {
	Invalid,
	Element,
	ProcessingInstruction,
	Text,
	nTypes
    };

    inline XmlNode() throw ()
    {
	successor = 0;
    }

    virtual ~XmlNode() {};

    XmlNode* successor;
};

///////////////////////////////////////////////////////////////////////////////

struct XmlTaggedNode : public XmlNode
{
    inline XmlTaggedNode()
    {
	tag = 0;
    }

    XmlSymbolId tag;
};

///////////////////////////////////////////////////////////////////////////////

struct XmlElement : public XmlTaggedNode
{
    XmlAttrList attrs;
    // TODO: XmlTree     children;
};

///////////////////////////////////////////////////////////////////////////////

struct XmlProcessingInstruction : public XmlTaggedNode
{
    std::string pi;
};

///////////////////////////////////////////////////////////////////////////////

struct XmlText : public XmlNode
{
    inline explicit XmlText(const std::string& text_)
    : text(text_)
    {
	// empty
    }

    std::string text;
};

///////////////////////////////////////////////////////////////////////////////

class XmlBasicParser
{
public:
    virtual ~XmlBasicParser() {}

    virtual void reset();
    virtual XmlSymbolId symbolId(const std::string& name) = 0;
    virtual std::string symbolName(XmlSymbolId id) const = 0;

    virtual bool preserveWhitespace() const throw ();

    virtual bool xmlFileInfo(const XmlFileInfo& xfi);

    virtual bool startTag(XmlSymbolId id, const XmlAttrList& attrs,bool empty);
    virtual bool endTag(XmlSymbolId id);
    virtual bool processingInstruction(XmlSymbolId id, 
				       const std::string& instr);
    virtual bool comment(const std::string& str);
    virtual bool text(const std::string& str);
    virtual bool cdata(const std::string& str);

    inline TextFilePosition tfp() const throw ();

protected:
    friend class XmlTokenizer;
    XmlTokenizer* _tokenizer;
};

///////////////////////////////////////////////////////////////////////////////

class XmlTokenizer
{
public:
    static const char ExclamationMark = 33; // Unicode of !
    static const char DoubleQuotes    = 34; // Unicode of "
    static const char Ampersand       = 38; // Unicode of '
    static const char SingleQuotes    = 39; // Unicode of '
    static const char Hyphen	      = 45; // Unicode of -
    static const char Slash	      = 47; // Unicode of /
    static const char LessThanSign    = 60; // Unicode of <
    static const char EqualSign       = 61; // Unicode of =
    static const char GreaterThanSign = 62; // Unicode of >
    static const char QuestionMark    = 63; // Unicode of ?
    static const char OpenBracket     = 91; // Unicode of [
    static const char CloseBracket    = 93; // Unicode of ]

    void parse(
	std::istream&         in, 
	XmlBasicParser&       parser, 
	const char*           iname = 0,
	positix::TextCoding te    = positix::TextCodingUndefined);

    inline const TextFilePosition& tfp() const throw ()
    {
	return _tfp;
    }

private:
    bool parseXmlDecl              ();
    bool parseStartTag             ();
    bool parseEndTag               ();
    bool parseExclamation	   (); 
    bool parseQuestion		   ();
    bool parseText		   ();
    bool parseDecl		   ();
    bool parseDocTypeDecl	   ();
    bool parseEntityDecl	   ();
    bool parseElementDecl	   ();
    bool parseAttListDecl	   ();
    bool parseNotationDecl	   ();
    bool parseUnknownDecl	   ();
    bool parseComment		   ();
    bool parseBracket		   ();
    bool parseCData		   ();

    void guessInputEncoding();
    void setInputEncoding(positix::TextCoding te);
    void setInputEncoding(const std::string& s);

    std::string   readAssignment();
    std::string   readQuotedString();
    std::string   readName(bool required);
    XmlSymbolId readSymbol(bool required);

    void parseAttrList(XmlAttrList& attr);

    XmlChar extract();
    XmlChar getChar();
    void    putback(UnicodeChar c) throw ();

    bool skipSpace() throw ();

    inline XmlChar peek()
    {
	XmlChar c = extract();
	_putback = c;
	return c;
    }

    typedef positix::InStreamGlyphExtractorFunction GlyphDecoder;

    std::istream*       _in;
    unsigned	        _lastCol;
    positix::TextCoding _tc;
    GlyphDecoder        _decode;
    XmlBasicParser*	_parser;
    XmlChar		_putback;
    TextFilePosition    _tfp;
};

///////////////////////////////////////////////////////////////////////////////

struct XmlHandler
{
    typedef bool (*StartTagFun)(
	XmlParser&         parser,
	void*		   state,
	XmlSymbolId        id, 
	const XmlAttrList& attrs, 
	bool               empty);

    typedef bool (*EndTagFun)(
	XmlParser&  parser,
	void*       state,
	XmlSymbolId id);
    
    typedef bool (*ProcessingInstructionFun)(
	XmlParser&         parser,
	void*		   state,
	XmlSymbolId        id, 
	const std::string& instr);

    typedef bool (*CommentFun)(
	XmlParser&         parser,
	void*		   state,
	const std::string& str);

    typedef bool (*TextFun)(
	XmlParser&         parser,
	void*		   state,
	const std::string& str);

    StartTagFun              startTag;
    EndTagFun                endTag;
    ProcessingInstructionFun processingInstruction;
    CommentFun		     comment;
    TextFun		     text;
    TextFun		     cdata;

    static bool StartTag(
	XmlParser&         parser,
	void*		   state,
	XmlSymbolId        id, 
	const XmlAttrList& attrs, 
	bool               empty);

    static bool EndTag(
	XmlParser&  parser,
	void*       state,
	XmlSymbolId id);
    
    static bool ProcessingInstruction(
	XmlParser&         parser,
	void*		   state,
	XmlSymbolId        id, 
	const std::string& instr);

    static bool Comment(
	XmlParser&         parser,
	void*		   state,
	const std::string& str);

    static bool Text(
	XmlParser&         parser,
	void*		   state,
	const std::string& str);
};
extern XmlHandler TheStandardXmlHandler;

// For use as basic initializer of own XmlHandlers:
// XmlHandler MyXmlHandler =
// {
//     &positix::XmlHandler::StartTag,
//     &positix::XmlHandler::EndTag,
//     &positix::XmlHandler::ProcessingInstruction,
//     &positix::XmlHandler::Comment,
//     &positix::XmlHandler::Text,
//     &positix::XmlHandler::Text
// };


///////////////////////////////////////////////////////////////////////////////

class XmlParser : public XmlBasicParser
{
public:
    virtual void reset();

    virtual bool startTag(
	XmlSymbolId        id, 
	const XmlAttrList& attrs, 
	bool               empty)
    = 0;

    virtual bool endTag(XmlSymbolId id);

    virtual bool processingInstruction(XmlSymbolId id, 
				       const std::string& instr);
    virtual bool comment(const std::string& str);
    virtual bool text(const std::string& str);
    virtual bool cdata(const std::string& str);

    // Callbacks for XML handlers:

    virtual bool defaultStartTag(
	XmlSymbolId        id, 
	const XmlAttrList& attrs, 
	bool               empty);

    virtual bool defaultProcessingInstruction(XmlSymbolId id, 
					      const std::string& instr);
    virtual bool defaultComment(const std::string& str);
    virtual bool defaultText(const std::string& str);
    virtual bool defaultCdata(const std::string& str);

    inline unsigned level() const throw ()
    {
	return _openTags.size();
    }

    inline void push(XmlSymbolId id, const XmlHandler* handler, void* value)
    {
	_openTags.push_back(OpenTag(id, handler, value, tfp()));
    }

    inline bool delegateStartTag(
	XmlSymbolId        id, 
	const XmlAttrList& attrs, 
	bool               empty)
    {
	OpenTag& ot = _openTags.back();
	return ot.handler->startTag(*this, ot.value, id, attrs, empty);
    }

private:
    struct OpenTag
    {
	inline OpenTag(
	    XmlSymbolId         id, 
	    const XmlHandler*   handler_,
	    void*               value_,
	    const TextPosition& tp) 
        throw ()
	: handler(handler_),
	  value(value_),
	  tag(id),	  
	  pos(tp)
	{
	    // empty
	}

	const XmlHandler*  handler;
	void*		   value;
	XmlSymbolId	   tag;
	TextPosition	   pos;
    };
    typedef std::vector<OpenTag> OpenTags;
    OpenTags _openTags;
};

///////////////////////////////////////////////////////////////////////////////

inline TextFilePosition XmlBasicParser::tfp() const throw ()
{
    return _tokenizer ? _tokenizer->tfp() : TextFilePosition();
}

///////////////////////////////////////////////////////////////////////////////
} // namespace positix

#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: xml.hh
