/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : types.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_types_hh__
#define __positix_types_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <iosfwd>

namespace positix_types {

///////////////////////////////////////////////////////////////////////////////

struct Undefined
{
    // empty!
};

///////////////////////////////////////////////////////////////////////////////

template <class Type_>
class UnionMember
{
public:
    typedef Type_ Type;

    inline operator Type& () throw ()
    {
	return reinterpret_cast<Type&>(_data);
    }

    inline operator const Type& () const throw ()
    {
	return reinterpret_cast<const Type&>(_data);
    }

    inline Type& operator() () throw ()
    {
	return reinterpret_cast<Type&>(_data);
    }

    inline const Type& operator() () const throw ()
    {
	return reinterpret_cast<const Type&>(_data);
    }

    inline Type* operator->() throw ()
    {
	return reinterpret_cast<Type*>(_data);
    }

    inline const Type* operator->() const throw ()
    {
	return reinterpret_cast<const Type*>(_data);
    }

private:
    char _data[sizeof(Type)];
};

///////////////////////////////////////////////////////////////////////////////

// Some @!#$ had defined the macros "major" and "minor" with a single argument
// in GNU/Linux and other UNIX variants!
// But we definitely want to use these names in our struct Version AND we
// definitely want to use member initialization lists in our constructors!
// We cannot write "major (X)" as member initializer because this will be
// interpreted as a macro call. Therefore, we defined the empty macro
// __POSITIX_EMPTY__ such that we can write "major __POSITIX_EMPTY__ (X)" which
// will be translated to the desired "major (X)" by the preprocessor
#define __POSITIX_EMPTY__

struct Version
{
    const unsigned major :  8;
    const unsigned minor :  8;
    const int      patch : 16;

    inline Version() throw ()
    : major __POSITIX_EMPTY__ (0),
      minor __POSITIX_EMPTY__ (0),
      patch __POSITIX_EMPTY__ (0)
    {
	// empty
    }

    inline Version(unsigned M, unsigned m, int p)
    : major __POSITIX_EMPTY__ (M),
      minor __POSITIX_EMPTY__ (m),
      patch __POSITIX_EMPTY__ (p)
    {
	// empty
    }
};

// see above
#undef __POSITIX_EMPTY__

extern std::ostream& operator << (std::ostream& os, Version vers);

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif

// end of positix++: types.hh
