/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : def.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_defs_hh__
#define __positix_defs_hh__

#define POSITIX_CLASS_NOCOPY(Class) \
    private: \
        Class(const Class&); \
        Class& operator = (const Class&)

#define POSITIX_CLASS_ROR(Type, name) \
    public : const Type& name() const throw () { return _##name; } \
    private: Type _##name; \
    public :

#define POSITIX_CLASS_ROV(Type, name) \
    public : const Type name() const throw () { return _##name; } \
    private: Type _##name; \
    public :

#define POSITIX_CMP_OP(op,type,expr) \
    inline bool operator op (const type& a, const type& b) throw () { \
        return expr op 0; }

#define POSITIX_CLASS_CMP(Class) \
    POSITIX_CMP_OP(<, Class, a.compare(b)) \
    POSITIX_CMP_OP(>, Class, a.compare(b)) \
    POSITIX_CMP_OP(==,Class, a.compare(b)) \
    POSITIX_CMP_OP(!=,Class, a.compare(b)) \
    POSITIX_CMP_OP(<=,Class, a.compare(b)) \
    POSITIX_CMP_OP(>=,Class, a.compare(b)) 

#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: def.hh
