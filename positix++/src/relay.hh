/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : relay.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_relay_hh__
#define __positix_relay_hh__

namespace positix {

template <class Value_>
class Relay
{
public:
    typedef Value_ Value;

    inline Relay()
    {
	// empty
    }

    explicit Relay(const Relay& relay) throw ()
    : _value(relay._value.release())
    {
	// empty
    }

    explicit Relay(Value& value) throw ()
    : _value(value.release())
    {
	// empty
    }

    inline Relay& operator = (const Relay& relay) throw ()
    {
	_value = relay._value.release();
	return *this;
    }

    inline Relay& setReleased(Value& value) throw ()
    {
	_value = value.release();
	return *this;
    }

    inline Value& operator() () throw ()
    {
	return _value;
    }

    inline const Value& operator() () const throw ()
    {
	return _value;
    }

    inline operator Value& () throw ()
    {
	return _value;
    }

    inline operator const Value& () const throw ()
    {
	return _value;
    }

private:
    mutable Value _value;
};

} // namespace positix


#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: relay.hh
