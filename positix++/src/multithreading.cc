/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : multithreading.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "multithreading.hh"

namespace positix {
    

///////////////////////////////////////////////////////////////////////////////

void Modex::wrLock() throw ()
{
    Thread cur = Thread::Current();
    _mutex.lock();
    if ( _writer != cur )
    {
	++_nWaitingWriters;
	while ( _nReaders > 0 && _nWriters > 0 )
	    _wrcond.wait(_mutex);
	_writer = cur;
	--_nWaitingWriters;
    }
    ++_nWriters;
    _mutex.unlock();
}

///////////////////////////////////////////////////////////////////////////////

void Modex::wrUnlock() throw ()
{
    _mutex.lock();
    if ( --_nWriters == 0 )
    {
	_writer = Thread();
	wake();
    }
    _mutex.unlock();
}


///////////////////////////////////////////////////////////////////////////////

void Modex::rdLock() throw ()
{
    _mutex.lock();	
    if ( _writer != Thread::Current() )
    {
	++_nWaitingReaders;
	while ( _nWriters > 0 )
	    _rdcond.wait(_mutex);
	--_nWaitingReaders;
    }
    ++_nReaders;
    _mutex.unlock();
}

///////////////////////////////////////////////////////////////////////////////

void Modex::rdUnlock() throw ()
{
    _mutex.lock();
    if ( --_nReaders == 0 )
    {
	if ( _writer != Thread::Current() )
	    wake();
    }
    _mutex.unlock();
}

///////////////////////////////////////////////////////////////////////////////

void Modex::wake() throw ()
{
    if ( _pref == ReaderPreference )
    {
	if ( _nWaitingReaders > 0 )
	    _rdcond.broadcast();
	else
	    _wrcond.signal();
    }
    else
    {
	if ( _nWaitingWriters > 0 )
	    _wrcond.signal();
	else
	    _rdcond.broadcast();
    }
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

// end of positix++: multithreading.cc
