/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : format.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef _POSITIX_FORMAT_
#error You must define _POSITIX_FORMAT_
#endif

#ifndef _POSITIX_FMT_
#error You must define _POSITIX_FMT_
#endif

#ifndef _POSITIX_FORMAT_RESULT_
#error You must define _POSITIX_FORMAT_
#endif

#ifndef _POSITIX_FORMAT_PARAM_
#error You must define _POSITIX_FORMAT_PARAM_
#endif

#ifndef _POSITIX_FORMAT_ARG_
#error You must define _POSITIX_FORMAT_ARG_
#endif

#ifndef _POSITIX_FORMAT_FUN_
#error You must define _POSITIX_FORMAT_FUN_
#endif

#ifndef _POSITIX_FORMARG_
#error You must define _POSITIX_FORMARG_
#endif

#ifndef _POSITIX_FORMARG_CONS_
#error You must define _POSITIX_FORMARG_CONS_
#endif

///////////////////////////////////////////////////////////////////////////////

#ifdef _POSITIX_FMT0_

template <class V0>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    const V0& v0)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ 
                                 _POSITIX_FMT0_, args, 1u);
}

#endif

///////////////////////////////////////////////////////////////////////////////

template <class V0>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 1u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 2u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 3u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2,class V3>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2,
    const V3& v3)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2),
	_POSITIX_FORMARG_CONS_(V3, v3)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 4u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2,class V3,class V4>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2,
    const V3& v3,
    const V4& v4)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2),
	_POSITIX_FORMARG_CONS_(V3, v3),
	_POSITIX_FORMARG_CONS_(V4, v4)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 5u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2,class V3,class V4,
	  class V5>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2,
    const V3& v3,
    const V4& v4,
    const V5& v5)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2),
	_POSITIX_FORMARG_CONS_(V3, v3),
	_POSITIX_FORMARG_CONS_(V4, v4),
	_POSITIX_FORMARG_CONS_(V5, v5)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 6u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2,class V3,class V4,
	  class V5,class V6>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2,
    const V3& v3,
    const V4& v4,
    const V5& v5,
    const V6& v6)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2),
	_POSITIX_FORMARG_CONS_(V3, v3),
	_POSITIX_FORMARG_CONS_(V4, v4),
	_POSITIX_FORMARG_CONS_(V5, v5),
	_POSITIX_FORMARG_CONS_(V6, v6)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 7u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2,class V3,class V4,
	  class V5,class V6,class V7>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2,
    const V3& v3,
    const V4& v4,
    const V5& v5,
    const V6& v6,
    const V7& v7)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2),
	_POSITIX_FORMARG_CONS_(V3, v3),
	_POSITIX_FORMARG_CONS_(V4, v4),
	_POSITIX_FORMARG_CONS_(V5, v5),
	_POSITIX_FORMARG_CONS_(V6, v6),
	_POSITIX_FORMARG_CONS_(V7, v7)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 8u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2,class V3,class V4,
	  class V5,class V6,class V7,class V8>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2,
    const V3& v3,
    const V4& v4,
    const V5& v5,
    const V6& v6,
    const V7& v7,
    const V8& v8)
{
    _POSITIX_FORMARG_ args[] = {
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2),
	_POSITIX_FORMARG_CONS_(V3, v3),
	_POSITIX_FORMARG_CONS_(V4, v4),
	_POSITIX_FORMARG_CONS_(V5, v5),
	_POSITIX_FORMARG_CONS_(V6, v6),
	_POSITIX_FORMARG_CONS_(V7, v7),
	_POSITIX_FORMARG_CONS_(V8, v8)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 9u);
}

///////////////////////////////////////////////////////////////////////////////

template <class V0,class V1,class V2,class V3,class V4,
	  class V5,class V6,class V7,class V8,class V9>
inline _POSITIX_FORMAT_RESULT_ _POSITIX_FORMAT_ (
    _POSITIX_FORMAT_PARAM_
    _POSITIX_FMT_ fmt,
    const V0& v0,
    const V1& v1,
    const V2& v2,
    const V3& v3,
    const V4& v4,
    const V5& v5,
    const V6& v6,
    const V7& v7,
    const V8& v8,
    const V9& v9)
{
    _POSITIX_FORMARG_ args[]={
	_POSITIX_FORMARG_CONS_(V0, v0),
	_POSITIX_FORMARG_CONS_(V1, v1),
	_POSITIX_FORMARG_CONS_(V2, v2),
	_POSITIX_FORMARG_CONS_(V3, v3),
	_POSITIX_FORMARG_CONS_(V4, v4),
	_POSITIX_FORMARG_CONS_(V5, v5),
	_POSITIX_FORMARG_CONS_(V6, v6),
	_POSITIX_FORMARG_CONS_(V7, v7),
	_POSITIX_FORMARG_CONS_(V8, v8),
	_POSITIX_FORMARG_CONS_(V9, v9)};
    return _POSITIX_FORMAT_FUN_ (_POSITIX_FORMAT_ARG_ fmt, args, 10u);
}

#undef _POSITIX_FORMAT_FUN_
#undef _POSITIX_FORMAT_
#undef _POSITIX_FMT_
#undef _POSITIX_FMT0_
#undef _POSITIX_FORMARG_
#undef _POSITIX_FORMARG_CONS_
#undef _POSITIX_FORMAT_RESULT_
#undef _POSITIX_FORMAT_PARAM_
#undef _POSITIX_FORMAT_ARG_

// end of positix++: format.hh
