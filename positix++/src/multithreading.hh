/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : multithreading.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_multithreading_hh__
#define __positix_multithreading_hh__

#include "def.hh"

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#ifndef __positix_error_hh__
#include "error.hh"
#endif

namespace positix {

class Mutex;
class CondVar;
class FastMutex;
class Modex;
class RecursiveMutex;
class Thread;
class ThreadCreationError;
class SynchronizationError;

///////////////////////////////////////////////////////////////////////////////

class ThreadCreationError : public SystemError
{
public:
    inline ThreadCreationError(const char* prefix, SysErrCode code)
    : SystemError(code, prefix)
    {
       // empty
    }    
};

///////////////////////////////////////////////////////////////////////////////

class SynchronizationError : public SystemError
{
public:
    inline SynchronizationError(const char* prefix, SysErrCode code)
    : SystemError(code, prefix)
    {
       // empty
    }    
};

///////////////////////////////////////////////////////////////////////////////

class Thread
{
public:
    typedef void* (Entry)(void*);
    
    Thread() throw ();

    inline explicit Thread(const SysThread& systhread) throw ()
    : _thread(systhread)
    {
	// empty
    }
    
    Thread(Entry entry, void* arg) throw (ThreadCreationError);

    static Thread Create(Entry entry, void* arg) throw ();

    static Thread Current() throw ();

    inline SysThread systhread() const throw ()
    {
	return _thread;
    }

    bool valid() const throw ();

    inline bool operator!() const throw ()
    {
	return !valid();
    }

private:
    friend bool positix::operator==(const Thread& a, const Thread& b) throw ();
    SysThread _thread;
    static void CreationError(int code) throw (ThreadCreationError);
};

inline bool operator != (Thread a, Thread b) throw ()
{
    return !(a==b);
}

///////////////////////////////////////////////////////////////////////////////

class Mutex
{
public:
    ~Mutex() throw ();

    void lock() throw ();
    void unlock() throw ();

    class Lock
    {
    public:
	inline Lock(Mutex& mutex) throw ()
	: _mutex(mutex)
	{
	    mutex.lock();
	}

	inline ~Lock() throw ()
	{
	    _mutex.unlock();
	}

    private:
	Mutex& _mutex;
	POSITIX_CLASS_NOCOPY(Lock);
    };

protected:
    Mutex();

    friend class CondVar;
    SysMutex _mutex;
    POSITIX_CLASS_NOCOPY(Mutex);
};

///////////////////////////////////////////////////////////////////////////////

class FastMutex : public Mutex
{
public:
    FastMutex();
    ~FastMutex() throw ();

private:
    POSITIX_CLASS_NOCOPY(FastMutex);
};

///////////////////////////////////////////////////////////////////////////////

class RecursiveMutex : public Mutex
{
public:
    RecursiveMutex();
    ~RecursiveMutex() throw ();

private:
    POSITIX_CLASS_NOCOPY(RecursiveMutex);
};
    
///////////////////////////////////////////////////////////////////////////////

class CondVar
{
public:
    CondVar();
    ~CondVar() throw ();

    enum Result
    {
	Ok,
	Timeout,
	Interrupt
    };
    
    Result wait(positix_types::fu64 nanoseconds, Mutex& mutex) throw ();

    void wait(Mutex& mutex) throw ();
    void signal() throw ();
    void broadcast() throw ();

private:
    SysCondVar _cond;
    POSITIX_CLASS_NOCOPY(CondVar);
};

///////////////////////////////////////////////////////////////////////////////

class Modex
{
public:
    enum Preference
    {
	ReaderPreference,
	WriterPreference
    };

    inline Modex(Preference pref)
    {
	_nReaders        =
	_nWriters        =
	_nWaitingReaders = 
	_nWaitingWriters = 0;
	_pref            = pref;
    }

    inline void setPreference(Preference pref) throw ()
    {
	_mutex.lock();
	_pref = pref;
	_mutex.unlock();
    }

    inline Preference getPreference() const throw ()
    {
	// We don't lock _mutex here, because the preference may be changed
	// immediately after this call, anyway.
	return _pref;
    }

    void wrLock() throw ();
    void wrUnlock() throw ();
    void rdLock() throw ();
    void rdUnlock() throw ();

    class RdLock
    {
    public:
	inline RdLock(Modex& modex) throw ()
	: _modex(modex)
	{
	    modex.rdLock();
	}

	inline ~RdLock() throw ()
	{
	    _modex.rdUnlock();
	}

    private:
	Modex& _modex;
	POSITIX_CLASS_NOCOPY(RdLock);
    };

    class WrLock
    {
    public:
	inline WrLock(Modex& modex) throw ()
	: _modex(modex)
	{
	    modex.wrLock();
	}

	inline ~WrLock() throw ()
	{
	    _modex.wrUnlock();
	}

    private:
	Modex& _modex;
	POSITIX_CLASS_NOCOPY(WrLock);
    };
        
private:
    FastMutex _mutex;
    CondVar _wrcond;
    CondVar _rdcond;
    // TODO: Change counter types, e.g. to u32
    volatile unsigned _nReaders; 
    volatile unsigned _nWriters; 
    volatile unsigned _nWaitingReaders;
    volatile unsigned _nWaitingWriters;
    Thread   _writer;
    Preference _pref;

    void wake() throw ();
   
    POSITIX_CLASS_NOCOPY(Modex);
};

///////////////////////////////////////////////////////////////////////////////

class Semaphore
{
public:
    Semaphore();
    explicit Semaphore(unsigned initialValue);
    ~Semaphore();

    static unsigned MaxValue() throw ();

    void wait() throw ();
    bool trywait() throw ();

    void post() throw ();

    unsigned value() const throw ();

    class Lock
    {
    public:
	inline Lock(Semaphore& sem) throw ()
	: _sem(sem)
	{
	    sem.wait();
	}

	inline ~Lock() throw ()
	{
	    _sem.post();
	}

    private:
	Semaphore& _sem;
    };

private:
    mutable SysSemaphore _sem;
    static void CreationError(int rc) throw (positix::SynchronizationError);
    static void DestructionError(int rc) throw (positix::SynchronizationError);
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#if defined(POSITIX_SYS_USE_PTHREAD)
#define POSITIX_INLINE inline
#include "sys/pthread.icc"
#undef POSITIX_INLINE
#else
#error Unrecognized low-level multithreading interface
#endif

#ifndef __POSITIX_HAS_RWLOCK__
namespace positix {
typedef Modex RwLock;
}
#endif

#include "undef.hh"

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: multithreading.hh
