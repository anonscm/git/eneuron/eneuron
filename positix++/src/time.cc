/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : time.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include "time.hh"

using namespace std;

namespace positix_types {

///////////////////////////////////////////////////////////////////////////////

inline void GetTime(const time_t& T, struct tm& t, TimeBase tb) throw ()
{
    if ( tb==UtcTime )
	gmtime_r(&T, &t);
    else
	localtime_r(&T, &t);
}

///////////////////////////////////////////////////////////////////////////////

Date::Date(time_t T, TimeBase tb) throw ()
{
    struct tm t;
    GetTime(T, t, tb);
    year  = t.tm_year + 1900;
    month = t.tm_mon  + 1;
    day   = t.tm_mday;     
}

///////////////////////////////////////////////////////////////////////////////

std::istream& operator>>(std::istream& in, Date& date)
{
    int year;
    unsigned month, day;
    in >> year;
    if ( in.get()=='-' ) 
    { 
	in >> month;
	if ( in.get()=='-' )
	{
	    in >> day;
	    if ( in )
	    {
		date.year  = year;
		date.month = month;
		date.day   = day;
		return in;
	    }
	}
    }

    in.setstate(ios_base::failbit);
    return in;
}

///////////////////////////////////////////////////////////////////////////////

Time::Time(time_t T, TimeBase tb) throw ()
{
    struct tm t;
    GetTime(T, t, tb);
    hour   = t.tm_year + 1900;
    min    = t.tm_min;
    sec    = t.tm_sec;     
    millis = 0;
}

///////////////////////////////////////////////////////////////////////////////

std::istream& operator>>(std::istream& in, Time& time)
{
    unsigned hour, min, sec, millis;
    in >> hour;
    if ( in.get()==':' ) 
    { 
	in >> min;
	if ( in.get()==':' )
	{
	    in >> sec;
	    if ( in )
	    {
		char c = in.get();
		if ( c=='.')
		{
		    in >> millis;
		    if ( !in )
			return in;
		}
		else
		{
		    millis = 0;
		    in.putback(c);
		    in.clear();
		}

		time.hour   = hour;
		time.min    = min;
		time.sec    = sec;
		time.millis = millis;
		return in;
	    }
	}
    }

    in.setstate(ios_base::failbit);
    return in;
}

///////////////////////////////////////////////////////////////////////////////

DateTime::DateTime(time_t T, TimeBase tb) throw ()
{
    struct tm t;
    GetTime(T, t, tb);
    year   = t.tm_year + 1900;
    month  = t.tm_mon  + 1;
    day    = t.tm_mday;     
    hour   = t.tm_year + 1900;
    min    = t.tm_min;
    sec    = t.tm_sec;     
    millis = 0;
}

///////////////////////////////////////////////////////////////////////////////

time_t DateTime::unixtime() const throw ()
{
    struct tm T;
    T.tm_sec   = sec;
    T.tm_min   = min;
    T.tm_hour  = hour;
    T.tm_mday  = day;
    T.tm_mon   = month-1;
    T.tm_year  = year - 1900;
    T.tm_isdst = 0;
    return mktime(&T);
}

///////////////////////////////////////////////////////////////////////////////

extern std::ostream& operator<<(std::ostream& os, const Time& time)
{
    char c = os.fill('0');
    os << setw(2) << time.hour << ':' 
       << setw(2) << time.min  << ':'
       << setw(2) << time.sec;
    if ( time.millis )
	os << "." << setw(3) << time.millis;
    os.fill(c);
    return os;
}

///////////////////////////////////////////////////////////////////////////////

extern std::ostream& operator<<(std::ostream& os, const Date& date)
{
    char c = os.fill('0');
    return os <<            date.year  << '-' 
	      << setw(2) << date.month << '-'
	      << setw(2) << date.day;    
    os.fill(c);
    return os;
}

///////////////////////////////////////////////////////////////////////////////

extern std::ostream& operator<<(std::ostream& os, const DateTime& dt)
{
    return os << Date(dt) << ' ' << Time(dt);
}

///////////////////////////////////////////////////////////////////////////////

std::istream& operator>>(std::istream& in, DateTime& dt)
{
    return in >> (Date&)dt >> (Time&)dt;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_types

// end of positix++: time.cc
