/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : bytes.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "bytes.hh"

namespace positix {

///////////////////////////////////////////////////////////////////////////////

void ByteBuf::copyFrom(const ByteBuf& buf)
{
    if ( !buf._buf )
    {
	_used = 0;
    }
    else
    {
	if ( _reserved < buf._used )
	{
	    BytePtr p = PX_NEW_BYTEARRAY(buf._used);
	    delete[] _buf;
	    _buf      = p;
	    _reserved = buf._used;
	}
	CopyBytes(_buf, buf._buf, buf._used);
	_used = buf._used;
    }    
}

///////////////////////////////////////////////////////////////////////////////

ByteBuf& ByteBuf::assign(ConstBytePtr buf, size_t len)
{
    use(len);
    CopyBytes(_buf, buf, len);
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

void ByteBuf::reserve(size_t len)
{
    if ( len > _reserved )
    {
	BytePtr p = PX_NEW_BYTEARRAY(len);
	CopyBytes(p, _buf, _used);
	delete[] _buf;
	_buf      = p;
	_reserved = len;
    }
}

///////////////////////////////////////////////////////////////////////////////

void ByteBuf::use(size_t len)
{
    if ( len > _reserved )
	reserve(len);
    _used = len;
}

///////////////////////////////////////////////////////////////////////////////

void ShallowByteBuffer::reserve(size_t len)
{
    if ( len < _len )
	return;

    BytePtr p = PX_NEW_BYTEARRAY(len);
    if ( _leased )
    {
	delete[] _buf;
	_leased = false;
    }
    _buf = p;
    _len = len;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: bytes.cc
