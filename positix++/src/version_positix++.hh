/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : version_positix++.hh.in
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////
// This file serves as input to version.sh to create version_positix++.hh
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_version_hh__

#include "types.hh"

#define POSITIX_VERSION_LIBCXX_MAJOR 1
#define POSITIX_VERSION_LIBCXX_MINOR 5
#define POSITIX_VERSION_LIBCXX_PATCH 0

namespace positix {

extern positix_types::Version VersionPositixXX() throw ();

} // namespace positix

#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: version_positix++.hh.in
