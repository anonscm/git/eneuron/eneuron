/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : unicode.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_unicode_hh__
#define __positix_unicode_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <iterator>

///////////////////////////////////////////////////////////////////////////////
namespace positix {
///////////////////////////////////////////////////////////////////////////////

typedef std::basic_string<UnicodeChar> UnicodeString;
static const UnicodeChar UnicodeEof = std::char_traits<UnicodeChar>::eof();
static const UnicodeChar UnicodeBom = 0xFEFF;
static const UnicodeChar UnicodeMax = 0x10FFFF;

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar ExtractUCS2BE(std::istream& in);
extern UnicodeChar ExtractUCS2LE(std::istream& in);
extern UnicodeChar ExtractUCS4BE(std::istream& in);
extern UnicodeChar ExtractUCS4LE(std::istream& in);

extern UnicodeChar ExtractUTF8   (std::istream& in);
extern UnicodeChar ExtractUTF8c  (std::istream& in); // extended legal check
extern UnicodeChar ExtractUTF16LE(std::istream& in);
extern UnicodeChar ExtractUTF16BE(std::istream& in);

///////////////////////////////////////////////////////////////////////////////

// UTF-8 representation in a 32-bit word:
// Character Code                  32-bit UTF-8 representation:
//           
// Hex      b987654321a9876543210 1c987654321b987654321a9876543210 
//
// ..00007F 00000000000000aaaaaaa 0000000000000000000000000aaaaaaa
// ..0007FF 0000000000bbbbbaaaaaa 0000000000000000110bbbbb10aaaaaa
// ..00FFFF 00000ccccbbbbbbaaaaaa 000000001110cccc10bbbbbb10aaaaaa
// ..1FFFFF dddccccccbbbbbbaaaaaa 11110ddd10cccccc10bbbbbb10aaaaaa
//
// Notes: D800..DFFF are "surrogate" values for UTF-16 and must not be used
//        as "regular" character code. Hence, this range must not occur
//        when encoding or decoding UTF-8, UCS-2, UCS-4 or UTF-32.
// When en/decoding invalid codes, the result will be zero. That means,
// a zero output for a non-zero input indicates an invalid code.
//
// Unicode only defines characters up to 0x10FFFF not 0x1FFFFF.

extern positix_types::fu32 EncodeUTF8 (UnicodeChar c)         throw ();
extern UnicodeChar         DecodeUTF8 (positix_types::fu32 u) throw ();
extern bool		   IsValidUTF8(positix_types::fu32 u) throw ();

// UTF-16 representation i a 32-bit word:
//
// Hex      b987654321a9876543210 1c987654321b987654321a9876543210 
//
// ..00FFFF 00000aaaaaaaaaaaaaaaa 0000000000000000aaaaaaaaaaaaaaaa
// ..10FFFF --> u-0x10000 -->
// ..  ffff 0yyyyyyyyyyxxxxxxxxxx 110110yyyyyyyyyy110111xxxxxxxxxx

extern positix_types::fu32 EncodeUTF16(UnicodeChar c)          throw ();
extern UnicodeChar         DecodeUTF16 (positix_types::fu32 u) throw ();
extern bool		   IsValidUTF16(positix_types::fu32 u) throw ();

///////////////////////////////////////////////////////////////////////////////

extern UnicodeChar BadUnicodeInsertHandlerThrowError(UnicodeChar c);
extern UnicodeChar BadUnicodeInsertHandlerIgnore(UnicodeChar c);

typedef UnicodeChar (*BadUnicodeCharInsertHandlerFunction)(UnicodeChar c);
extern BadUnicodeCharInsertHandlerFunction TheBadUnicodeCharInsertHandler;

// Assumes legal character:
template <class OutputIterator>
OutputIterator InsertUTF8(OutputIterator it, UnicodeChar c)
{
 retry:
    if ( c <=0x7F )
	*it++ = char(c); // a
    else if ( c <= 0x7ff )
    {
	*it++ = char( (c >> 6  ) | 0xC0 ); // b
	*it++ = char( (c & 0x3F) | 0x80 ); // a
    }
    else if ( c <= 0xFFFF )
    {
	if ( c>=0xD800 && c<=0xDFFF )
	    goto bad;
	*it++ = char( ((c >> 12) & 0x0F ) | 0xE0 ); // c
	*it++ = char( ((c >>  6) & 0x3F ) | 0x80 ); // b
	*it++ = char( ((c      ) & 0x3F ) | 0x80 ); // a
    }
    else if ( c <= 0x10FFFF )
    {
	*it++ = char( ((c >> 18) & 0x07 ) | 0xF0 ); // d
	*it++ = char( ((c >> 12) & 0x3F ) | 0x80 ); // c
	*it++ = char( ((c >>  6) & 0x3F ) | 0x80 ); // b
	*it++ = char( ((c      ) & 0x3F ) | 0x80 ); // a;
    }
    else
    {
    bad:
	c = TheBadUnicodeCharInsertHandler(c);
	if ( c<=UnicodeMax )
	    goto retry;
    }

    return it;
}

///////////////////////////////////////////////////////////////////////////////

inline void AppendUTF8(std::string& s, UnicodeChar c)
{
    InsertUTF8(std::back_inserter(s), c);
}

///////////////////////////////////////////////////////////////////////////////

// Assumes legal character:
template <class OutputIterator>
OutputIterator InsertUTF16BE(OutputIterator it, UnicodeChar c)
{
    if ( c<=0xFFFF )
    {
	*it++ = char((c>>8)&255);
	*it++ = char((c   )&255);
    }
    else
    {
	c -= 0x10000;
	*it++ = char(((c>>24)&255)|0xD8);
	*it++ = char((c>>16)&255);
	*it++ = char(((c>>8)&255)|0xDC);
	*it++ = char(c & 255);
    }
    return it;
};

///////////////////////////////////////////////////////////////////////////////
} // namespace positix
///////////////////////////////////////////////////////////////////////////////

#endif

// end of positix++: unicode.hh
