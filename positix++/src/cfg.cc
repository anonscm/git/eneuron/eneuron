/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : cfg.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include "error.hh"
#include "cfg.hh"
#include "msg.hh"

using namespace std;

static const char* TrueValues [] = {"yes", "true", "enabled", "1"};
static const char* FalseValues[] = {"no", "false", "disabled", "0"};

static bool OneOf(const std::string& str, const char** ss, unsigned n)
throw ()
{
    while ( n>0 )
    {
	if ( positix::CompareCaseInsensitive(str, *ss++)==0 )
	    return true;
	--n;
    }

    return false;
}

namespace positix {

///////////////////////////////////////////////////////////////////////////////

void Configurator::badValue()
{
    syntaxError(Msg(TXT("Invalid value %0 for parameter %1"), _value, _key));
}

///////////////////////////////////////////////////////////////////////////////

void Configurator::getValue(bool& flag)
{
    string str(_value);
    if ( OneOf(str, 
	       TrueValues,
	       sizeof(TrueValues)/sizeof(TrueValues[0])) )
    {
	flag = true;
    }
    else if ( OneOf(str,
		    FalseValues,
		    sizeof(FalseValues)/sizeof(FalseValues[0])) )
    {
	flag = false;
    }
    else
    {
	badValue();
    }
}

///////////////////////////////////////////////////////////////////////////////

FileConfigurator::FileConfigurator(const std::string& filename)
: _tokenizer(filename)
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

bool FileConfigurator::hasNext() const
{
    if ( !_tokenizer.hasNext() )
	return false;
    int line = _tokenizer.line();
    _tokenizer.get(_key);
    if ( !_tokenizer.hasNext() || _tokenizer.line()!=line )
    {
	_tokenizer.syntaxError(
	    Msg(TXT("No value for parameter %0 on line %1"), _key, line));
    }
    _tokenizer.get(_value);
    return true;
}

///////////////////////////////////////////////////////////////////////////////

void FileConfigurator::syntaxError(const TextMessage& msg)
{
    _tokenizer.syntaxError(msg);
}

///////////////////////////////////////////////////////////////////////////////

bool OptionConfigurator::hasNext() const
{
    if ( !_opt.next() )
	return false;
    _key   = _opt.key();
    _value = _opt.value();
    return true;
}

///////////////////////////////////////////////////////////////////////////////

void OptionConfigurator::syntaxError(const TextMessage& msg)
{
    throw Error(Msg(TXT("Invalid option: %0"), msg));
}

///////////////////////////////////////////////////////////////////////////////


} // namespace positix

// end of positix++: cfg.cc
