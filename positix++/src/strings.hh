/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : strings.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_strings_hh__
#define __positix_strings_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <sstream>

namespace positix {

extern const char* const PxNullStr;
extern const std::string PxNullString;
extern const std::wstring PxNullWString;

inline void Wipe(std::string& s) throw ()
{
    std::string::iterator b = s.begin();
    std::string::iterator e = s.end();
    while ( b != e )
	*b++ = 0;
    s.clear();
}

extern void Trim(std::string& s) throw ();
extern void NormalizeWhitespace(std::string& s) throw ();

///////////////////////////////////////////////////////////////////////////////

inline 
void
StrCopy(
    const std::string& src,
    char*              buf,
    size_t             len)
{
    buf[src.copy(buf, len-1)]=0;
}

inline 
void
StrCopy(
    const std::string&     src,
    std::string::size_type pos,
    char*                  buf,
    size_t                 len)
{
    buf[src.copy(buf, len-1, pos)]=0;
}

template <class Char, class Traits, class Alloc>
inline 
std::basic_string<Char,Traits,Alloc> 
CondStringCopy(
    const std::basic_string<Char,Traits,Alloc>& str)
{
    if ( str.length()==str.capacity() )
	return str;
    else
	return std::basic_string<Char,Traits,Alloc>(str.data(), str.length());
}

///////////////////////////////////////////////////////////////////////////////

class ConstStrParam
{
public:
    inline ConstStrParam(const char* s) throw ()
    : _ptr(s ? s : PxNullStr),
      _len(-1)
    {
	// empty
    }

    inline ConstStrParam(const std::string& s) throw ()
    : _ptr(s.c_str()),
      _len(s.length())
    {
	// empty
    }

    inline operator const char* () const throw ()
    {
	return _ptr;
    }

    inline size_t length() const throw ()
    {
	return _len>=0 ? _len : (_len=strlen(_ptr));
    }

private:
    const char*     _ptr;
    mutable ssize_t _len;
};

///////////////////////////////////////////////////////////////////////////////

struct WriteQuotedString
{
    explicit inline WriteQuotedString(
	const std::string& s, 
	bool fcond = false) 
    throw ()
    : str(s),
      cond(fcond)
    {
	// empty
    }

    const std::string& str;
    const bool         cond;
};

struct WriteQuotedChars
{
    explicit inline WriteQuotedChars(
	const char* s,
	bool  fcond = false) 
    throw ()
    : str(s),
      cond(fcond)
    {
	// empty
    }

    const char* str;
    const bool  cond;
};


inline
WriteQuotedString WriteQuoted(const std::string& s, bool fcond = false) 
throw ()
{
    return WriteQuotedString(s, fcond);
}

inline
WriteQuotedChars WriteQuoted(const char* s, bool fcond = false) throw ()
{
    return WriteQuotedChars(s, fcond);
}

extern std::ostream& operator<<(std::ostream& os, const WriteQuotedString& wq);
extern std::ostream& operator<<(std::ostream& os, const WriteQuotedChars&  wq);

///////////////////////////////////////////////////////////////////////////////

extern const char* SkipPrefix(const char* prefix, const char* str) throw ();
extern size_t LengthOfCommonPrefix(const char* a, const char* b) throw ();

///////////////////////////////////////////////////////////////////////////////

template <class Value>
inline std::string ToString(const Value& value)
{
    std::ostringstream s;
    s << value;
    return s.str();
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: strings.hh
