/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : sys.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_sys_hh__
#define __positix_sys_hh__

#define POSITIX_SYS_VALUE(Name_,Value_) \
    struct Sys##Name_ {                 \
        typedef Value_ Value;           \
        Value value;                    \
    };

#define POSITIX_SYS_HANDLE(Name_,Value_,Null_) \
    struct Sys##Name_ {                        \
        typedef Value_ Value;                  \
        static const Value Null = Null_;       \
        inline static Value null() throw () {  \
            return Null_; }                    \
        Value value;                           \
        inline Sys##Name_ () throw ()          \
        : value(Null_) {}                      \
        inline explicit Sys##Name_ (const Value& val) throw () \
        : value(val) {} \
        inline bool valid() const throw () { return value!=Null_; } \
    }; \
    inline bool operator==(Sys##Name_ a, Sys##Name_ b) throw () { \
        return a.value==b.value; } \
    inline bool operator!=(Sys##Name_ a, Sys##Name_ b) throw () { \
        return a.value!=b.value; }


#define POSITIX_SYS_HANDLE_PTR(Name_,Value_) \
    struct Sys##Name_ {                        \
        typedef Value_ Value;                  \
        static const Value_ const Null;        \
        inline static const Value null() throw () {  \
            return 0; }                        \
        Value value;                           \
        inline Sys##Name_ () throw ()          \
        : value(0) {}                          \
        inline explicit Sys##Name_ (const Value& val) throw () \
        : value(val) {} \
        inline bool valid() const throw () { return value!=0; } \
    }; \
    inline bool operator==(Sys##Name_ a, Sys##Name_ b) throw () { \
        return a.value==b.value; } \
    inline bool operator!=(Sys##Name_ a, Sys##Name_ b) throw () { \
        return a.value!=b.value; }


///////////////////////////////////////////////////////////////////////////////
// Linux
#if defined(__linux__)

#include "sys/gnucc.hh"
#include "sys/posix.hh"

///////////////////////////////////////////////////////////////////////////////
// Unrecognized platform
#else

#error Unrecognized platform

#endif

#include <limits.h>
#include <string>

///////////////////////////////////////////////////////////////////////////////

#undef POSITIX_SYS_HANDLE
#undef POSITIX_SYS_VALUE

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: sys.hh
