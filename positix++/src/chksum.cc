/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : chksum.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "chksum.hh"

namespace positix {

ChkSumNone ChkSumNone::Std;

///////////////////////////////////////////////////////////////////////////////

void ChkSumNone::reset() throw ()
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

void ChkSumNone::block(char* /*data*/, size_t /*length*/) throw ()
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

ChkSumVal ChkSumNone::value() const throw ()
{
    return ChkSumVal(0, 0);
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix


// end of positix++: chksum.cc
