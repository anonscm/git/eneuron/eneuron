/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : sockets.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "sockets.hh"
#include "error.hh"
#include "msg.hh"

using namespace std;

namespace positix {

///////////////////////////////////////////////////////////////////////////////

bool IPAddr::tryset(const char* addr) throw ()
{
    if ( inet_aton(addr, &_addr.ip4) )
    {
	_type = AF_INET;
	return true;
    }
    else if ( inet_pton(AF_INET6, addr, &_addr.ip6)>0 )
    {
	_type = AF_INET6;
	return true;
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////

IPAddr& IPAddr::set(const char* addr)
{
    if ( tryset(addr) )
	return *this;

    throw Error(Msg(TXT("Invalid IP address: %0"), addr));
}

///////////////////////////////////////////////////////////////////////////////

IPAddr& IPAddr::setFromLocal(SysSocket sock)
{
    SocketAddr addr;
    addr.setFromLocal(sock);
    return set(addr.sockaddr());
}

///////////////////////////////////////////////////////////////////////////////

IPAddr& IPAddr::setFromPeer(SysSocket sock)
{
    SocketAddr addr;
    addr.setFromPeer(sock);
    return set(addr.sockaddr());
}

///////////////////////////////////////////////////////////////////////////////

IPAddr& IPAddr::set(const struct sockaddr* sa) throw ()
{
    switch ( sa->sa_family )
    {
    default:
	_type = AF_INET;
	_addr.ip4.s_addr = INADDR_NONE;
	return *this;

    case AF_INET:
	_type = AF_INET;
	_addr.ip4 = ((struct sockaddr_in*)sa)->sin_addr;
	return *this;

    case AF_INET6:
	_type = AF_INET6;
	_addr.ip6 = ((struct sockaddr_in6*)sa)->sin6_addr;
	return *this;
    }
}

///////////////////////////////////////////////////////////////////////////////

int IPAddr::compare(const IPAddr& addr) const throw ()
{
    int d = _type - addr._type;
    if (d) return d;
    if ( _type==AF_INET )
    {
	return positix_types::fi32(ntohl(_addr.ip4.s_addr))
	      -positix_types::fi32(ntohl(addr._addr.ip4.s_addr));
    }
    else
    {
	for (unsigned i = 0; i < sizeof(_addr.ip6); ++i)
	{
	    uint8_t i8 = _addr.ip6.s6_addr[i];
	    uint8_t o8 = addr._addr.ip6.s6_addr[i];
	    if (i8!=o8)
		return i8<i8 ? -1 : 1;
	}
	return 0;
    }	    
}

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator << (std::ostream& os, const IPAddr& addr)
{
    char buf[INET6_ADDRSTRLEN];
    if ( inet_ntop(addr.type(), addr.addr(), buf, INET6_ADDRSTRLEN) )
	return os << buf;
    else
	return os << '0';
}

///////////////////////////////////////////////////////////////////////////////

SockAddrInfo::SockAddrInfo(
    const char*            node,
    const char*            service,
    const struct addrinfo* hints)
: _ai(0)
{
    int r = getaddrinfo(node, service, hints, &_ai);
    if ( r!=0 && _ai )
	freeaddrinfo(_ai);
}
 
///////////////////////////////////////////////////////////////////////////////

void SocketAddr::set(
    const char* s,
    int         family,   // PF_xxx
    int         socktype, // SOCK_xxx,
    int         protocol  // 0, IPPROTO_xxx, ...
    )
{
    _socktype = socktype;
    _protocol = protocol;

    switch ( family )
    {
    default:
      throw Error(Msg(TXT("Unknown socket address family %0"), family));

    case PF_UNSPEC:
      // fall through! Try to interpret address as IP address

    case PF_INET:
    case PF_INET6: {
	const char* svc = strchr(s, ':');
	struct addrinfo hints;
	int r;
	struct addrinfo* ai = 0;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family   = family;
	hints.ai_socktype = socktype;
	hints.ai_protocol = protocol;
	if ( svc )
	{
	    string node(s, svc - s);
	    r = getaddrinfo(node.c_str(), svc+1, &hints, &ai);
	}
	else
	{
	    r = getaddrinfo(s, 0, &hints, &ai);
	}

	if ( r!=0 )
	{
	    if (ai) freeaddrinfo(ai);
	    if ( r==EAI_SYSTEM )
	    {
		int e = errno;
		throw SystemError(
		    e, 
		    Msg(TXT("Cannot create socket address for %0"), s));
	    }
	    throw Error(Msg(TXT("Cannot create socket address for %0: %1"),
			    s, gai_strerror(r)));
	}
	if ( ai->ai_addrlen>sizeof(_u.ip4) && ai->ai_addrlen>sizeof(_u.ip6) )
	    throw Error(TXT("IP address doesn't fit"));
	memcpy(&_u.gen, ai->ai_addr, ai->ai_addrlen);
	_socktype = ai->ai_socktype;
	_protocol = ai->ai_protocol;
	return;
    }
	
#ifdef __unix__
    case PF_UNIX: {
	unsigned len = strlen(s);
	if ( len > sizeof(_u.un.sun_path)-1 )
	    throw Error(Msg(TXT("Socket address path too long: %0"), s));
	strcpy(_u.un.sun_path, s);
	return;
    }
#endif

#ifdef __linux__
    case PF_NETLINK:
      throw Error("netlink addresses not supported yet");
#endif
    }
}

///////////////////////////////////////////////////////////////////////////////

SocketAddr::SocketAddr(
    const struct sockaddr& sa,
    int                    socktype,
    int                    protocol)
: _socktype(socktype),
  _protocol(protocol)
{
    setAddr(sa);
}

///////////////////////////////////////////////////////////////////////////////

SocketAddr::SocketAddr(const struct addrinfo& ai)
: _socktype(ai.ai_socktype),
  _protocol(ai.ai_protocol)
{
    setAddr(*ai.ai_addr);
}

///////////////////////////////////////////////////////////////////////////////

void SocketAddr::setAddr(const struct sockaddr& sa)
{
    switch ( sa.sa_family )
    {
    default: 
	throw Error(Msg(TXT("Unsupported socket address family %0"), 
			sa.sa_family));

    case PF_UNSPEC : _u.gen.sa_family = PF_UNSPEC; return;
    case PF_INET   : memcpy(&_u.ip4, &sa, sizeof(&_u.ip4)); return;
    case PF_INET6  : memcpy(&_u.ip6, &sa, sizeof(&_u.ip6)); return;
#ifdef __unix__
    case AF_LOCAL  : memcpy(&_u.un,  &sa, sizeof(&_u.un )); return;
#endif
#ifdef __linux__
    case AF_NETLINK: memcpy(&_u.netlink, &sa, sizeof(&_u.netlink)); return;
#endif
    }
}

///////////////////////////////////////////////////////////////////////////////

socklen_t SocketAddr::addrlen() const throw ()
{
    switch ( _u.gen.sa_family )
    {
    default        : return 0;
    case PF_INET   : return sizeof(_u.ip4);
    case PF_INET6  : return sizeof(_u.ip6);
#ifdef __unix__
    case PF_UNIX   : return sizeof(_u.un);
#endif
#ifdef __linux__
    case PF_NETLINK: return sizeof(_u.netlink);
#endif
    }
}

///////////////////////////////////////////////////////////////////////////////

int SocketAddr::compare(const SocketAddr& a) const throw ()
{
    int d;

    d = _u.gen.sa_family - a._u.gen.sa_family;
    if (d) return d;

    switch ( _u.gen.sa_family )
    {
    default:
	return -1;

    case AF_UNSPEC:
	return 0;

    case AF_INET: 
      {
	u_int32_t iaddr = ntohl(_u.ip4.sin_addr.s_addr);
	u_int32_t oaddr = ntohl(a._u.ip4.sin_addr.s_addr);
	if (iaddr<oaddr) return -1;
	if (iaddr>oaddr) return 1;
	uint16_t iport = ntohs(_u.ip4.sin_port);
	uint16_t oport = ntohs(a._u.ip4.sin_port);
	if (iport<oport) return -1;
	if (iport>oport) return 1;
	return 0;
      }

    case AF_INET6:
      {
	for (unsigned i = 0; i < sizeof(_u.ip6.sin6_addr.s6_addr); ++i)
	{
	    uint8_t i8 = _u.ip6.sin6_addr.s6_addr[i];
	    uint8_t o8 = a._u.ip6.sin6_addr.s6_addr[i];
	    if ( i8!=o8 )
		return (i8<o8) ? -1 : 1;
	}
	uint16_t iport = ntohs(_u.ip6.sin6_port);
	uint16_t oport = ntohs(a._u.ip6.sin6_port);
	if (iport<oport) return -1;
	if (iport>oport) return 1;
	return 0;
      }

#ifdef __unix__
    case AF_UNIX:
	return strcmp(_u.un.sun_path, a._u.un.sun_path);
#endif

#ifdef __linux__
    case AF_NETLINK:
	if (_u.netlink.nl_pid<a._u.netlink.nl_pid) return -1;
	if (_u.netlink.nl_pid>a._u.netlink.nl_pid) return  1;
	return 0;
#endif	
    }
}

///////////////////////////////////////////////////////////////////////////////

SocketAddr& SocketAddr::setFromLocal(SysSocket sock)
{
    socklen_t len = sizeof(_u);
    if ( ::getsockname(sock.value, &_u.gen, &len) == -1 )
    {
	SysLastErr e;
	SystemError(e, Msg(TXT("Cannot get local socket address")));
    }
    _protocol = _u.gen.sa_family;
    _socktype = 0;
    len = sizeof(_protocol);
    getsockopt(sock.value, SOL_SOCKET, SO_TYPE, &_socktype, &len);
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

SocketAddr& SocketAddr::setFromPeer(SysSocket sock)
{
    socklen_t len = sizeof(_u);
    if ( ::getpeername(sock.value, &_u.gen, &len) == -1 )
    {
	SysLastErr e;
	SystemError(e, Msg(TXT("Cannot get remote socket address")));
    }
    _protocol = _u.gen.sa_family;
    _socktype = 0;
    len = sizeof(_protocol);
    getsockopt(sock.value, SOL_SOCKET, SO_TYPE, &_socktype, &len);
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

positix_types::fu16 SocketAddr::port(positix_types::fu16 p) throw ()
{
    positix_types::fu16 old = 0;
    switch ( _u.gen.sa_family )
    {
    case AF_INET : 
	old = ntohs(_u.ip4.sin_port);  
	_u.ip4.sin_port = htons(p);
	break;

    case AF_INET6: 
	old = ntohs(_u.ip6.sin6_port); 
	_u.ip6.sin6_port = htons(p);
	break;
    }
    return old;
}

///////////////////////////////////////////////////////////////////////////////

positix_types::fu16 SocketAddr::port() const throw ()
{
   switch ( _u.gen.sa_family )
   {
   default      : return 0;
   case AF_INET : return ntohs(_u.ip4.sin_port);
   case AF_INET6: return ntohs(_u.ip6.sin6_port);
   }
}

///////////////////////////////////////////////////////////////////////////////

extern std::ostream& operator << (std::ostream& os, const SocketAddr& addr)
{
    switch ( addr._u.gen.sa_family )
    {
    default:
	return os << TXT("<invalid socket address>");

    case AF_UNSPEC:
	return os << TXT("<uninitialized socket address>");

    case AF_LOCAL:
	return os << addr._u.un.sun_path;

    case AF_INET: {	
	u_int32_t iaddr = ntohl(addr._u.ip4.sin_addr.s_addr);
	for(int i = 24;;i-=8)
	{
	    os << ((iaddr>>i) & 0xff);
	    if ( i==0 )
		break;
	    os << '.';
	}
	return os << ':' << ntohs(addr._u.ip4.sin_port);
    }

    case AF_INET6: {
	const uint8_t* p = addr._u.ip6.sin6_addr.s6_addr;
	const uint8_t* e = p+16;
	os << hex;
	for(;;)
	{
	    os << ((unsigned(p[0])<<8)|unsigned(p[1]));
	    if ( (p+=2) == e )
		break;
	    os << ':';
	}
	return os << dec << ntohs(addr._u.ip6.sin6_port);
    }

#ifdef __linux__
    case AF_NETLINK:
	return os << "netlink://" 
		  << addr._u.netlink.nl_pid    << '/'
		  << addr._u.netlink.nl_groups;
#endif
    } // switch
}

///////////////////////////////////////////////////////////////////////////////

Socket& Socket::operator = (SysSocket syssock) throw ()
{
    if ( syssock != _socket )
    {
	close();
	_socket = syssock;
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

Socket::~Socket()
{
    if ( _socket.valid() )
	::close(_socket.value);
}

///////////////////////////////////////////////////////////////////////////////

void Socket::close()
{
    if ( _socket.valid() )
    {
	::close(_socket.value);
	_socket.value = SysSocket::Null;
    }
}

///////////////////////////////////////////////////////////////////////////////

void Socket::setAddr(const SocketAddr& addr)
{
    if ( !_socket.valid() )
    {
	_socket.value = ::socket(addr.family(), 
				 addr.socktype(), 
				 addr.protocol());
	if ( !_socket.valid() )
	{
	    int e = errno;
	    throw SystemError(e, Msg("Cannot create socket for address %0", 
				     addr));
	}
    }
}

///////////////////////////////////////////////////////////////////////////////

void Socket::bind(const SocketAddr& addr, bool fReuseAddr)
{
    setAddr(addr);
    if ( fReuseAddr )
    {
	int one = 1;
	if ( setsockopt(_socket.value,SOL_SOCKET,SO_REUSEADDR,&one,sizeof(one))
	     !=0 )
	{
	    int e = errno;
	    throw SystemError(
		e, 
		Msg(TXT("Cannot set socket option SO_REUSEADDR")));
	}
    }
    if ( ::bind(_socket.value, 
		const_cast<struct sockaddr*>(addr.sockaddr()), 
		addr.addrlen()) 
	 != 0 )
    {
	int e = errno;
	throw SystemError(e, Msg(TXT("Cannot bind to socket to address %0"),
				 addr));
    }
}

///////////////////////////////////////////////////////////////////////////////

void Socket::listen(int backlog)
{
    if ( ::listen(_socket.value, backlog) != 0 )
    {
	int e = errno;
	// TODO: Report socket address if available
	throw SystemError(e, TXT("Cannot listen on socket"));
    }
}

///////////////////////////////////////////////////////////////////////////////

void Socket::connect(const SocketAddr& addr)
{
    setAddr(addr);
    if ( ::connect(_socket.value, 
		const_cast<struct sockaddr*>(addr.sockaddr()), 
		addr.addrlen()) 
	 != 0 )
    {
	int e = errno;
	throw SystemError(e, Msg(TXT("Cannot connect to socket at address %0"),
				 addr));
    }
}

///////////////////////////////////////////////////////////////////////////////

SysSocket Socket::accept(SocketAddr* addr)
{
    socklen_t addrlen = sizeof(addr->_u);
    SysSocket sock;

    sock.value = ::accept(_socket.value, addr ? &addr->_u.gen : 0, &addrlen);

    if ( !sock.valid() )
    {
 	int e = errno;
	// TODO: Report address of this socket if available!
	throw SystemError(e, Msg(TXT("Cannot accept socket connection")));
    }

    return SysSocket(sock);
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

// end of positix++: sockets.cc
