/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : files.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_files_hh__
#define __positix_files_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <iosfwd>
#include <string>
#include "error.hh"
#include "time.hh"

#include "def.hh" 

namespace positix {

///////////////////////////////////////////////////////////////////////////////

extern std::ostream& operator<<(std::ostream& os, const SysFile& fd);

///////////////////////////////////////////////////////////////////////////////

extern std::string CurrentDirectory();
extern bool FileExists(const std::string& filename) throw ();
extern int CompareFilenames(const char* a, const char* b) throw ();

///////////////////////////////////////////////////////////////////////////////

class FileError : public SystemError
{
public:
    FileError(SysFile fd, SysErrCode code, const char* op);
    FileError(SysFile fd, const char* what, const char* op);
};

///////////////////////////////////////////////////////////////////////////////

// Mimicks POSIX interface. Mainly meant as wrapper for Windows using native
// file handles. Uses exceptions instead of error return codes.
class File
{
public:
    typedef SysFileOffset Offset;
    typedef SysFileOffset Size;

    enum Type
    {
	Invalid,
	Unknown,
	Regular,
	Directory,
	SymLink,
	Pipe,
	Socket,
	CharDev,
	BlockDev
    };

    struct Stat : public SysFileStat
    {
	inline Stat() throw ()
	{
	    // empty ==> Undefined values!
	}

	explicit Stat(const File& file);
	explicit Stat(SysFile fd);
	explicit Stat(const char* filename);

	Stat& get(const char* filename);
	Stat& get(const File& file);
	Stat& get(SysFile fd);

	inline Offset size() const throw ()
	{
	    return SysFileStatSize(*this);
	}

	inline mode_t mode() const throw ()
	{
	    return SysFileStatMode(*this);
	}

	inline ino_t inode() const throw ()
	{
	    return SysFileStatInode(*this);
	}

	inline dev_t dev() const throw ()
	{
	    return SysFileStatDev(*this);
	}

	inline nlink_t nlinks() const throw ()
	{
	    return SysFileStatNlinks(*this);
	}

	inline Type type() const throw ()
	{
	    return Type(SysFileStatType(*this));
	}

	inline positix_types::DateTime ctime() const throw ()
	{
	    return SysFileStatCtime(*this);
	}

	inline positix_types::DateTime mtime() const throw ()
	{
	    return SysFileStatMtime(*this);
	}

	inline positix_types::DateTime atime() const throw ()
	{
	    return SysFileStatAtime(*this);
	}
    };

    inline File() throw ()
    {
	// empty
    }

    inline File(SysFile fd)
    : _fd(fd)
    {
	// empty
    }

    inline File(const char* name, int flags)
    : _fd(SysFileOpen(name, flags, 0))
    {
	postOpenTest(name, flags);
    }

    inline File(const char* name, int flags, mode_t mode)
    : _fd(SysFileOpen(name, flags, mode))
    {
	postOpenTest(name, flags);
    }

    inline ~File()
    {
	close();
    }

    inline File& operator=(SysFile fd)
    {
	if ( fd!=_fd )
	{
	    close();
	    _fd = fd;
	}
	return *this;
    }

    inline SysFile get() const throw ()
    {
	return _fd;
    }

    inline SysFile release() throw ()
    {
	SysFile r = _fd;
	_fd.value = SysFile::Null;
	return r;
    }

    void open(const char* name, int flags);
    void open(const char* name, int flags, mode_t mode);
    bool close() throw ();
    size_t read(void* buf, size_t count);
    void readn(void* buf, size_t count);
    size_t write(const void* buf, size_t count);
    void writen(const void* buf, size_t count);
    size_t read(File::Offset offset, void* buf, size_t count);
    void readn(File::Offset offset, void* buf, size_t count);
    size_t write(File::Offset offset, const void* buf, size_t count);
    void writen(File::Offset offset, const void* buf, size_t count);
    File::Offset seek(File::Offset offset, int whence);
    inline File::Offset here()
    {
	return seek(0, SEEK_CUR);
    }
    void resize(Offset newlength);
    bool sync(bool required = false);
    bool datasync(bool required = false);
    void getStat(Stat& stat) const;

    inline bool isOpen() const throw ()
    {
	return _fd.valid();
    }

    inline bool valid() const throw ()
    {
	return _fd.valid();
    }

    /////////////////////////////////////////////////////////////////////
    // Copying/Appending files.
    // Return values:
    //    0: OK
    //   -1: Read error (consult errno)
    //    1: Write error (consult errno)

    int copyTo(
	const char* name,
	int         flags,
	mode_t      mode,
	unsigned    blksize = 0,
	char*       buf     = 0);

    void copyCompletelyTo(
	const char* name,
	int         flags,
	mode_t      mode,
	unsigned    blksize = 0,
	char*       buf     = 0);

    int appendTo(
	SysFile  file,
	unsigned blksize = 0,
	char*    buf     = 0);

    inline int copyTo(
	const std::string& name, 
	int                flags,
	mode_t             mode,
	unsigned           blksize = 0,
	char*              buf     = 0)
    {
	return copyTo(name.c_str(), flags, mode, blksize, buf);
    }

    void copyCompletelyTo(
	const std::string& name, 
	int                flags,
	mode_t             mode,
	unsigned           blksize = 0,
	char*              buf     = 0)
    {
	return copyCompletelyTo(name.c_str(), flags, mode, blksize, buf);
    }

    inline int appendTo(
	File&    file,
	unsigned blksize = 0,
	char*    buf     = 0)
    {
	return appendTo(file.get(), blksize, buf);
    }

    /////////////////////////////////////////////////////////////////////

    static inline bool Exists(const std::string& filename) throw ()
    {
	return FileExists(filename);
    }

private:
    SysFile _fd;
    void postOpenTest(const char* name, int flags);

    POSITIX_CLASS_NOCOPY(File);
};

inline File::Stat::Stat(const File& file)
{
    file.getStat(*this);
}

inline File::Stat::Stat(SysFile fd)
{
    get(fd);
}

inline File::Stat::Stat(const char* filename)
{
    get(filename);
}

inline File::Stat& File::Stat::get(const File& file)
{
    file.getStat(*this);
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

struct File_Stat
{
    File_Stat(const char* name, int flags)
    : file(name, flags),
      stat(file)
    {
	// empty
    }
	
    File_Stat(const char* name, int flags, mode_t mode)
    : file(name, flags, mode),
      stat(file)
    {
	// empty
    }

    File_Stat(SysFile fd)
    : file(fd),
      stat(file)
    {
	// empty
    }

    inline void update()
    {
	stat.get(file);
    }

    File       file;
    File::Stat stat;
};

///////////////////////////////////////////////////////////////////////////////

class PidFile
{
public:
    inline PidFile()
    {
	// empty: No pid file!
    }

    inline explicit PidFile(const std::string& name)
    {
	create(name);
    }

    inline ~PidFile()
    {
	remove();
    }

    void create(const std::string& name);
    void remove();

private:
    std::string _name;
};

///////////////////////////////////////////////////////////////////////////////

class MemoryMap
{
public:
    enum Access
    {
	NoAccess,
	Read,
	Write,
	ReadWrite,
	Execute
    };

    enum Type
    {
	Private,
	Shared
    };

    enum RemapAddressMode
    {
	AddressMayMove,
	AddressMustRemain
    };

    enum SyncTime
    {
	SyncWait,
	SyncSchedule
    };

    enum SyncMode
    {
	KeepOtherMaps,
	InvalidateOtherMaps
    };

    inline MemoryMap() throw ()
    {
	// empty
    }

    inline explicit MemoryMap(const SysMemoryMap& sm) throw ()
    : _map(sm)
    {
	// empty
    }

    inline ~MemoryMap()
    {
	sync(SyncSchedule, InvalidateOtherMaps);
	unmap();
    }

    void map(
	SysFile      file, 
	File::Offset off, 
	size_t       length, 
	Access       acc,
	Type         type,
	void*        start = 0);

    void remap(size_t newlength, RemapAddressMode ram);

    bool trymap(
	SysFile file, 
	File::Offset   off, 
	size_t  length, 
	Access  acc, 
	Type    type,
	void*   start = 0)
    throw ();

    bool tryremap(size_t newlength, RemapAddressMode ram) throw ();

    void sync(SyncTime st, SyncMode sm);
    void unmap();


    inline size_t length() const throw ()
    {
	return _map.length();
    }

    inline operator char* () throw ()
    {
	return _map.ptr;
    }

    inline operator const char* () const throw ()
    {
	return _map.ptr;
    }

    SysMemoryMap get() throw ()
    {
	return _map;
    }

    SysMemoryMap release() throw ()
    {
	SysMemoryMap r = _map;
	_map.ptr = 0;
	return r;
    }

private:
    SysMemoryMap _map;
    POSITIX_CLASS_NOCOPY(MemoryMap);
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#include "undef.hh"

#endif

// end of positix++: files.hh
