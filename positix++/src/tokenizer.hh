/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : tokenizer.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_tokenizer_hh__
#define __positix_tokenizer_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <sstream>
#include <fstream>

#include "text.hh"
#include "def.hh"

namespace positix {

class SimpleTokenizer
{
public:
    static const char* Separators;

    SimpleTokenizer(std::istream& in)
    : _in(in.rdbuf())
    {
	_valid = false;
	_line = _col = 1;
    }

    SimpleTokenizer(std::istream& in, const std::string& filename)
    : _in(in.rdbuf()),
      _filename(filename)
    {
	_valid = false;
	_line = _col = 1;
    }

    explicit SimpleTokenizer(const std::string& filename);

    ~SimpleTokenizer();

    bool hasNext();
    bool next(const char* token);
    SimpleTokenizer& get(std::string& token);

    template <class Value>
    SimpleTokenizer& get(Value& value, const char* typedescr = 0)
    {
	std::string token;
	get(token);
	std::istringstream in(token);
	in >> value;
	if ( in.fail() )
	    getValueFailed(token, typedescr);
	return *this;
    }

    void unexpected();
    void syntaxError(const TextMessage& msg);

    inline const std::string& current() const throw ()
    {
	return _token;
    }

    inline int line() const throw ()
    {
	return _line;
    }

    inline int col() const throw ()
    {
	return _col;
    }

    inline const std::string& filename() const throw ()
    {
	return _filename;
    }

    inline std::string getToken()
    {
	std::string token;
	get(token);
	return token;
    }

private:
    void getValueFailed(const std::string& found, const char* expected = 0);

    bool readToken();

    std::istream _in;
    std::string  _token;
    std::string  _filename;
    std::filebuf _filebuf;
    int          _line;
    int          _col;
    bool         _valid;

    POSITIX_CLASS_NOCOPY(SimpleTokenizer);
};

///////////////////////////////////////////////////////////////////////////////

inline SimpleTokenizer& operator >> (SimpleTokenizer& T, std::string& token)
{
    return T.get(token);
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

///////////////////////////////////////////////////////////////////////////////

#include "undef.hh"

#endif

// end of positix++: tokenizer.hh
