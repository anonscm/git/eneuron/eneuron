/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : log.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_log_hh__
#define __positix_log_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <iostream>
#include <fstream>
#include "multithreading.hh"
#include "time.hh"
#include "text.hh"
#include "def.hh"

namespace positix {

class LogStream;

///////////////////////////////////////////////////////////////////////////////

enum LogLevel
{
    LogEmergency,
    LogAlert,
    LogCritical,
    LogError,
    LogWarning,
    LogNotice,
    LogInfo,
    LogDebug,
    nLogLevels
};

///////////////////////////////////////////////////////////////////////////////

enum LogFacility
{
    LogUser,
    LogDaemon,
    LogMail
};

///////////////////////////////////////////////////////////////////////////////

extern void SysOpenLog(const char* ident, LogFacility facility);
extern void SysLog(LogLevel level, const std::string& msg);
extern void SysCloseLog();

///////////////////////////////////////////////////////////////////////////////

extern void FormatV(
    LogLevel         level,
    LogStream&       out, 
    const char*      fmt, 
    const FormatArg* args, 
    unsigned         n)
throw ();

///////////////////////////////////////////////////////////////////////////////

class LogStream
{
public:
    static const unsigned Tid = 1;

    inline explicit LogStream(std::ostream& os, unsigned mask = 0)
    : _out(os.rdbuf()),
      _syslog(false),
      _nologtime(false),
      _loglevel(nLogLevels),
      _mask(mask)
    {
	// empty
    }

    inline explicit LogStream(unsigned mask = 0)
    : _out(std::cerr.rdbuf()),
      _syslog(false),
      _nologtime(false),
      _loglevel(nLogLevels),
      _mask(mask)
    {
	// empty
    }

    inline ~LogStream()
    {
	close();
    }

    void set(std::ostream& os);
    void setFile(const char* name, unsigned days);
    void syslog(const char* ident, LogFacility facility);

    inline bool usesSyslog() const throw ()
    {
	return _syslog;
    }

    inline void setFile(std::string& name, unsigned days = 0)
    {
	setFile(name.c_str(), days);
    }

    inline const std::string& filename() const throw ()
    {
	return _filename;
    }

    void close();

    LogLevel loglevel(LogLevel newLevel) throw ();
    LogLevel loglevel() throw ();

    inline bool logtime() const throw ()
    {
	return !_nologtime;
    }

    inline void logtime(bool f) throw ()
    {
	_nologtime = !f;
    }

    inline bool tee() const throw ()
    {
	return _tee;
    }

    inline void tee(bool f) throw ()
    {
	_tee = f;
    }

private:
    friend void FormatV(
	LogLevel         level,
	LogStream&       out, 
	const char*      fmt, 
	const FormatArg* args, 
	unsigned         n) throw ();

    FastMutex           _mutex;
    std::ostream	_out;
    std::string		_filename;
    std::filebuf	_filebuf;
    bool		_syslog    : 1;
    bool		_nologtime : 1;
    bool		_tee       : 1;
    LogLevel		_loglevel;
    unsigned		_mask;
    unsigned            _rotateDays;
    positix_types::Date _dateOfLog;

private:
    POSITIX_CLASS_NOCOPY(LogStream);
};

///////////////////////////////////////////////////////////////////////////////

#define _POSITIX_FORMAT_            Format
#define _POSITIX_FMT_               const char*
#define _POSITIX_FMT0_              "%0"
#define _POSITIX_FORMARG_           FormatArg
#define _POSITIX_FORMARG_CONS_(V,v) FormatArg(&v, &_PxFormatter<V>)
#define _POSITIX_FORMAT_FUN_        FormatV
#define _POSITIX_FORMAT_RESULT_     void
#define _POSITIX_FORMAT_PARAM_      LogLevel level, LogStream& os,
#define _POSITIX_FORMAT_ARG_        level, os,
#include "format.hh"

} // namespace positix

#include "undef.hh"

#endif

// end of positix++: log.hh
