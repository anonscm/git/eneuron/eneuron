/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : cmdopt.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include "error.hh"
#include "cmdopt.hh"
#include "text.hh"
#include "msg.hh"

using namespace std;

namespace positix {

///////////////////////////////////////////////////////////////////////////////

bool PxOptionParser::next() throw ()
{
    char** end = _argv + _argc;

    if ( _cur < end )
    {
	++_cur;
	if ( _cur < end )
	{
	    _opt = *_cur;

	    if ( _opt && _opt[0]=='-' )
	    {
		if ( _opt[1]=='-' && _opt[2]==0 )
		{
		    ++_cur;
		    return false;
		}

		do
		    ++_opt;
		while ( *_opt && *_opt=='-' );

		if ( *_opt )
		{
		    _value = strchr(_opt, '=');
		    if ( _value )
			++_value;
		    return true;
		}
	    }
	}
    }

    _opt = _value = 0;
    return false;
}

///////////////////////////////////////////////////////////////////////////////

std::string PxOptionParser::key() const
{
    if ( !_opt )
	return std::string();
    else if ( !_value )
	return _opt;
    else
	return std::string(_opt, 0, (_value - _opt) - 1);
}

///////////////////////////////////////////////////////////////////////////////

bool PxOptionParser::iskey(const char* key) const throw ()
{
    if ( !_opt )
	return false;

    if ( !_value )
	return 0==strcmp(_opt, key);
    else
	return 0==strncmp(_opt, key, (_value - _opt) - 1);
}

///////////////////////////////////////////////////////////////////////////////

const char* PxOptionParser::removeCurrent() throw ()
{
    if ( _cur == _argv + _argc )
        return 0;
    int i = _cur - _argv;
    if ( i < _argc-1 )
        memmove(_argv + i, _argv + (i+1), sizeof(char*) * (_argc-i-1));
    --_argc;
    return _value;
}

///////////////////////////////////////////////////////////////////////////////

void PxOptionParser::removeAll() throw ()
{
    int n = (_argv + _argc) - _cur;
    memmove(_argv + 1, _cur, n*sizeof(char*));
    _argc -= (_cur - _argv) - 1;    
}

///////////////////////////////////////////////////////////////////////////////

std::string PxLocateCfgFile(
    const char* vendor,
    const char* filename,
    const char* ev,
    int&        argc,
    char*       argv[],
    bool        raiseException,
    bool        lookAtHome,
    std::string* origin)
{
    PxOptionParser opt(argc, argv);
    const char* value = 0;

    // (1) Check for command line argument -cfg=FILE
    while ( opt.next() )
    {
	if ( opt.iskey("cfg") || opt.iskey("conf") )
	{
	    if ( !value )
		value = opt.removeCurrent();
	    else
		throw Error(Msg(TXT("Only one -cfg or -conf option allowed")));
	}
    }
    if ( value )
    {
	if ( origin )
	    *origin = "-cfg";
	return string(value);
    }

    // (2) Check for environment variables in ev (separated by semicolon)
    for(;;)
    {
	const char* e = strchr(ev, ';');
	if ( e )
	{
	    string name(ev, e - ev);
	    value = getenv(name.c_str());
	    if ( value )
	    {
		if ( origin )
		    *origin = name;
		return string(value);
	    }
	    ev = e + 1;
	}
	else
	{
	    value = getenv(ev);
	    if ( value )
	    {
		if ( origin )
		    *origin = ev;
		return string(value);
	    }
	    break;
	}
    }

    std::string cfg = PxLocateCfgFile(vendor, filename, lookAtHome);
    
    if ( cfg.empty() && raiseException )
    {
	// File not found but required!
	throw Error(
	    Msg(TXT("Configuration file %0 not found in its"
		    " standard locations"),
		filename));
    }

    if ( origin )
	*origin = TXT("standard location");

    return cfg;
}

///////////////////////////////////////////////////////////////////////////////

bool PxBoolOpt(const char* value)
{
    if ( !value || *value == 0 )
	return true;

    if ( 0==strcasecmp(value, "true")  || 0==strcmp(value, "1") )
	return true;

    if ( 0==strcasecmp(value, "false")  || 0==strcmp(value, "0") )
	return false;

    throw positix::Error(Msg(TXT("Invalid flag value %0"), value));
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

// end of positix++: cmdopt.cc
