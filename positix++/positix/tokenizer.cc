/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : tokenizer.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "error.hh"
#include "tokenizer.hh"
#include "msg.hh"

using namespace std;

namespace positix {

///////////////////////////////////////////////////////////////////////////////

const char* SimpleTokenizer::Separators = "()[]{}<>,;";

///////////////////////////////////////////////////////////////////////////////

SimpleTokenizer::SimpleTokenizer(const std::string& filename)
: _in(0),
  _filename(filename)
{
    _valid = false;
    _line = _col = 1;

    if ( !_filebuf.open(filename.c_str(), ios::in) )
    {
	int e = errno;
	throw SystemError(e, Msg(TXT("Cannot open file %0"), _filename));
    }
    _in.rdbuf(&_filebuf);
}

///////////////////////////////////////////////////////////////////////////////

SimpleTokenizer::~SimpleTokenizer()
{
    if ( _filebuf.is_open() )
    {
	_filebuf.close();
	_in.rdbuf(0);
    }
}

///////////////////////////////////////////////////////////////////////////////

bool SimpleTokenizer::readToken()
{
    _token.erase();

    while ( !_in.eof() )
    {
	istream::int_type c = _in.get();

	if ( c == istream::traits_type::eof() )
	    break;

	if ( c == '\n' )
	{
	    ++_line;
	    _col = 1;
	    continue;
	}

	if ( c == '#' )
	{
	    // Skip comment
	    while ( !_in.eof() && _in.get()!='\n' )
		/*empty*/;
	    ++_line;
	    _col = 1;
	    continue;
	}

	++_col;

	if ( isspace(c) )
	{
	    if ( c == '\t' )
		_col += 7;
	    continue;
	}

	if ( strchr(Separators, c) )
	{
	    _token = char(c);
	    return true;
	}

	if ( c == '\"' )
	{
	    int col = _col;
	    // Read quoted string
	    for(;;)
	    {		
		if ( _in.eof() || (c=_in.get())=='\n' 
		     || c == istream::traits_type::eof() )
		{
		    syntaxError(
			Msg(TXT("%0:%1:%2: Unterminated quoted string"),
			    _filename,
			    col,
			    _line));
		}

		++_col;

		if ( c == '\"' )
		{
		    if ( _in.eof() )
			return true;
		    
		    c = _in.get();

		    if ( c == istream::traits_type::eof() )
			return true;

		    if ( c != '\"' )
		    {
			_in.putback(c);
			return true;
		    }
		}
		else if ( c>=0 && c < ' ' )
		{
		    c = ' ';
		}

		_token += c;
	    }		    	    
	}
	else
	{
	    // Read "standard" token
	    _token = char(c);

	    while ( !_in.eof() )
	    {
		c = _in.get();

		if ( c == istream::traits_type::eof() )
		    return true;

		if ( isspace(c) || strchr(Separators, c) )
		{
		    _in.putback(c);
		    return true;
		}

		_token += c;
		++_col;
	    }
	}
    }

    return !_token.empty();
}

///////////////////////////////////////////////////////////////////////////////

bool SimpleTokenizer::hasNext()
{
    if ( !_valid )
	_valid = readToken();
    return _valid;
}

///////////////////////////////////////////////////////////////////////////////

SimpleTokenizer& SimpleTokenizer::get(std::string& token)
{
    if ( !_valid )
	syntaxError(TXT("Unexpected end of file"));
    token = _token;
    _valid = readToken();
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

bool SimpleTokenizer::next(const char* str)
{
    if ( _token == str )
    {
	_valid = readToken();
	return true;
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////

void SimpleTokenizer::unexpected()
{
    throw SyntaxError(Msg(TXT("%0:%1:%2: Unexpected token %3"),
			  _filename,
			  _line,
			  _col,
			  _token));
}

///////////////////////////////////////////////////////////////////////////////

void SimpleTokenizer::syntaxError(const TextMessage& msg)
{
    throw SyntaxError(Msg(TXT("%0:%1:%2: %3"),
			  _filename,
			  _line,
			  _col,
			  msg));
}

///////////////////////////////////////////////////////////////////////////////

void SimpleTokenizer::getValueFailed(
    const std::string& found,
    const char*        expected)
{
    if ( !expected )
	unexpected();

    throw SyntaxError(Msg(TXT("%0:%1:%2: Expected %3 but found %4"),
			  _filename,
			  _line,
			  _col,
			  expected,
			  found));
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

///////////////////////////////////////////////////////////////////////////////

// end of positix++: tokenizer.hh
