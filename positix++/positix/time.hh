/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : time.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_time_hh__
#define __positix_time_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <iosfwd>

#include "types.hh"

namespace positix_types {

///////////////////////////////////////////////////////////////////////////////

enum TimeBase
{
    UtcTime,
    LocalTime
};

///////////////////////////////////////////////////////////////////////////////

// represent value * pow(10, exponent) seconds
struct Seconds
{
    inline Seconds() throw ()
    {
	value    = 0;
	exponent = 0;
    }

    inline Seconds(fi32 v, fi32 e)
    {
	value    = v;
	exponent = e;
    }

    fi32 value;
    fi32 exponent;
};

///////////////////////////////////////////////////////////////////////////////

struct Date
{
    inline Date() throw ()
    {
	year  = 0;
	month = 0;
	day   = 0;
    }

    inline Date(Undefined) throw ()
    {
	// empty
    }

    inline Date(int y, unsigned m, unsigned d) throw ()
    {
	year  = y;
	month = m;
	day   = d;
    }

    Date(time_t T, TimeBase tb = UtcTime) throw ();

    int      year  : 23; // -4'194'304 ... 4'194'305
    unsigned month :  4;
    unsigned day   :  5;
};

extern std::ostream& operator<<(std::ostream& os, const Date& date);
extern std::istream& operator>>(std::istream& is, Date& date);
extern Date Today() throw ();

inline bool operator<(const Date& a, const Date& b) throw ()
{
    return (a.year<b.year)
	|| (a.year==b.year 
	    && (a.month<b.month
		|| (a.month==b.month
		    && a.day<b.day)));
}

inline bool operator==(const Date& a, const Date& b) throw ()
{
    return a.day   == b.day
	&& a.month == b.month
	&& a.year  == b.year;
}

inline bool operator!=(const Date& a, const Date& b) throw ()
{
    return a.day   != b.day
	|| a.month != b.month
	|| a.year  != b.year;
}

///////////////////////////////////////////////////////////////////////////////

struct Time
{
    inline Time() throw ()
    {
	hour   = 0;
	min    = 0;
	sec    = 0;
	millis = 0;
    }

    inline Time(Undefined) throw ()
    {
	// empty
    }

    inline Time(unsigned h, unsigned m, unsigned s, unsigned ms)
    {
	hour   = h;
	min    = m;
	sec    = s;
	millis = ms;
    }

    Time(time_t T, TimeBase tb = UtcTime) throw ();

    unsigned hour   :  5;
    unsigned min    :  6;
    unsigned sec    :  6;
    unsigned millis : 15;
};

extern std::ostream& operator<<(std::ostream& os, const Time& time);
extern std::istream& operator>>(std::istream& is, Time& time);
extern Time Clock() throw ();

inline bool operator<(const Time& a, const Time& b) throw ()
{
    return (a.hour<b.hour)
	|| (a.hour==b.hour 
	    && (a.min<b.min
		|| (a.min==b.min
		    && (a.sec<b.sec
			|| (a.sec==b.sec
			    && a.millis<b.millis)))));
       
}

inline bool operator==(const Time& a, const Time& b) throw ()
{
    return a.millis == b.millis
	&& a.sec    == b.sec
	&& a.min    == b.min
	&& a.hour   == b.hour;
}

inline bool operator!=(const Time& a, const Time& b) throw ()
{
    return a.millis != b.millis
	|| a.sec    != b.sec
	|| a.min    != b.min
	|| a.hour   != b.hour;
}

///////////////////////////////////////////////////////////////////////////////

struct DateTime : public Date, public Time
{
    inline DateTime() throw ()
    : Date(),
      Time()
    {
	// empty
    }

    inline DateTime(Undefined x) throw ()
    : Date(x),
      Time(x)
    {
	// empty
    }

    inline DateTime(
	int      Y, unsigned M, unsigned D,
	unsigned h, unsigned m, unsigned s, unsigned ms) throw ()
    : Date(Y, M, D),
      Time(h, m, s, ms)
    {
	// empty
    }
	
    inline DateTime(const Date& date, const Time& time) throw ()
    : Date(date),
      Time(time)
    {
	// empty
    }

    inline DateTime(const Date& date) throw ()
    : Date(date)
    {
	// empty
    }

    inline DateTime(const Time& time) throw ()
    : Date(Today()),
      Time(time)
    {
	// empty
    }

    DateTime(time_t T, TimeBase tb = UtcTime) throw ();

    time_t unixtime() const throw ();
};

extern std::ostream& operator<<(std::ostream& os, const DateTime& dt);
extern std::istream& operator>>(std::istream& is, DateTime& dt);
extern DateTime Now() throw ();

inline bool operator < (const DateTime& a, const DateTime& b) throw ()
{
    return Date(a) < Date(b) || (Date(a)==Date(b) && Time(a) < Time(b));
}

inline bool operator==(const DateTime& a, const DateTime& b) throw ()
{
    return Time(a)==Time(b) && Date(a)==Date(b);
}

inline bool operator!=(const DateTime& a, const DateTime& b) throw ()
{
    return Time(a)!=Time(b) || Date(a)!=Date(b);
}

///////////////////////////////////////////////////////////////////////////////

inline DateTime Epoch() throw ()
{
    return DateTime(1970,1,1, 0,0,0,0);
}

///////////////////////////////////////////////////////////////////////////////

class TimeStamp
{
public:
    // If TimeStamp zero denotes January 1st, 1970 A.D., 0 h, 
    // then the largest TimeStamp (using 64 bits) can represent times up to the
    // year 2553 with each year being a leap year:
    // 1 year <= 366 days
    //         = 366 * 24 hours
    //         = 366 * 24 * 60 minutes
    //         = 366 * 24 * 60 * 60 seconds
    //         = 366 * 24 * 60 * 60 * 10^9 nanoseconds
    //         = 31622400000000000 nanoseconds
    // 2^64 / 31622400000000000 = 583.34421402896527828374
    // 1970 + 583 = 2553

    typedef positix_types::fu64 Value;

    inline TimeStamp() throw ()
    {
	_value = 0;
    }

    inline explicit TimeStamp(Value value) throw ()
    : _value(value)
    {
	// empty
    }

    inline operator Value() const throw ()
    {
	return _value;
    }

    inline static Value Resolution() throw ()
    {
	return GetTimeResolution();
    }

    inline static Value Now() throw ()
    {
	return GetNanoTime();
    }

private:
    Value _value;
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_types

#endif

// end of positix++: time.hh
