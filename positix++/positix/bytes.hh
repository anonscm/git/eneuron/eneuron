/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : bytes.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_bytes_hh__
#define __positix_bytes_hh__

#include "sys.hh"
#include "sys/bytes.h"
#include "arrays.hh"

namespace positix {

///////////////////////////////////////////////////////////////////////////////

#if CHAR_BIT==8
typedef unsigned char*       BytePtr;
typedef const unsigned char* ConstBytePtr;
#define PX_BYTE_ARRAY(name, len) unsigned char name[len]
#define PX_NEW_BYTEARRAY(len) (new unsigned char[len])
#define PX_NEW_BYTEARRAY_NOTHROW(len) (new (nothrow) unsigned char[len])
typedef AutoArrayPtr<unsigned char> AutoByteArrayPtr;
#else
#error This release only supports platforms with CHAR_BIT==8
#endif

inline void CopyBytes(BytePtr dst, ConstBytePtr src, size_t len) throw ()
{
    ::memcpy(dst, src, len);
}

inline void ZeroBytes(BytePtr p, size_t len) throw ()
{
    ::memset(p, 0, len);
}

inline void SetBytes(BytePtr p, unsigned char c, size_t len) throw ()
{
    ::memset(p, c, len);
}

class ByteBuf
{
public:
    inline ByteBuf() throw ()
    : _buf(0),
      _reserved(0),
      _used(0)
    {
	// empty
    }

    inline explicit ByteBuf(size_t len)
    : _buf(0),
      _reserved(0),
      _used(0)
    {
	use(len);
    }

    inline explicit ByteBuf(size_t used, size_t reserved)
    : _buf(0),
      _reserved(0),
      _used(0)
    {
	if ( used>reserved )
	    reserved = used;
	reserve(reserved);
	use(used);
    }

    inline ByteBuf(const ByteBuf& buf)
    : _buf(0),
      _reserved(0),
      _used(0)
    {
	copyFrom(buf);
    }

    inline ~ByteBuf() throw ()
    {
	delete[] _buf;
    }

    inline ByteBuf& operator=(const ByteBuf& buf)
    {
	if ( &buf!=this )
	    copyFrom(buf);
	return *this;
    }

    ByteBuf& assign(ConstBytePtr buf, size_t len);

    inline size_t size() const throw ()
    {
	return _used;
    }

    inline size_t used() const throw ()
    {
	return _used;
    }

    inline size_t reserved() const throw ()
    {
	return _used;
    }

    inline operator BytePtr     ()       throw () { return _buf; }
    inline operator ConstBytePtr() const throw () { return _buf; }

    void use(size_t u);
    void reserve(size_t r);

    inline void destroy() throw ()
    {
	delete[] _buf;
	_buf = 0;
	_reserved = _used = 0;
    }

private:
    BytePtr _buf;
    size_t  _reserved;
    size_t  _used;

    void copyFrom(const ByteBuf& buf);
};

class ShallowByteBuffer
{
public:
    inline ShallowByteBuffer() throw ()
    {
	_leased = false;
	_buf    = 0;
	_len    = 0;
    }

    inline explicit ShallowByteBuffer(size_t len)
    {
	_leased = false;
	_buf    = 0;
	_len    = 0;
	reserve(len);
    }

    inline ShallowByteBuffer(size_t len, BytePtr buf)
    {
	if ( buf )
	{
	    _leased = true;
	    _buf    = buf;
	    _len    = len;
	}
	else
	{
	    _leased = false;
	    _buf    = 0;
	    _len    = 0;
	    reserve(len);
	}
    }

    inline ShallowByteBuffer(const ShallowByteBuffer& bb) throw ()
    : _buf(bb._buf),
      _len(bb._len),
      _leased(true)
    {
	// empty
    }

    inline ShallowByteBuffer& operator=(const ShallowByteBuffer& bb) throw ()
    {
	if ( bb != *this )
	{
	    if ( !_leased )
		delete[] _buf;
	    _buf    = bb._buf;
	    _len    = bb._len;
	    _leased = true;
	}

	return *this;
    }
    
    inline ~ShallowByteBuffer() throw ()
    {
	if ( !_leased )
	    delete[] _buf;
    }

    inline BytePtr      buf()            throw () { return _buf; }
    inline ConstBytePtr buf()      const throw () { return _buf; }
    inline size_t       size()     const throw () { return _len; }
    inline bool         leased()   const throw () { return _leased; }

    inline operator BytePtr     ()       throw () { return _buf; }
    inline operator ConstBytePtr() const throw () { return _buf; }

    void reserve(size_t len);

private:
    BytePtr _buf;
    size_t  _len;
    bool    _leased;
};

///////////////////////////////////////////////////////////////////////////////

#if POSITIX_CPU_BYTE_ORDER==4321

inline positix_types::fu16 GetBE16(ConstBytePtr p) throw ()
{
    return *reinterpret_cast<uint16_t*>(p)
}

inline positix_types::fu32 GetBE32(ConstBytePtr p) throw ()
{
    return *reinterpret_cast<uint32_t*>(p)
}

inline positix_types::fu64 GetBE64(ConstBytePtr p) throw ()
{
    return *reinterpret_cast<uint64_t*>(p)
}

inline void SetBE16(ConstBytePtr p, positix_types::fu16 val) throw ()
{
    *reinterpret_cast<uint16_t*>(p) = uint16_t(val);
}

inline void SetBE32(ConstBytePtr p, positix_types::fu32 val) throw ()
{
    *reinterpret_cast<uint32_t*>(p) = uint32_t(val);
}

inline void SetBE64(ConstBytePtr p, positix_types::fu64 val) throw ()
{
    *reinterpret_cast<uint64_t*>(p) = uint64_t(val);
}

inline positix_types::fu16 GetLE16(ConstBytePtr p) throw ()
{
    return bswap_16(*reinterpret_cast<uint16_t*>(p));
}

inline positix_types::fu32 GetLE32(ConstBytePtr p) throw ()
{
    return bswap_32(*reinterpret_cast<uint32_t*>(p));
}

inline positix_types::fu64 GetLE64(ConstBytePtr p) throw ()
{
    return bswap_64(*reinterpret_cast<uint64_t*>(p));
}

inline void SetLE16(ConstBytePtr p, positix_types::fu16 val) throw ()
{
    *reinterpret_cast<uint16_t*>(p) = bswap_16(uint16_t(val));
}

inline void SetLE32(ConstBytePtr p, positix_types::fu32 val) throw ()
{
    *reinterpret_cast<uint32_t*>(p) = bswap_32(uint32_t(val));
}

inline void SetLE64(ConstBytePtr p, positix_types::fu64 val) throw ()
{
    *reinterpret_cast<uint64_t*>(p) = bswap_64(uint64_t(val));
}


#elif POSITIX_CPU_BYTE_ORDER==1234


inline positix_types::fu16 GetBE16(ConstBytePtr p) throw ()
{
    return bswap_16(*reinterpret_cast<const uint16_t*>(p));
}

inline positix_types::fu32 GetBE32(ConstBytePtr p) throw ()
{
    return bswap_32(*reinterpret_cast<const uint32_t*>(p));
}

inline positix_types::fu64 GetBE64(ConstBytePtr p) throw ()
{
    return bswap_64(*reinterpret_cast<const uint64_t*>(p));
}

inline void SetBE16(BytePtr p, positix_types::fu16 val) throw ()
{
    *reinterpret_cast<uint16_t*>(p) = bswap_16(uint16_t(val));
}

inline void SetBE32(BytePtr p, positix_types::fu32 val) throw ()
{
    *reinterpret_cast<uint32_t*>(p) = bswap_32(uint32_t(val));
}

inline void SetBE64(BytePtr p, positix_types::fu64 val) throw ()
{
    *reinterpret_cast<uint64_t*>(p) = bswap_64(uint64_t(val));
}

inline positix_types::fu16 GetLE16(ConstBytePtr p) throw ()
{
    return *reinterpret_cast<const uint16_t*>(p);
}

inline positix_types::fu32 GetLE32(ConstBytePtr p) throw ()
{
    return *reinterpret_cast<const uint32_t*>(p);
}

inline positix_types::fu64 GetLE64(ConstBytePtr p) throw ()
{
    return *reinterpret_cast<const uint64_t*>(p);
}

inline void SetLE16(BytePtr p, positix_types::fu16 val) throw ()
{
    *reinterpret_cast<uint16_t*>(p) = uint16_t(val);
}

inline void SetLE32(BytePtr p, positix_types::fu32 val) throw ()
{
    *reinterpret_cast<uint32_t*>(p) = uint32_t(val);
}

inline void SetLE64(BytePtr p, positix_types::fu64 val) throw ()
{
    *reinterpret_cast<uint64_t*>(p) = uint64_t(val);
}

#else

#warning Using inefficient functions for GetLE* and friends

inline positix_types::fu16 GetLE16(ConstBytePtr p) throw ()
{
    return  (positix_types::fu16(p[0])   )
	|   (positix_types::fu16(p[1])<<8);
}

inline void SetBE16(BytePtr p, positix_types::fu16 u) throw ()
{
    p[0] = (u>>8)&0xff;
    p[1] = (u   )&0xff;
}

inline void SetLE16(BytePtr p, positix_types::fu16 u) throw ()
{
    p[0] = (u   ) & 0xff;
    p[1] = (u>>8) & 0xff;
}

inline positix_types::fu32 GetBE32(ConstBytePtr p) throw ()
{
    return (positix_types::fu32(p[0])<<24) 
	|  (positix_types::fu32(p[1])<<16) 
	|  (positix_types::fu32(p[2])<< 8)
	|  (positix_types::fu32(p[3]));
}

inline positix_types::fu32 GetLE32(ConstBytePtr p) throw ()
{
    return (positix_types::fu32(p[0])) 
	|  (positix_types::fu32(p[1])<< 8) 
	|  (positix_types::fu32(p[2])<<16)
	|  (positix_types::fu32(p[3])<<24);
}

inline void SetBE32(BytePtr p, positix_types::fu32 u) throw ()
{
    p[0] = (u>>24)&0xff;
    p[1] = (u>>16)&0xff;
    p[2] = (u>> 8)&0xff;
    p[3] = (u    )&0xff;
}

inline void SetLE32(BytePtr p, positix_types::fu32 u) throw ()
{
    p[0] = (u    )&0xff;
    p[1] = (u>> 8)&0xff;
    p[2] = (u>>16)&0xff;
    p[3] = (u>>24)&0xff;
}

inline positix_types::fu64 GetBE64(ConstBytePtr p) throw ()
{
    return (positix_types::fu64(p[0])<<56) 
	|  (positix_types::fu64(p[1])<<48) 
	|  (positix_types::fu64(p[2])<<40)
	|  (positix_types::fu64(p[3])<<32)
	|  (positix_types::fu64(p[4])<<24)
	|  (positix_types::fu64(p[5])<<16)
	|  (positix_types::fu64(p[6])<< 8)
	|  (positix_types::fu64(p[7]));
}

inline positix_types::fu64 GetLE64(ConstBytePtr p) throw ()
{
    return (positix_types::fu64(p[0])    ) 
	|  (positix_types::fu64(p[1])<< 8) 
	|  (positix_types::fu64(p[2])<<16)
	|  (positix_types::fu64(p[3])<<24)
	|  (positix_types::fu64(p[4])<<32)
	|  (positix_types::fu64(p[5])<<40)
	|  (positix_types::fu64(p[6])<<48)
	|  (positix_types::fu64(p[7])<<56);
}

inline void SetBE64(BytePtr p, positix_types::fu64 u) throw ()
{
    p[0] = (u>>56)&0xff;
    p[1] = (u>>48)&0xff;
    p[2] = (u>>40)&0xff;
    p[3] = (u>>32)&0xff;
    p[4] = (u>>24)&0xff;
    p[5] = (u>>16)&0xff;
    p[6] = (u>> 8)&0xff;
    p[7] = (u    )&0xff;
}

inline void SetLE64(BytePtr p, positix_types::fu64 u) throw ()
{
    p[0] = (u    )&0xff;
    p[1] = (u>> 8)&0xff;
    p[2] = (u>>16)&0xff;
    p[3] = (u>>24)&0xff;
    p[4] = (u>>32)&0xff;
    p[5] = (u>>40)&0xff;
    p[6] = (u>>48)&0xff;
    p[7] = (u>>56)&0xff;
}

#endif

///////////////////////////////////////////////////////////////////////////////

inline ConstBytePtr GetLE64p(ConstBytePtr p, positix_types::fu64& u) throw ()
{
    u = GetLE64(p);
    return p+8;
}

inline ConstBytePtr GetBE64p(ConstBytePtr p, positix_types::fu64& u) throw ()
{
    u = GetBE64(p);
    return p+8;
}

inline ConstBytePtr GetLE32p(ConstBytePtr p, positix_types::fu32& u) throw ()
{
    u = GetLE32(p);
    return p+4;
}

inline ConstBytePtr GetBE32p(ConstBytePtr p, positix_types::fu32& u) throw ()
{
    u = GetBE32(p);
    return p+4;
}

inline ConstBytePtr GetLE16p(ConstBytePtr p, positix_types::fu16& u) throw ()
{
    u = GetLE16(p);
    return p+2;
}

inline ConstBytePtr GetBE16p(ConstBytePtr p, positix_types::fu16& u) throw ()
{
    u = GetBE16(p);
    return p+2;
}

///////////////////////////////////////////////////////////////////////////////

inline BytePtr SetLE64p(BytePtr p, positix_types::fu64 u) throw ()
{
    SetLE64(p, u);
    return p+8;
}

inline BytePtr SetBE64p(BytePtr p, positix_types::fu64 u) throw ()
{
    SetBE64(p, u);
    return p+8;
}

inline BytePtr SetLE32p(BytePtr p, positix_types::fu32 u) throw ()
{
    SetLE32(p, u);
    return p+4;
}

inline BytePtr SetBE32p(BytePtr p, positix_types::fu32 u) throw ()
{
    SetBE32(p, u);
    return p+4;
}

inline BytePtr SetLE16p(BytePtr p, positix_types::fu16 u) throw ()
{
    SetLE16(p, u);
    return p+2;
}

inline BytePtr SetBE16p(BytePtr p, positix_types::fu16 u) throw ()
{
    SetBE16(p, u);
    return p+2;
}

///////////////////////////////////////////////////////////////////////////////

inline positix_types::fu32 GetBE24(ConstBytePtr p) throw ()
{
    return (positix_types::fu32(p[0])<<16) 
	|  (positix_types::fu32(p[1])<< 8)
	|  (positix_types::fu32(p[2]));
}

inline positix_types::fu32 GetLE24(ConstBytePtr p) throw ()
{
    return (positix_types::fu32(p[0])) 
	|  (positix_types::fu32(p[1])<< 8)
	|  (positix_types::fu32(p[2])<<16);
}

inline void SetBE24(BytePtr p, positix_types::fu32 u) throw ()
{
    p[0] = (u>>16)&0xff;
    p[1] = (u>> 8)&0xff;
    p[2] = u & 0xff;
}

inline void SetLE24(BytePtr p, positix_types::fu32 u) throw ()
{
    p[0] = (u    )&0xff;
    p[1] = (u>> 8)&0xff;
    p[2] = (u>>16)&0xff;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: arrays.hh
