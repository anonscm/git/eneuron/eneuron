/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : log.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

#include "msg.hh"
#include "log.hh"

using namespace std;
using namespace positix_types;

namespace positix {

///////////////////////////////////////////////////////////////////////////////

LogLevel LogStream::loglevel(LogLevel newLevel) throw ()
{
    Mutex::Lock x(_mutex);
    LogLevel oldLevel = _loglevel;
    _loglevel = newLevel;
    return oldLevel;
}

///////////////////////////////////////////////////////////////////////////////

LogLevel LogStream::loglevel() throw ()
{
    Mutex::Lock x(_mutex);
    return _loglevel;
}

///////////////////////////////////////////////////////////////////////////////

static void LogTime(std::ostream& os, const struct tm& TM, bool fLogThread)
{
    char c = os.fill();
    os << setfill('0')
       << setw(4) << TM.tm_year + 1900 << '-'
       << setw(2) << TM.tm_mon  + 1    << '-'
       << setw(2) << TM.tm_mday        << ' '
       << setw(2) << TM.tm_hour        << ':'
       << setw(2) << TM.tm_min         << ':'
       << setw(2) << TM.tm_sec         << ' '
       << setfill(c);
    if ( fLogThread )
	os << '[' << Thread::Current().systhread().value 
	   << "] ";
}

void FormatV(
    LogLevel         level,
    LogStream&       out, 
    const char*      fmt, 
    const FormatArg* args, 
    unsigned         n)
throw ()
{
    try
    {
	if ( level > out._loglevel )
	    return;

	if ( out._syslog )
	{
	    ostringstream os;
	    if ( out._mask & LogStream::Tid )
		os << '[' << Thread::Current().systhread().value << "] ";
	    FormatV(os, fmt, args, n);
	    std::string msg(os.str());
	    SysLog(level, msg);
	}
	else
	{
	    Mutex::Lock x(out._mutex);
	    if ( !out._filename.empty() && out._rotateDays )
	    {
		// BUG: Valid for _days==1 only:
		if ( out._dateOfLog != Today() )
		{
		    string fname(out._filename);
		    out.setFile(fname.c_str(), out._rotateDays);
		}
	    }

	    ostream&  os = out._out;
	    if ( !out._nologtime )
	    {
		time_t    T = time(0);
		struct tm TM;
		if ( localtime_r(&T, &TM) )
		{
		    if ( out._tee )
			LogTime(cerr, TM, out._mask & LogStream::Tid);
		    LogTime(os, TM, out._mask & LogStream::Tid);
		}
	    }
	    FormatV(os, fmt, args, n);
	    out._out << endl;

	    if ( out._tee )
	    {
		FormatV(cerr, fmt, args, n);
		cerr << endl;
	    }
	}
    }
    catch ( ... )
    {
	cerr << "** LOGGING FAILED **\n";
    }
}

///////////////////////////////////////////////////////////////////////////////

void LogStream::close()
{
    if ( _syslog )
    {
	_syslog = false;
	SysCloseLog();
    }
    
    if ( _filebuf.is_open() )
	_filebuf.close();	
}

///////////////////////////////////////////////////////////////////////////////

void LogStream::set(std::ostream& os)
{
    close();
    _out.rdbuf(os.rdbuf());
    _filename.erase();
}

///////////////////////////////////////////////////////////////////////////////

void LogStream::setFile(const char* name, unsigned days)
{
    close();
    _rotateDays = days;

    if ( days )
    {
	Date today(Today());
	ostringstream fname;
	fname << name 
	      << setfill('0')
	      << '_'
	      << setw(4) << today.year  << '-'
	      << setw(2) << today.month << '-'
	      << setw(2) << today.day;
	_filename  = fname.str();
	_dateOfLog = today;
    }
    else
    {
	_filename = name;
    }

    if ( !_filebuf.open(_filename.c_str(), ios::app|ios::out) )
    {
	int e = errno;
	_filename.erase();
	throw SystemError(e, Msg(TXT("Cannot open log file %0"), name));
    }
    if ( days )
	_filename = name;
    _out.rdbuf(&_filebuf);
}

///////////////////////////////////////////////////////////////////////////////

void LogStream::syslog(const char* ident, LogFacility facility)
{
    close();
    SysOpenLog(ident, facility);
    _syslog = true;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix


// end of positix++: log.cc
