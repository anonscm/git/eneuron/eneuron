/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : strings.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <ctype.h>
#include <iostream>
#include "strings.hh"

using namespace std;

namespace positix {

///////////////////////////////////////////////////////////////////////////////

void Trim(std::string& s) throw ()
{
    string::iterator p = s.begin();
    while ( p!=s.end() && isspace(*p) )
	++p;
    s.erase(s.begin(), p);

    p = s.end();
    while ( p!=s.begin() )
    {
	--p;
	if ( !isspace(*p) )
	{
	    ++p;
	    break;
	}
    }
    s.erase(p, s.end());
}

///////////////////////////////////////////////////////////////////////////////

void NormalizeWhitespace(std::string& s) throw ()
{
    Trim(s);
    string::iterator p = s.begin();
    
    while ( p!=s.end() )
    {
	if ( !isspace(*p) )
	    ++p;
	else
	{
	    string::iterator q = p + 1;
	    while ( q!=s.end() && isspace(*q) )
		++q;
	    s.erase(p+1, q);
	    p = q;
	}
    }
}

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream& os, const WriteQuotedString& wq)
{
    std::string::const_iterator p = wq.str.begin();
    std::string::const_iterator e = wq.str.end();

    if ( wq.cond )
    {
	bool mustBeQuoted = false;

	while ( p!=e )
	{
	    char c = *p;
	    if ( c=='"' || isspace(c) )
	    {
		mustBeQuoted = true;
		break;
	    }
	    ++p;
	}

	if ( !mustBeQuoted )
	    return os << wq.str;
    }

    p = wq.str.begin();

    os << '"';
    while ( p != e )
    {
	if ( *p == '"' )
	    os << '"';
	os << *p++;
    }
    return os << '"';
}

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream& os, const WriteQuotedChars& wq)
{
    const char* s = wq.str;

    if ( !s )
	return os << "<null>";

    if ( wq.cond )
    {
	bool mustBeQuoted = false;

	while ( *s )
	{
	    char c = *s;
	    if ( c=='"' || isspace(c) )
	    {
		mustBeQuoted = true;
		break;
	    }
	    ++s;
	}

	if ( !mustBeQuoted )
	    return os << wq.str;
    }

    s = wq.str;

    os << '"';
    while ( *s )
    {
	if ( *s == '"' )
	    os << '"';
	os << *s++;
    }
    return os << '"';
}

///////////////////////////////////////////////////////////////////////////////

const char* SkipPrefix(const char* prefix, const char* str) throw ()
{
    while ( *prefix )
    {
	if ( *prefix++ != *str++ )
	    return 0;
    }

    return str;
}

///////////////////////////////////////////////////////////////////////////////

size_t LengthOfCommonPrefix(const char* a, const char* b) throw ()
{
    size_t L = 0;
    while ( *a && *b && *a++ == *b++ )
	++L;
    return L;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: strings.cc
