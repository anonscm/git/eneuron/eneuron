/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : cmdopt.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_cmdopt_hh__
#define __positix_cmdopt_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>

namespace positix {

class PxOptionParser
{
public:
    inline PxOptionParser(int& argc, char* argv[]) throw ()
    : _cur(argv),
      _first(1),
      _argc(argc),
      _argv(argv)
    {
	_opt = _value = 0;
    }

    inline PxOptionParser(int first, int& argc, char* argv[]) throw ()
    : _cur(argv + first - 1),
      _first(first),
      _argc(argc),
      _argv(argv)
    {
	_opt = _value = 0;
    }

    bool iskey(const char* key) const throw ();

    std::string key() const;

    inline const char* value() const throw ()
    {
	return _value;
    }

    inline const char* current() const throw ()
    {
	return *_cur;
    }

    bool next() throw ();

    void removeAll() throw ();
    const char* removeCurrent() throw (); // returns value of current option

private:
    char** _cur;
    int    _first;
    int&   _argc;
    char** _argv;
    char*  _opt;
    char*  _value;

    void parseOption();
};

///////////////////////////////////////////////////////////////////////////////

extern 
std::string 
PxLocateCfgFile(
    const char* vendor,
    const char* filename,
    const char* ev,
    int&        argc,
    char*       argv[],
    bool        raiseException,
    bool        lookAtHome,
    std::string* origin = 0);

extern 
std::string 
PxLocateCfgFile(
    const char* vendor, 
    const char* filename,
    bool        lookAtHome);

extern
bool
PxBoolOpt(
    const char* value);

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif

// end of positix++: cmdopt.hh
