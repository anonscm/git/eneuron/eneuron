/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : chksum.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_chksum_hh__
#define __positix_chksum_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

namespace positix {

///////////////////////////////////////////////////////////////////////////////

struct ChkSumVal
{
    inline ChkSumVal() throw ()
    {
	data   = 0;
	length = 0;
    }

    inline ChkSumVal(const char* D, size_t L) throw ()
    : data(D),
      length(L)
    {
	// empty
    }

    const char*  data;
    size_t       length;
};

class ChkSum
{
public:
    virtual ~ChkSum() throw () {};

    virtual void reset() throw () = 0;
    virtual void block(char* data, size_t length) throw () = 0;
    virtual ChkSumVal value() const throw () = 0;
};

class ChkSumNone : public ChkSum
{
public:
    inline ChkSumNone() throw ()
    {
	// empty
    }

    virtual void reset() throw ();
    virtual void block(char* data, size_t length) throw ();
    virtual ChkSumVal value() const throw ();

    static ChkSumNone Std;
};


///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif

// end of positix++: chksum.hh
