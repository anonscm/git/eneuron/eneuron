/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : text.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_text_hh__
#define __positix_text_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <iostream>
#include <iomanip>

///////////////////////////////////////////////////////////////////////////////
namespace positix {
///////////////////////////////////////////////////////////////////////////////

class TextMessage;

struct TextPosition
{
    inline TextPosition() throw ()
    {
	column = line = 0;
    }

    unsigned line;
    unsigned column;
};

extern std::ostream& operator<<(std::ostream& os, const TextPosition& tp);

enum TextCoding
{
    TextCodingUndefined   =    0,
    TextCodingDynamic	  =    1,

    TextCodingUTF8	  =    2,
    TextCodingUTF16LE	  =    3,
    TextCodingUTF16BE	  =    4,
    TextCodingUTF32LE	  =    5,
    TextCodingUTF32BE	  =    6,

    TextCodingUCS2LE	  =    7,
    TextCodingUCS2BE	  =    8,
    TextCodingUCS4LE	  =    9,
    TextCodingUCS4BE	  =   10,

    nTextCodings
};

///////////////////////////////////////////////////////////////////////////////

extern const char* String(TextCoding te) throw ();
extern TextCoding MakeTextCoding(const char* s) throw ();

// Extraction function which work with state-less encodings.
typedef wchar_t (*InStreamGlyphExtractorFunction)(std::istream&);

extern InStreamGlyphExtractorFunction 
TheInStreamGlyphExtractorFunctions[nTextCodings];

///////////////////////////////////////////////////////////////////////////////

struct TextFilePosition : public TextPosition
{
    inline TextFilePosition() throw ()
    {
	column = line = 0;
	filename = 0;
    }

    const char* filename;
};

extern std::ostream& operator<<(std::ostream& os, const TextFilePosition& tfp);

///////////////////////////////////////////////////////////////////////////////

extern 
int CompareCaseInsensitive(
    const std::string& a, 
    const std::string& b)
throw ();

extern 
int CompareCaseInsensitive(
    const std::string& a, 
    const char*        b)
throw ();

extern 
int CompareCaseInsensitive(
    const char* a, 
    const char* b)
throw ();

inline int CompareCaseInsensitive(const char* a, const std::string& b) throw ()
{
    return -positix::CompareCaseInsensitive(b,a);
}

extern const char* const PxNullStr;
extern const std::string PxNullString;
extern const std::wstring PxNullWString;

inline const char* PxPassStr(const char* p) throw ()
{
    return p ? p : PxNullStr;
}

///////////////////////////////////////////////////////////////////////////////

extern std::locale Locale() throw ();

///////////////////////////////////////////////////////////////////////////////

extern std::ostream& operator << (std::ostream& os, const TextMessage& msg);

class TextMessage
{
public:
    inline TextMessage()
    : _locale(Locale())
    {
	// empty
    }

    inline TextMessage(const std::wstring& msg)
    : _locale(Locale())
    {
	assign(msg);
    }

    inline TextMessage(const wchar_t* msg)
    : _locale(Locale())
    {
	assign(msg);
    }

    inline TextMessage(const std::string& msg)
    : _msg(msg),
      _locale(Locale())
    {
	// empty
    }

    inline TextMessage(const char* msg)
    : _msg(msg),
      _locale(Locale())
    {
	// empty
    }

    inline TextMessage(const std::wstring& msg, std::locale loc)
    : _locale(loc)
    {
	assign(msg);
    }

    inline TextMessage(const wchar_t* msg, std::locale loc)
    : _locale(loc)
    {
	assign(msg);
    }

    inline TextMessage(const std::string& msg, std::locale loc)
    : _msg(msg),
      _locale(loc)
    {
	// empty
    }

    inline TextMessage(const char* msg, std::locale loc)
    : _msg(msg),
      _locale(loc)
    {
	// empty
    }

    TextMessage& assign(const char* s);
    TextMessage& assign(const std::string& s);

    inline TextMessage& assign(const char* s, std::locale loc)
    {
	_locale = loc;
	_msg    = s;
	return *this;
    }

    inline TextMessage& assign(const std::string& s, std::locale loc)
    {
	_locale = loc;
	_msg    = s;
	return *this;
    }

    TextMessage& assign(const wchar_t* msg);
    TextMessage& assign(const std::wstring& msg);

    inline TextMessage& assign(const wchar_t* msg, std::locale loc)
    {
	_locale = loc;
	return assign(msg);
    }

    inline TextMessage& assign(const std::wstring& msg, std::locale loc)
    {
	_locale = loc;
	return assign(msg);
    }

    TextMessage& append(const char* s);
    inline TextMessage& append(const std::string& s)
    {
	return append(s.c_str());
    }

    inline const std::string& str() const throw ()
    {
	return _msg;
    }

    inline const char* c_str() const
    {
	return _msg.c_str();
    }

    inline std::locale locale() const throw ()
    {
	return _locale;
    }

private:
    friend std::ostream& operator<<(std::ostream& os, const TextMessage& msg);
    std::string _msg;
    std::locale _locale;
};

///////////////////////////////////////////////////////////////////////////////

class Fmt
{
public:
    enum Adjustment
    {
	Left,
	Right,
	Internal
    };

    static void Set(std::ostream& os, Adjustment adj) throw ();

    enum FloatFormat
    {
	FloatDefault,
	Scientific,
	Fixed
    };

    static void Set(std::ostream& os, FloatFormat ff) throw ();
};

///////////////////////////////////////////////////////////////////////////////

template <class Value>
class FmtHex_ : public Fmt
{
public:
    inline FmtHex_(const Value& value) throw ()
    : _value(value)
    {
	// empty
    }

    std::ostream& format(std::ostream& os) const
    {
	return os << std::hex << _value << std::dec;
    }

private:
    const Value& _value;
};

template <class Value>
inline std::ostream& operator << (std::ostream& os, const FmtHex_<Value>& fmt)
{
    return fmt.format(os);
}

template <class Value>
inline FmtHex_<Value> FmtHex(const Value& value) throw ()
{
    return FmtHex_<Value>(value);
}

template <>
inline std::ostream& FmtHex_<unsigned char>::format(std::ostream& os) const
{
    return os << std::hex << unsigned(_value) << std::dec;
}

template <>
inline std::ostream& FmtHex_<signed char>::format(std::ostream& os) const
{
    return os << std::hex << int(_value) << std::dec;
}

///////////////////////////////////////////////////////////////////////////////

template <class Value>
class FmtField_ : public Fmt
{
public:
    inline FmtField_(
	const Value&  value,
	unsigned      width)
    throw ()
    : _value(value),
      _width(width),
      _adjust(Right),
      _fill(' ')
    {
	// empty
    }

    inline FmtField_(
	const Value&  value,
	unsigned      width,
	Adjustment    adjust)
    throw ()
    : _value(value),
      _width(width),
      _adjust(adjust),
      _fill(' ')
    {
	// empty
    }

    inline FmtField_(
	const Value&  value,
	unsigned      width,
	char          fill)
    : _value(value),
      _width(width),
      _adjust(Fmt::Left),
      _fill(fill)
    {
	// empty
    }

    inline FmtField_(
	const Value&  value,
	unsigned      width,
	Adjustment    adjust,
	char          fill)
    : _value(value),
      _width(width),
      _adjust(adjust),
      _fill(fill)
    {
	// empty
    }

    std::ostream& format(std::ostream& os) const
    {
	Set(os, _adjust);
	os.width(_width);
	char c = os.fill(_fill);
	os << _value;
	os.fill(c);
	Set(os, Right);	    
	return os;
    }

private:
    const Value& _value;
    unsigned     _width;
    Adjustment   _adjust;
    char         _fill;
};

template <class Value>
inline std::ostream& operator << (std::ostream& os,const FmtField_<Value>& fmt)
{
    return fmt.format(os);
}

template <class Value>
inline 
FmtField_<Value> 
FmtField(
    const Value& value, 
    unsigned     width) 
throw ()
{
    return FmtField_<Value>(value, width);
}

template <class Value>
inline FmtField_<Value> 
FmtField(
    const Value&    value,     
    unsigned        width,
    Fmt::Adjustment adj) 
throw ()
{
    return FmtField_<Value>(value, width, adj);
}

template <class Value>
inline 
FmtField_<Value> 
FmtField(
    const Value&    value,     
    unsigned        width,
    char            fill) 
throw ()
{
    return FmtField_<Value>(value, width, fill);
}


template <class Value>
inline 
FmtField_<Value> 
FmtField(
    const Value&    value,     
    unsigned        width,
    Fmt::Adjustment adj,
    char            fill) 
throw ()
{
    return FmtField_<Value>(value, width, adj, fill);
}

///////////////////////////////////////////////////////////////////////////////

template <class Value>
class FmtFloat_ : public Fmt
{
public:
    inline FmtFloat_(
	const Value& value,
	unsigned     precision)
    throw ()
    : _value(value),
      _precision(precision),
      _floatFormat(FloatDefault)
    {
	// empty
    }

    inline FmtFloat_(
	const Value& value,
	unsigned     precision,
	FloatFormat  floatFormat)
    throw ()
    : _value(value),
      _precision(precision),
      _floatFormat(floatFormat)
    {
	// empty
    }
  
    std::ostream& format(std::ostream& os) const
    {
	std::streamsize prec = os.precision(_precision);
	Set(_floatFormat);
	os << _value;
	os.precision(prec);
	Set(FloatDefault);
	return os;
    }

private:
    const Value& _value;
    unsigned     _precision;
    FloatFormat  _floatFormat;
};

template <class Value>
inline std::ostream& operator << (std::ostream& os,const FmtFloat_<Value>& fmt)
{
    return fmt.format(os);
}

template <class Value>
inline 
FmtFloat_<Value> 
FmtFloat(
    const Value& value, 
    unsigned     precision)
throw ()
{
    return FmtFloat_<Value>(value, precision);
}


template <class Value>
inline 
FmtFloat_<Value> 
FmtFloat(
    const Value&     value, 
    unsigned         precision,
    Fmt::FloatFormat ff)
throw ()
{
    return FmtFloat_<Value>(value, precision, ff);
}

///////////////////////////////////////////////////////////////////////////////

template <class Value>
inline void _PxFormatter(std::ostream& os, const void* p)
{
    os << *(static_cast<const Value*>(p));
}

///////////////////////////////////////////////////////////////////////////////

typedef void (*FormatterFunction)(std::ostream& os, const void* p);

///////////////////////////////////////////////////////////////////////////////

struct FormatArg
{
    inline FormatArg(const void* p, FormatterFunction f) throw ()
    : value(p),
      formatter(f)
    {
	// empty
    }

    const void*             value;
    const FormatterFunction formatter;
};

///////////////////////////////////////////////////////////////////////////////

extern std::ostream& FormatV(
    std::ostream&    os, 
    const char*      fmt, 
    const FormatArg* args, 
    unsigned         n);

#define _POSITIX_FORMAT_            Format
#define _POSITIX_FMT_               const char*
#define _POSITIX_FMT0_              "%0"
#define _POSITIX_FORMARG_           FormatArg
#define _POSITIX_FORMARG_CONS_(V,v) FormatArg(&v, &_PxFormatter<V>)
#define _POSITIX_FORMAT_FUN_        FormatV
#define _POSITIX_FORMAT_RESULT_     std::ostream&
#define _POSITIX_FORMAT_PARAM_      std::ostream& os,
#define _POSITIX_FORMAT_ARG_        os,
#include "format.hh"

///////////////////////////////////////////////////////////////////////////////

extern TextMessage MsgV(
    const char*      fmt, 
    const FormatArg* args, 
    unsigned         n);

#define _POSITIX_FORMAT_            Msg
#define _POSITIX_FMT_               const char*
#define _POSITIX_FMT0_              "%0"
#define _POSITIX_FORMARG_           FormatArg
#define _POSITIX_FORMARG_CONS_(V,v) FormatArg(&v, &_PxFormatter<V>)
#define _POSITIX_FORMAT_FUN_        MsgV
#define _POSITIX_FORMAT_RESULT_     TextMessage
#define _POSITIX_FORMAT_PARAM_
#define _POSITIX_FORMAT_ARG_
#include "format.hh"

///////////////////////////////////////////////////////////////////////////////
} // namespace positix
///////////////////////////////////////////////////////////////////////////////

#endif

// end of positix++: text.hh
