/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : text.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "text.hh"
#include "unicode.hh"
#include "error.hh"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <locale>
#include "msg.hh"

using namespace std;

// TODO: TRUE !!! character set conversion!

///////////////////////////////////////////////////////////////////////////////
namespace positix {

const char* const PxNullStr = "";
const std::string PxNullString;
const std::wstring PxNullWString;

///////////////////////////////////////////////////////////////////////////////

wchar_t ExtractUndefined(std::istream& /*in*/)
{
    throw Error(
	TXT("No glyph extraction possible for undefined text encoding"));
}

///////////////////////////////////////////////////////////////////////////////

wchar_t ExtractDynamic(std::istream& /*in*/)
{
    throw Error(
	TXT("No glyph extraction possible for dynamic text encoding"));
}

///////////////////////////////////////////////////////////////////////////////

InStreamGlyphExtractorFunction 
TheInStreamGlyphExtractorFunctions[nTextCodings] =
{
    ExtractUndefined,
    ExtractDynamic,
    ExtractUTF8,
    ExtractUTF16LE,
    ExtractUTF16BE,
    ExtractUCS4LE,
    ExtractUCS4BE,
    ExtractUCS2LE,
    ExtractUCS2BE,
    ExtractUCS4LE,
    ExtractUCS4BE
};

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream& os, const TextPosition& tp)
{
    return os << tp.line << ':' << tp.column;
}

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream& os, const TextFilePosition& tfp)
{
    if ( tfp.filename )
	os << tfp.filename << ':';
    return os << tfp.line << ':' << tfp.column;
}

///////////////////////////////////////////////////////////////////////////////

static const char TextCodingStringUnknown[] = "<unknown-textencoding>";
static const char* TextCodingString[nTextCodings] =
{
    "<undefined-textencoding>",
    "<dynamic-textencoding>",
    "UTF-8",
    "UTF-16LE",
    "UTF-16BE",
    "UTF-32LE",
    "UTF-32BE",
    "UCS-2LE",
    "UCS-2BE",
    "UCS-4LE",
    "UCS-4BE"
};

///////////////////////////////////////////////////////////////////////////////

extern const char* String(TextCoding te) throw ()
{
    return (te>=TextCodingUndefined && te<nTextCodings)
	? TextCodingString[te]
	: TextCodingStringUnknown;
}

///////////////////////////////////////////////////////////////////////////////

extern TextCoding MakeTextCoding(const char* s) throw ()
{
    for (unsigned i=0; i<nTextCodings; ++i)
    {
	if ( strcmp(s, TextCodingString[i])==0 )
	    return TextCoding(i);
    }
    return TextCodingUndefined;
}

///////////////////////////////////////////////////////////////////////////////

int CompareCaseInsensitive(const std::string& a, const std::string& b) throw ()
{
    string::const_iterator A  = a.begin();
    string::const_iterator Ae = a.end();
    string::const_iterator B  = b.begin();
    string::const_iterator Be = b.end();

    for (;;)
    {
	if ( A==Ae )
	{
	    // A came to the end
	    // If B also came to the end, the strings are equal.
	    // Otherwise, A is a true prefix of B and hence A is 
	    // "smaller" than B
	    return B==Be ? 0 : -1;
	}
	else if ( B==Be )
	{
	    // B came to the end, but A did not
	    // Therefore, B is a true prefix of A and hence is smaller than A
	    return 1;
	}

	unsigned char ca = (unsigned char) toupper(*A++);
	unsigned char cb = (unsigned char) toupper(*B++);

	if ( ca != cb )
	    return (ca<cb) ? -1 : 1;
    }
}

///////////////////////////////////////////////////////////////////////////////

int CompareCaseInsensitive(const std::string& a, const char* b) throw ()
{    
    string::const_iterator A  = a.begin();
    string::const_iterator Ae = a.end();

    for (;;)
    {
	if ( A==Ae )
	    return *b==0 ? 0 : -1;
	else if ( *b==0 )
	    return 1;

	unsigned char ca = (unsigned char) toupper(*A++);
	unsigned char cb = (unsigned char) toupper(*b++);

	if ( ca != cb )
	    return (ca<cb) ? -1 : 1;
    }
}

///////////////////////////////////////////////////////////////////////////////

int CompareCaseInsensitive(const char* a, const char* b) throw ()
{
    for (;;)
    {
	if ( *a==0 )
	    return *b==0 ? 0 : -1;
	else if ( *b!=0 )
	    return 1;

	unsigned char ca = (unsigned char) toupper(*a++);
	unsigned char cb = (unsigned char) toupper(*b++);

	if ( ca != cb )
	    return (ca<cb) ? -1 : 1;
    }
}

///////////////////////////////////////////////////////////////////////////////

std::locale Locale() throw ()
{
    return locale();
}

///////////////////////////////////////////////////////////////////////////////

TextMessage& TextMessage::assign(const wchar_t* s)
{
    if ( !s )
	_msg.erase();
    else 
    {
	while ( *s )
	    _msg += char(*s++);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////////

TextMessage& TextMessage::assign(const std::wstring& s)
{
    _msg.erase();
    _msg.reserve(s.length());
    for (std::wstring::const_iterator p = s.begin();
	 p != s.end();
	 ++p)
    {
	_msg += char(*p++);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////////

TextMessage& TextMessage::append(const char* s)
{
    _msg.append(s);
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

std::ostream& operator << (std::ostream& os, const TextMessage& msg)
{
    return os << msg._msg;
}

///////////////////////////////////////////////////////////////////////////////

// TextStream& 
// TextStream::putDigits(
//     const unsigned char* digits, 
//     unsigned             n, 
//     bool		 negative)
// {
//     if ( n==0 )
//     {
// 	put(UnicodeChar(48));
//     }
//     else
//     {
// 	do
// 	{
// 	    put(UnicodeChar(digits[--n] + 48));
// 	} while ( n );
//     }

//     return condFlush();
// }

///////////////////////////////////////////////////////////////////////////////

std::ostream&
FormatV(
    std::ostream&    os, 
    const char*      fmt, 
    const FormatArg* args, 
    unsigned         n)
{
    const char* p = fmt;

    while ( *p )
    {
	if ( *p == '%' )
	{
	    unsigned i;
	    ++p;
	    if ( *p==0 )
		return os;
	    if ( *p>='0' && *p<='9' )
	    {
		i = (*p - '0');
		++p;
	    }
	    else if ( *p=='{' )
	    {
		i = 0;
		do
		{
		    if ( *p<'0' || *p>'9' )
			break;
		    i = i*10 + (*p-'0');
		    ++p;			
		}
		while ( *p && *p!='}' );
	    }
	    else
	    {
		os << *p++;
		continue;
	    }

	    if ( i >= n )
	    {
		string msg;
		msg.reserve(64 + strlen(fmt));
		msg += "Format parameter index out of range in '";
		msg += fmt;
		msg += "'";
		throw std::logic_error(msg);
	    }

	    const FormatArg& arg = args[i];
	    arg.formatter(os, arg.value);
	}
	else
	{
	    os << *p++;
	}
    }

    return os;
}

///////////////////////////////////////////////////////////////////////////////

TextMessage 
MsgV(
    const char*      fmt, 
    const FormatArg* args, 
    unsigned         n)
{
    ostringstream os;
    FormatV(os, fmt, args, n);
    return os.str();
}

///////////////////////////////////////////////////////////////////////////////

void Fmt::Set(std::ostream& os, Adjustment adj) throw ()
{
    switch ( adj )
    {
    case Left    : os.setf(ios_base::left,     ios_base::adjustfield); return;
    case Right   : os.setf(ios_base::right,    ios_base::adjustfield); return;
    case Internal: os.setf(ios_base::internal, ios_base::adjustfield); return;
    }
}

///////////////////////////////////////////////////////////////////////////////

void Fmt::Set(std::ostream& os, FloatFormat adj) throw ()
{
    switch ( adj )
    {
    case FloatDefault: 
	os.setf(ios_base::fmtflags(0), ios_base::floatfield); 
	return;

    case Scientific:
	os.setf(ios_base::scientific, ios_base::floatfield); 
	return;

    case Fixed:
	os.setf(ios_base::fixed, ios_base::floatfield); 
	return;
    }
}

///////////////////////////////////////////////////////////////////////////////
} // namespace positix
///////////////////////////////////////////////////////////////////////////////


// end of positix++: text.cc
