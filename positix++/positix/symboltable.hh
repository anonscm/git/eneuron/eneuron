/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : symboltable.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_symboltable_hh__
#define __positix_symboltable_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <map>
#include <vector>
#include "strings.hh"

///////////////////////////////////////////////////////////////////////////////
namespace positix {
///////////////////////////////////////////////////////////////////////////////

template <int _first_ = 1, class _CharType_ = char>
class SymbolTable
{
public:
    typedef _CharType_              Char;
    typedef std::basic_string<Char> String;
    typedef int			    Id;
    static const int FirstId = _first_;

    SymbolTable()
    {
	// empty
    }

    SymbolTable(const Char** entries, unsigned nEntries)
    {
	++nEntries;
	while ( --nEntries )
	{
	    add(*entries);
	    ++entries;
	}
    }

    inline Id find(const String& symbol) const throw ()
    {
	ConstMapIt it = _map.find(symbol);
	return it==_map.end() ? (FirstId-1) : it->second;
    }

    inline const String* get(Id id) const throw ()
    {
	return ( id<FirstId || (id-FirstId)>int(_table.size()) )
	    ? ((String*)0)
	    : &_table[id-FirstId]->first;
    }

    Id add(const Char* symbol)
    {
	std::pair<MapIt,bool> x = _map.insert(std::make_pair(symbol,0));
	if ( x.second )
	{
	    // New entry
	    _table.push_back(x.first);
	    x.first->second = _table.size() + (FirstId-1);
	}
	return x.first->second;
    }

    Id add(const String& symbol)
    {
	std::pair<MapIt,bool> x 
	    = _map.insert(std::make_pair(CondStringCopy(symbol),0));
	if ( x.second )
	{
	    // New entry
	    _table.push_back(x.first);
	    x.first->second = _table.size() + (FirstId-1);
	}
	return x.first->second;
    }

    size_t size() const throw ()
    {
	return _table.size();
    }

private:
    typedef typename std::map<String,Id> Map;
    typedef typename Map::iterator       MapIt;
    typedef typename Map::const_iterator ConstMapIt;
    typedef typename std::vector<MapIt>  Table;
    Map   _map;
    Table _table;
};

///////////////////////////////////////////////////////////////////////////////
} // namespace positix
///////////////////////////////////////////////////////////////////////////////

#endif

// end of positix++: symboltable.hh
