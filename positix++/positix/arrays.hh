/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : arrays.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_arrays_hh__
#define __positix_arrays_hh__

#include "types.hh"

namespace positix {

///////////////////////////////////////////////////////////////////////////////

template <class Type_>
class AutoArrayPtr
{
public:
    typedef Type_ Type;
    typedef Type_ element_type;
    inline AutoArrayPtr() throw ()
    {
	_array = 0;
    }

    explicit inline AutoArrayPtr(Type* p) throw ()
    {
	_array = p;
    }

    template <class T>
    inline AutoArrayPtr(AutoArrayPtr<T>& p) throw ()
    {
	_array = p._array;
	p.release0();
    }

    inline ~AutoArrayPtr() throw ()
    {
	delete[] _array;
    }

    inline Type* get() throw ()
    {
	return _array;
    }

    inline void release0() throw ()
    {
	_array = 0;
    }

    inline Type* release() throw ()
    {
	Type* p = _array;
	_array = 0;
	return p;
    }

    inline void reset() throw ()
    {
	delete[] _array;
	_array = 0;
    }

    inline void reset(Type* p) throw ()
    {
	if ( _array != p )
	{
	    delete[] _array;
	    _array = p;
	}
    }

    inline const Type& operator[](int i) const throw ()
    {
	return _array[i];
    }

    inline Type& operator[](int i) throw ()
    {
	return _array[i];
    }

private:
    Type* _array;
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: arrays.hh
