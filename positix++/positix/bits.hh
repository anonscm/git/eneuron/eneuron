/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : bits.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_bits_hh__
#define __positix_bits_hh__

namespace positix {

///////////////////////////////////////////////////////////////////////////////

template <class Integer>
inline unsigned GetBit(Integer value, unsigned index) throw ()
{
    return unsigned((value>>index) & 1);
}

///////////////////////////////////////////////////////////////////////////////

template <class Integer>
inline void AssignBit(Integer& x, unsigned index, bool value) throw ()
{
    if ( value )
	x |= (Integer(1)<<index);
    else
	x &= ~(Integer(1)<<index);
}

///////////////////////////////////////////////////////////////////////////////

template <class Integer>
inline void SetBit(Integer& x, unsigned index) throw ()
{
    x |= (Integer(1)<<index);
}

///////////////////////////////////////////////////////////////////////////////

// Potential BUG: On some systems, a char may not consist of 8 bits!

template <class Integer>
inline void ClearBit(Integer& x, unsigned index) throw ()
{
    x &= ~(Integer(1)<<index);
}

///////////////////////////////////////////////////////////////////////////////

template <class Integer>
inline bool BmAllSet(const Integer& a, const Integer& b) throw ()
{
    return (a&b)==b;
}

///////////////////////////////////////////////////////////////////////////////

template <class Integer>
inline bool BmSomeSet(const Integer& a, const Integer& b) throw ()
{
    return (a&b)!=0;
}

///////////////////////////////////////////////////////////////////////////////

template <class Integer>
inline bool BmNoneSet(const Integer& a, const Integer& b) throw ()
{
    return (a&b)==0;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: bits.hh
