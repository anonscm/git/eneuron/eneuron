/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : accounting.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_accounting_hh__
#define __positix_accounting_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>

namespace positix {

///////////////////////////////////////////////////////////////////////////////

class User
{
public:
    explicit User(const char* name);
    explicit User(const SysUserId& uid);

    enum MissingIgnorance
    {
	IgnoreMissing,
	DontIgnoreMissing
    };

    User(const SysUserId& uid, MissingIgnorance mui);

    static SysUserId CurrentUserId();

    inline const std::string& name() const throw ()
    {
	return _name;
    }

    inline const std::string& password() const throw ()
    {
	return _password;
    }

    inline const std::string& home() const throw ()
    {
	return _home;
    }

    inline const SysUserId& sys_uid() const throw ()
    {
	return _uid;
    }

    inline const SysGroupId& sys_defaultGroup() const throw ()
    {
	return _gid;
    }

protected:
    std::string _name;
    std::string _password;
    std::string _home;
    SysUserId   _uid;
    SysGroupId  _gid;
};

inline bool operator==(const User& a, const User& b) throw ()
{
    return a.sys_uid()==b.sys_uid();
}

inline bool operator!=(const User& a, const User& b) throw ()
{
    return a.sys_uid()!=b.sys_uid();
}

///////////////////////////////////////////////////////////////////////////////

class Group
{
public:
    explicit Group(const char* name);
    explicit Group(const SysGroupId& gid);

    enum MissingIgnorance
    {
	IgnoreMissing,
	DontIgnoreMissing
    };

    Group(const SysGroupId& uid, MissingIgnorance mgi);

    inline const std::string& name() const throw ()
    {
	return _name;
    }

    inline const SysGroupId& sys_gid() const throw ()
    {
	return _gid;
    }

protected:
    std::string _name;
    SysGroupId  _gid;
};

inline bool operator==(const Group& a, const Group& b) throw ()
{
    return a.sys_gid()==b.sys_gid();
}

inline bool operator!=(const Group& a, const Group& b) throw ()
{
    return a.sys_gid()!=b.sys_gid();
}

///////////////////////////////////////////////////////////////////////////////

extern void SwitchPersonality(const SysUserId& uid);
extern void SwitchGroup(const SysGroupId& gid);
    
} // namespace positix

#endif

// end of positix++: accounting.hh
