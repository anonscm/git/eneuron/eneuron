/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : dl.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_dl_hh__
#define __positix_dl_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <map>

#include "relay.hh"
#include "def.hh"

namespace positix {

class Library
{
public:
    inline Library() throw ()
    {
	// empty
    }

    inline Library(const char* name, const char* directory = 0)
    {
	open(name, directory);
    }

    inline Library(SysLibrary lib) throw ()
    : _handle(lib)
    {
	// empty
    }

    inline ~Library()
    {
	close();
    }

    Library& operator = (SysLibrary lib)
    {
	close();
	_handle = lib;
	return *this;
    }

    std::string open(const char* name, const char* directory = 0);
    void close();

    void* address(const char* symbol, std::string& errmsg) throw ();

    inline SysLibrary get() throw ()
    {
	return _handle;
    }

    inline SysLibrary release() throw ()
    {
	SysLibrary r = _handle;
	_handle.value = SysLibrary::null();
	return r;
    }

    inline bool valid() throw ()
    {
	return _handle.valid();
    }

    class Loader
    {
    public:
	inline Loader(Library& lib, const std::string& libname)
	: _lib(lib),
	  _libname(libname)
	{
	    // empty
	}

	void* operator() (const char* symbol);

    private:
	Library&           _lib;
	const std::string& _libname;
	std::string        _errmsg;
    };

private:
    SysLibrary _handle;
    POSITIX_CLASS_NOCOPY(Library);
};

///////////////////////////////////////////////////////////////////////////////

template <class Interface_>
class LibraryCollection
{
public:
    typedef Interface_ Interface;

    Interface& load(
	const std::string& name, 
	const char* prefix    = 0,
	const char* directory = 0)
    {
	Lib& lib = _libs[name];
	if ( !lib.handle().valid() )
	{
	    positix::Library L;
	    std::string libname;
	    if ( prefix )
	    {
		std::string xname;
		xname.reserve(strlen(prefix) + name.length());
		xname  = prefix;
		xname += name;
		libname = L.open(xname.c_str(), directory);
	    }
	    else
	    {
		libname = L.open(name.c_str(), directory);
	    }
	    lib.interface = Interface(L, libname);
	    lib.handle.setReleased(L);
	}
	return lib.interface;
    }

private:
    struct Lib
    {
	positix::Relay<positix::Library> handle;
	Interface                        interface;
    };

    typedef std::map<std::string,Lib> Libs;
    Libs _libs;
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix


#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: dl.hh
