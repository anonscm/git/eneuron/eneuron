/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : math.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_math_hh__
#define __positix_math_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <math.h>
#include <stdlib.h>
#include <sstream>

namespace positix_math {

///////////////////////////////////////////////////////////////////////////////

extern void ThrowBadNumber(const char* snum);

///////////////////////////////////////////////////////////////////////////////

template <class Number>
int CeilLd(Number num) throw ()
{
    Number n = num>=0 ? num : -num;
    int    c = 0;
    while ( n>0 )
    {
	++n;
	n>>=1;
    }

    return num>=0 ? n : -n;
}

///////////////////////////////////////////////////////////////////////////////

template <class Number_>
struct Div
{
    typedef Number_ Number;
    inline Div(Number a, Number b) throw ()
    {
	quot = a/b;
	rem  = a%b;
    }

    Number quot;
    Number rem;
};

///////////////////////////////////////////////////////////////////////////////

template <>
struct Div<int> : public div_t
{
    typedef int Number;
    inline Div(int a, int b) throw ()
    {
	static_cast<div_t&>(*this) = ::div(a, b);
    }
};

///////////////////////////////////////////////////////////////////////////////

template <class Number>
inline Div<Number> DivRem(Number a, Number b) throw ()
{
    return Div<Number>(a, b);
}

///////////////////////////////////////////////////////////////////////////////

template <class Number>
Number ParseInt(const char* s)
{
    std::istringstream in(s);
    Number v;
    in >> v;
    if ( in.fail() )
	ThrowBadNumber(s);
    return v;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix


#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: math.hh
