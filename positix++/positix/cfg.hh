/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : cfg.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_cfg_hh__
#define __positix_cfg_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <string>
#include <vector>

#include "tokenizer.hh"
#include "cmdopt.hh"

namespace positix {

///////////////////////////////////////////////////////////////////////////////

class Configurator
{
public:
    virtual ~Configurator()
    {
	// empty
    }

    virtual bool hasNext() const = 0;
    virtual void syntaxError(const TextMessage& msg) = 0;

    inline const std::string& key() const throw ()
    {
	return _key;
    }

    inline bool iskey(const char* k) const throw ()
    {
	return _key==k;
    }

    inline const std::string& value() const throw ()
    {
	return _value;
    }

    inline bool isvalue(const char* v) const throw ()
    {
	return _value==v;
    }

    void getValue(bool& flag);

    template <class Value>
    void getValue(Value& value)
    {
	std::istringstream in(_value);
	in >> value;
	if ( in.fail() )
	    badValue(); // throw exception!
    }

    template <class List>
    void extendList(List& list)
    {
	if ( _value.empty() )
	    list.erase();
	else
	{
	    typename List::value_type value;
	    std::istringstream in(_value);
	    in >> value;	    
	    if ( in.fail() )
		badValue(); // throws exception!
	    list.insert(list.end(), value);
	}
    }

    void badValue();

protected:
    mutable std::string _key;
    mutable std::string _value;
};    

///////////////////////////////////////////////////////////////////////////////

class FileConfigurator : public Configurator
{
public:
    explicit FileConfigurator(const std::string& filename);

    virtual bool hasNext() const;
    virtual void syntaxError(const TextMessage& msg);

private:
    mutable SimpleTokenizer _tokenizer;
};

///////////////////////////////////////////////////////////////////////////////

class OptionConfigurator : public Configurator
{
public:
    inline OptionConfigurator(int& argc, char* argv[]) throw ()
    : _opt(argc, argv)
    {
	// empty
    }

    virtual bool hasNext() const;
    virtual void syntaxError(const TextMessage& msg);

private:
    mutable PxOptionParser _opt;
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif

// end of positix++: cfg.hh
