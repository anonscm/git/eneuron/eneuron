/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : sockets.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_sockets_hh__
#define __positix_sockets_hh__

#ifndef __positix_sys_hh__
#include "sys.hh"
#endif

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#ifdef __unix__
#include <sys/un.h>
#endif

#ifdef __linux__
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#endif

#include <string>
#include <iostream>
#include <sstream>
#include <streambuf>

#include "sys.hh"
#include "types.hh"
#include "def.hh"

namespace positix {

class Socket;
class SocketAddr;

///////////////////////////////////////////////////////////////////////////////

class IPAddr
{
public:
    static const unsigned AddrStrLen = INET6_ADDRSTRLEN;

    inline IPAddr() throw ()
    {
	_type            = AF_INET;
	_addr.ip4.s_addr = INADDR_NONE;
    }

    inline IPAddr(const struct in_addr& ip4) throw ()
    {
	_type     = AF_INET;
	_addr.ip4 = ip4;
    }

    inline IPAddr(const struct in6_addr& ip6) throw ()
    {
	_type     = AF_INET6;
	_addr.ip6 = ip6;
    }

    inline explicit IPAddr(const std::string& addr)
    {
	set(addr.c_str());
    }

    inline explicit IPAddr(const char* addr)
    {
	set(addr);
    }

    inline explicit IPAddr(const struct sockaddr* sa)
    {
	set(sa);
    }

    IPAddr& set(const char* addr);
    bool tryset(const char* addr) throw ();

    inline IPAddr& set(const std::string& addr)
    {
	return set(addr.c_str());
    }

    IPAddr& set(const struct sockaddr* sa) throw ();

    IPAddr& setFromLocal(SysSocket sock);
    IPAddr& setFromPeer(SysSocket sock);

    IPAddr& setFromLocal(const Socket& socket);
    IPAddr& setFromPeer(const Socket& socket);

    inline IPAddr& operator = (const struct in_addr ip4) throw ()
    {
	_type     = AF_INET;
	_addr.ip4 = ip4;
	return *this;
    }

    inline IPAddr& operator = (const struct in6_addr ip6) throw ()
    {
	_type     = AF_INET6;
	_addr.ip6 = ip6;
	return *this;
    }

    int compare(const IPAddr& addr) const throw ();

    inline int type() const throw ()
    {
	return _type;
    }

    inline const void* addr() const throw ()
    {
	return &_addr;
    }

private:
    int _type;
    union
    {
	struct in_addr  ip4;
	struct in6_addr ip6;
    } _addr;
};

POSITIX_CLASS_CMP(IPAddr);
extern std::ostream& operator << (std::ostream& os, const IPAddr& addr);

///////////////////////////////////////////////////////////////////////////////

class SockAddrInfo
{
public:
    inline SockAddrInfo() throw ()
    : _ai(0)
    {
	// empty
    }

    SockAddrInfo(
	const char*            node,
	const char*            service = 0,
	const struct addrinfo* hints   = 0);
    
    inline ~SockAddrInfo() throw ()
    {
	if ( _ai )
	    freeaddrinfo(_ai);
    }

    inline operator const struct addrinfo* () const throw ()
    {
	return _ai;
    }

    inline const struct addrinfo* operator->() const throw ()
    {
	return _ai;
    }

private:
    struct addrinfo* _ai;
    POSITIX_CLASS_NOCOPY(SockAddrInfo);
};

///////////////////////////////////////////////////////////////////////////////

class SocketAddr
{
public:
    inline SocketAddr() throw ()
    {
	_u.gen.sa_family = AF_UNSPEC;
    }

    inline SocketAddr(
	const std::string& saddr,
	int                family,    // PF_xxx
	int                socktype,  // SOCK_xxx,
	int                protocol   // 0, IPPROTO_xxx, ...
	)
    {
	set(saddr.c_str(), family, socktype, protocol);
    }

    inline SocketAddr(
	const char* saddr,
	int         family,    // PF_xxx
	int         socktype,  // SOCK_xxx,
	int         protocol   // 0, IPPROTO_xxx, ...
	)
    {
	set(saddr, family, socktype, protocol);
    }

    SocketAddr(
	const struct sockaddr& sa,
	int             socktype,
	int             protocol);

    SocketAddr(const struct addrinfo& ai);

    SocketAddr& operator=(const struct sockaddr& sa);
    SocketAddr& operator=(const struct addrinfo& ai);

    SocketAddr& setFromLocal(SysSocket sock);
    SocketAddr& setFromPeer(SysSocket sock);

    SocketAddr& setFromLocal(const Socket& socket);
    SocketAddr& setFromPeer(const Socket& socket);

    void set(
	const char* saddr,
	int         family,    // PF_xxx
	int         socktype,  // SOCK_xxx,
	int         protocol   // 0, IPPROTO_xxx, ...
	);

    inline void set(
	const std::string& saddr,
	int                 family,    // PF_xxx
	int                 socktype,  // SOCK_xxx,
	int                 protocol   // 0, IPPROTO_xxx, ...
	)
    {
	set(saddr.c_str(), family, socktype, protocol);
    }

    inline int family() const throw ()
    {
	return _u.gen.sa_family;
    }

    inline int socktype() const throw ()
    {
	return _socktype;
    }

    inline int protocol() const throw ()
    {
	return _protocol;
    }

    inline const struct sockaddr* sockaddr() const throw ()
    {
	return &_u.gen;
    }

    positix_types::fu16 port(positix_types::fu16 port) throw ();
    positix_types::fu16 port() const throw ();

    socklen_t addrlen() const throw ();

    int compare(const SocketAddr& b) const throw ();

private:
    friend std::ostream& operator<<(std::ostream& os, const SocketAddr& addr);
    friend class Socket;
    int _socktype;
    int _protocol;
    union
    {
	struct sockaddr     gen;
	struct sockaddr_in  ip4;
	struct sockaddr_in6 ip6;
#ifdef __unix__
	struct sockaddr_un  un;
#endif
#ifdef __linux__
	struct sockaddr_nl  netlink;
#endif
    } _u;

    void setAddr(const struct sockaddr&);
};

POSITIX_CLASS_CMP(SocketAddr);

extern std::ostream& operator << (std::ostream& os, const SocketAddr& addr);

inline std::string String(const SocketAddr& addr)
{
    std::ostringstream xos;
    xos << addr;
    return xos.str();
}

///////////////////////////////////////////////////////////////////////////////

class Socket
{
public:
    inline Socket() throw ()
    {
	// empty
    }

    inline Socket(SysSocket syssock)
    : _socket(syssock)
    {
	// empty
    }

    ~Socket();

    inline bool valid() const throw ()
    {
	return _socket.valid();
    }

    void close();

    void bind(const SocketAddr& addr, bool fReuseAddr = false);
    void listen(int backlog);

    void connect(const SocketAddr& addr);

    SysSocket accept(SocketAddr* addr);

    inline ssize_t recv(void* buf, size_t size, int flags = 0) throw ()
    {
	return ::recv(_socket.value, buf, size, flags);
    }

    ssize_t send(const void* buf, size_t size, int flags = 0) throw ()
    {
	return ::send(_socket.value, buf, size, flags);
    }

    inline SysSocket get() const throw ()
    {
	return _socket;
    }

    inline SysSocket release() throw ()
    {
	SysSocket sys(_socket);
	_socket.value = SysSocket::Null;
	return sys;
    }

    Socket& operator=(SysSocket syssock) throw ();

private:
    SysSocket _socket;
    void setAddr(const SocketAddr& addr);
    POSITIX_CLASS_NOCOPY(Socket);
};

///////////////////////////////////////////////////////////////////////////////

inline IPAddr& IPAddr::setFromLocal(const Socket& socket)
{
    return setFromLocal(socket.get());
}

inline IPAddr& IPAddr::setFromPeer(const Socket& socket)
{
    return setFromPeer(socket.get());
}

inline SocketAddr& SocketAddr::setFromLocal(const Socket& socket)
{
    return setFromLocal(socket.get());
}

inline SocketAddr& SocketAddr::setFromPeer(const Socket& socket)
{
    return setFromPeer(socket.get());
}

///////////////////////////////////////////////////////////////////////////////

template <class Char, class Traits = std::char_traits<Char> >
class BasicSocketStreamBuf : public std::basic_streambuf<Char,Traits>
{
public:
    typedef Char   char_type;
    typedef Traits traits_type;
    typedef typename traits_type::int_type int_type;

    BasicSocketStreamBuf(
	SysSocket socket, 
	size_t    inbuflen = 512, 
	size_t    outbuflen = 512)
    : _socket(socket),
      _buf(0),
      _inbuflen(inbuflen),
      _outbuflen(outbuflen)
    {
	_buf = new Char[_inbuflen + _outbuflen];
	setp(_buf + _inbuflen, _buf + _inbuflen + _outbuflen);
	setg(_buf, _buf+_inbuflen, _buf+_inbuflen);
	_outErr = _inErr = 0;
    }

    BasicSocketStreamBuf(
	Socket& socket, 
	size_t  inbuflen = 512, 
	size_t  outbuflen = 512)
    : _socket(socket.release()),
      _buf(0),
      _inbuflen(inbuflen),
      _outbuflen(outbuflen)
    {
	_buf = new Char[_inbuflen + _outbuflen];
	setp(_buf + _inbuflen, _buf + _inbuflen + _outbuflen);
	setg(_buf, _buf+_inbuflen, _buf+_inbuflen);
	_outErr = _inErr = 0;
    }

    ~BasicSocketStreamBuf() throw ()
    {
	sendbuffer();
	delete[] _buf;
    }

    inline void assign(SysSocket syssock) throw ()
    {
	if ( _socket.get()!=syssock )
	{
	    _socket.close();
	    _socket = syssock;
	}
	setp(_buf + _inbuflen, _buf + _inbuflen + _outbuflen);
	setg(_buf, _buf+_inbuflen, _buf+_inbuflen);	    
	_outErr = _inErr = 0;
    }

    inline void close() throw ()
    {
	_socket.close();
    }

    inline SysSocket sys_socket() const throw ()
    {
	return _socket.get();
    }

    inline bool is_open() const throw ()
    {
	return _socket.get().valid();
    }

    inline SysErrCode oErr() const throw ()
    {
	return _inErr;
    }

    inline SysErrCode iErr() const throw ()
    {
	return _inErr;
    }


protected:
    inline int sendbuffer() throw ()
    {
	ptrdiff_t size = this->pptr() - this->pbase();
	if ( size > 0 )
	{
	    ssize_t sent = _socket.send(this->pbase(), size, 0);
	    if ( size!=sent )
	    {
		if ( sent<0 )
		    _outErr = SysLastErr();
		return -1;
	    }
	    this->pbump(-size);
	}
	return 0;
    }

    virtual int sync()
    {
	return sendbuffer();
    }

    int_type overflow(int_type c)
    {	
	if ( sendbuffer()!=0 )
	    return traits_type::eof();
	else if ( !traits_type::eq_int_type(c, traits_type::eof()) )
	    return sputc(traits_type::to_char_type(c));
	else
	    return traits_type::not_eof(c);
    }

    std::streamsize xsputn(const char_type* s, std::streamsize n)
    {
	if ( n>0 )
	{
	    std::streamsize nn = n;
	    for(;;)
	    {
		std::streamsize len = this->epptr() - this->pptr();
		if ( len > nn )
		    len = nn;
		::memcpy(this->pptr(), s, len);
		this->pbump(len);
		nn -= len;
		s  += len;
		if ( nn==0 )
		    break;
		sendbuffer();
	    }
	}

	return n;
    }

    int_type underflow()
    {
	if ( this->gptr() < this->egptr() )
	    return traits_type::to_int_type(*this->gptr());

	ssize_t size = _socket.recv(_buf, _inbuflen, 0);

	if ( size == 0 )
	    return traits_type::eof();
	else if ( size < 0 )
	{
	    _inErr = SysLastErr();
	    return traits_type::eof();
	}
	else
	{
	    setg(_buf, _buf, _buf + size);
	    return 0;
	}
    }

private:
    Socket _socket;
    Char*  _buf;
    size_t _inbuflen;
    size_t _outbuflen;
    SysErrCode _inErr;
    SysErrCode _outErr;
};

///////////////////////////////////////////////////////////////////////////////

template <class Char, class Traits = std::char_traits<Char> >
class BasicSocketStream : public std::basic_iostream<Char, Traits>
{
public:
    inline BasicSocketStream(
	size_t    inbuflen = 512, 
	size_t    outbuflen = 512)
    : std::basic_iostream<Char,Traits>(&_buf),
      _buf(SysSocket(), inbuflen, outbuflen)
    {
	// empty
    }

    inline BasicSocketStream(
	SysSocket socket,
	size_t    inbuflen = 512, 
	size_t    outbuflen = 512)
    : std::basic_iostream<Char,Traits>(&_buf),
      _buf(socket, inbuflen, outbuflen)
    {
	// empty
    }

    inline BasicSocketStream(
	Socket& socket,
	size_t  inbuflen = 512, 
	size_t  outbuflen = 512)
    : std::basic_iostream<Char,Traits>(&_buf),
      _buf(socket, inbuflen, outbuflen)
    {
	// empty
    }

    void open(const SocketAddr& addr)
    {
	Socket socket;
	socket.connect(addr);
	assign(socket.release());
    }

    void open(const SocketAddr& connAddr, const SocketAddr& bindAddr)
    {
	Socket socket;
	socket.bind(bindAddr);
	socket.connect(connAddr);
	assign(socket.release());
    }

    inline void assign(SysSocket syssock) throw ()
    {
	_buf.assign(syssock);
	this->clear();
    }

    inline void close() throw ()
    {
	_buf.close();
    }

    inline bool is_open() throw ()
    {
	return _buf.is_open();
    }

    inline SysSocket sys_socket() const throw ()
    {
	return _buf.sys_socket();
    }

    inline SysErrCode oErr() const throw ()
    {
	return _buf.oErr();
    }

    inline SysErrCode iErr() const throw ()
    {
	return _buf.iErr();
    }

private:
    BasicSocketStreamBuf<Char,Traits> _buf;
};

///////////////////////////////////////////////////////////////////////////////

typedef BasicSocketStream<char> SocketStream;
typedef BasicSocketStream<wchar_t> SocketWideStream;

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

#endif

// end of positix++: sockets.hh
