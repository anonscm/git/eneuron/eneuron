/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : posix.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_posix_hh__
#define __positix_posix_hh__

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/mman.h>
#include <stddef.h>
#include <netinet/in.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <fcntl.h>
#include <errno.h>
#include <iconv.h>

#ifdef __linux__
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#endif

#include <string>

namespace positix {

///////////////////////////////////////////////////////////////////////////////

#define POSITIX_DIR_SEP "/"
#define POSITIX_DIR_SEPC '/'

///////////////////////////////////////////////////////////////////////////////

POSITIX_SYS_HANDLE(File,int,-1)
POSITIX_SYS_VALUE(FileStat, struct stat)
POSITIX_SYS_VALUE(TextTranscoder, iconv_t)

typedef off_t SysFileOffset;

extern bool PxUseFileNameMap;
extern void PxAddToFileNameMap(SysFile fd, const char* name);
extern void PxRemoveFromFileNameMap(SysFile fd);
extern const char* PxGetFromFileNameMap(SysFile fd);

inline SysFile SysFileOpen(const char* name, int flags, mode_t mode) 
throw ()
{
    SysFile fd(::open(name, flags, mode));
    if ( fd.value!=-1 && PxUseFileNameMap )
	PxAddToFileNameMap(fd, name);
    return fd;	
}

inline bool SysFileClose(SysFile fd)
throw ()
{
    bool r = ::close(fd.value)==0;
    if ( PxUseFileNameMap )
	PxRemoveFromFileNameMap(fd);
    return r;
}

inline ssize_t SysFileRead(SysFile fd, void* buf, size_t count)
throw ()
{
    return ::read(fd.value, buf, count);
}

inline ssize_t SysFileWrite(SysFile fd, const void* buf, size_t count)
throw ()
{
    return ::write(fd.value, buf, count);
}

inline ssize_t SysFileRead(
    SysFile fd, 
    SysFileOffset offset, void* buf, size_t count)
throw ()
{
    return ::pread(fd.value, buf, count, offset);
}

inline ssize_t SysFileWrite(SysFile fd,
			    SysFileOffset offset, const void* buf,size_t count)
throw ()
{
    return ::pwrite(fd.value, buf, count, offset);
}

inline SysFileOffset SysFileSeek(SysFile fd, SysFileOffset offset, int whence)
throw ()
{
    return ::lseek(fd.value, offset, whence);
}

inline bool SysFileSync(SysFile fd)
throw ()
{
    return ::fsync(fd.value)==0;
}

inline bool SysFileDataSync(SysFile fd)
throw ()
{
    return ::fdatasync(fd.value)==0;
}

inline bool SysFileGetStat(const char* filename, SysFileStat& st)
throw ()
{
    return ::stat(filename, &st.value)==0;
}

inline bool SysFileGetStatL(const char* filename, SysFileStat& st)
throw ()
{
    return ::lstat(filename, &st.value)==0;
}

inline bool SysFileGetStat(const char* filename, SysFileStat& st, 
			   bool fDontFollowSymLink)
throw ()
{
    return fDontFollowSymLink 
	? ::lstat(filename, &st.value)==0
	: ::stat(filename, &st.value)==0;
}

inline bool SysFileGetStat(SysFile fd, SysFileStat& st)
throw ()
{
    return ::fstat(fd.value, &st.value)==0;
}

inline bool SysFileResize(SysFile fd, SysFileOffset newLength)
throw ()
{
    return ::ftruncate(fd.value, newLength)==0;
}

inline positix_types::fu64 SysFileStatSize(const SysFileStat& st) throw ()
{
    return st.value.st_size;
}

inline mode_t SysFileStatMode(const SysFileStat& st) throw ()
{
    return st.value.st_mode;
}

inline ino_t SysFileStatInode(const SysFileStat& st) throw ()
{
    return st.value.st_ino;
}

inline dev_t SysFileStatDev(const SysFileStat& st) throw ()
{
    return st.value.st_dev;
}

inline nlink_t SysFileStatNlinks(const SysFileStat& st) throw ()
{
    return st.value.st_nlink;
}

inline time_t SysFileStatCtime(const SysFileStat& st) throw ()
{
    return st.value.st_ctime;
}

inline time_t SysFileStatMtime(const SysFileStat& st) throw ()
{
    return st.value.st_mtime;
}

inline time_t SysFileStatAtime(const SysFileStat& st) throw ()
{
    return st.value.st_atime;
}

extern int SysFileStatType(const SysFileStat& st) throw ();

extern bool SysReadLink(const char* link, std::string& contents) throw ();

///////////////////////////////////////////////////////////////////////////////

typedef int SysErrCode;

struct SysLastErr
{
    inline SysLastErr() throw ()
    : value(errno)
    {
	// empty
    }

    inline operator SysErrCode () const throw ()
    {
	return value;
    }

    SysErrCode value;
};

struct SysKeepLastErr : public SysLastErr
{
    inline ~SysKeepLastErr() throw ()
    {
	errno = value;
    }
};

///////////////////////////////////////////////////////////////////////////////

POSITIX_SYS_HANDLE(UserId,uid_t,uid_t(-1))
POSITIX_SYS_HANDLE(GroupId,gid_t,gid_t(-1))

///////////////////////////////////////////////////////////////////////////////

typedef SysFile SysSocket;

inline bool SysSocketClose(SysSocket sock) throw ()
{
    return ::close(sock.value)==0;
}

///////////////////////////////////////////////////////////////////////////////

struct SysMemoryMap
{
    inline SysMemoryMap() throw ()
    : ptr(0),
      len(0)
    {
	// empty
    }

    char* ptr;
    size_t len;
    inline size_t length() const throw ()
    {
	return len;
    }
};

///////////////////////////////////////////////////////////////////////////////

POSITIX_SYS_HANDLE_PTR(Library,void*)

///////////////////////////////////////////////////////////////////////////////

class DirScanner
{
public:   
    inline explicit DirScanner(const char* path)
    {
	open(path);
    }

    inline explicit DirScanner(const std::string& path)
    {
	open(path.c_str());
    }

    inline ~DirScanner() throw ()
    {
	::closedir(_dir);
    }

    const char* next();

    inline const char* name() const throw ()
    {
	return _u.ent.d_name;
    }

private:
    DIR* _dir;
    static struct dirent* Dummy;
    union
    {
	struct dirent ent;
	char          c[sizeof(*Dummy)-sizeof(Dummy->d_name) + NAME_MAX + 1];
    } _u;

    void open(const char* path);
};

///////////////////////////////////////////////////////////////////////////////

} // namespace positix

///////////////////////////////////////////////////////////////////////////////

namespace positix_types {

///////////////////////////////////////////////////////////////////////////////

inline fu64 GetMicroTime() throw ()
{
    // Note: Return value of gettime is not checked as POSIX mandates this
    //       function to return zero.
    struct timeval tv;
    gettimeofday(&tv, 0);
    return fu64(tv.tv_sec)*fu64(1000000uL) + fu64(tv.tv_usec);
}

inline fu64 GetTimeResolution() throw ()
{
    // Note: Return value of clock_getres is not checked.
    //       There are two possible error conditions:
    //       + The system doesn't support clock_getres. 
    //         This should result in a compiler error.
    //       + The specified clock (CLOCK_REALTIME) is not defined.
    //         POSIX mandates the existance of this clock, hence this error
    //         shall not occur.

    struct timespec ts;
    clock_getres(CLOCK_REALTIME, &ts);
    return fu64(ts.tv_sec)*fu64(1000000000uL) + fu64(ts.tv_nsec);
}

inline fu64 GetNanoTime() throw ()
{
    // Note: The return value of clock_gettime is not check. 
    //       See GetTimeResolution.
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return fu64(ts.tv_sec)*fu64(1000000000uL) + fu64(ts.tv_nsec);
}

///////////////////////////////////////////////////////////////////////////////

} // namespace positix_types

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: posix.hh
