/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : sys/bytes.h
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_sys_bytes_h__
#define __positix_sys_bytes_h__

///////////////////////////////////////////////////////////////////////////////
// Determine byte order
///////////////////////////////////////////////////////////////////////////////

#ifdef __GNUC__
#include <endian.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
#    define POSITIX_CPU_BYTE_ORDER 1234
#elif __BYTE_ORDER = __BIG_ENDIAN
#    define POSITIX_CPU_BYTE_ORDER 4321
#endif

#endif // __GNUC__

///////////////////////////////////////////////////////////////////////////////
// Define bswap_X
///////////////////////////////////////////////////////////////////////////////

#ifdef __GNUC__
#include <byteswap.h>

#else
#warning Using less efficients functions for bswap_X

inline uint_fast16_t bswap_16(uint_fast16_t val)
{
    return (val>>8)|(val<<8);
}

inline uint_fast32_t bswap_32(uint_fast32_t val)
{
    return ((val&0x000000FF)<<24)
	|  ((val&0x0000FF00)<< 8)
	|  ((val&0x00FF0000)>> 8)
	|  ((val&0xFF000000)>>24);
}

inline uint_fast32_t bswap_64(uint_fast64_t val)
{
    return ((val&0x000000000000FFuLL)<<56)
	|  ((val&0x0000000000FF00uLL)<<40)
	|  ((val&0x00000000FF0000uLL)<<24)
	|  ((val&0x000000FF000000uLL)<< 8)
	|  ((val&0x0000FF00000000uLL)>> 8)
	|  ((val&0x00FF0000000000uLL)>>24)
	|  ((val&0xFF000000000000uLL)>>56);
}
#endif

///////////////////////////////////////////////////////////////////////////////

#endif

// Local Variables: ***
// mode:c ***
// End: ***

// end of positix++: sys/bytes.h

