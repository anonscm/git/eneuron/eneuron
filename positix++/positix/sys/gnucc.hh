/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : gnucc.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_gnucc_hh__
#define __positix_gnucc_hh__

#ifdef _POSIX_C_SOURCE
#if _POSIX_C_SOURCE<200112L
#undef _POSIX_C_SOURCE
#endif
#endif

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 500
#endif

#ifndef _XOPEN_SOURCE_EXTENDED
#define _XOPEN_SOURCE_EXTENDED
#endif

#ifndef _ISOC99_SOURCE
#define _ISOC99_SOURCE
#endif

#ifndef _REENTRANT
#define _REENTRANT
#endif

#ifndef _THREAD_SAFE
#define _THREAD_SAFE
#endif

#if _FILE_OFFSET_BITS<64
#error You must define _FILE_OFFSET_BITS>=64 in your Makefile
#endif

///////////////////////////////////////////////////////////////////////////////
// Character types

namespace positix { typedef wchar_t UnicodeChar; }

///////////////////////////////////////////////////////////////////////////////
// integer types

#include <stdint.h>

namespace positix_types {

typedef int_least8_t    si8;
typedef int_least16_t  si16;
typedef int_least32_t  si32;
typedef int_least64_t  si64;
typedef uint_least8_t   su8;
typedef uint_least16_t su16;
typedef uint_least32_t su32;
typedef uint_least64_t su64;

typedef int_fast8_t    fi8;
typedef int_fast16_t  fi16;
typedef int_fast32_t  fi32;
typedef int_fast64_t  fi64;
typedef uint_fast8_t   fu8;
typedef uint_fast16_t fu16;
typedef uint_fast32_t fu32;
typedef uint_fast64_t fu64;

}

///////////////////////////////////////////////////////////////////////////////
// Multithreading

#include "pthread.hh"

#endif


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: gnucc.hh
