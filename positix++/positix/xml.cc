/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix++
// File  : xml.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "xml.hh"
#include "bytes.hh"
#include "strings.hh"
#include "msg.hh"

using namespace std;

// TODO: end-of-line conversion
// TODO: byte order mark detection
// TODO: Store start of lengthy constructs for error messages.
// TODO: Restrict memory consumption on long strings (names, comments, text, ...)
// TODO: More efficient use of memory (strings are constantly allocated and deallocated, though only a few will be used simultaneously)

namespace positix {

///////////////////////////////////////////////////////////////////////////////

bool IsXmlSpace(XmlChar c) throw ()
{
    switch ( c )
    {
    default  : return false;
    case 0x20:
    case 0x09:
    case 0x0d:
    case 0x0a: return true;
    }
}

///////////////////////////////////////////////////////////////////////////////

bool IsXmlNameStartChar(XmlChar c) throw ()
{
#define u ((unsigned wchar_t)c)
    return (u>=97 && u<=122)        // a-z
	|| (u>=65 && u<=90)         // A-Z
	|| u==58                    // :
	|| u==95                    // _
	|| (u>=0x00c0 && u<=0x00d6) // XML Spec 1.1, Section 2.3
	|| (u>=0x00d8 && u<=0x00f6) 
	|| (u>=0x00f8 && u<=0x02ff) 
	|| (u>=0x0370 && u<=0x037d) 
	|| (u>=0x037f && u<=0x1fff) 
	|| (u>=0x200c && u<=0x200d) 
	|| (u>=0x2070 && u<=0x218f) 
	|| (u>=0x2c00 && u<=0x2fef) 
	|| (u>=0x3001 && u<=0xd7ff) 
	|| (u>=0xf900 && u<=0xfdcf) 
	|| (u>=0xfdf0 && u<=0xfffd) 
	|| (u>=0x10000 && u<=0xeffff);
#undef u
}

///////////////////////////////////////////////////////////////////////////////

bool IsXmlNameChar(XmlChar c) throw ()
{
#define u ((unsigned wchar_t)c)
    return IsXmlNameStartChar(u) 
	|| (u>=48 && u<=59)         // 0-9
	|| u==46		    // .
	|| u==45		    // -
	|| u==0xb7                  // XML Spec 1.1, Section 2.3
	|| (u>=0x0300 && u<=0x36f)
	|| (u>=0x203f && u<=0x2040);
#undef u
}

///////////////////////////////////////////////////////////////////////////////

// XML 1.1, Appendix E: Autodetection of Character Encodings
// As supported by positix++:
// 00 00 00 3C UCS-4BE
// 00 00 FE FF UCS-4BE  (BOM)
// 00 3C 00 3F UTF-16BE / UCS2-BE
// 3C 00 00 00 UCS-4LE
// 3C 00 3F 00 UTF-16LE / UCS2-LE
// 3C 3F 78 6D UTF-8, ISO 646, ASCII, some part of ISO 8859, Shift-JIS, EUC
// EF BB BF xx UTF-8    (BOM)
// FE FF xx xx UTF-16BE (BOM)
// FF FE 00 00 UCS-4LE  (BOM)
// FF FE xx xx UTF-16LE (BOM)

void XmlTokenizer::guessInputEncoding()
{
    istream::int_type i = _in->get(); // Byte 0

    if ( i==0x00 ) // 00 ...
    {
	// 00 00 00 3C UCS-4BE
	// 00 00 FE FF UCS-4BE  (BOM)
	// 00 3C 00 3F UTF-16BE / UCS2-BE
	i = _in->get();  // Byte 1
	if ( i==0x00 ) // 00 00 ...
	{
	    // 00 00 00 3C UCS-4BE
	    // 00 00 FE FF UCS-4BE  (BOM)
	    XmlChar c = ExtractUCS2BE(*_in); // Bytes 2, 3
	    if ( c==0xFEFF || c==0x3C ) // 00 00 FE FF ... | 00 00 00 3C ...
	    {
		setInputEncoding(TextCodingUCS4BE);
		_putback = i;
		return;
	    }
	}
	else if ( i==0x3C ) // 00 3C ...
	{
	    // 00 3C 00 3F UTF-16BE / UCS2-BE
	    setInputEncoding(TextCodingUTF16BE);
	    _putback = i;
	    return;
	}
    }
    else if ( i==0x3c ) // 3C ...
    {
	// 3C 00 00 00 UCS-4LE
	// 3C 00 3F 00 UTF-16LE / UCS2-LE
	// 3C 3F 78 6D UTF-8, ISO 646, ASCII, ...
	i = _in->get(); // Byte 1
	if ( i==0x00 ) // 3C 00 ...
	{
	    // 3C 00 00 00 UCS-4LE
	    // 3C 00 3F 00 UTF-16LE / UCS2-LE
	    i = _in->get(); // Byte 2
	    if ( i==0x00 ) // 3C 00 00 ...
	    {
		if ( _in->get() == 0x00 ) // Byte 3
		{
		    setInputEncoding(TextCodingUCS4LE);
		    _putback = 0x3c;
		    return;
		}
	    }
	    else  // 3C 00 ... except 3C 00 3F ...
	    {
		_in->putback(i); // Return Byte 2 to stream
		// 3C 00 3F 00 UTF-16LE / UCS2-LE
		// Assume UTF-16LE
		setInputEncoding(TextCodingUTF16LE);
		_putback = 0x3c;
		return;
	    }
	}
	else if ( i==0x3F                 // 3C 3F ... ASCII of <? ....
	       || (i>=0 && i<=255 
		   && ( (i & 0x80)==0x00
		     || (i & 0xE0)==0xC0
		     || (i & 0xF0)==0xE0
		     || (i & 0xF8)==0xF0))
	        )
	{
	    // Assume UTF-8
	    _in->putback(i); // Return Byte 1 to stream
	    setInputEncoding(TextCodingUTF8);
	    _putback = 0x3C;
	    return;
	}
    }
    else if ( i==0xEF ) // EF ...
    {
	// EF BB BF xx UTF-8    (BOM)
	if ( _in->get()==0xBB ) // EF BB ...
	{
	    if ( _in->get()==0xBF ) // EF BB BF ...
	    {
		// BOM character. Ignore it.
		setInputEncoding(TextCodingUTF8);
		return;
	    }
	}
    }
    else if ( i==0xFE ) // FE ..
    {
	// FE FF xx xx UTF-16BE (BOM)
	if ( _in->get()==0xFF ) // FE FF ...
	{
	    setInputEncoding(TextCodingUTF16BE);
	    return;
	}
    }
    else if ( i==0xFF ) // FF ...
    {
	// FF FE 00 00 UCS-4LE  (BOM)
	// FF FE xx xx UTF-16LE (BOM)

	if ( _in->get()==0xFE ) // FF FE
	{
	    XmlChar c = ExtractUCS2LE(*_in); // Bytes 2, 3
	    if ( c==0x0000 ) // FF FE 00 00
	    {
		setInputEncoding(TextCodingUCS4LE);
		return;
	    }
	    else
	    {
		setInputEncoding(TextCodingUTF16LE);
		_putback = c;
		return;
	    }
	}
    }
    else // Anything else: Just try UTF-8...
    {
	_in->putback(i);
	setInputEncoding(TextCodingUTF8);
	return;
    }

    throw XmlSyntaxError(
	Msg(TXT("%0: Unrecognized or unsupported text encoding"),
	    tfp()));
}

///////////////////////////////////////////////////////////////////////////////

void XmlTokenizer::setInputEncoding(TextCoding te)
{
    if ( te<TextCodingUTF8 || te>TextCodingUCS4BE )
	throw Error(Msg(TXT("XML parser cannot read %0"), String(te)));
    _tc     = te;
    _decode = TheInStreamGlyphExtractorFunctions[te];
}

///////////////////////////////////////////////////////////////////////////////

void XmlTokenizer::setInputEncoding(const std::string& enc)
{
    const char* s = String(_tc);
    size_t L = LengthOfCommonPrefix(String(_tc), enc.c_str());
    if ( L!=enc.length() )
    {
	if ( L==0 )
	    throw XmlSyntaxError(
		Msg(TXT("%0: Unsupported character encoding %1"), tfp(), enc));
	if ( strcmp(s+L, "LE")!=0 && strcmp(s+L, "BE")!=0 )
	{
	    throw XmlSyntaxError(
		Msg(TXT("%0: Inconsistent encoding declaration %1."
			" XML Entity encoding detected: %2."), 
		    tfp(), enc, s));
	}
    }
}

///////////////////////////////////////////////////////////////////////////////

void XmlTokenizer::parse(
    std::istream&   in, 
    XmlBasicParser& parser,
    const char*     name,
    TextCoding    te)
{
    try
    {
	_lastCol       = 1;
	_in            = &in;
	_parser        = &parser;
	_putback       = 0;
	_tfp.line      = 1;
	_tfp.column    = 1;
	_tfp.filename  = name;
	parser._tokenizer = this;

	if ( te==TextCodingUndefined || te==TextCodingDynamic )
	    guessInputEncoding();
	else
	    setInputEncoding(te);

	cerr << "TEST: TextCoding=" << String(_tc) << endl;

	XmlChar c = getChar();
	if ( c!=UnicodeBom )
	    putback(c);

	_parser->reset();

	bool cont = true;
	do
	{
	    c = getChar();
	    if ( c==0 )
		break;
	
	    if ( c == LessThanSign )
	    {	
		c = getChar();
		if ( c == ExclamationMark )
		{
		    cont = parseExclamation();
		}
		else if ( c == QuestionMark )
		{
		    cont = parseQuestion();
		}
		else if ( c == Slash )
		{
		    cont = parseEndTag();
		}
		else if ( IsXmlNameStartChar(c) )
		{
		    putback(c);
		    cont = parseStartTag();
		}
		else if ( c==0 )
		{
		    throw XmlSyntaxError(
			Msg(TXT("%0: End of file after '<'"),
			    tfp()));
		}
		else
		{
		    throw XmlSyntaxError(
			Msg(TXT("%0: Invalid character after '<'"),
			    tfp()));
		}
	    }
	    else
	    {
		putback(c);
		cont = parseText();
	    }
	} while ( cont );
    }
    catch ( ... )
    {
	parser._tokenizer = 0;
	throw;
    }

    parser._tokenizer = 0;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::skipSpace() throw ()
{   
    for(;;)
    {
	XmlChar c = getChar();
	if ( c==0 )
	    return false;
	if ( !IsXmlSpace(c) )
	{
	    putback(c);
	    break;
	}
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////

std::string XmlTokenizer::readName(bool f)
{
    XmlChar c = getChar();
    if ( c!=0 )
    {
	if ( !IsXmlNameStartChar(c) )
	{
	    putback(c);
	}
	else
	{
	    std::string name;
	    name.reserve(64);
	    name = c;
	    for(;;)
	    {
		c = getChar();
		if ( !IsXmlNameChar(c) )
		{
		    putback(c);
		    break;
		}
		name += c; // TODO: Encode to UTF8
	    }
	    
	    return name;
	}
    }

    if ( f )
	throw XmlSyntaxError(Msg(TXT("%0: name expected"), tfp()));

    return std::string();
}

///////////////////////////////////////////////////////////////////////////////

XmlSymbolId XmlTokenizer::readSymbol(bool f)
{
    std::string name = readName(f);
    return name.length() > 0 ? _parser->symbolId(name) : 0;
}

///////////////////////////////////////////////////////////////////////////////

std::string XmlTokenizer::readQuotedString()
{
    XmlChar quote = getChar();
    if ( quote!=DoubleQuotes && quote!=SingleQuotes )
    {
	throw XmlSyntaxError(Msg(TXT("%0: Invalid quote character %1"), 
				 tfp(), quote));
    }
    std::string str;
    for(;;)
    {
	XmlChar c = getChar();
	if ( c==0 )
	{
	    throw XmlSyntaxError(
		Msg(TXT("%0: Unexpected end of file: Pending quoted string"),
		    tfp()));
	}
	if ( c==quote )
	    break;
	// TODO: Check for & % --> references
	AppendUTF8(str, c);
    }

    return str;
}

///////////////////////////////////////////////////////////////////////////////

void XmlTokenizer::parseAttrList(XmlAttrList& attrList)
{
    while ( skipSpace() )
    {
	XmlSymbolId sid = readSymbol(false);
	if ( !sid )
	    break;
	XmlChar c = getChar();
	if ( c!=EqualSign )
	    XmlSyntaxError(Msg(TXT("%0: '=' expected"), tfp()));
	std::string value( readQuotedString() );
	attrList.push_back(XmlAttr(sid, value));
    }
}

///////////////////////////////////////////////////////////////////////////////

bool  XmlTokenizer::parseStartTag()
{
    // Precondition: input marker beyond '<' but before first name character
    XmlSymbolId id = readSymbol(true);
    XmlAttrList attrs;
    parseAttrList(attrs);
    XmlChar c = getChar();
    if ( c == Slash )
    {
	c = getChar();
	if ( c != GreaterThanSign )
	    throw XmlSyntaxError(Msg(TXT("%0: '>' expected after '/'"),tfp()));
	return _parser->startTag(id, attrs, true)
	    && _parser->endTag(id);
    }
    else if ( c==GreaterThanSign )
    {
	return _parser->startTag(id, attrs, false);
    }
    else
    {
	throw XmlSyntaxError(Msg(TXT("%0: '>' expected"), tfp()));
    }
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseEndTag()
{
    // Precondition: input marker beyond "</" but before first name character
    XmlSymbolId id = readSymbol(true);
    XmlChar c = getChar();
    if ( c==GreaterThanSign )
    {
	return _parser->endTag(id);
    }
    else
    {
	throw XmlSyntaxError(Msg(TXT("%0: '>' expected"), tfp()));
    }
}

///////////////////////////////////////////////////////////////////////////////

string XmlTokenizer::readAssignment()
{
    skipSpace();
    if ( getChar()!=EqualSign )
	throw XmlSyntaxError(Msg(TXT("%0: '=' expected"), tfp()));
    skipSpace();
    return readQuotedString();
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseXmlDecl()
{
    // Precondition: input marker beyond "<?xml"
    // VersionInfo (required)
    XmlFileInfo fi;

    skipSpace(); // Note: violation of XML Spec (no space allowed)
    string str = readName(false);
    if ( str != "version" )
    {
	throw XmlSyntaxError(
	    Msg(TXT("%0: Version specification required"), tfp()));
    }
    str = readAssignment();
    fi.version.major = 1;
    if ( str=="1.0" )
	fi.version.minor = 0;
    else if ( str=="1.1" )
	fi.version.minor = 1;
    else
    {
	throw XmlSyntaxError(
	    Msg(TXT("%0: Unsupported XML version %1"), tfp(), str));
    }

    // EncodingDecl (optional)
    skipSpace();
    str = readName(false);
    if ( str=="encoding" )
    {
	str = readAssignment();
	setInputEncoding(str);
	str = readName(false); // fill str with potential next key value
    }
    if ( str=="standalone" )
    {
	str = readAssignment();
	if ( str=="yes" )
	    fi.standAlone = true;
	else if ( str=="no" )
	    fi.standAlone = false;
    }

    skipSpace();
    if ( getChar()!=QuestionMark || getChar()!=GreaterThanSign )
	throw XmlSyntaxError(Msg(TXT("%0: '?>' expected"), tfp()));

    fi.coding = _tc;
    return _parser->xmlFileInfo(fi);
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseQuestion()
{
    // Precondition: input marker beyond "<?" but before first name character
    std::string str = readName(true);
    if ( str == "xml" )
    {
	return parseXmlDecl();
    }
    else
    {
	XmlSymbolId id = _parser->symbolId(str);
	XmlChar     c;
	TextPosition start = tfp();
	str.erase();

	for(;;)
	{
	    c = getChar();
	    if ( c==0 )
		return true;
	    if ( c == QuestionMark )
	    {
		c = getChar();
		if ( c == GreaterThanSign )
		    return _parser->processingInstruction(id, str);
		str += char(QuestionMark);
	    }
	    AppendUTF8(str, c);
	}
	throw XmlSyntaxError(
	    Msg(TXT("%0: '<?' beginning at %1 not terminated with '?>'"),
		tfp(), start));
    }
}

///////////////////////////////////////////////////////////////////////////////

void XmlTokenizer::putback(UnicodeChar c) throw ()
{
    _putback = c;
    if ( c==0x0A )
    {
	--_tfp.line;
	_tfp.column = _lastCol;
    }
    else
    {
	--_tfp.column;
    }
}

///////////////////////////////////////////////////////////////////////////////

XmlChar XmlTokenizer::extract()
{
    istream::int_type i = _in->peek();
    if ( i==istream::traits_type::eof() )
	return UnicodeEof;
    UnicodeChar c = _decode(*_in);
    if ( _in->bad() )
	throw XmlSyntaxError(Msg(TXT("%0: Invalid byte sequence"), tfp()));
    return c;
}

///////////////////////////////////////////////////////////////////////////////

XmlChar XmlTokenizer::getChar()
{
    XmlChar c;

    if ( _putback )
    {
	c = _putback;
	_putback = 0;
    }
    else
    {
	c = extract();
    }

    if ( c==UnicodeEof )
	return XmlChar(0);

    if ( c==0 )
    {
	throw XmlSyntaxError(
	    Msg(TXT("%0: character with code zero not allowed in XML"),tfp()));
    }

    // End-of-Line Handling (cf. XML 1.1, Section 2.11)
    if ( c==0xD )
    {
	c = extract();
	if ( c!=UnicodeEof && c!=0xA && c!=0x85 )
	    putback(c);
	c = 0xA;
    }
    else if ( c==0x85 || c==0x2028 )
	c = 0xA;

    if ( c==0xA )
    {
	++_tfp.line;
	_lastCol = _tfp.column;
	_tfp.column = 1;
    }
    else
    {
	++_tfp.column;
    }

    return c;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseText()
{
    // Note: In contrast to XML 1.1 specs, the sequence ]]> will be allowed.
    std::string str;
    bool preserveSpace = _parser->preserveWhitespace();
    
    for(;;)
    {
	XmlChar c = getChar();	
	if ( c == 0 )
	    break;
	if ( c == LessThanSign )
	{
	    putback(c);
	    break;
	}
	else if ( c == Ampersand )
	{
	    // TODO: read reference
	    str += c; // TODO: incorrect!
	}
	else
	{
	    if ( preserveSpace || !IsXmlSpace(c) 
		 || (str.length()>0 && str[str.length()-1]!=' ') )
	    {
		AppendUTF8(str, c);
	    }
	}
    }

    if ( !preserveSpace )
	Trim(str);

    return (str.length() > 0)
	? _parser->text(str)
	: false;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseCData()
{
    // Terminator sequence: ]]>
    TextPosition tp = tfp();
    std::string text;
    unsigned  nBrackets = 0;
    for(;;)
    {
	XmlChar c = getChar();
	switch ( c )
	{
	default:
	    while ( nBrackets )
	    {
		text += char(CloseBracket);
		--nBrackets;
	    }
	    AppendUTF8(text, c);
	    nBrackets = 0;
	    break;
	    
	case 0:
	    throw XmlSyntaxError(
		Msg(TXT("%0: <![CDATA[ startet at %1 is not terminated"),
		    tfp(), tp));

	case CloseBracket:
	    if ( nBrackets<2 )
		++nBrackets;
	    break;

	case GreaterThanSign:
	    if ( nBrackets>=2 )
		goto endLoop;
	}
    }

 endLoop:
    return _parser->cdata(text);
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseDocTypeDecl()
{
    return parseUnknownDecl();
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseEntityDecl()
{
    return parseUnknownDecl();
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseElementDecl()
{
    return parseUnknownDecl();
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseAttListDecl()
{
    return parseUnknownDecl();
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseNotationDecl()
{
    return parseUnknownDecl();
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseUnknownDecl()
{
    throw Error(TXT("positix++ doesn't handle declarations yet"));
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseDecl()
{
    std::string name = readName(true);

    if ( name == "DOCTYPE" )
    {
	return parseDocTypeDecl();
    }
    else if ( name == "ENTITY" )
    {
	return parseEntityDecl();
    }
    else if ( name == "ELEMENT" )
    {
	return parseElementDecl();
    }
    else if ( name == "ATTLIST" )
    {
	return parseAttListDecl();
    }
    else if ( name == "NOTATION" )
    {
	return parseNotationDecl();
    }
    else
    {
	return parseUnknownDecl();
    }
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseComment()
{
    // Precondition: input marker beyond "<!-"
    TextPosition tp = tfp();
    XmlChar c = getChar();
    if ( c!=Hyphen )
	throw XmlSyntaxError(Msg(TXT("%0: Invalid start of comment"), tfp()));

    std::string comment;
    unsigned  nHyphens = 0;
    for(;;)
    {
	c = getChar();
	switch ( c )
	{
	default:
	    while ( nHyphens )
	    {
		comment += char(Hyphen);
		--nHyphens;
	    }
	    AppendUTF8(comment, c);
	    nHyphens = 0;
	    break;

	case 0:
	    throw XmlSyntaxError(
		Msg(TXT("%0: Comment startet at %1 is not terminated"),
		    tfp(), tp));

	case Hyphen:
	    if ( nHyphens<2 )
		++nHyphens;
	    break;

	case GreaterThanSign:
	    if ( nHyphens>=2 )
		goto endLoop;
	}
    }

 endLoop:
    return _parser->comment(comment);
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseBracket()
{
    // Precondition: input marker beyond "<!["
    // Possible continuations in XML 1.1:
    // <![CDATA[
    // <![.INCLUDE.[
    // <![.IGNORE.[
    // Here, the dot denotes an arbitrary amount of xml-whitespace
    // Hence, we will use all text up to the next [ to identify the statement.

    if ( !_in->eof() )
    {
	XmlChar c        = peek();
	bool    hasSpace = false;
	if ( IsXmlSpace(c) )
	{
	    hasSpace = true;
	    skipSpace();
	}
	std::string name = readName(true);	
	if ( !_in->eof() )
	{
	    c = peek();
	    if ( IsXmlSpace(c) )
	    {
		hasSpace = true;
		skipSpace();
	    }
	    c = getChar();

	    if ( c==OpenBracket )
	    {
		if ( name=="CDATA" )
		{
		    if ( hasSpace )
		    {
			throw XmlSyntaxError(
			    Msg(TXT("%0: Invalid CDATA declaration"), tfp()));
		    }
		    return parseCData();
		}
		else if ( name=="IGNORE" || name=="INCLUDE" ) 
		{
		    throw XmlSyntaxError(
			Msg(TXT("%0: positix++ cannot handle <![%1[ yet"), 
			    tfp(), name));
		}
		else
		{
		    throw XmlSyntaxError(
			Msg(TXT("%0: Invalid bracket declaration"), tfp()));
		}
	    }
	}
    }

    throw XmlSyntaxError(Msg(TXT("%0: '[' expected"), tfp()));
}

///////////////////////////////////////////////////////////////////////////////

bool XmlTokenizer::parseExclamation()
{
    // Precondition: input marker beyond "<!" but before first name character
    switch ( getChar() )
    {
    default         : return parseDecl();    break; // <!name
    case Hyphen     : return parseComment(); break; // <!--
    case OpenBracket: return parseBracket(); break; // <![CDATA, <![INCLUDE,...
    }
}

///////////////////////////////////////////////////////////////////////////////

void XmlBasicParser::reset()
{
    // nothing to do
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::preserveWhitespace() const throw ()
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::xmlFileInfo(const XmlFileInfo& /*xfi*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::startTag(
    XmlSymbolId /*id*/, const XmlAttrList& /*attrs*/, bool)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::endTag(XmlSymbolId /*id*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::processingInstruction(
    XmlSymbolId      /*id*/, 
    const std::string& /*instr*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::comment(const std::string& /*str*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::text(const std::string& /*str*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlBasicParser::cdata(const std::string& str)
{
    return text(str);
}

///////////////////////////////////////////////////////////////////////////////

void XmlParser::reset()
{
    _openTags.clear();
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::endTag(XmlSymbolId id)
{
    if ( _openTags.size()==0 )
    {
	throw XmlParserError(Msg(TXT("%0: No pending start tag"), tfp()));
    }

    OpenTag& ot = _openTags.back();
    
    if ( ot.tag!=id )
    {
	throw XmlParserError(
	    Msg(TXT("%0: Attempt to close tag %1 at %2 with %3"),
		tfp(),
		symbolName(ot.tag),
		ot.pos,
		symbolName(id)));
    }
    

    ot.handler->endTag(*this, ot.value, id);
    _openTags.pop_back();
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::processingInstruction(XmlSymbolId id, const std::string& instr)
{
    if ( level() )
    {
	OpenTag& ot = _openTags.back();
	return ot.handler->processingInstruction(*this, ot.value, id, instr);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::comment(const std::string& str)
{
    if ( level() )
    {
	OpenTag& ot = _openTags.back();
	return ot.handler->comment(*this, ot.value, str);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::text(const std::string& str)
{
    if ( level() )
    {
	OpenTag& ot = _openTags.back();
	return ot.handler->text(*this, ot.value, str);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::cdata(const std::string& str)
{
    if ( level() )
    {
	OpenTag& ot = _openTags.back();
	return ot.handler->cdata(*this, ot.value, str);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::defaultStartTag(
    XmlSymbolId        id, 
    const XmlAttrList& /*attrs*/, 
    bool               /*empty*/)
{
    push(id, &TheStandardXmlHandler, 0);
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::defaultProcessingInstruction(
    XmlSymbolId        /*id*/,
    const std::string& /*instr*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::defaultComment(const std::string& /*str*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::defaultText(const std::string& /*str*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlParser::defaultCdata(const std::string& /*str*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlHandler::StartTag(
    XmlParser&         parser,
    void*	       /*state*/,
    XmlSymbolId        id, 
    const XmlAttrList& /*attrs*/, 
    bool               /*empty*/)
{
    parser.push(id, &TheStandardXmlHandler, 0);
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlHandler::EndTag(
    XmlParser&  /*parser*/,
    void*       /*state*/,
    XmlSymbolId /*id*/)
{
    return true;
}


///////////////////////////////////////////////////////////////////////////////
    
bool XmlHandler::ProcessingInstruction(
    XmlParser&         /*parser*/,
    void*	       /*state*/,
    XmlSymbolId        /*id*/, 
    const std::string& /*instr*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlHandler::Comment(
    XmlParser&         /*parser*/,
    void*	       /*state*/,
    const std::string& /*str*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool XmlHandler::Text(
    XmlParser&         /*parser*/,
    void*	       /*state*/,
    const std::string& /*str*/)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////

XmlHandler TheStandardXmlHandler =
{
    &XmlHandler::StartTag,
    &XmlHandler::EndTag,
    &XmlHandler::ProcessingInstruction,
    &XmlHandler::Comment,
    &XmlHandler::Text,
    &XmlHandler::Text
};

///////////////////////////////////////////////////////////////////////////////
} // namespace positix

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix++: xml.hh
