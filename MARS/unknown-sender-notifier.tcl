#!/usr/bin/tclsh
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

package require Tcl
package require mime
package require smtp

set InPort 9999
set Domain positix.localdomain

set Subject "Mailbox registration required"
set Body {
Dear owner of $sender,

You tried to send a message to following recipients:
    [join $recipients "\n    "]
But in our fight against spam-mails, we only accept known senders.

The mail you have sent was stored and will be deleted within 7 days.

You can register your mail address and make your mail delivered 
by visiting following web page
    http://xxx.institution.tld/mail-registration/[file tail $file].
}

proc getRecipients {file} {

    set recipients {}

    set fd [open $file]

    while {[gets $fd line]>=0} {
	set word [lindex $line 0]
	if {[string compare -nocase $word "DATA"]==0} break
	if {[string compare -nocase $word "RCPT"]==0} {
	    set L [string first < $line]
	    incr L
	    set R [string last  > $line]
	    incr R -1
	    lappend recipients [string range $line $L $R]
	}
    }

    close $fd

    return $recipients
}

proc notifySender {file sender} {

    puts "File       $file"
    puts "Sender     $sender"

    set recipients [getRecipients $file]

    set token [mime::initialize -canonical text/plain \
		   -string [subst $::Body]]
    mime::setheader $token Subject $::Subject
    mime::setheader $token To      $sender
    set r [smtp::sendmessage $token \
                             -originator postmaster@$::Domain \
	                     -recipients $sender]
    mime::finalize $token
    if {$r==""} {
	return "OK"
    } else {
	return "KO: $r"
    }
}

proc servant {channel clientAddr clientPort} {

    puts "Notfication request: $clientAddr $clientPort"

    set file       ""
    set sender     ""

    fconfigure $channel -translation crlf

    while {[gets $channel line]>=0} {
	if {$line==""} break
	set key   [lindex $line 0]
	set value [string trim [lreplace $line 0 0]]
	switch -exact -- $key {
	    file      {set file           $value}
	    sender    {set sender         $value}
	}
    }

    if {$sender==""} {
	puts $channel "Empty sender"
    } elseif { $file==""} {
	puts $channel "Empty sender"
    } else {
	puts $channel [notifySender $file $sender]
	close $channel
    }
}

puts "Starting Server on localhost:$InPort"
socket -server servant -myaddr localhost $InPort
vwait WaitVar
puts "Server terminated"
