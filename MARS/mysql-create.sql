/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
CREATE DATABASE IF NOT EXISTS pxmars;

USE pxmars;

-- Status codes:
--    black   0
--    white   1
--    open    2
--    private 3


CREATE TABLE IF NOT EXISTS Extern
(
suffix             VARCHAR(255)          NOT NULL,
status             SMALLINT              NOT NULL DEFAULT 0,
insertionTime      TIMESTAMP,
initiator          VARCHAR(255),
PRIMARY KEY (suffix)
);

CREATE TABLE IF NOT EXISTS Intern
(
name               VARCHAR(255)          NOT NULL,
status             SMALLINT              NOT NULL DEFAULT 4,
alias              VARCHAR(255)                   DEFAULT NULL,
PRIMARY KEY (name)
);
