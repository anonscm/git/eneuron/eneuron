#!/bin/sh
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.
###############################################################################
# Module: positix MARS
# File  : pxmars_permissions.sh
###############################################################################
# Version: 1.0.0
###############################################################################
# Sets the owner and permissions of the positix MARS files
###############################################################################

set -e

###############################################################################

if [ -z "$1" ] ; then
    echo Usage: $0 '<user>' '<group>'
    echo Set the permissions of the installed positix MARS files.
    echo All files get '<user>':'<group>' \
	 as their respective owner where needed
    exit
fi


###############################################################################

files=$(rpm -ql positix_mars)

for file in $files; do
    case $(basename $file) in
	pxmarsd | pxmars | notify-unknown-sender.sh)
	    chown $1:$2 $file
	    chmod 550 $file
	    changed="$changed $file"
	    ;;

	errmail-address-headers)
	    chown $1:$2 $file
	    chmod 640 $file
	    changed="$changed $file"
	    ;;

	pxmars.conf)
	   chown $1:$2 $file
	   chmod 640 $file
	   changed="$changed $file"
	   cp $file $file~
	   sed \
	   -e "s/^[[:space:]]*user[[:space:]]\+[^[:space:]]*$/user $1/g" \
           -e "s/^[[:space:]]*group[[:space:]]*[^[:space:]]*$/group $2/g" \
	   -e "s/^[[:space:]]*db-user[[:space:]]*[^[:space:]]*$/db-user $1/g" \
	   $file~ > $file
	   conf=$file
	   ;;
    esac
done
    
# Permission of data directory
dir=$(rpm -q  positix_mars --qf '[%{PREFIXES} %{INSTPREFIXES}\n]'\
      | grep /var/opt/positix/mars \
      | sed -e 's:/var/opt/positix/mars ::g')

echo TEST: dir=$dir
if [ -n "$dir" ] ; then
    chown $1:$2 "$dir"
    chmod 750 $dir
    chown $1:$2 $dir/* || true
    chmod 750 $dir/*   || true
    changed="$changed $dir"
fi

###############################################################################

if [ -z "$changed" ] ; then
    echo No permission changed
else
    echo Permission changed:
    ls -ld $changed
    if [ -n "$conf" ] ; then
	echo $conf was modified automatically. Please check!
	echo The original file was stored in $conf~
    fi
fi

# end of pxmars_permissions.sh
