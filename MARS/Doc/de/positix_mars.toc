\select@language {german}
\contentsline {chapter}{\numberline {1}Einf\"uhrung}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Erw\"unschte Mails}{8}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Offene Mailkonten}{8}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Aktive Absenderfreischaltung}{9}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Zusammenfassung}{9}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Gef\"alschte Absenderadressen}{9}{section.1.2}
\contentsline {section}{\numberline {1.3}Leere Absenderadressen}{10}{section.1.3}
\contentsline {section}{\numberline {1.4}Das Mail-System}{11}{section.1.4}
\contentsline {section}{\numberline {1.5}Die lokale Sicht}{12}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Verwendung lokaler Mailserver}{12}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Erweiterung durch positix MARS}{13}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Verwendung externer Mailserver}{14}{subsection.1.5.3}
\contentsline {chapter}{\numberline {2}Funktionsweise}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Mailadressen}{17}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Klassifikation von Mailadressen}{18}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Interne Mailadressen}{19}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}Kontextfreie Klassifikation interner Mailadressen}{20}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}Muster f\"ur private Mailadressen}{21}{subsubsection.2.1.2.2}
\contentsline {subsection}{\numberline {2.1.3}Externe Adressbezeichner}{21}{subsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.3.1}Kontextfreie Klassifikation externer Mailadressen}{22}{subsubsection.2.1.3.1}
\contentsline {section}{\numberline {2.2}Leeres Absenderfeld bei eingehenden Mails}{22}{section.2.2}
\contentsline {section}{\numberline {2.3}Mailversand}{23}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Absenderklassifikation}{25}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Empf\"angerklassifikation}{26}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}\"Ubertragung des Mailinhaltes}{26}{subsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.3.1}Ausgehende Mail}{27}{subsubsection.2.3.3.1}
\contentsline {subsubsection}{\numberline {2.3.3.2}Eingehende Mail mit bekanntem Absender}{27}{subsubsection.2.3.3.2}
\contentsline {subsubsection}{\numberline {2.3.3.3}Eingehende Mail mit unbekanntem Absender}{27}{subsubsection.2.3.3.3}
\contentsline {subsubsection}{\numberline {2.3.3.4}Eingehende Mail mit leerem Absender}{27}{subsubsection.2.3.3.4}
\contentsline {subsection}{\numberline {2.3.4}Weiterleitung}{28}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Sicherung des Mailverkehrs}{29}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Sicherungsdateien}{30}{subsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.1.1}Dateinamen}{30}{subsubsection.2.4.1.1}
\contentsline {subsubsection}{\numberline {2.4.1.2}Dateiformat}{30}{subsubsection.2.4.1.2}
\contentsline {section}{\numberline {2.5}Zusammenfassung}{32}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Ausgehende Mails}{32}{subsection.2.5.1}
\contentsline {paragraph}{Beispiel.}{32}{paragraph*.2}
\contentsline {subsection}{\numberline {2.5.2}Eingehende Mails}{33}{subsection.2.5.2}
\contentsline {paragraph}{Beispiel.}{34}{paragraph*.3}
\contentsline {paragraph}{Beispiel.}{34}{paragraph*.4}
\contentsline {chapter}{\numberline {3}Unbekannte Absender}{37}{chapter.3}
\contentsline {section}{\numberline {3.1}Offene Mailkonten}{38}{section.3.1}
\contentsline {section}{\numberline {3.2}Vorl\"aufige Akzeptanz unbekannter Absender}{39}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Benachrichtigung}{40}{subsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.1.1}Programm zur Benachrichtigung}{41}{subsubsection.3.2.1.1}
\contentsline {paragraph}{Beispiel.}{41}{paragraph*.5}
\contentsline {subsubsection}{\numberline {3.2.1.2}Implementation des Programms}{41}{subsubsection.3.2.1.2}
\contentsline {subsubsection}{\numberline {3.2.1.3}Serverdienst zur Benachrichtigung}{43}{subsubsection.3.2.1.3}
\contentsline {paragraph}{Beispiel.}{43}{paragraph*.6}
\contentsline {subsubsection}{\numberline {3.2.1.4}Implementation des Servers}{44}{subsubsection.3.2.1.4}
\contentsline {subsection}{\numberline {3.2.2}Zeitraum der Freischaltung}{46}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Freischaltung}{46}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Freischaltung mittels Programmaufruf}{47}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Verwendung in einem Webformular (CGI-Skript)}{47}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Freischaltung mittels Mail}{49}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}Konfiguration}{51}{chapter.4}
\contentsline {section}{\numberline {4.1}Konfigurationsgrundlagen}{51}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Konfigurationsdatei}{52}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Kommandozeilenoptionen}{53}{subsection.4.1.2}
\contentsline {paragraph}{Beispiele.}{53}{paragraph*.7}
\contentsline {section}{\numberline {4.2}Konfigurationsentscheidungen}{54}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Sicherung des Mailverkehrs}{54}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Zul\"assigkeit privater Mailadressen}{55}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Verwaltung erw\"unschter Absender}{56}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Datenbank-Konfiguration}{57}{section.4.3}
\contentsline {section}{\numberline {4.4}Konfigurationsvariablen}{58}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}accept-all-senders}{58}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}activation-mails}{59}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}activation-mail-redirect}{59}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}audit}{59}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}audit-file}{60}{subsection.4.4.5}
\contentsline {subsection}{\numberline {4.4.6}black-target-action}{60}{subsection.4.4.6}
\contentsline {subsection}{\numberline {4.4.7}cfg}{60}{subsection.4.4.7}
\contentsline {subsection}{\numberline {4.4.8}client-timeout}{61}{subsection.4.4.8}
\contentsline {subsection}{\numberline {4.4.9}daemon}{61}{subsection.4.4.9}
\contentsline {subsection}{\numberline {4.4.10}db-host}{61}{subsection.4.4.10}
\contentsline {subsection}{\numberline {4.4.11}db-name}{61}{subsection.4.4.11}
\contentsline {subsection}{\numberline {4.4.12}db-user}{62}{subsection.4.4.12}
\contentsline {subsection}{\numberline {4.4.13}db-pass}{62}{subsection.4.4.13}
\contentsline {subsection}{\numberline {4.4.14}domain}{62}{subsection.4.4.14}
\contentsline {subsection}{\numberline {4.4.15}errmail-recipient}{62}{subsection.4.4.15}
\contentsline {subsection}{\numberline {4.4.16}errmail-address-headers}{63}{subsection.4.4.16}
\contentsline {subsection}{\numberline {4.4.17}group}{63}{subsection.4.4.17}
\contentsline {subsection}{\numberline {4.4.18}idb-host}{64}{subsection.4.4.18}
\contentsline {subsection}{\numberline {4.4.19}idb-name}{64}{subsection.4.4.19}
\contentsline {subsection}{\numberline {4.4.20}idb-user}{64}{subsection.4.4.20}
\contentsline {subsection}{\numberline {4.4.21}idb-pass}{65}{subsection.4.4.21}
\contentsline {subsection}{\numberline {4.4.22}intern}{65}{subsection.4.4.22}
\contentsline {paragraph}{Beispiel.}{65}{paragraph*.8}
\contentsline {subsection}{\numberline {4.4.23}listen}{65}{subsection.4.4.23}
\contentsline {subsection}{\numberline {4.4.24}log}{66}{subsection.4.4.24}
\contentsline {subsection}{\numberline {4.4.25}loglevel}{66}{subsection.4.4.25}
\contentsline {subsection}{\numberline {4.4.26}max-msg-size}{66}{subsection.4.4.26}
\contentsline {subsection}{\numberline {4.4.27}max-recipients-from-unknown}{67}{subsection.4.4.27}
\contentsline {subsection}{\numberline {4.4.28}max-threads}{67}{subsection.4.4.28}
\contentsline {subsection}{\numberline {4.4.29}notifier-account}{67}{subsection.4.4.29}
\contentsline {subsection}{\numberline {4.4.30}open-recipient}{67}{subsection.4.4.30}
\contentsline {paragraph}{Beispiel.}{68}{paragraph*.9}
\contentsline {subsection}{\numberline {4.4.31}private-allow}{68}{subsection.4.4.31}
\contentsline {subsection}{\numberline {4.4.32}private-prefix}{68}{subsection.4.4.32}
\contentsline {subsection}{\numberline {4.4.33}private-strip}{69}{subsection.4.4.33}
\contentsline {subsection}{\numberline {4.4.34}private-suffix}{69}{subsection.4.4.34}
\contentsline {subsection}{\numberline {4.4.35}server-incoming}{69}{subsection.4.4.35}
\contentsline {subsection}{\numberline {4.4.36}server-incoming-from}{70}{subsection.4.4.36}
\contentsline {subsection}{\numberline {4.4.37}server-outgoing}{70}{subsection.4.4.37}
\contentsline {subsection}{\numberline {4.4.38}server-outgoing-from}{70}{subsection.4.4.38}
\contentsline {subsection}{\numberline {4.4.39}server-timeout}{70}{subsection.4.4.39}
\contentsline {subsection}{\numberline {4.4.40}store-dir-in}{71}{subsection.4.4.40}
\contentsline {subsection}{\numberline {4.4.41}store-dir-out}{71}{subsection.4.4.41}
\contentsline {subsection}{\numberline {4.4.42}store-dir-unknown}{71}{subsection.4.4.42}
\contentsline {subsection}{\numberline {4.4.43}unknown-sender-notifier}{72}{subsection.4.4.43}
\contentsline {subsection}{\numberline {4.4.44}user}{72}{subsection.4.4.44}
\contentsline {subsection}{\numberline {4.4.45}tmpdir}{72}{subsection.4.4.45}
\contentsline {subsection}{\numberline {4.4.46}xdb-host}{73}{subsection.4.4.46}
\contentsline {subsection}{\numberline {4.4.47}xdb-name}{73}{subsection.4.4.47}
\contentsline {subsection}{\numberline {4.4.48}xdb-user}{73}{subsection.4.4.48}
\contentsline {subsection}{\numberline {4.4.49}xdb-pass}{73}{subsection.4.4.49}
\contentsline {chapter}{\numberline {5}Server}{75}{chapter.5}
\contentsline {section}{\numberline {5.1}Regul\"arer Prozess}{75}{section.5.1}
\contentsline {section}{\numberline {5.2}D\"amon-Prozess}{75}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Starten des Dienstes}{76}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}\"Uberpr\"ufung des Dienstes}{76}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Beendigung des Dienstes}{76}{subsection.5.2.3}
\contentsline {chapter}{\numberline {6}Dienstprogramme}{77}{chapter.6}
\contentsline {section}{\numberline {6.1}pxmars}{77}{section.6.1}
\contentsline {paragraph}{Beispiel.}{77}{paragraph*.10}
\contentsline {paragraph}{Beispiel.}{77}{paragraph*.11}
\contentsline {chapter}{\numberline {A}Installation}{79}{appendix.A}
\contentsline {section}{\numberline {A.1}Systemanforderungen}{79}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}Software}{79}{subsection.A.1.1}
\contentsline {subsection}{\numberline {A.1.2}Hardware}{79}{subsection.A.1.2}
\contentsline {section}{\numberline {A.2}Installationsverzeichnisse}{80}{section.A.2}
\contentsline {section}{\numberline {A.3}RPM Installationsskript}{80}{section.A.3}
\contentsline {section}{\numberline {A.4}Konfiguration}{80}{section.A.4}
\contentsline {subsection}{\numberline {A.4.1}Anpassung der SMTP-Server}{80}{subsection.A.4.1}
\contentsline {subsection}{\numberline {A.4.2}positix MARS}{81}{subsection.A.4.2}
\contentsline {subsection}{\numberline {A.4.3}Nutzerkonten}{82}{subsection.A.4.3}
\contentsline {subsection}{\numberline {A.4.4}Einrichtung der Datenbank}{82}{subsection.A.4.4}
\contentsline {subsection}{\numberline {A.4.5}Einrichtung des Hintergrunddienstes (D\"amon)}{83}{subsection.A.4.5}
\contentsline {section}{\numberline {A.5}Deinstallation}{83}{section.A.5}
