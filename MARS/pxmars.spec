#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.
%define platform %{_vendor}/{_arch}
%define _topdir %(echo $HOME)/out

Summary: positix Mail and Relay Service
Name: positix_mars
Version: %(source VERSION_PXMARS; echo $PXMARS_MAJOR.$PXMARS_MINOR.$PXMARS_PATCH| sed -e 's/-/beta/g')
Release: 1
Copyright: commercial
Group: System Environment/Daemons
URL: http://www.positix.com
Vendor: positix GmbH
Packager: positix GmbH
Requires: %(sh ../rpmplatform.sh)

%define pxbindir /opt/positix/bin
%define pxdocdir /opt/positix/doc
%define pxetcdir /opt/positix/etc
%define pxinitdir /etc/init.d
%define pxvardir /var/opt/positix/mars

# RPM_INSTALL_PREFIX0
Prefix: %{pxbindir}

# RPM_INSTALL_PREFIX1
Prefix: %{pxdocdir}

# RPM_INSTALL_PREFIX2
Prefix: %{pxetcdir}

# RPM_INSTALL_PREFIX3
Prefix: %{pxvardir}

# RPM_INSTALL_PREFIX4
Prefix: %{pxinitdir}

%description 

The "positix Mail And Relay Service" is basically a SMTP server supporting an
existing MTA in its fight against Spam and offers transparent mail storing
service.

This package mainly consists of the SMTP server program "pxmarsd" and a client
program "pxmars". The latter can be used to "replay" stored mails as well as
inserting stored but non-delivered mails from unknown senders.

This package was built for %{platform}.


%prep
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{pxbindir}
mkdir -p $RPM_BUILD_ROOT/%{pxdocdir}
mkdir -p $RPM_BUILD_ROOT/%{pxetcdir}
mkdir -p $RPM_BUILD_ROOT/%{pxvardir}
mkdir -p $RPM_BUILD_ROOT/%{pxinitdir}
cp ~/out/release/bin/pxmarsd $RPM_BUILD_ROOT/%{pxbindir}
cp ~/out/release/bin/pxmars  $RPM_BUILD_ROOT/%{pxbindir}
strip $RPM_BUILD_ROOT/%{pxbindir}/*
cp ~/PROJEKTE/MARS/notify-unknown-sender.sh $RPM_BUILD_ROOT/%{pxbindir}
cp ~/PROJEKTE/MARS/pxmars.conf $RPM_BUILD_ROOT/%{pxetcdir}
cp ~/PROJEKTE/MARS/errmail-address-headers $RPM_BUILD_ROOT/%{pxetcdir}
cp ~/PROJEKTE/MARS/scripts/pxmars-header.%{_vendor} \
   $RPM_BUILD_ROOT/%{pxinitdir}/pxmars
cat ~/PROJEKTE/MARS/scripts/pxmars >> $RPM_BUILD_ROOT/%{pxinitdir}/pxmars
cp ~/PROJEKTE/MARS/Doc/de/positix_mars.pdf $RPM_BUILD_ROOT/%{pxdocdir}/positix_mars-%{version}.pdf

%install

%files
%defattr(-, root, root)
%attr(755, root, root) %docdir %{pxdocdir}
%attr(444, root, root) %doc %{pxdocdir}/positix_mars-%{version}.pdf
%attr(640, root, root) %config %{pxetcdir}/pxmars.conf
%attr(640, root, root) %config %{pxetcdir}/errmail-address-headers
%attr(755, root, root) %dir %{pxbindir}
%attr(755, root, root) %dir %{pxdocdir}
%attr(755, root, root) %dir %{pxetcdir}
%attr(750, root, root) %dir %{pxvardir}
%attr(550, root, root) %{pxbindir}/pxmarsd
%attr(550, root, root) %{pxbindir}/notify-unknown-sender.sh
%attr(550, root, root) %{pxbindir}/pxmars
%attr(700, root, root) %{pxinitdir}/pxmars

%clean
rm -rf $RPM_BUILD_ROOT

%post
if sed=$(command -v sed) ; then
    x=$RPM_INSTALL_PREFIX2/pxmars.conf
    cp -p $x $x~
    cat $x~ \
    | sed -e "s:%{pxvardir}:$RPM_INSTALL_PREFIX3:g" \
    > $x

    x=$RPM_INSTALL_PREFIX4/pxmars
    cp -p $x $x~
    cat $x~ \
    | sed -e "s:%{pxbindir}:$RPM_INSTALL_PREFIX0:g" \
          -e "s:%{pxetcdir}:$RPM_INSTALL_PREFIX2:g" \
          -e "s:%{pxvardir}:$RPM_INSTALL_PREFIX3:g" \
    > $x
else
    echo sed not found. You may have to adjust \
         $RPM_INSTALL_PREFIX2/pxmars.conf and \
         $RPM_INSTALL_PREFIX4/pxmars manually
fi
if [ %{_vendor} = "redhat" ] ; then
    chkconfig --add pxmars || true
fi

%preun
$RPM_INSTALL_PREFIX4/pxmars stop > /dev/null 2&>1 || true
chkconfig --del pxmars > /dev/null 2>&1 || true
