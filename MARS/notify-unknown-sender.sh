#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

# Es werden mindestens zwei Argumente erwartet.
# $1: Name der Maildatei
# $2: Absenderadresse
if [ -z "$2" ] ; then
    echo Missing argument
    exit 1
fi

set -x

# Dieses Konstrukt filtert die Empfaenger aus der Maildatei
recipients=$(sed -e '/^[[:space:]]*DATA[:space::]*/{d;q}' \
                 -e '/[[:space:]]*RCPT/!d' -e 's/.*<//'   \
                 -e 's/>.*//' \
                 -e 's/[[:cntrl:]]//g' \
                 "$1")

# Vom Maildateinamen den Verzeichnisanteil herausnehmen --> $id
id=$(basename "$1")

FromAddress="postmaster@positix.localdomain"
Subject="Mailbox registration required"

#export MAILRC=/opt/positix/etc/pxmars.mailrc
mail -v -n -s "Mailbox registration required" \
     -r "postmaster@positix.localdomain" \
     "$2" <<EOF
Dear owner of the mail address $2,

You tried to send a message to following recipients
$(for a in $recipients; do echo "    " $a; done)
But in our fight against spam-mails, we only accept known senders.

The mail you have sent was stored and will be deleted within 7 days.

You can register your mail address and make your mail delivered 
by visiting following web page
http://xxx.institution.tld/mail-registration/$id.
EOF
