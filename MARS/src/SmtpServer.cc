/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdarg.h>
#include <syslog.h>
#include <algorithm>

#include <positix/strings.hh>
#include <positix/system.hh>
#include <positix/log.hh>
#include <positix/time.hh>
#include <positix/dcs.hh>

#include "SmtpServer.hh"
#include "msg.hh"
#include "version_pxmars.hh"

using namespace std;
using namespace positix;
using namespace positix_types;
using namespace positix_dcs;

// TODO: Full mail address beginning with dot ==> Will be interpreted as
//       incomplete adress identifier in table Extern!

///////////////////////////////////////////////////////////////////////////////

#define MARK cerr << "TEST: " << __LINE__ << endl

#define CRLF "\xD\xA"

#define CATCH \
    catch ( positix::Error& e ) { \
        Format(LogError, TheLog, "%0", e.msg()); } \
    catch ( std::exception& e ) { \
        Format(LogError, TheLog, "%0", e.what()); } \
    catch ( ... ) { \
        Format(LogError, TheLog, TXT("Unknown exception caught")); }

///////////////////////////////////////////////////////////////////////////////

extern positix::LogStream TheLog;

///////////////////////////////////////////////////////////////////////////////

static char EvaluationCopy[] =
{57,93,102,141,7,30,132,242,200,215,60,85,214,130,57,23,84,84,31,181,228,41,39,
 17,7,175,233,67,182,157,175,223,120,146};

static const char EvaluationCopy0[] =
{52,87,76,167,45,62,244,157,187,190,72,60,174,162,116,86,6,7,63,240,146,72,75,
 100,102,219,128,44,216,189,133,245,82};

///////////////////////////////////////////////////////////////////////////////

SmtpServer::SmtpServer()
: _stopAsap(false),
  _idleServants(0),
  _activeServants(0),
  _sessionCount(0),
  _nServants(0),
  _iErrmailRecipient(0)
{
    // **** Not thread-safe:
    const size_t L = sizeof(EvaluationCopy);
    // if ( EvaluationCopy[L-1] )
    //    positix_CL::DescrambleText1(EvaluationCopy, EvaluationCopy0, L);
}

///////////////////////////////////////////////////////////////////////////////

SmtpServer::~SmtpServer()
{
    // TODO: Destroy servant threads ???
}

///////////////////////////////////////////////////////////////////////////////

SmtpServant* SmtpServer::seizeServant() throw ()
{
    Mutex::Lock x(_mutex);

    if ( _idleServants )
    {
	SmtpServant* srv = _idleServants;
	srv->_unlicensed = (_licode!=(2*3*5));
	_idleServants = srv->_nextServant;
	srv->_nextServant = _activeServants;
	srv->_prevServant = 0;
	if ( _activeServants )
	    _activeServants->_prevServant = srv;
	_activeServants = srv;
	// srv->_unlicensed |= (_licenseFile.empty() || _license.empty());
	return srv;
    }
    else if ( _nServants < _mServants )
    {
	try
	{
	    auto_ptr<SmtpServant> srv(new SmtpServant(this));
	    srv->_thread = Thread(SmtpServant::ThreadEntry, srv.get());
	    ++_nServants;
	    //srv->_unlicensed = srv->_unlicensed = (_licode!=(2*3*5));
	    srv->_nextServant = _activeServants;
	    srv->_prevServant = 0;
	    if ( _activeServants )
		_activeServants->_prevServant = srv.get();
	    //srv->_unlicensed |= (_licenseFile.empty() || _license.empty());
	    _activeServants = srv.get();
	    return srv.release();
	}
	CATCH;
    }
    
    return 0;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServer::releaseServant(SmtpServant* servant) throw ()
{
    Mutex::Lock x(_mutex);

    if ( servant->_prevServant )
	servant->_prevServant->_nextServant = servant->_nextServant;
    else
	_activeServants = servant->_nextServant;

    if ( servant->_nextServant )
	servant->_nextServant->_prevServant = servant->_prevServant;

    servant->_nextServant = _idleServants;
    _idleServants = servant;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServer::stop() throw ()
{
    _stopAsap = true;
}

///////////////////////////////////////////////////////////////////////////////

inline void CloseOnExec(int fd) throw ()
{
    int flags = fcntl(fd, F_GETFD, 0);
    flags &= ~FD_CLOEXEC;
    fcntl(fd, F_SETFD, flags);
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServer::run()
{
    PidFile pidfile;
    Format(LogInfo, TheLog, TXT("Starting positix MARS server %0"), 
	   POSITIX_VERSION_MARS);

    if ( !_pidFile.empty() )
	pidfile.create(_pidFile);

    makeStoreDirectory(_tmpDir);
    makeStoreDirectory(_storeDirIn);
    makeStoreDirectory(_storeDirOut);
    makeStoreDirectory(_storeDirUnknown);

    if ( _endPoints.empty() )
	throw Error(Msg(TXT("Server cannot run: No endpoints defined.")));

    fd_set fds;
    int    nfds = 0;
    string overload;
    overload.reserve(256);

    FD_ZERO(&fds);

    for (EndPoints::iterator p = _endPoints.begin();
	 p != _endPoints.end();
	 ++p)
    {
	if ( p->addr.port()==0 )
	    p->addr.port(25);
	Format(LogInfo, TheLog, TXT("Listen on %0"), p->addr);
	p->socket().bind(p->addr, true);	
	p->socket().listen(100);
	int fd = p->socket().get().value;
	if ( fd > nfds )
	    nfds = fd;
	FD_SET(fd, &fds);
    }

    switchPersonality();
    connectDB();

    ++nfds;

    while ( !_stopAsap )
    {
	int n = select(nfds, &fds, 0, 0, 0);
	if ( n < 0 )
	{
	    int e = errno;
	    if ( e == EINTR )
		continue;
	    throw SystemError(e, TXT("select failed"));
	}

	for (EndPoints::iterator p = _endPoints.begin();
	     n && p != _endPoints.end();
	     ++p)	    
	{
	    int fd = p->socket().get().value;
	    if ( FD_ISSET(fd, &fds) )
	    {
		SysSocket sock;
		bool ok  = false;
		try
		{
		    SocketAddr addr;
		    sock = p->socket().accept(&addr);
		    CloseOnExec(sock.value);
		    SmtpServant* servant = seizeServant();
		    if ( servant )
		    {
			servant->_sockClient = sock;
			servant->_hostname   = p->hostname;
			servant->_sem.post();
			ok = true;
		    }
		    else
		    {
		       Format(
			  LogAlert, TheLog, 
			  TXT("Connection rejected due to limited resources"));
		       overload  = "421 ";
		       overload += p->hostname;
		       overload += " positix MailServer overload"CRLF;
		    }
		}
		CATCH;
		if ( !ok && sock.valid() )
		{
		    ::send(sock.value,overload.c_str(),overload.length(),0);
		    ::close(sock.value);
		}
	    }
	    else
	    {
		FD_SET(fd, &fds);
	    }
	}
    }

    Format(LogInfo, TheLog, TXT("Stopping positix MARS server"));
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServer::init(SmtpServant* servant) throw ()
{
    unsigned count;
    {Mutex::Lock x(_mutex);
    count = ++_sessionCount;
    }

    try
    {
	struct timeval t;
	struct tm      T;
	gettimeofday(&t, 0);
	localtime_r(&t.tv_sec, &T);
	ostringstream out;
	out.fill('0');
	out << '<' 
	    << servant->_peerAddr        << ' '
	    << setw(4) << T.tm_year+1900 << '-'
	    << setw(2) << T.tm_mon+1     << '-'
	    << setw(2) << T.tm_mday      << '_'
	    << setw(2) << T.tm_hour      << '-'
	    << setw(2) << T.tm_min       << '-'
	    << setw(2) << T.tm_sec       << '_'
	    << setw(6) << t.tv_usec      << " #"
	    << count                     << " T"
	    << pthread_self()
	    << '>' << '\0';

	StrCopy(out.str(), servant->_sessionId, sizeof(servant->_sessionId));

	servant->_outgoing = isInternalClient(servant);

	return true;
    }
    catch ( ... )
    {
	return false;
    }
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServer::isInternalClient(SmtpServant* servant) const throw ()
{
    IPAddr addr;
    addr.setFromPeer(servant->_sockClient);

    for (InternalAddrRanges::const_iterator p = _internalAddresses.begin();
	 p != _internalAddresses.end();
	 ++p)
    {
	if ( addr>=p->first && addr<=p->last )
	{
	    if ( _auditStatus )
	    {
		Mutex::Lock x(_auditMutex);
		_audit << Now() << ' ' << servant->_sessionId
		       << " ?? Internal client: " <<  addr << endl;
	    }
	    return true;
	}
    }

    if ( _auditStatus )
    {
	Mutex::Lock x(_auditMutex);
	_audit << Now() << ' ' << servant->_sessionId
	       << " ?? External client: " <<  addr << endl;
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServer::makeStoreDirectory(const std::string& dir)
{
    if ( dir.length()==0 )
	return;

    const char* s = dir.c_str();

    if ( mkdir(s, 0770)==0 )
    {
	Format(LogInfo, TheLog, TXT("Created directory %0"), s);
	return;
    }

    int e = errno;
    if ( e == EEXIST )
    {
	struct stat st;
	if ( stat(s, &st)==-1 )
	{
	    e = errno;
	    throw SystemError(e, Msg(TXT("Cannot access %0"), s));
	}
	else if ( !S_ISDIR(st.st_mode) )
	{
	    throw SystemError(
		e, 
		Msg(TXT("Cannot create directory %0:"
			" Already exists as non-directory"), s));
	}
	else
	    return;
    }

    throw SystemError(e, Msg(TXT("Cannot create directory %0"), s));
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServer::pickErrmailRecipient(char* addr) throw ()
{
    unsigned i;

    if ( _errmailRecipients.empty() )
    {
	*addr = 0;
	return;
    }

    {Mutex::Lock x(_mutex);
	i = ++_iErrmailRecipient;
	if ( i >= _errmailRecipients.size() )
	    i = _iErrmailRecipient = 0;
    }

    StrCopy(_errmailRecipients[i], addr, SmtpServerBase::MaxAddressLength+1);
}

///////////////////////////////////////////////////////////////////////////////

void* SmtpServant::ThreadEntry(void* param)
{
#define servant (static_cast<SmtpServant*>(param))

    while ( !(servant->_server)->_stopAsap )
    {
	servant->_sem.wait();

	if ( (servant->_server)->_stopAsap )
	    break;

	try
	{
	    servant->serve();
	}
	catch ( ... )
	{
	    // May be from some diagnostig message. Ignore.
	}

	(servant->_server)->releaseServant(servant);
    }

    return 0;

#undef servant
}

///////////////////////////////////////////////////////////////////////////////

SmtpServant::SmtpServant(SmtpServer* server)
: _server(server),
  _nextServant(0),
  _helo(new char[server->_maxLineLength]),
  _tmpName(new char[server->_tmpDir.length() + sizeof(_mailId)+1]),
  _inbuf (new char[server->_maxLineLength]),
  _outbuf(new char[server->_maxLineLength]),
  _tmpiobuf(new char[server->_maxLineLength])
{
    string::size_type L = max(max(_server->_storeDirIn.length(), 
				  _server->_storeDirOut.length()),
			      _server->_storeDirUnknown.length());

    _storeName      = new char[L+sizeof(_mailId)+1];
    _activationName = new char[_server->_storeDirUnknown.length()
			       +sizeof(_mailId)+1];

    _endIn  = _inbuf  + server->_maxLineLength;
    _endOut = _outbuf + server->_maxLineLength - 2;    
}

///////////////////////////////////////////////////////////////////////////////

SmtpServant::~SmtpServant() throw ()
{
    try{closeConnections();} catch (...) {}
    delete[] _activationName;
    delete[] _storeName;
    delete[] _tmpiobuf;
    delete[] _outbuf;
    delete[] _inbuf;
    delete[] _tmpName;
    delete[] _helo;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::closeConnections() throw ()
{
    _sockClient.close();
    closeConnectionToServer();
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::openConnectionToServer(bool fRelay) throw ()
{
    closeConnectionToServer();
    if ( _outgoing )
    {
	_serverAddr = _server->_oMailServerAddr;
	_bindAddr   = _server->_oMailBindAddr;
    }
    else
    {
	_serverAddr = _server->_iMailServerAddr;
	_bindAddr   = _server->_iMailBindAddr;
    }

    try
    {
	Format(LogInfo, TheLog, TXT("Connecting to server %0 using %1"),
	       _serverAddr, _bindAddr);

	_sockServer.bind(_bindAddr);
	_sockServer.connect(_serverAddr);
    }
    catch ( positix::Error& e )
    {
	Format(LogError, TheLog, 
	       TXT("Connection to server %0 using %1 failed: %2"),
	       _serverAddr, _bindAddr, e.msg());
	nextReply(554, "Target MTA down or out of local resources");
	return false;
    }
    catch ( ... )
    {
	nextReply(452, "Local error or insufficient resources");
	return false;
    }

    _quitSentToServer = false;

    return fRelay ? relayReplies() : chatWithServer(0, true);
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::closeConnectionToServer() throw ()
{
    if ( _sockServer.valid() )
    {
	if ( !_quitSentToServer )
	    chatWithServer("QUIT");
	_quitSentToServer = true;
	_sockServer.close();
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::reconnectServer() throw ()
{
    closeConnectionToServer();
    openConnectionToServer(false);
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::readLineServer() throw ()
{
    _nextIn = _inbuf;
    int len = SmtpReadLine(_sockServer.get(),
			   _inbuf, 
			   _server->_maxLineLength, 
			   _server->_serverTimeout);

    if ( len >= 0 )
    {
	if ( _server->_auditServerIn )
	{
	    Mutex::Lock x(_server->_auditMutex);
	    _server->_audit << Now() << ' ' << _sessionId
			    << " SI |" << _inbuf << "| " << _serverAddr 
			    << endl;
	}
	_inLen = unsigned(len);
	return true;
    }

    switch ( len )
    {
    default:
	Format(LogError, TheLog, TXT("%0 Internal error (read)"), 
	       _sessionId);
	break;

    case -1: //Line too long
	Format(LogError, TheLog, TXT("%0 Long line from server %1"), 
	       _sessionId, _serverAddr);
	break;

    case -2:
	Format(LogError, TheLog, TXT("%0 Very long line from server %1"), 
	       _sessionId, _serverAddr);
	break;

    case -3:
	Format(LogInfo, TheLog, TXT("%0 Timeout server %1"), 
	       _sessionId, _serverAddr);
	break;

    case -4: {
	int e = errno;
	Format(LogError, TheLog, TXT("%0 Recv error from server %1: %2"), 
	       _sessionId, _serverAddr, SystemError::Message(e));
        }
	break;

    case -5:
	Format(LogInfo, TheLog,TXT("%0 Connection closed by remote server %1"),
	       _sessionId, _serverAddr);
	_quit = true;
	return false;
    }

    nextReply(451, "Local error in processing (relay)");
    _quit = true;
    return false;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::readLineClient() throw ()
{
    _nextIn = _inbuf;
    int len = SmtpReadLine(_sockClient.get(), 
			   _inbuf, 
			   _server->_maxLineLength, 
			   _server->_clientTimeout);

    if ( len >= 0 )
    {
	if ( _server->_auditClientIn )
	{
	    Mutex::Lock x(_server->_auditMutex);
	    _server->_audit << Now() << ' ' << _sessionId
			    << " CI |" << _inbuf << "|\n";
	}
	_inLen = unsigned(len);
	return true;
    }

    switch ( len )
    {
    default:
	nextReply(451, "Local error in processing (read)");
	_quit = true;
	return false;

    case -1: //Line too long
	nextReply(500, "Line too long");
	return false;

    case -2:
	Format(LogError, TheLog, TXT("%0 Very long line"), _sessionId);
	nextReply(500, "Line suspiciously too long. Terminating connection");
	_quit = true;
	return false;

    case -3:
	Format(LogInfo, TheLog, TXT("%0 Timeout"), _sessionId);
	nextReply(554, "Timeout exceeded. Terminating connection");
	_quit = true;
	return false;

    case -4: {
	int e = errno;
	Format(LogError, TheLog, TXT("%0 Recv error: %1"), 
	       _sessionId, SystemError::Message(e));
	nextReply(451, "I/O error. Terminating connection");
	_quit = true;
	return false;
        }

    case -5:
	Format(LogError, TheLog, TXT("%0 Connection closed by client"),
	       _sessionId);
	_quit = true;
	return false;
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::setInbuf(const char* s) throw ()
{
    strncpy(_inbuf, s, _server->_maxLineLength);
    _inbuf[_server->_maxLineLength] = 0;
    _inLen = strlen(_inbuf);
    _nextIn = _inbuf;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::skipWS() throw ()
{
    char* p = _nextIn;
    char* e = _endIn;

    while ( p<e && *p>=0 && *p<=32 )
	++p;

    _nextIn = p;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::trim() throw ()
{
    skipWS();
    
    char* b = _nextIn;
    char* s = _inbuf + _inLen;

    while ( s > b )
    {
	if ( !isspace(*(--s)) )
	{
	    ++s;
	    break;
	}
    }

    *s     = 0;
    _inLen = s-_inbuf;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::parseWord(const char* s) throw ()
{
    char* p = const_cast<char*>(SmtpServer::ParseWord(s, _nextIn));
    if ( !p )
	return false;
    _nextIn = p;
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::parseNext(const char* s) throw ()
{
    char* p = const_cast<char*>(SmtpServer::ParseNext(s, _nextIn));
    if ( !p )
	return false;
    _nextIn = p;
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::parseMailPath(char* addr) throw ()
{
    char* s = const_cast<char*>(SmtpServer::ParseMailPath(_nextIn, addr));
    if ( !s )
	return false;
    _nextIn = s;
    return true;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::reply(unsigned n) throw ()
{
    char buf[(CHAR_BIT*sizeof(n))/2+1];
    char* b = buf;
    
    do
    {
	*b++ = (n%10) + 48;
    } while ( (n/=10)!=0 );

    char* p = _nextOut;
    char* e = _endOut;

    while ( p<e && b!=buf )
	*p++ = *(--b);   

    _nextOut = p;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::reply(const char* s) throw ()
{
    char* p = _nextOut;
    char* e = _endOut;
    
    while ( p<e && *s )
	*p++ = *s++;
    _nextOut = p;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::reply(const std::string& s) throw ()
{
    char* p = _nextOut;
    char* e = _endOut;
    string::const_iterator ss = s.begin();
    string::const_iterator ee = s.end();
    
    while ( p<e && ss!=ee )
	*p++ = *ss++;
    _nextOut = p;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::nextReply(unsigned code) throw ()
{
    if ( _nextOut!=_outbuf )
    {
	_outbuf[3]  = '-';
	if ( !sendReply() )
	    return;
    }

    reply(code);
    reply(' ');
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::sendReply() throw ()
{
    if ( _outbuf==_nextOut )
	return true;

    *_nextOut++ = 0x0d;
    *_nextOut++ = 0x0a;
    if ( send(_sockClient.get().value, _outbuf, _nextOut-_outbuf, 0)==-1 )
    {
	_nextOut = _outbuf;
	int e = errno;
	Format(LogError, TheLog, TXT("%0. Send error: %s"), 
	       SystemError::Message(e));
	_quit = true;
	return false;
    }
    _nextOut[-2] = 0;
    if ( _server->_auditClientOut )
    {
	Mutex::Lock x(_server->_auditMutex);
	_server->_audit << Now() << ' ' << _sessionId
			<< " CO |" << _outbuf << "|\n";
    }
    _nextOut = _outbuf;
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::chatWithServer(const char* cmd, bool fDontRelayReplies) 
throw ()
{
    SysSocket sock = _sockServer.get();
    if ( cmd )
    {
	if ( _server->_auditServerOut )
	{
	    Mutex::Lock x(_server->_auditMutex);
	    _server->_audit << Now() << ' ' << _sessionId
			    << " SO |" << cmd << "| " << _serverAddr 
			    << endl;
	}

	if (    ::send(sock.value, cmd,  strlen(cmd), 0)==-1 
	     || ::send(sock.value, CRLF, 2,           0)==-1 )
	{
	    nextReply(421, "Service not available now");
	    _quit = true;
	    return false;
	}	
    }

    bool ok = true;
    char* ep;
    do
    {
	readLineServer();
	unsigned long rc = strtoul(_inbuf, &ep, 10);
	if ( rc<100 || rc>=600 )
	{
	    nextReply(451, "Local error in processing (serv)");
	    _quit = true;
	    return false;
	}
	ok &= (rc>=100 && rc<400);
	if ( !fDontRelayReplies )
	    nextReply(rc, ep+1);
    } while ( *ep=='-' );

    return ok;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::sendLineServer() throw ()
{
    if ( !_sockServer.valid() && !openConnectionToServer(false) )
	return false;

    if ( _server->_auditServerOut )
    {
	Mutex::Lock x(_server->_auditMutex);
	_server->_audit << Now() << ' ' << _sessionId
			<< " SO |" << _inbuf << "| " << _serverAddr << endl;
    }

    char* s = _inbuf+_inLen;
    s[0] = 0x0d;
    s[1] = 0x0a;

    if ( ::send(_sockServer.get().value, _inbuf, _inLen+2, 0)==-1 )
    {
	s[0] = 0;
	nextReply(421, "Service not available now");
	_quit = true;
	return false;
    }
    
    s[0] = 0;

    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::relayReplies() throw ()
{
    bool  ok = true;
    char* ep;

    do
    {
	readLineServer();
	unsigned long rc = strtoul(_inbuf, &ep, 10);
	if ( rc<100 || rc>=600 )
	{
	    nextReply(451, "Local error in processing (serv)");
	    _quit = true;
	    return false;
	}
	ok &= (rc>=100 && rc<400);
	nextReply(rc, ep+1);
    } while ( *ep=='-' );

    return ok;
}

///////////////////////////////////////////////////////////////////////////////

const char SmtpServant::ServeFailureMsg[] 
= "452 Insufficient resources or local error"CRLF;

void SmtpServant::serve() throw ()
{
    try
    {
	_quit             = false;
	_ehlo             = false;
	_quitSentToServer = true;
	_nextIn           = _inbuf;
	_nextOut          = _outbuf;

	_peerAddr.setFromPeer(_sockClient);

	if ( !_server->init(this) )
	    return;

	Format(LogInfo, TheLog, TXT("Serving %0"), _sessionId);

	if ( !openConnectionToServer(true) || !sendReply() )
	{
	    sendReply();
	    closeConnections();
	    return;
	}
    
	reset();
	_stage = SmtpStageInit;
	do
	{
	    if ( readLineClient() )
	    {
		skipWS();
		if ( parseWord("RCPT") )
		    rcpt();
		else if ( parseWord("DATA") )
		    data();
		else if ( parseWord("MAIL") )
		    mail();
		else if ( parseWord("HELO") )
		    helo();
		else if ( parseWord("EHLO") )
		    ehlo();
		else if ( parseWord("RSET") )
		    rset();
		else if ( parseWord("QUIT") )
		    quit();
		else
		    relay();
	    }

	    if ( _server->_stopAsap )
	    {
		nextReply(421, "Shutting down. Please try again later.");
		_quit = true;
	    }

	    sendReply();

	} while ( !_quit );

	stopRecording("disconnect");
	closeConnections();

	Format(LogInfo, TheLog, TXT("%0 served"), _sessionId);

	return;
    }
    catch ( positix::Error& e ) 
    {
        Format(LogError, TheLog, "%0", e.msg()); 
    }
    catch ( std::exception& e ) 
    {
        Format(LogError, TheLog, "%0", e.what()); 
    }
    catch ( ... ) 
    { 
        Format(LogError, TheLog, TXT("Unknown exception caught")); 
    }

    send(_sockClient.get().value,ServeFailureMsg,sizeof(ServeFailureMsg)-1, 0);
    closeConnections();
    stopRecording("exception");
    Format(LogInfo, TheLog, TXT("%0 aborted"), _sessionId);
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::startRecording()
{
    struct timeval tv;
    unsigned       dl = _server->_tmpDir.length();

    _tmpName[0]   = 0;
    _storeName[0] = 0;

    if ( dl==0 || _senderStatus==MailAddrPrivate )
    {
	// We don't need any recording
	return true;
    }

    for(;;)
    {
	gettimeofday(&tv, 0);
	localtime_r(&tv.tv_sec, &_tm);

	snprintf(_mailId, sizeof(_mailId),
		 "%c_%04u-%02u-%02u_%02u-%02u-%02u_%06u",
		_outgoing ? 'o' : (_senderStatus==MailAddrWhite ? 'i' : 'u'),
		_tm.tm_year+1900,
		_tm.tm_mon+1,
		_tm.tm_mday,
		_tm.tm_hour,
		_tm.tm_min,
		_tm.tm_sec,
		unsigned(tv.tv_usec));
	_server->_tmpDir.copy(_tmpName, dl);
	_tmpName[dl] = POSITIX_DIR_SEPC;
	strcpy(_tmpName+dl+1, _mailId);
	_file.open(_tmpName, O_RDWR|O_CREAT|O_TRUNC|O_EXCL, 0640);
	if ( _file.isOpen() )
	    break;
	int e = errno;
	if ( e!=EEXIST )
	{
	    Format(LogError, TheLog, TXT("Cannot create file %0: %1"),
		   _tmpName, SystemError::Message(e));
	    return false;
	}
    }

    if ( _server->_auditActions )
    {
	Mutex::Lock x(_server->_auditMutex);
	_server->_audit << Now() << ' ' << _sessionId
			<< " R+ " << _tmpName << endl;
    }

    ostringstream os;
    os <<     "FILE " << _mailId
       << CRLF"PEER " << _peerAddr << CRLF
       << _helo
       << CRLF CRLF;

    string out(os.str());
    _file.write(out.c_str(), out.length());

    record();

    return true;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::do_record()
{
    try
    {
	_inbuf[_inLen]   = 0xd;
	_inbuf[_inLen+1] = 0xa;
	_file.write(_inbuf, _inLen+2);
	_inbuf[_inLen] = 0;
    }
    catch ( ... )
    {
	_recordingError = true;
	_inbuf[_inLen] = 0;
	throw;
    }
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::stopRecording(const char* errmsg)
{
    if ( !_file.isOpen() )
	return true;

    if ( _server->_auditActions )
    {
	Mutex::Lock x(_server->_auditMutex);
	ostream& audit = _server->_audit;
	audit << Now() << ' ' << _sessionId;
	if ( errmsg )
	    audit << " Rs " << errmsg << endl;
	else
	    audit << " RS" << endl;
    }

    _file.close();

    if ( !_server->_privateAllow )
	_privateMail = false;
    else
	_privateMail |= _allRecipientsPrivate;

    if ( errmsg || _privateMail  || _recordingError )
    {
	return unlinkFile(_tmpName) && _privateMail;
    }
    else
    {
	unsigned len;
	if ( _outgoing )
	{
	    len = _server->_storeDirOut.length();
	    _server->_storeDirOut.copy(_storeName, len);
	}
	else if ( _senderStatus==MailAddrGrey && !_allRecipientsOpen )
	{
	    len = _server->_storeDirUnknown.length();
	    _server->_storeDirUnknown.copy(_storeName, len);
	}
	else
	{
	    len = _server->_storeDirIn.length();
	    _server->_storeDirIn.copy(_storeName, len);
	}
	_storeName[len] = POSITIX_DIR_SEPC;
	strcpy(_storeName+len+1, _mailId);
	
	if ( !moveFile(_tmpName, _storeName) )
	    return false;

	if ( _senderStatus==MailAddrGrey 
	     && !(_outgoing || _allRecipientsOpen) )
	{
	    return notifyUnknownSender();
	}
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::moveFile(const char* from, const char* to)
{
    if ( _server->moveFile(from, to, _sessionId) )
	return true;
    nextReply(450, "Local error in processing");
    return false;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::unlinkFile(const char* name)
{
    if ( _server->_auditActions )
    {
	Mutex::Lock x(_server->_auditMutex);
	_server->_audit << Now() << ' ' << _sessionId
			<< " rm " << name << endl;
    }

    if ( ::unlink(name)==0 )
	return true;
    int e = errno;

    nextReply(450, "Local error in processing");

    Format(LogError, TheLog, TXT("Cannot remove file %0: %1"), 
	   name, 
	   SystemError::Message(e));

    return false;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::notifyUnknownSender()
{
    const std::string& N = _server->_unknownSenderNotifier;
    const char* notifier = N.c_str();

    if ( _server->_auditActions )
    {
	Mutex::Lock x(_server->_auditMutex);
	_server->_audit << Now() << ' ' << _sessionId
			<< " N  " << N << ": " << _storeName 
			<< ' ' << _sender << endl;
    }

    if ( notifier[0]!='/' )
    {
	char answer[512];
	SocketAddr sa(N, AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP);
	Socket sock;
	sock.connect(sa);
	sock.send("file ",   5); sock.send(_storeName, strlen(_storeName));
	sock.send(CRLF, 2);
	sock.send("sender ", 7); sock.send(_sender,    strlen(_sender));
	sock.send(CRLF CRLF, 4);
	strcpy(answer, TXT("No reply"));
	int rc = SmtpReadLine(sock.get(), answer, sizeof(answer), 
			      _server->_serverTimeout);
	if ( rc>=0 && strcmp(answer, "OK")==0 )
	    return true;
	Format(LogError, TheLog, 
	       TXT("Notification of unknown sender %0 failed: %1"),
	       _sender, rc>=0 ? answer : "<bad answer>");
	return false;
    }
    else
    {
	char* args[] = {(char*)notifier, _storeName, _sender, 0};

	pid_t pid = fork();

	if ( pid_t(-1) == pid )
	{
	    SysLastErr e;
	    Format(LogError, TheLog, TXT("Cannot create child process: %0"),
		   SystemError::Message(e));
	    nextReply(450, "Local error in processing");
	    return false;
	}
	else if ( pid==0 )
	{
	    // Child process
	    execv(notifier, args);
	    int e = errno;
	    const char* s = strerror(e);
	    syslog(LOG_MAIL|LOG_CRIT, "Cannot execute %s: %s", notifier, s);
	    _exit(1);
	}
	else
	{
	    int status;
	    // Parent process
	    int rc = waitpid(pid, &status, 0);
	    if ( rc==-1 )
	    {
		SysLastErr e;
		Format(LogError, TheLog, 
		       TXT("No response from child process: %0"),
		       SystemError::Message(e));
		nextReply(450, "Local error in processing");
		return false;
	    }
	    if ( !WIFEXITED(status) )
	    {
		Format(LogError, TheLog, TXT("Child exited abnormally"));
		nextReply(450, "Local error in processing");
		return false;
	    }
	    if ( WEXITSTATUS(status)!=0 )
	    {
		Format(LogError, TheLog, 
		       TXT("Child terminated with status %0"), status);
		nextReply(450, "Local error in processing");
		return false;
	    }
	    return true;
	}	    
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::reset() throw ()
{
    stopRecording("reset");

    _stage                 = SmtpStageMail;    
    _sender[0]             = 0;
    _tmpName[0]            = 0;
    _storeName[0]          = 0;
    _senderStatus          = MailAddrInvalid;
    _nRecipients           = 0;
    _nActivationRecipients = 0;
    _msgSize               = 0;
    _admin                 = false;
    _mayAdmitExternal      = false;
    _recordingError        = false;
    _privateMail           = false;
    _allRecipientsOpen     = true;
    _allRecipientsPrivate  = true;

    // if ( _server->_licenseFile.empty() || _server->_license.empty() )
    //    _server->_maxMsgSize = 4096;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::helo()
{
    reset();
    strcpy(_helo, _inbuf);
    if ( relay() )
    {
	_ehlo  = false;
	_stage = SmtpStageMail;
    }
}

///////////////////////////////////////////////////////////////////////////////

static const char* SupportedExtensions[] =
{
    "PIPELINING",
    "SIZE",
    "8BITMIME",
    0 // Must be here as terminator!
};

void SmtpServant::ehlo()
{
    reset();
    strcpy(_helo, _inbuf);

    if ( !sendLineServer() )
	return;

    // First reply is mandatory and contaings greeting
    if ( !readLineServer() )
	return;

    char* ep;
    unsigned long rc = strtoul(_inbuf, &ep, 10);
    bool     ok = (rc>=100 && rc<400);
    nextReply(rc, ep+1);

    while ( *ep=='-' )
    {
	if ( !readLineServer() )
	    goto bad;
	rc = strtoul(_inbuf, &ep, 10);
	if ( rc<100 || rc>=600 )
	{
	bad:
	    nextReply(451, "Local error in processing (serv)");
	    _quit = true;
	    return;
	}

	if ( rc==250 )
	{
	    _nextIn = ep+1;
	    for (const char** e = SupportedExtensions; *e; ++e)
	    {
		if ( parseWord(*e) )
		{
		    nextReply(rc, ep+1);
		    break;
		}
	    }
	}
	else
	{
	    ok &= (rc>=100 && rc<400);
	    nextReply(rc, ep+1);
	}
    }

    if ( ok )
    {
	_stage = SmtpStageMail;
	_ehlo  = true;
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::mail()
{
    // "MAIL FROM:"  ("<>" / Reverse-Path) [SP Mail-parameters] CRLF

    if ( _stage!=SmtpStageMail )
    {
	nextReply(503, "Bad sequence of commands");
	return;
    }

    skipWS();
    if ( !parseNext("FROM:") )
    {
	nextReply(501, "Syntax error");
	return;
    }

    reset();
    skipWS();
    _sender[0] = 0;
    if ( !parseMailPath(_sender) )
    {
	nextReply(553, "Invalid sender address");
	return;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Checking sender status
    _alias[0] = 0;
    if ( _outgoing )
    {
	_mayAdmitExternal = !_server->isNotifierAccount(_sender);
	_senderStatus = _server->statusInternal(_sender, _alias);
	if ( _alias[0] )
	    replaceLastAddrByAlias(_sender);
    }
    else
    {
	_mayAdmitExternal = false;
	_senderStatus     = _server->statusExternal(_sender);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Processing sender status

    if ( _server->_auditStatus )
    {
	Mutex::Lock x(_server->_auditMutex);
	ostream& audit = _server->_audit;
	audit << Now() << ' '<< _sessionId
	      << " ?S " << String(_senderStatus) << ' '
	      << _sender;
	if ( _mayAdmitExternal )
	    audit << " (May admit external)";
	audit << endl;
    }

    switch ( _senderStatus )
    {
    case MailAddrInvalid:
	if ( _server->_auditActions )
	{
	    Mutex::Lock x(_server->_auditMutex);
	    _server->_audit << Now() << _sessionId << _server->_actionString
			    << "Reject invalid sender address: " 
			    << _sender << endl;
	}
	if ( !_server->_acceptAllSenders )
	{
	    nextReply(550, "Invalid sender address"); 
	    return;
	}
	break;

    case MailAddrBlack:
	if ( _server->_auditActions )
	{
	    Mutex::Lock x(_server->_auditMutex);
	    _server->_audit << Now() << ' ' << _sessionId 
			    << _server->_actionString
			    << "Reject black sender " << _sender << endl;
	}
	if ( !_server->_acceptAllSenders )
	{
	    nextReply(550, "Sender is blacklisted"); 
	    return;
	}
	break;

    case MailAddrGrey:
	if ( _server->_acceptAllSenders )
	    _senderStatus = MailAddrWhite;
	else if ( _server->_storeDirUnknown.length()==0 )
	{
	    nextReply(553, "Mail from unknown sender not wanted");
	    _quit = true;
	    return;
	}
	break;

    case MailAddrPrivate:
	if ( _server->_privateAllow )
	    _privateMail = true;
	else
	    _senderStatus = MailAddrWhite;
	break;

    case MailAddrEmpty:
	if ( !_server->_acceptAllSenders &&
	     !_outgoing && _server->_errmailHeaders.empty()
	     && _server->_errmailRecipients.empty() )
	{
	    nextReply(553, "Mail from empty sender not wanted");
	    return;
	}
	break;

    case MailAddrWhite:
    case MailAddrOpen:
	// Nothing to be done
	break;
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Now, finally relay command and reply

// For testing WatchDog
//     if ( *_sender=='#' )
// 	*(char*)0 = 0;

    if ( startRecording() && relay() )
	_stage = SmtpStageRcpt;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::replaceLastAddrByAlias(const char* addr) throw ()
{
    char*  s  = _nextIn-1; // Points to terminating '>' of mail address
    int    rl = int(strlen(addr));
    int    al = int(strlen(_alias));
    int    d  = al - rl;
    if ( int(_inLen)+d > int(_server->_maxLineLength) )
    {
	nextReply(500, "Transformed line will be too long");
	return;
    }
    memmove(s+d, s, _inLen - (s-_inbuf) + 1);
    memcpy(s-rl, _alias, al);
    _inLen  += d;
    _nextIn += d;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::rcpt()
{    
    // "RCPT TO:" ("<Postmaster@" domain ">" / "<Postmaster>" 
    //             / Forward-Path) [SP Rcpt-parameters] CRLF
   
    if ( _stage!=SmtpStageRcpt && _stage!=SmtpStageData )
    {
	nextReply(503, "Bad sequence of commands");
	return;
    }

    skipWS();
    if ( !parseNext("TO:") )
    {
	nextReply(501, "Syntax error");
	return;
    }

    skipWS();
    if ( !parseMailPath(_mailAddrBuf) )
    {
	nextReply(553, "Invalid recipient address");
	return;
    }

    if ( _senderStatus==MailAddrGrey && !_allRecipientsOpen
	 && _nRecipients>=_server->_maxRecipientsFromUnknown )
    {
	nextReply(550, "Too many recipients from unknown sender");
	return;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Getting recipient's status
    MailAddrStatus mas;
    bool           exactMatch = false;

    _alias[0] = 0;
    if ( _outgoing )
    {
	cerr << "TEST: outgoing\n";
	const std::string& admin = _server->_admin;
	if ( admin.length() && admin==_mailAddrBuf )
	{
	    if ( !_mayAdmitExternal )
	    {
		nextReply(553, "Recipient not accepted for sender");
		return;
	    }
	    if ( _server->_auditStatus )
	    {
		Mutex::Lock x(_server->_auditMutex);
		_server->_audit << Now() << ' ' << _sessionId 
				<< " ?! Administrative mail" << endl;
	    }	    
	    _admin = true;
	    mas    = MailAddrWhite;
	}
	else if ( _server->isActivationAddress(_mailAddrBuf, _alias) )
	{    
	    if ( _server->_auditStatus )
	    {
		Mutex::Lock x(_server->_auditMutex);
		_server->_audit << Now() << ' ' << _sessionId 
				<< " ?! Activation mail" << endl;
	    }	    

	    ++_nActivationRecipients;
	    if ( !activate(_mailAddrBuf) )
		return;
	    if ( _alias[0]==0 )
	    {
		_stage = SmtpStageData;
		nextReply(250, "Mail activated");
		return;
	    }
	    replaceLastAddrByAlias(_mailAddrBuf);
	    mas = MailAddrWhite;
	}
	else if ( _server->hasInternalDomain(_mailAddrBuf) )
	{
	    mas = _server->statusInternal(_mailAddrBuf, _alias);
	    if ( _alias[0] )
		replaceLastAddrByAlias(_mailAddrBuf);
	}
	else
	    mas = _server->statusExternal(_mailAddrBuf, &exactMatch);
    }
    else if ( !_server->hasInternalDomain(_mailAddrBuf) )
    {
	nextReply(554, "Relay access denied");
	return;
    }
    else if ( _server->isActivationAddress(_mailAddrBuf, _alias) )
    {
	if ( _senderStatus==MailAddrEmpty )
	{
	    nextReply(554, "Bad transfer request. Bye");
	    _quit = true;
	    return;
	}
	if ( _nActivationRecipients>0 )
	{
	    nextReply(553, "Too many recipients for activation mail");
	    return;
	}
	++_nActivationRecipients;
	if ( !activate(_mailAddrBuf, _sender) )
	    return;
	if ( _alias[0]==0 )
	{
	    _stage = SmtpStageData;
	    nextReply(250, "Mail activated");
	    return;
	}
	replaceLastAddrByAlias(_mailAddrBuf);
	mas = MailAddrWhite;
    }
    else if ( _senderStatus==MailAddrEmpty 
	      && _server->_errmailHeaders.empty() )
    {
	// Any mail from empty, external sender should be directed to some 
	// errmail-recipient
	if ( _nRecipients>0 )
	{
	    nextReply(553, "Too many recipients from empty sender");
	    return;
	}
	_server->pickErrmailRecipient(_alias);
	if ( _alias[0] )
	{
	    replaceLastAddrByAlias(_mailAddrBuf);
	    mas = MailAddrWhite;
	}
	else
	{
	    nextReply(553, "Empty sender forbidden");
	    return;
	}
    }
    else
    {
	mas = _server->statusInternal(_mailAddrBuf, _alias);
	if ( _alias[0] )
	    replaceLastAddrByAlias(_mailAddrBuf);
    }

    if ( _server->_auditStatus )
    {
	Mutex::Lock x(_server->_auditMutex);
	ostream& audit = _server->_audit;
	audit << Now() << ' ' << _sessionId 
	      << " ?R " << _mailAddrBuf << ' ' << String(mas);
	if ( _alias[0] )
	    audit << " --> " << _alias;
	audit << endl;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Processing recipient's status

    switch ( mas )
    {
    case MailAddrInvalid:
	nextReply(501, "Bad recipient");
	return;

    case MailAddrEmpty:
	nextReply(501, "Empty recipient not allowed");
	return;

    case MailAddrBlack:
	if ( _server->_blackTargetAction == BlackTargetReject )
	{
	    if ( _server->_auditActions )
	    {
		Mutex::Lock x(_server->_auditMutex);
		_server->_audit << Now() << ' ' << _sessionId
				<< _server->_actionString
				<< " Reject black recipient" << endl;
	    }
	    nextReply(553, "Recipient is blacklisted");
	    return;
	}
	else if ( _mayAdmitExternal )
	{
	    // Implies !_outgoing ==> Recipient is extern, _sender is intern
	    _server->setExtern(_mailAddrBuf, MailAddrWhite, _sender, 
			       exactMatch, _sessionId);
	}
	break;

    case MailAddrOpen:
	_allRecipientsPrivate = false;
	break;

    case MailAddrPrivate:
	_allRecipientsOpen = false;
	break;

    case MailAddrGrey:
	if ( _mayAdmitExternal )
	{
	    // Implies !_outgoing ==> Recipient is extern, _sender is intern
	    _server->setExtern(_mailAddrBuf, MailAddrWhite, _sender, 
			       exactMatch, _sessionId);
	}
	// fall through MailAddrWhite:

    case MailAddrWhite:
	_allRecipientsOpen    = false;
	_allRecipientsPrivate = false;
	break;
    }

    record();

    if ( relay() )
    {
	_stage = SmtpStageData;
	++_nRecipients;
    }
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::updateMsgSize()
{
    _msgSize += _inLen+2;

    unsigned max = _server->_maxMsgSize;

    if ( max==0 || max>=_msgSize )
    {
	return true;
    }
    else
    {
	nextReply(552, "Mail too long");
	return false;
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::data()
{
    if ( _stage!=SmtpStageData )
    {
	nextReply(503, "Bad sequence of commands");
	return;
    }

    if ( _nRecipients==0 )
    {
	// Only possible in activation mail
	dataToNowhere();
	return;
    }

    if ( _server->_privateAllow && _privateMail )
    {
	if ( !stopRecording("private") )
	    return;
    }

    record();

    switch ( _senderStatus )
    {
    default: 
	dataFromKnownSender();   
	return;

    case MailAddrGrey:
	if ( _allRecipientsOpen )
	    dataFromKnownSender();
	else
	    dataFromUnknownSender(); 
	return;

    case MailAddrEmpty: 
	if ( _outgoing || _server->_acceptAllSenders )
	    dataFromKnownSender(); // Empty internal sender is OK!
	else if ( _server->_errmailHeaders.empty() )
	    dataFromKnownSender(); // Will be directed to errmail-recipient
	else
	    dataFromEmptySender(); // Parse mail contents
	return;
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::dataFromKnownSender()
{
    // Relay DATA verb:
    if ( !relay() || !insertReceived(true) || !sendReply() )
	return;

    bool admin  = _admin;
    bool licmsg = false;
    _adminBody = false;

    for(;;)
    {
	if ( !readLineClient() )
	    return;
	if ( !updateMsgSize() )
	    return;
	if ( _unlicensed && !licmsg && _inLen==0 )
	{
	    setInbuf(EvaluationCopy);
	    licmsg = true;
	}
	record();
	if ( _nextIn[0]=='.' && _nextIn[1]==0 )
	    break;
	if ( !sendLineServer() )
	    return;
	if ( admin && !parseAdminLine() )
	    return;
    }

    if ( !stopRecording(0) )
    {
	nextReply(451, "Queuing failed");
	reconnectServer();
	reset();
	return;
    }

    relay();
    reset();
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::parseAdminLine() throw ()
{
    if ( !_adminBody )
    {
	// Look for empty line
	_adminBody = (_inbuf[0]==0);
	return true;
    }

    try
    {
	if ( parseWord("ALLOW") )
	    adminSetExternalStatus(MailAddrWhite);
	else if ( parseWord("DENY") )
	    adminSetExternalStatus(MailAddrBlack);
	else if ( parseNext("ACCEPT") )
	    adminAccept();
	else
	    setInbuf(TXT("500 Unknown command"));
    }
    catch ( positix::Error& e )
    {
	setInbuf(e.what());
    }
    catch ( ... )
    {
	setInbuf(TXT("451 local error in processing"));
    }

    return sendLineServer();
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::adminSetExternalStatus(MailAddrStatus mas)
{
    trim();

    if ( *_nextIn!='@' && *_nextIn!='.' 
	 && !SmtpServer::FullMailDomain(_nextIn) )
    {
	setInbuf(TXT("553 Invalid mail identifier"));
	return;
    }

    bool exact;
    { // 
	SqlRequestParams<1> params;
	params[0] = SqlRequestParam(_nextIn, strlen(_nextIn));
	auto_ptr<SqlResult> res(
	    _server->_iDB->execute(SmtpServerBase::FindExtern, params, 
				   SqlStoreLocal));
	exact = (res->fetchRow()!=0);
    }

    _server->setExtern(_nextIn, mas, _sender, exact, _sessionId);
    setInbuf(exact 
	     ? TXT("250 updated") 
	     : TXT("250 inserted"));
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::adminAccept()
{
    trim();
    if ( strchr(_nextIn, '/')!=0 || _nextIn[0]!='u' 
	 || unsigned(_inbuf+_inLen-_nextIn)>=sizeof(_mailId) )
    {
	setInbuf("501 Invalid mail id");
	return;
    }

    string filename;
    filename.reserve(_server->_storeDirUnknown.length()+_inLen+1);
    filename  = _server->_storeDirUnknown;
    filename += POSITIX_DIR_SEPC;
    filename += _nextIn;
   
    unsigned len = _server->_storeDirIn.length();
    _server->_storeDirIn.copy(_storeName, len);
    _storeName[len]   = POSITIX_DIR_SEPC;
    _storeName[len+1] = 'i';
    strcpy(_storeName+(len+2), _nextIn+1);
    _server->sendMailFile(filename.c_str(), _outbuf, _mailAddrBuf, 
			  _sender, _storeName, _sessionId);
    setInbuf("250 Mail forwarded");
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::activate(
    const char* filename, 
    const char* requiredSender)
{
    unsigned len = _server->_storeDirUnknown.length();
    _server->_storeDirUnknown.copy(_activationName, len);
    _activationName[len]   = POSITIX_DIR_SEPC;
    strcpy(_activationName+(len+1), filename);

    len = _server->_storeDirIn.length();
    _server->_storeDirIn.copy(_storeName, len);
    _storeName[len]   = POSITIX_DIR_SEPC;
    _storeName[len+1] = 'i';
    strcpy(_storeName+(len+2), filename);

    cerr << "TEST: activationName=" << _activationName << endl;

    if ( access(_activationName, F_OK)!=0 )
    {
	nextReply(550, "Activation account doesn't exist");
	return false;
    }

    try
    {
	_server->sendMailFile(_activationName, 
			      _tmpiobuf,
			      _mailAddrBufA,
			      _sender,
			      _storeName,
			      _sessionId,
			      0, // No redirection
			      requiredSender);
    }
    catch ( positix::Error& e )
    {
	const char* msg = e.what();
	char* e;
	unsigned long rc = strtoul(msg, &e, 10);
	Format(LogError, TheLog, "%0 Activation failed: %1", _sessionId, msg);
	if ( rc>=400 && rc<=999 )
	    nextReply(rc, e);
	else
	    nextReply(550, "Activation failed.");
	return false;
    }
    catch ( ... )
    {
	nextReply(451, "Local error in processing");
	return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::dataFromUnknownSender()
{
    if ( !chatWithServer("RSET", true) )
	return;

    nextReply(354, "Go ahead");
    if ( !sendReply() || !insertReceived(false) )
	return;

    for(;;)
    {
	if ( !readLineClient() )
	    return;
	if ( !updateMsgSize() )
	    return;

	record();

	if ( _nextIn[0]=='.' && _nextIn[1]==0 )
	    break;
    }

    if ( !stopRecording(0) )
    {
	nextReply(451, "Queuing failed");
	reset();
	return;
    }

    nextReply(250, "Message stored but will not be delivered");

    reset();
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::dataFromEmptySender()
{
    // First, just go ahead as if relaying the mail
    // Relay DATA verb:
    if ( !relay() || !insertReceived(true) || !sendReply() )
	return;

    MailAddrStatus mas = MailAddrPrivate;
    _sender[0] = 0;

    for(;;)
    {
	if ( !readLineClient() )
	    return;
	if ( !updateMsgSize() )
	    return;

	mas = _server->errmailHeader(mas, _inbuf, _sender);

	record();
	if ( _nextIn[0]=='.' && _nextIn[1]==0 )
	    break;
	if ( !sendLineServer() )
	    return;
    }

    if ( !stopRecording(0) )
    {
	nextReply(451, "Queuing failed");
	reconnectServer();
	reset();
	return;
    }

    if ( *_sender==0 )
	mas = MailAddrEmpty;

    switch ( mas )
    {
    case MailAddrInvalid:
    case MailAddrBlack  :
    case MailAddrGrey   :
	nextReply(554, "Message not queued because of spam suspicion"
	               " due to invalid, unknown or unwanted address"
		       " in mail body");
	if ( *_storeName )
	    unlinkFile(_storeName);
	_quit = true;
	return;

    case MailAddrEmpty:
	if ( _server->_errmailRecipients.empty() )
	{
	    nextReply(554, "Message not queued because of spam suspicion"
		           " as there was no appropriate address found"
		           " in the mail body");
	    _quit = true;
	    return;
	}
	_quitSentToServer = true; //Don't send a QUIT before we reconnect
	reconnectServer();
	_server->pickErrmailRecipient(_mailAddrBuf);
	_server->sendMailFile(_storeName,    // File to be sent
			      _inbuf,        // Transfer buffer
			      0,             // No sender address
			      0,             // No internal address
			      0,             // Don't move file
			      _sessionId,    // Session ID 
			      _mailAddrBuf); // Redirection address
	reset();
	return;

    case MailAddrWhite  :
    case MailAddrOpen   :
    case MailAddrPrivate:
	// Normal relaying
	relay();
	reset();
	return;
    }

    nextReply(554, "Error in handling empty address in MAIL FROM");
    reconnectServer();
    reset();
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServant::insertReceived(bool fRelay)
{
    // ABNF-Syntax of time-stamp-line, cf. RFC 2821
    //
    // Time-stamp-line = "Received:" FWS Stamp <CRLF>
    //
    // Stamp       = From-domain By-domain Opt-info ";" FWS date-time
    // From-domain = "FROM" FWS Extended-Domain CFWS
    // By-domain   = "BY"   FWS Extended-Domain CFWS
    // Extended-Domain = Domain / ( Domain FWS "(" TCP-info ")" )
    //                   / ( Address-literal FWS "(" TCP-info ")" )
    // TCP-info        = Address-literal / ( Domain FWS Address-literal )
    //                   ; Information from socket!
    // Opt-info        = [Via] [With] [ID] [For]
    // Via             = "VIA"  FWS Link CFWS 
    // With            = "WITH" FWS Protocol CFWS
    // ID              = "ID"   FWS String / msg-id CFWS
    // For             = "FOR"  FWS 1*( Path / Mailbox ) CFWS
    // address-literal = "[" IPv4-address-literal /
    //                       IPv6-address-literal /
    //                       General-address-literal "]"
    // ... see RFC 2821, 2822
    //
    // We'll insert following lines (making use of CFWS). 
    // <...> are placeholders
    //
    // Received: FROM [<client-ip>] (<helo>=<domain>)
    //  BY <hostname> WITH ESMTP (positix) 
    //  ID "<id>" ; <date-time RFC822>

    char tm[64];
    ostringstream os;
    strftime(tm, sizeof(tm), "%a, %d %b %Y %H:%M:%S %z", &_tm);
    IPAddr clientIP;
    clientIP.setFromPeer(_sockClient);
    const char* prot = "ESMTP";
    if ( !_ehlo )
	++prot;

    os <<        "Received: FROM [" << clientIP << "] (" << _helo 
       << ")"CRLF"        BY " << _hostname << " WITH " << prot << " (positix)"
       << CRLF"        ID \"" << _mailId << "\" ; " << tm;

    string s(os.str());
    if ( s.length() > _server->_maxLineLength+2 )
	return false;
    s.copy(_inbuf, s.length());
    _inbuf[s.length()] = 0;
    _inLen = s.length();
    _msgSize += _inLen+2;

    record();
    if ( fRelay && !sendLineServer() )
    {
	nextReply(451, "Reception failed");
	return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::dataToNowhere()
{
    nextReply(354, "Go ahead. Data will be discarded!");
    if ( !sendReply() )
	return;

    for(;;)
    {
	if ( !readLineClient() )
	    return;
	if ( !updateMsgSize() )
	    return;
	if ( _nextIn[0]=='.' && _nextIn[1]==0 )
	    break;
    }

    // Tell SMTP-Server to reset its state
    setInbuf("RSET");
    relay();
    reset();
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::rset()
{
    if ( relay() )
	reset();
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServant::quit()
{
    if ( _sockServer.valid() )
    {
	if ( relay() )
	{
	    _quit             = true;
	    _quitSentToServer = true;
	}
    }
    else
    {
	nextReply(221, "Ciao.");
	_quit = true;
    }
}

///////////////////////////////////////////////////////////////////////////////
