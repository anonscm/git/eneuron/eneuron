/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix SMTP
// File  : SmtpServer.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_SmtpServer_hh__
#define __positix_SmtpServer_hh__

#include "SmtpServerBase.hh"

///////////////////////////////////////////////////////////////////////////////

class SmtpServer : public SmtpServerBase
{
public:
    // Constructor is *not* thread-safe, see code
    SmtpServer();
    ~SmtpServer();

    void run();
    void stop() throw ();

protected:
    friend class SmtpServant;
    bool init(SmtpServant* servant) throw ();
    bool isInternalClient(SmtpServant* servant) const throw ();
    void pickErrmailRecipient(char* addr) throw ();

private:    
    volatile bool               _stopAsap;
    mutable positix::FastMutex  _mutex;
    SmtpServant*                _idleServants;
    SmtpServant*                _activeServants;
    unsigned                    _sessionCount;
    unsigned                    _nServants;
    unsigned                    _iErrmailRecipient;

private:
    void makeStoreDirectory(const std::string& sub);
    SmtpServant* seizeServant() throw ();
    void releaseServant(SmtpServant* servant) throw ();

    POSITIX_CLASS_NOCOPY(SmtpServer);
};

///////////////////////////////////////////////////////////////////////////////

class SmtpServant
{
public:
    ~SmtpServant() throw ();

protected:
    void serve() throw ();

protected:
    void reset()            throw ();
    bool readLineClient()   throw ();
    bool readLineServer()   throw ();
    void closeConnections() throw ();

    bool openConnectionToServer(bool fRelay)  throw ();
    void closeConnectionToServer() throw ();
    void reconnectServer()         throw ();

    bool startRecording();
    void do_record();
    inline void record()
    {
	if ( _file.isOpen() )
	    do_record();
    }
    bool stopRecording(const char* errmsg);
    bool unlinkFile(const char* name);
    bool moveFile(const char* a, const char* b);

    bool activate(const char* filename, const char* requiredSender = 0);
    bool notifyUnknownSender();

    void nextReply(unsigned code)                throw ();
    void nextReply(unsigned code, const char* s) throw ();
    void reply(char c)                           throw ();
    void reply(const char* text)                 throw ();
    void reply(const std::string& text)          throw ();
    void reply(unsigned num)                     throw ();
    bool sendReply()                             throw ();

    bool sendLineServer() throw ();
    bool relayReplies()   throw ();
    bool chatWithServer(const char* cmd, 
			bool fDontRelayReplies = false) throw ();

    inline bool relay() throw ()
    {
	return sendLineServer() && relayReplies();
    }

    void dataFromKnownSender();
    void dataFromUnknownSender();
    void dataFromEmptySender();
    void dataForAdmin();
    void dataToNowhere();
    bool insertReceived(bool fRelay);
    bool insertLicMsg();
    bool updateMsgSize();

    bool parseAdminLine() throw ();
    void adminSetExternalStatus(MailAddrStatus mas);
    void SmtpServant::adminAccept();

protected:
    void helo();
    void ehlo();
    void rset();
    void mail();
    void rcpt();
    void data();
    void quit();

protected:
    void skipWS()                    throw ();
    void trim()                      throw ();
    bool parseWord(const char* word) throw ();    
    bool parseNext(char        c)    throw ();
    bool parseNext(const char* word) throw ();
    bool parseMailPath(char* addr)   throw ();
    void setInbuf(const char* str)   throw ();

protected:
    // _nextIn *must* point to '>' terminating a mail address.
    // addr must point to a copy of that mail address.
    void replaceLastAddrByAlias(const char* addr) throw ();

protected:
    SmtpServer*           _server;
    SmtpServant*          _prevServant;
    SmtpServant*          _nextServant;
    positix::Thread       _thread;
    positix::Semaphore    _sem;
    positix::Socket       _sockClient;
    positix::Socket       _sockServer;

    positix::SocketAddr   _localAddrClient;
    positix::SocketAddr   _localAddrServer;
    positix::SocketAddr   _peerAddr;
    positix::SocketAddr   _bindAddr;
    positix::SocketAddr   _serverAddr;
    std::string           _hostname;

    char*                 _helo;
    char                  _sessionId[80];
    char                  _mailId[32];
    struct tm             _tm;
    char*                 _tmpName;
    char*                 _storeName;
    char*                 _activationName;
    MailAddrStatus        _senderStatus;
    unsigned              _nRecipients;
    unsigned              _nActivationRecipients;
    positix::File         _file;
    positix::File::Offset _mailDataOffset;

    char                  _sender[SmtpServerBase::MaxAddressLength+1];
    char                  _mailAddrBuf [SmtpServerBase::MaxAddressLength+1];
    char                  _mailAddrBufA[SmtpServerBase::MaxAddressLength+1];
    char                  _alias[SmtpServerBase::MaxAddressLength+1];

    SmtpStage             _stage;
    bool                  _quit;
    bool                  _outgoing;

    char*                 _inbuf;
    char*                 _endIn;
    char*                 _nextIn;
    unsigned              _inLen;

    char*                 _outbuf;
    char*                 _endOut;
    char*                 _nextOut;

    char*                 _tmpiobuf;
    positix_types::fu64   _msgSize;

    unsigned _ehlo                 : 1;
    unsigned _mayAdmitExternal     : 1;
    unsigned _privateMail          : 1;
    unsigned _allRecipientsPrivate : 1;
    unsigned _allRecipientsOpen    : 1;
    unsigned _recordingError       : 1;
    unsigned _quitSentToServer     : 1;
    unsigned _admin                : 1;
    unsigned _adminBody            : 1;
    unsigned _unlicensed           : 1;
       
protected:
    friend class SmtpServer;
    explicit SmtpServant(SmtpServer* server);

    static void* ThreadEntry(void* param);
    static const char ServeFailureMsg[];

private:
    POSITIX_CLASS_NOCOPY(SmtpServant);
};

inline void SmtpServant::reply(char c) throw ()
{
    if ( _nextOut < _endOut )
	*_nextOut++ = c;
}

inline void SmtpServant::nextReply(unsigned code, const char* s) throw ()
{
    nextReply(code);
    reply(s);
}
///////////////////////////////////////////////////////////////////////////////

#include <positix/undef.hh>

#endif

// end of positix SMTP: SmtpServer.hh
