/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix Mail And Relay Service (MARS)
// File  : pxmars.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include "SmtpServer.hh"
#include "msg.hh"

using namespace std;

static SmtpServerBase TheSmtpServer;
positix::LogStream    TheLog;

char ExtAddr[SmtpServerBase::MaxAddressLength];
char IntAddr[SmtpServerBase::MaxAddressLength];

int main(int argc, char* argv[])
{
    unsigned rc  = 0;
    char*    buf = 0;
    string destname;
    destname.reserve(256);

    try
    {
	TheSmtpServer.configure(argc, argv);
	TheSmtpServer.switchPersonality();
	TheSmtpServer.connectDB();

	buf = new char[TheSmtpServer.maxLineLength()];

	for (int i = 1; i < argc; ++i)
	{
	    try
	    {
		const char* srcfile  = argv[i];
		const char* basename = strrchr(srcfile, '/');
		const char* dstfile  = 0;

		if ( basename )
		    ++basename;
		else
		    basename = srcfile;
		
		if ( *basename=='u' )
		{
		    destname  = TheSmtpServer.storeDirIn();
		    destname += '/';
		    destname += 'i';
		    destname += basename+1;
		    dstfile = destname.c_str();
		}		  
			    
		cout << srcfile << endl;
		TheSmtpServer.sendMailFile(srcfile, 
					   buf,
					   ExtAddr,
					   "",
					   dstfile,
					   "");
	    }
	    catch ( positix::Error& e )
	    {
		rc = 1;
		cout << e.msg() << endl;
	    }
	}

	delete[] buf;

	return rc;
    }
    catch ( positix::Error& e )
    {
	rc = 2;
	cout << e.msg() << endl;
    }
    catch ( std::exception& e )
    {
	rc = 2;
	cout << TXT("A runtime exception was raised: ") << e.what() << endl;
    }
    catch ( ... )
    {
	rc = 2;
	cout << TXT("An unknown exception was raised") << endl;
    }

    delete[] buf;
    return rc;
}

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix MARS: pxmars.cc
