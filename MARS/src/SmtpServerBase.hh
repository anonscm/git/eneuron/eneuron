/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix SMTP
// File  : SmtpServerBase.hh
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_SmtpServerBase_hh__
#define __positix_SmtpServerBase_hh__

#include <positix/cfg.hh>
#include <positix/multithreading.hh>
#include <positix/files.hh>
#include <positix/sockets.hh>
#include <positix/relay.hh>
#include <positix/dcs.hh>
// #include <positix/pxlicense.hh>
#include <positix/def.hh>

///////////////////////////////////////////////////////////////////////////////

inline char* FindCRLF(char* b, char* e) throw ()
{
    for(;;)
    {
	b = static_cast<char*>(memchr(b, 0x0d, e-b));
	if ( !b )
	    return 0;
	++b;
	if ( b<e && *b==0x0a )
	    return b+1;
    }
}

///////////////////////////////////////////////////////////////////////////////

enum SmtpStage
{
    // Stage           Allowed commands
    SmtpStageInit,  // EHLO HELO NOOP                          QUIT
    SmtpStageMail,  // EHLO HELO NOOP RSET VRFY MAIL           QUIT
    SmtpStageRcpt,  // EHLO HELO NOOP RSET VRFY      RCPT      QUIT
    SmtpStageData   // EHLO HELO NOOP RSET VRFY      RCPT DATA QUIT
};

///////////////////////////////////////////////////////////////////////////////

// SmtpReadLine
// Return codes:
// OK                : >=0, Length of line (excluding CRLF, excl. NUL)
// Line too long     : -1
// Line much too long: -2
// Timeout           : -3
// Read Error        : -4

extern 
int 
SmtpReadLine(
    positix::SysSocket sock, 
    char*              buf, 
    unsigned           maxlen, 
    unsigned           timeout)
throw ();

///////////////////////////////////////////////////////////////////////////////

enum MailAddrStatus
{
    // Internal value       DB value
    MailAddrInvalid = 0,    // n/a
    MailAddrEmpty   = 1,    // n/a
    MailAddrGrey    = 2,    // n/a
    MailAddrBlack   = 3,    // 0
    MailAddrWhite   = 4,    // 1
    MailAddrOpen    = 5,    // 2
    MailAddrPrivate = 6     // 3
};
extern const char* String(MailAddrStatus mas) throw ();
extern MailAddrStatus MailAddrStatusDB(const char* s) throw ();
extern const unsigned MailAddrStatusOrder[MailAddrPrivate+1];

inline MailAddrStatus MoreRestrictive(MailAddrStatus a, MailAddrStatus b)
throw ()
{
    return MailAddrStatusOrder[a] < MailAddrStatusOrder[b] ? a : b;
}


enum BlackTargetAction
{
    BlackTargetReject,
    BlackTargetAdmit
};

///////////////////////////////////////////////////////////////////////////////

class SmtpServant;

///////////////////////////////////////////////////////////////////////////////

class SmtpServerBase
{
public:
    SmtpServerBase();

    void configure(int& argc, char* argv[]);
    void configure(positix::Configurator& cfg);
    void connectDB();

    void switchLog();
    void switchPersonality();

    bool matchesPrivatePattern(const char* addr, char* alias) throw ();
    bool isOpenRecipient      (const char* addr) throw ();
    bool isNotifierAccount    (const char* addr) throw ();
    bool hasInternalDomain    (const char* addr) throw ();
    bool isActivationAddress  (char* addr, char* alias) throw ();

    MailAddrStatus errmailHeader(
	MailAddrStatus mas, 
	const char*    line,
	char*          addr);

    MailAddrStatus errmailHeader(
	MailAddrStatus      mas,
	const std::string&  hdrPrefix,
	const char*         line,
	char*               addr);

    static const char* FullMailDomain(const char* addr) throw ();
    MailAddrStatus statusExternal(char* sender, bool* exact = 0);
    MailAddrStatus statusInternal(char* sender, char* alias);
    void setExtern(const char* extAddr, MailAddrStatus mas, 
		   const char* intAddr, bool fUpdate, const char* sessionId);

    void sendMailFile(
	const char* filename, 
	char*       buf,     // I/O buffer
	char*       extAddr, // Buffer! Not an input parameter!
	const char* intAddr,
	const char* destFilename,
	const char* sessionId,
	const char* redirection    = 0,
	const char* requiredSender = 0);

    inline unsigned maxLineLength() const throw ()
    {
	return _maxLineLength;
    }

    inline const std::string& storeDirOut() const throw ()
    {
	return _storeDirOut;
    }

    inline const std::string& storeDirIn() const throw ()
    {
	return _storeDirIn;
    }

    inline const std::string& storeDirUnknown() const throw ()
    {
	return _storeDirUnknown;
    }

    static const positix_dcs::SqlRequestPattern SmtpServerBase::FindExtern;
    static const positix_dcs::SqlRequestPattern SmtpServerBase::FindIntern;
    static const positix_dcs::SqlRequestPattern SmtpServerBase::InsertExtern;
    static const positix_dcs::SqlRequestPattern SmtpServerBase::UpdateExtern;

    static const unsigned MaxAddressLength = 256;

protected:
    friend class SmtpServant;

    bool moveByCopy(const char* from, const char* to);
    bool moveFile(const char* from, const char* to, 
		  const char* sessionId, bool induced = false);


    unsigned ReadLine(std::istream& in, char* buf, unsigned len);
    void transferCommand(positix::SysSocket sock, 
			 char* buf, unsigned len, const char* sessionId);
    void transferLine(positix::SysSocket sock, char* buf, unsigned len,
		      const char* sessionId);
    void receiveReply(positix::SysSocket sock, char* buf, 
		      unsigned len, const char* sessionId);

    static const char* ParseMailPath(const char* buf, char* addr) throw ();
    static const char* ParseWord(const char* s, const char* p) throw ();
    static const char* ParseNext(const char* s, const char* p) throw ();


    static inline bool ExtendAddr(char*& buf, char* e, char c) throw ()
    {
	if ( buf>=e )
	    return false;
	*buf++ = c;
	return true;
    }

    void appendErrmailHeaders(const std::string& filename);

protected:
    typedef std::vector<std::string> StringVector;


    struct EndPoint
    {
	EndPoint(const std::string& saddr);

	positix::Relay<positix::Socket> socket;
	positix::SocketAddr             addr;
	std::string                     hostname;
	std::string                     saddr;
    };
    typedef std::vector<EndPoint> EndPoints;

    struct InternalAddrRange
    {
	inline InternalAddrRange(
	    const positix::IPAddr& a, 
	    const positix::IPAddr& b) 
	    throw ()
	: first(a),
	  last (b)
	{
	    // empty
	}

	positix::IPAddr first;
	positix::IPAddr last;
    };
    typedef std::vector<InternalAddrRange> InternalAddrRanges;

    mutable positix::FastMutex  _auditMutex;
    mutable std::ostream _audit;
    mutable std::filebuf _auditBuf;
//  positix_CL::LicenseItems _license;
    std::string          _pidFile;
    std::string          _auditFile;
    bool                 _auditStatus;
    bool                 _auditActions;
    bool                 _auditServerIn;
    bool                 _auditServerOut;
    bool                 _auditClientIn;
    bool                 _auditClientOut;
    const char*          _actionString;

    InternalAddrRanges  _internalAddresses;
    StringVector        _internalDomains;

    unsigned           _maxLineLength;
    unsigned           _clientTimeout;
    unsigned           _serverTimeout;
    std::string        _licenseFile;
    std::string        _user;
    std::string        _group;
    std::string        _logFile;
    std::string        _hostname;
    std::string        _admin;
    std::string        _mailActivationRedirect;
    std::string        _privatePrefix;
    std::string        _privateSuffix;
    std::string        _tmpDir;
    std::string        _storeDirIn;
    std::string        _storeDirOut;
    std::string        _storeDirUnknown;
    std::string        _unknownSenderNotifier;
    StringVector       _errmailHeaders;
    unsigned           _maxRecipientsFromUnknown;
    BlackTargetAction  _blackTargetAction;
    bool               _acceptAllSenders;
    bool               _privateAllow;
    bool               _stripPrivate;
    bool               _mailActivationPerMail;
    bool               _licensed;

    positix_types::fu64 _maxMsgSize;

    StringVector       _notifierAccounts;
    StringVector       _openRecipients;
    StringVector       _errmailRecipients;

    unsigned           _mServants;
    EndPoints          _endPoints;

    positix::SocketAddr _iMailServerAddr;
    positix::SocketAddr _iMailBindAddr;
    std::string         _iDbHost;
    std::string         _iDbName;
    std::string         _iDbUser;
    std::string         _iDbPassword;
    positix_dcs::SqlDB  _iDB;

    positix::SocketAddr _oMailServerAddr;
    positix::SocketAddr _oMailBindAddr;
    std::string         _xDbHost;
    std::string         _xDbName;
    std::string         _xDbUser;
    std::string         _xDbPassword;
    positix_dcs::SqlDB  _xDB;

    unsigned            _licode;

private:
    bool checkDomainLicenses();
    bool checkListenLicenses();
    bool checkMacLicense();
    void writeLicenseTemplate();

private:
    POSITIX_CLASS_NOCOPY(SmtpServerBase);
};

#endif

// end of positix SMTP: SmtpServer.hh
