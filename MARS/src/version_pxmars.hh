/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix MARS
// File  : version_pxmars.hh.in
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////
// This file serves as input to version.sh to create version_pxmars.hh
///////////////////////////////////////////////////////////////////////////////

#ifndef __positix_version_pxmars_hh__
#define __positix_version_pxmars_hh__

#define POSITIX_VERSION_MARS_MAJOR 1
#define POSITIX_VERSION_MARS_MINOR 0
#define POSITIX_VERSION_MARS_PATCH -15

#define VERSION_STRING_MARSX(a,b,c) #a"."#b"."#c
#define VERSION_STRING_MARS(a,b,c) VERSION_STRING_MARSX(a,b,c)

#define POSITIX_VERSION_MARS \
    VERSION_STRING_MARS(POSITIX_VERSION_MARS_MAJOR, \
                        POSITIX_VERSION_MARS_MINOR, \
                        POSITIX_VERSION_MARS_PATCH)

#define VERSION_STRING_PXMARSX2(a,b) #a"."#b
#define VERSION_STRING_PXMARS2(a,b) VERSION_STRING_PXMARSX2(a,b)

#define POSITIX_VERSION_MARS2 \
  VERSION_STRING_PXMARS2(POSITIX_VERSION_MARS_MAJOR,POSITIX_VERSION_MARS_MINOR)

#endif

// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix MARS: version_pxmars.hh.in
