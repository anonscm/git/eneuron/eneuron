/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdarg.h>
#include <syslog.h>
#include <algorithm>

#include <positix/strings.hh>
#include <positix/system.hh>
#include <positix/log.hh>
#include <positix/time.hh>
#include <positix/accounting.hh>
#include <positix/dcs.hh>

#include "SmtpServerBase.hh"
#include "msg.hh"
#include "version_pxmars.hh"

#define CRLF "\xD\xA"
#define MARK cerr << "TEST: (base) " << __LINE__ << endl


using namespace std;
using namespace positix;
using namespace positix_types;
using namespace positix_dcs;
// using namespace positix_CL;

///////////////////////////////////////////////////////////////////////////////

extern positix::LogStream TheLog;

const unsigned MailAddrStatusOrder[MailAddrPrivate+1] =
{
    0, // MailAddrInvalid
    1, // MailAddrEmpty
    3, // MailAddrGrey
    2, // MailAddrBlack
    4, // MailAddrWhite
    5, // MailAddrOpen
    6, // MailAddrPrivate
};

///////////////////////////////////////////////////////////////////////////////

static const char CfgAcceptAllSenders     [] = "accept-all-senders";
static const char CfgAdmin                [] = "admin";
static const char CfgAudit                [] = "audit";
static const char CfgAuditFile            [] = "audit-file";
static const char CfgBlackTarget          [] = "black-target-action";
static const char CfgClientTimeout        [] = "client-timeout";
static const char CfgDbHost               [] = "db-host";
static const char CfgDbName               [] = "db-name";
static const char CfgDbUser               [] = "db-user";
static const char CfgDbPass               [] = "db-pass";
static const char CfgDomain               [] = "domain";
static const char CfgErrmailRecipient     [] = "errmail-recipient";
static const char CfgErrmailHeaders       [] = "errmail-address-headers";
static const char CfgGroup                [] = "group";
static const char CfgIdbName              [] = "idb-name";
static const char CfgIdbHost              [] = "idb-host";
static const char CfgIdbUser              [] = "idb-user";
static const char CfgIdbPass              [] = "idb-pass";
static const char CfgIntern               [] = "intern";
static const char CfgLicenseFile          [] = "license-file";
static const char CfgListen               [] = "listen";
static const char CfgLog                  [] = "log";
static const char CfgLogLevel             [] = "loglevel";
static const char CfgMailActivation       [] = "activation-mails";
static const char CfgMailActivationRedirect  [] = "activation-mail-redirect";
static const char CfgMaxMsgSize              [] = "max-msg-size";
static const char CfgMaxRecipientsFromUnknown[]= "max-recipients-from-unknown";
static const char CfgMaxThreads           [] = "max-threads";
static const char CfgNotifierAccount      [] = "notifier-account";
static const char CfgOpenRecipient        [] = "open-recipient";
static const char CfgPidfile              [] = "pidfile";
static const char CfgPrivateAllow         [] = "private-allow";
static const char CfgPrivatePrefix        [] = "private-prefix";
static const char CfgPrivateStrip         [] = "private-strip";
static const char CfgPrivateSuffix        [] = "private-suffix";
static const char CfgIncomingServer       [] = "server-incoming";
static const char CfgInBindAddr           [] = "server-incoming-from";
static const char CfgOutgoingServer       [] = "server-outgoing";
static const char CfgOutBindAddr          [] = "server-outgoing-from";
static const char CfgServerTimeout        [] = "server-timeout";
static const char CfgStoreDirIn           [] = "store-dir-in";
static const char CfgStoreDirOut          [] = "store-dir-out";
static const char CfgStoreDirUnknown      [] = "store-dir-unknown";
static const char CfgUnknownSenderNotifier[] = "unknown-sender-notifier";
static const char CfgUser                 [] = "user";
static const char CfgTmpDir               [] = "tmpdir";
static const char CfgXdbName              [] = "xdb-name";
static const char CfgXdbHost              [] = "xdb-host";
static const char CfgXdbUser              [] = "xdb-user";
static const char CfgXdbPass              [] = "xdb-pass";

static const char Postmaster[] = "postmaster";

///////////////////////////////////////////////////////////////////////////////

const SqlRequestPattern SmtpServerBase::FindExtern(
    false, 
    "FindExtern",
    "SELECT status FROM Extern WHERE suffix=?");

const SqlRequestPattern SmtpServerBase::FindIntern(
    false,
    "FindIntern",
    "SELECT status,alias FROM Intern WHERE name=?");

const SqlRequestPattern SmtpServerBase::InsertExtern(
    false,
    "InsertExtern",
    "INSERT Extern (suffix,status,insertionTime,initiator)"
    " VALUES (?,?,DEFAULT,?)");

const SqlRequestPattern SmtpServerBase::UpdateExtern(
    false,
    "UpdateExtern",
    "UPDATE Extern SET status=?, insertionTime=NOW(), initiator=?"
    " WHERE suffix=?");

///////////////////////////////////////////////////////////////////////////////

static bool MustBeSet(
    const char* a, const std::string& as, 
    const char* b, const std::string& bs)
{
    if ( as.length() && !bs.length() )
    {
	Format(LogError, TheLog, 
	       TXT("Configuration variable %0 must be set if %1 is set"),
	       b, a);
	return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////

static std::string NoPort(const std::string& str)
{
    return string(str, 0, str.find(':'));
}

///////////////////////////////////////////////////////////////////////////////

const char* String(MailAddrStatus mas) throw ()
{
    switch ( mas )
    {
    default             : return "MailAddr???";
    case MailAddrInvalid: return "MailAddrInvalid";
    case MailAddrEmpty  : return "MailAddrEmpty";
    case MailAddrBlack  : return "MailAddrBlack";
    case MailAddrGrey   : return "MailAddrGrey";
    case MailAddrWhite  : return "MailAddrWhite";
    case MailAddrOpen   : return "MailAddrOpen";
    case MailAddrPrivate: return "MailAddrPrivate";
    }
}

///////////////////////////////////////////////////////////////////////////////

SmtpServerBase::EndPoint::EndPoint(const std::string& saddr_)
: addr(saddr_, AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP),
  hostname(Hostname()),
  saddr(saddr_)
{
    // TODO: Extend hostname by '.' and domain-name
    // TODO: hostname depends on endpoint IP address!
    // TODO: Make HOSTNAME parameterizable
}

///////////////////////////////////////////////////////////////////////////////

SmtpServerBase::SmtpServerBase()
: _audit(cerr.rdbuf()),
  _auditStatus(false),
  _auditActions(false),
  _auditServerIn(false),
  _auditServerOut(false),
  _auditClientIn(false),
  _auditClientOut(false),
  _actionString(0),
  _maxLineLength(1000),
  _clientTimeout(5 * 60),
  _serverTimeout(10 * 60),
  _hostname(Hostname()),
  _maxRecipientsFromUnknown(100),
  _blackTargetAction(BlackTargetReject),
  _acceptAllSenders(false),
  _privateAllow(false),
  _stripPrivate(false),
  _mailActivationPerMail(false),
  _licensed(false),
  _maxMsgSize(0),
  _mServants(100),
  _iMailServerAddr("localhost:587", AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP),
  _iMailBindAddr("0.0.0.0:0", AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP),
  _iDbHost("localhost"),
  _iDbName("pxmars"),
  _iDbUser("root"),
  _oMailServerAddr("localhost:587", AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP),
  _oMailBindAddr("0.0.0.0:0", AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP),
  _xDbHost("localhost"),
  _xDbName("pxmars"),
  _xDbUser("root"),
  _licode(1)
{
    // empty
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::checkDomainLicenses()
{    
 /*
    unsigned count = 0;
    for (StringVector::const_iterator d = _internalDomains.begin();
	 d!=_internalDomains.end();
	 ++d)
    {
	string domain("DOMAIN " + *d);
	bool   found = false;
	
	for (LicenseItems::iterator p=_license.begin(); p!=_license.end(); ++p)
	{
	    if ( domain==p->descr )
	    {
		found = true;
		++count;		
		break;
	    }
	}

	if ( !found )
	    throw Error(Msg(TXT("Unlicensed Domain: %0"), *d));
    }

    if ( count!=_internalDomains.size() )
    {
	// Someone tampered this routine ==> defer license check
	return true;
    }

    _licode *= 2;
    */
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::checkListenLicenses()
{
/*
    unsigned count = 0;
    for (EndPoints::const_iterator d = _endPoints.begin();
	 d!=_endPoints.end();
	 ++d)
    {
	string addr(NoPort(d->saddr));
	string domain("LISTEN " + addr);
	bool   found = false;
	
	for (LicenseItems::iterator p=_license.begin(); p!=_license.end(); ++p)
	{
	    if ( domain==NoPort(p->descr) )
	    {
		found = true;
		++count;		
		break;
	    }
	}

	if ( !found )
	    throw Error(Msg(TXT("Unlicensed Server Address: %0"), addr));
    }

    if ( count!=_endPoints.size() )
    {
	// Someone tampered this routine ==> defer license check
	return true;
    }

    _licode *= 3;
*/
    return true;
}


///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::checkMacLicense()
{
 /*
    string mac;
    GetFirstMAC(mac);
    string descr = "MAC " + mac;

    for (LicenseItems::iterator p=_license.begin(); p!=_license.end(); ++p)
    {
	if ( descr==p->descr )
	{
	    _licode *= 5;
	    return true;
	}
    }

    throw Error(Msg(TXT("Unlicensed MAC: %0"), mac));
 */
 return true;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::writeLicenseTemplate()
{
/*
    const char* fname = "/tmp/pxmars.lic.template";
    LicenseItems lic;
    string       mac;

    GetFirstMAC(mac);

    lic.push_back("! positix MARS "POSITIX_VERSION_MARS2);
    lic.push_back(TXT("# <replace by license number>"));
    lic.push_back(TXT("$ <replace by licensee>"));
    lic.push_back(TXT("* <replace by license key>"));
    mac = "MAC " + mac;
    lic.push_back(mac);
    
    // Domains
    for (StringVector::const_iterator p = _internalDomains.begin();
 	 p!=_internalDomains.end();
 	 ++p)
    {
	lic.push_back("DOMAIN " + *p);
    }

    // Listen Addresses
    for (EndPoints::const_iterator p = _endPoints.begin();
 	 p!=_endPoints.end();
 	 ++p)
    {
	string addr(NoPort(p->saddr));
	lic.push_back("LISTEN " + addr);
    }

    std::sort(lic.begin(), lic.end(), LicenseItemOrder());

    try
    {
	WriteLicenseFile(fname, lic);
    }
    catch ( std::exception& e )
    {
	Format(LogError, TheLog, 
	       Msg(TXT("Cannot write license template %0: %1"), 
		   fname, e.what()));
    }
 */
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::configure(int& argc, char* argv[])
{
    string origin;
    string cfgFilename = PxLocateCfgFile("positix",
					 "pxmars.conf",
					 "POSITIX_MARS",
					 argc, argv,
					 true, 
					 false,
					 &origin);

    if ( !cfgFilename.empty() )
    {// Read configuration file
	Format(LogInfo, TheLog,
	       TXT("Using configuration file %0 taken from %1"),
	       cfgFilename, origin);
	FileConfigurator cfg(cfgFilename);
	configure(cfg);
    }

    // Parse command line options
    OptionConfigurator cfg(argc, argv);
    configure(cfg);

    if ( _auditFile.length() )
    {
	_auditBuf.open(_auditFile.c_str(), ios::app|ios::out);
	SysLastErr e;
	if ( !_auditBuf.is_open() )
	    throw SystemError(e, Msg(TXT("Cannot open %0"), _auditFile));
	_audit.rdbuf(&_auditBuf);
    }

    bool ok 
	=  MustBeSet(CfgStoreDirIn,      _storeDirIn,      CfgTmpDir, _tmpDir)
	&& MustBeSet(CfgStoreDirOut,     _storeDirOut,     CfgTmpDir, _tmpDir)
	&& MustBeSet(CfgStoreDirUnknown, _storeDirUnknown, CfgTmpDir, _tmpDir);

    if ( !ok )
	throw positix::Error(TXT("Configuration error. See log for details."));

    _actionString = _acceptAllSenders ? " !- " : " !! ";

    Trim(_hostname);
    if ( _hostname.length()==0 )
	_hostname.assign("localhost");

    // Check whether _iMailServerAddr or _oMailServerAddr is not the same
    // as some listen address! Otherwise MARS will communicate with itself!
    const char* s = 0;

    for (EndPoints::iterator p = _endPoints.begin(); p!=_endPoints.end(); ++p)
    {
	if ( p->addr==_iMailServerAddr )
	{
	    s = CfgIncomingServer;
	    break;
	}
	if ( p->addr==_oMailServerAddr )
	{
	    s = CfgOutgoingServer;
	    break;
	}
    }

    if ( s )
    {
	throw Error(Msg(TXT("%0 is the same as some address specified by %1"),
			s, CfgListen));
    }

    // Licensing
    /*
    if ( !_licenseFile.empty() )
    {
	bool hasKey = ReadLicenseFile(_licenseFile, _license);
		Format(LogNotice, TheLog, "%0", TXT("Start of license data"));
	for (LicenseItems::const_iterator p = _license.begin();
	     p != _license.end();
	     ++p)
	{
	    Format(LogNotice, TheLog, " %0", *p);
	}
	Format(LogNotice, TheLog, "%0", TXT("End of license data"));

	if ( !hasKey )
	{
	    Format(LogCritical, TheLog, 
		   TXT("License file %0 doesn't contain license key"),
		   _licenseFile);
	    _license.clear();
	}
	else if ( !VerifyLicense(_license) )
	{	    
	    throw positix::Error(
		Msg(TXT("License key in file %0 doesn't match license data"), 
		    _licenseFile));
	}

	_licensed = checkDomainLicenses() 
		 && checkListenLicenses()
		 && checkMacLicense();
    }


    if ( _licenseFile.empty() || _license.empty() )
    {
	Format(LogCritical, TheLog, TXT("*** Evaluation Mode ***"));
	writeLicenseTemplate();
    }
    */
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::configure(Configurator& cfg)
{
    while ( cfg.hasNext() )
    {
	if ( cfg.iskey(CfgListen) )
	    _endPoints.push_back(EndPoint(cfg.value()));
	else if ( cfg.iskey(CfgDomain) )
	    _internalDomains.push_back(cfg.value());
	else if ( cfg.iskey(CfgIntern) )
	{
	    string addr(cfg.value());
	    string::size_type i = addr.find('-');
	    IPAddr a;
	    if ( i!=string::npos )
	    {
		string sub(addr, 0, i);
		IPAddr b;
		a.set(sub.c_str());
		b.set(addr.c_str() + i + 1);
		_internalAddresses.push_back(InternalAddrRange(a, b));
	    }
	    else if ( a.tryset(addr.c_str()) )
	    {
		_internalAddresses.push_back(InternalAddrRange(a, a));
	    }
	    else
	    {
		SockAddrInfo sai(addr.c_str());
		const struct addrinfo* ai = sai;
		if ( !ai )
		{
		    throw Error(Msg(TXT("Invalid host specification %0"), 
				    addr));
		}

		do
		{
		    a.set(ai->ai_addr);
		    _internalAddresses.push_back(InternalAddrRange(a,a));
		    ai = ai->ai_next;
		} while ( ai );
	    }
	}
	else if ( cfg.iskey(CfgAdmin) )
	    _admin = cfg.value();
	else if ( cfg.iskey(CfgClientTimeout) )
	    cfg.getValue(_clientTimeout);
	else if ( cfg.iskey(CfgServerTimeout) )
	    cfg.getValue(_serverTimeout);
	else if ( cfg.iskey(CfgMaxThreads) )
	    cfg.getValue(_mServants);
	else if ( cfg.iskey(CfgMaxMsgSize) )
	{
	    cfg.getValue(_maxMsgSize);
	    if ( _maxMsgSize >= (1uLL<<54) )
		cfg.badValue();
	    _maxMsgSize <<= 10;
	}
	else if ( cfg.iskey(CfgPidfile) )
	    _pidFile = cfg.value();
	else if ( cfg.iskey(CfgPrivatePrefix) )
	    _privatePrefix = cfg.value();
	else if ( cfg.iskey(CfgPrivateSuffix) )
	    _privateSuffix = cfg.value();
	else if ( cfg.iskey(CfgPrivateAllow) )
	    cfg.getValue(_privateAllow);
	else if ( cfg.iskey(CfgPrivateStrip) )
	    cfg.getValue(_stripPrivate);
	else if ( cfg.iskey(CfgAcceptAllSenders) )
	    cfg.getValue(_acceptAllSenders);
	else if ( cfg.iskey(CfgUnknownSenderNotifier) )
	    _unknownSenderNotifier = cfg.value();
	else if ( cfg.iskey(CfgTmpDir) )
	    _tmpDir = cfg.value();
	else if ( cfg.iskey(CfgStoreDirIn) )
	    _storeDirIn = cfg.value();
	else if ( cfg.iskey(CfgStoreDirOut) )
	    _storeDirOut = cfg.value();
	else if ( cfg.iskey(CfgStoreDirUnknown) )
	    _storeDirUnknown = cfg.value();
	else if ( cfg.iskey(CfgDbName) )
	    _iDbName = _xDbName = cfg.value();
	else if ( cfg.iskey(CfgDbHost) )
	    _iDbHost = _xDbHost = cfg.value();	
	else if ( cfg.iskey(CfgDbUser) )
	    _iDbUser = _xDbUser = cfg.value();
	else if ( cfg.iskey(CfgDbPass) )
	    _iDbPassword = _xDbPassword = cfg.value();
	else if ( cfg.iskey(CfgIdbName) )
	    _iDbName = cfg.value();
	else if ( cfg.iskey(CfgIdbHost) )
	    _iDbHost = cfg.value();	
	else if ( cfg.iskey(CfgIdbUser) )
	    _iDbUser = cfg.value();
	else if ( cfg.iskey(CfgIdbPass) )
	    _iDbPassword = cfg.value();
	else if ( cfg.iskey(CfgXdbName) )
	    _xDbName = cfg.value();
	else if ( cfg.iskey(CfgXdbHost) )
	    _xDbHost = cfg.value();	
	else if ( cfg.iskey(CfgXdbUser) )
	    _xDbUser = cfg.value();
	else if ( cfg.iskey(CfgXdbPass) )
	    _xDbPassword = cfg.value();
	else if ( cfg.iskey(CfgBlackTarget) )
	{
	    if ( cfg.value()=="reject" )
		_blackTargetAction = BlackTargetReject;
	    else if ( cfg.value()=="admit" )
		_blackTargetAction = BlackTargetAdmit;
	    else
		cfg.badValue();
	}
	else if ( cfg.iskey(CfgAuditFile) )
	    _auditFile = cfg.value();
	else if ( cfg.iskey(CfgAudit) )
	{
	    if ( cfg.value()=="all" )
	    {
		_auditClientIn = _auditClientOut =
		_auditServerIn = _auditServerOut = 
		_auditActions = 
		_auditStatus = true;
	    }
	    else if ( cfg.value()=="io" )
	    {
		_auditClientIn = _auditClientOut =
		_auditServerIn = _auditServerOut = true;
	    }
	    else if ( cfg.value()=="clientio" )
		_auditClientIn = _auditClientOut = true;
	    else if ( cfg.value()=="serverio" )
		_auditServerIn = _auditServerOut = true;
	    else if ( cfg.value()=="actions" )
		_auditActions = true;
	    else if ( cfg.value()=="status" )
		_auditStatus = true;
	    else if ( cfg.value()=="db" )
		positix_dcs::TheLogStream = &TheLog;
	    else
		cfg.badValue();
	}
	else if ( cfg.iskey(CfgMaxRecipientsFromUnknown) )
	    cfg.getValue(_maxRecipientsFromUnknown);
	else if ( cfg.iskey(CfgNotifierAccount) )
	{
	    _notifierAccounts.push_back(cfg.value());
	    if ( _notifierAccounts.back().find('@') == string::npos )
		cfg.badValue();
	}
	else if ( cfg.iskey(CfgOpenRecipient) )
	    _openRecipients.push_back(cfg.value());
	else if ( cfg.iskey(CfgErrmailRecipient) )
	    _errmailRecipients.push_back(cfg.value());
	else if ( cfg.iskey(CfgErrmailHeaders) )
	    appendErrmailHeaders(cfg.value());
	else if ( cfg.iskey(CfgIncomingServer) )
	{
	    _iMailServerAddr.set(cfg.value(),
				 AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP);
	    if ( _iMailServerAddr.port()==0 )
		_iMailServerAddr.port(25);
	}
	else if ( cfg.iskey(CfgInBindAddr) )
	{
	    _iMailBindAddr.set(cfg.value(),
			       AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP);
	    _iMailBindAddr.port(0);
	}
	else if ( cfg.iskey(CfgOutgoingServer) )
	{
	    _oMailServerAddr.set(cfg.value(),
				 AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP);
	    if ( _oMailServerAddr.port()==0 )
		_oMailServerAddr.port(25);
	}
	else if ( cfg.iskey(CfgOutBindAddr) )
	{
	    _oMailBindAddr.set(cfg.value(),
			       AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP);
	    _oMailBindAddr.port(0);
	}
	else if ( cfg.iskey(CfgLog) )
	    _logFile = cfg.value();
	else if ( cfg.iskey(CfgLogLevel) )
	{
	    unsigned level;
	    cfg.getValue(level);
	    TheLog.loglevel(LogLevel(LogWarning + level));
	}
	else if ( cfg.iskey(CfgUser) )
	    _user = cfg.value();
	else if ( cfg.iskey(CfgGroup) )
	    _group = cfg.value();
	else if ( cfg.iskey(CfgMailActivation) )
	    cfg.getValue(_mailActivationPerMail);
	else if ( cfg.iskey(CfgMailActivationRedirect) )
	{
	    _mailActivationRedirect = cfg.value();
	    if ( _mailActivationRedirect.length() > MaxAddressLength )
		cfg.syntaxError(Msg(TXT("Mail address %0 too long."),
				    _mailActivationRedirect));
	}
	else if ( cfg.iskey(CfgLicenseFile) )
	    _licenseFile = cfg.value();
	else
	{
	    // won't return:
	    cfg.syntaxError(Msg(TXT("Invalid parameter name %0"), 
				cfg.key()));
	}
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::switchLog()
{
    if ( _logFile.empty() || _logFile=="-" )
	return;

    bool syslog = (_logFile == "syslog");

    if ( !syslog || !TheLog.usesSyslog() )
	Format(LogNotice, TheLog, TXT("Switching to log-file %0"), _logFile);
    if ( syslog )
	TheLog.syslog("pxmars", LogMail);
    else
	TheLog.setFile(_logFile);
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::switchPersonality()
{
    if ( !_group.empty() )
    {
	Format(LogNotice, TheLog, TXT("Switching to group %0"), _group);
	Group group(_group.c_str());
	SwitchGroup(group.sys_gid());
    }

    if ( _user.empty() )
    {
	User user(User::CurrentUserId());
	_user = user.name();
	Format(LogNotice, TheLog, TXT("Running as user %0"), _user);
    }
    else
    {
	Format(LogNotice,TheLog,TXT("Switching to user %0"),_user);
	User user(_user.c_str());
	SwitchPersonality(user.sys_uid());
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::appendErrmailHeaders(const std::string& filename)
{
    ifstream in(filename.c_str());
    if ( !in.good() )
    {
	SysLastErr e;
	throw SystemError(e, Msg(TXT("Cannot open file %0"), filename));
    }

    string line;

    while ( !in.eof() )
    {
	getline(in, line);
	Trim(line);
	if ( line.length()>0 )
	    _errmailHeaders.push_back(line);
    }
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::connectDB()
{
    _iDB.connect("mysql4_0", _iDbHost, _iDbName, _iDbUser, _iDbPassword);
    _xDB.connect("mysql4_0", _xDbHost, _xDbName, _xDbUser, _xDbPassword);
}

///////////////////////////////////////////////////////////////////////////////

const char* SmtpServerBase::FullMailDomain(const char* addr) throw ()
{
    char*    e    = strchr(addr, 0); // Find terminating nul
    unsigned dots = 0;

    while ( e!=addr )
    {
	switch ( *(--e) )
	{
	case '.':
	    //We found a dot. Count it.
	    ++dots;
	    break;

	case '@':
	    // We found an "at". This terminates the domain-part 
	    // (seen from right).
	    // If we didn't find any dots, the domain consists of only
	    // one part which isn't a full domain. (eg: me@de)
	    // Exception <X@localhost>
	    return (dots>0 || strcasecmp(e+1, "localhost")==0) ? (e+1) : 0;

	case '"': 
	    // Double-quotes can only terminate the local-part
	    // Hence, we don't have a domain-part.
	    return 0;
	}
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

MailAddrStatus MailAddrStatusDB(const char* s) throw ()
{
    if ( !s || *s==0 )
	return MailAddrInvalid;
    int x = atoi(s);
    if ( x<0 || x>3 )
	return MailAddrInvalid;
    else
	return MailAddrStatus(x+3);
}

///////////////////////////////////////////////////////////////////////////////

MailAddrStatus SmtpServerBase::statusExternal(char* addr, bool* exact)
{
    if ( *addr==0 )
	return MailAddrEmpty;

    // The mail address must not be abbreviated.
    char* at = const_cast<char*>(FullMailDomain(addr));
    if ( !at )
	return MailAddrInvalid;
    --at; //at actually pointed behind @

    // Otherwise the address status may be found in the database   
    SqlRequestParams<1> params;
    params[0] = SqlRequestParam(addr, strlen(addr));

    // Try to find complete address
    auto_ptr<SqlResult> res(_xDB->execute(FindExtern, params, SqlStoreLocal));

    const SqlRow* row = res->fetchRow();

    if ( row )
    {
	if ( exact )
	    *exact = true;
    }
    else
    {
	if ( exact )
	    *exact = false;

	char* s = at;

	try
	{

	    params[0] = SqlRequestParam(s, strlen(s));
	    res.reset(_xDB->execute(FindExtern, params, SqlStoreLocal));
	    row = res->fetchRow();
	    if ( !row )    // empty
	    {
		*s = '.'; // Replace @ with dot 

		for(;;)
		{
		    params[0] = SqlRequestParam(s, strlen(s));
		    res.reset(_xDB->execute(FindExtern, params,SqlStoreLocal));
		    row = res->fetchRow();
		    if ( row )
			break;
		    s = strchr(s+1, '.');
		    if ( !s )
		    {
			*at = '@'; // Restore @;
			return MailAddrGrey;
		    }
		}
	    }
	}
	catch ( ... )
	{
	    *at = '@'; // Restore @
	    throw;
	}
	
	*at = '@'; // Restore @
    }

    const char* col = row->get(0);
    return MailAddrStatusDB(col);
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::setExtern(
    const char*    extAddr, 
    MailAddrStatus mas,
    const char*    intAddr,
    bool           fUpdate,
    const char*    sessionId)
{
    if ( _auditActions )
    {
	Mutex::Lock x(_auditMutex);
	_audit << Now() << ' ' << sessionId 
	       << " !! " << intAddr << " admits " << extAddr << endl;
    }

    SqlRequestParams<3> params;
    unsigned short      status = ((unsigned short)mas)-3;

    if ( fUpdate )
    {
	params[0] = &status;
	params[1] = SqlRequestParam(intAddr, intAddr ? strlen(intAddr) : 0);
	params[2] = SqlRequestParam(extAddr, extAddr ? strlen(extAddr) : 0);
	auto_ptr<SqlResult> res(_xDB->execute(UpdateExtern, params, 
					      SqlStoreLocal));
    }
    else
    {
	params[0] = SqlRequestParam(extAddr, extAddr ? strlen(extAddr) : 0);
	params[1] = &status;
	params[2] = SqlRequestParam(intAddr, intAddr ? strlen(intAddr) : 0);
	auto_ptr<SqlResult> res(_xDB->execute(InsertExtern, params,
					      SqlStoreLocal));
    }
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::isOpenRecipient(const char* addr) throw ()
{
    StringVector::const_iterator pe = _openRecipients.end();

    for (StringVector::const_iterator p = _openRecipients.begin(); p!= pe; ++p)
    {
	if ( strcasecmp(addr, p->c_str())==0 )
	    return true;
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::hasInternalDomain(const char* addr) throw ()
{
    const char* domain = strrchr(addr, '@');
    if ( !domain )
	return true;
    ++domain;

    StringVector::const_iterator pe = _internalDomains.end();

    for (StringVector::const_iterator p = _internalDomains.begin(); p!=pe; ++p)
    {
	if ( p->compare(domain)==0 )
	    return true;
    }

    return false;
}


///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::isNotifierAccount(const char* addr) throw ()
{
    if ( strncasecmp(addr, Postmaster, sizeof(Postmaster)-1)==0 )
    {
	if ( addr[sizeof(Postmaster)-1]==0 || addr[sizeof(Postmaster)-1]=='@' )
	    return true;	
    }

    StringVector::const_iterator pe = _notifierAccounts.end();

    for (StringVector::const_iterator p = _notifierAccounts.begin(); 
	 p!= pe; ++p)
    {
	unsigned len = p->length();
	if ( strncasecmp(addr, p->c_str(), len)==0 )
	{
	    if ( addr[len]==0 || addr[len]=='@' )
		return true;
	}
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////

MailAddrStatus SmtpServerBase::statusInternal(char* addr, char* alias)
{    
    alias[0] = 0;

    if ( isOpenRecipient(addr) )
	return MailAddrOpen;

    size_t addrlen = strlen(addr);
    SqlRequestParams<1> params;
    params[0] = SqlRequestParam(addr, addrlen);

    // Try to find complete address
    auto_ptr<SqlResult> res(_iDB->execute(FindIntern, params, SqlStoreLocal));
    const SqlRow* row = res->fetchRow();
    if ( row )
    {
	const char* s = row->get(1); // alias
	if ( s )
	{
	    strncpy(alias, s, SmtpServerBase::MaxAddressLength);
	    alias[SmtpServerBase::MaxAddressLength] = 0;
	}
	return MailAddrStatusDB(row->get(0));
    }
    else
    {
	// TODO: Return InternInvalid if Intern is declared to be complete!

	if ( _privateAllow && matchesPrivatePattern(addr, alias) )
	    return MailAddrPrivate;

	return MailAddrWhite;
    }
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::matchesPrivatePattern(const char* addr, char* alias) 
throw ()
{
    *alias = 0;

    if ( !_privateAllow )
	return false;

    const char* e = FullMailDomain(addr);
    if ( e )
	--e; // Now e points to '@' terminating local-part
    else
	e = strchr(addr, 0);
    unsigned addrlen = e-addr;

    const std::string& prefix = _privatePrefix;
    const std::string& suffix = _privateSuffix;
    unsigned           prelen = prefix.length();
    unsigned           suflen = suffix.length();
    unsigned           len    = prelen+suflen;

    if ( len==0 || len>=addrlen )
	return false;

    if ( prelen )
    {
	if ( strncasecmp(addr, prefix.c_str(), prelen)!=0 )
	    return false;
    }
    if ( suflen )
    {
	if ( suflen>=addrlen )
	    return false;
	if ( strncasecmp(addr+(addrlen-suflen), suffix.c_str(), suflen)!=0 )
	    return false;
    }

    if ( _stripPrivate )
    {
	unsigned L = addrlen-len;
	strncpy(alias, addr+prelen, L);
	strcpy(alias+L, e);
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::isActivationAddress(
    char* addr,
    char* alias)
throw ()
{
    *alias = 0;
    if ( !_mailActivationPerMail )
	return false;

    // Adress format: u_JJJJ-MM-TT_hh-mm-ss_uuuuuu
    if ( strlen(addr)<28 || addr[0]!='u' || addr[1]!='_' )
	return false;

    char* p = addr+2;
    char* e;
    unsigned long v;

    // Year: 1900..9999
    v = strtoul(p, &e, 10);
    if ( v<1900 || v>9999 || *e!='-' || e-p!=4 )
	return false;
    p = e+1;

    // Month: 01..12
    v = strtoul(p, &e, 10);
    if ( v<1 || v>12 || *e!='-' || e-p!=2 )
	return false;
    p = e+1;

    // Day: 01..31
    v = strtoul(p, &e, 10);
    if ( v<1 || v>31 || *e!='_' || e-p!=2 )
	return false;
    p = e+1;

    // Hours: 00..23
    v = strtoul(p, &e, 10);
    if ( v>23 || *e!='-' || e-p!=2 )
	return false;
    p = e+1;

    // Minutes: 00..59
    v = strtoul(p, &e, 10);
    if ( v<1 || v>59 || *e!='-' || e-p!=2 )
	return false;
    p = e+1;

    // Seconds: 00..59
    v = strtoul(p, &e, 10);
    if ( v<1 || v>59 || *e!='_' || e-p!=2 )
	return false;
    p = e+1;

    // Microseconds: 000000..999999
    v = strtoul(p, &e, 10);
    if ( v>999999 || (*e!=0 && *e!='@') || e-p!=6 )
	return false;

    if ( *e=='@' )
	*e = 0;

    // Pattern matches!
    if ( !_mailActivationRedirect.empty() )
	StrCopy(_mailActivationRedirect, alias, MaxAddressLength+1);
    return true;
}

///////////////////////////////////////////////////////////////////////////////

MailAddrStatus SmtpServerBase::errmailHeader(
    MailAddrStatus mas,
    const string&  hdrPrefix,
    const char*    line,
    char*          addr)
{
    string::const_iterator p = hdrPrefix.begin();
    string::const_iterator e = hdrPrefix.end();

    while ( p!=e && *line )
    {
	if ( isspace(*p) )
	{
	    if (!isspace(*line)) return mas;
	    do ++line; while ( *line && isspace(*line) );
	    do ++p;    while ( p!=e && isspace(*p) );	    
	}
	else if ( *p != *line )
	    return mas;
	else
	{
	    ++p;
	    ++line;
	}
    }

    // BUG: "Unusual mail addresses will not be recognized correctly"
    while ( mas>=MailAddrWhite )
    {
	while (*line && isspace(*line)) ++line;
	if ( !*line )
	    break;
	const char* r = line+1;
	while ( *r && *r!=',' )
	    ++r;
	size_t len = r-line;
	if ( len>MaxAddressLength )
	    return MailAddrInvalid;
	strncpy(addr, line, len);
	addr[len] = 0;
	MailAddrStatus m = statusExternal(addr);
	if ( m>MailAddrEmpty && m<mas )
	    mas = m;
	if ( !*r )
	    break;
	line = r+1;
    }

    return mas;
}

///////////////////////////////////////////////////////////////////////////////

MailAddrStatus SmtpServerBase::errmailHeader(
    MailAddrStatus mas, 
    const char*    line,
    char*          addr) 
{
    StringVector::const_iterator ph = _errmailHeaders.begin();
    StringVector::const_iterator eh = _errmailHeaders.end();

    while ( ph!=eh && mas>=MailAddrWhite )
	mas = errmailHeader(mas, *ph++, line, addr);

    return mas;
}

///////////////////////////////////////////////////////////////////////////////

const char* SmtpServerBase::ParseMailPath(const char* buf, char* addr) throw ()
{
    if ( !buf )
	return 0;

    const char* p = buf;
    char* aa = addr;
    char* ea = addr + SmtpServerBase::MaxAddressLength;

    while ( *p && isspace(*p) )
	++p;

    if ( !*p || *p++ != '<' )
	return 0;

    // Skip route
    if ( *p=='@' )
    {
	++p;
	do
	{
	    if ( !*p )
		return 0;
	} while ( *p++ != ':' );
    }

    // Local-part
    if ( *p=='"' )
    {
	// Quoted string
	if ( !ExtendAddr(aa, ea, '"') )
	    return 0;
	++p;
	for(;;)
	{
	    if ( !*p )
		return 0;
	    if ( *p=='"' )
		break;
	    if ( *p=='\\' )
	    {
		if ( !ExtendAddr(aa, ea, '\\') || !*(++p) )
		    return 0;
	    }
	    if ( !ExtendAddr(aa, ea, *p++) )
		return 0;
	}
    }

    // Simply parse until '>'
    const char* gt = strchr(p, '>');
    if ( !gt )
	return 0;
    unsigned L = (gt-p);
    if ( L  > SmtpServerBase::MaxAddressLength-(aa-addr) )
	return 0;
    memcpy(aa, p, L);
    aa[L] = 0;

    return gt+1;
}

///////////////////////////////////////////////////////////////////////////////

const char* SmtpServerBase::ParseWord(const char* s, const char* p) throw ()
{
    p = ParseNext(s, p);
    if ( !p || (*p && !isspace(*p)) )
	return 0;

    while ( *p && isspace(*p) )
	++p;

    return p;
}

///////////////////////////////////////////////////////////////////////////////

const char* SmtpServerBase::ParseNext(const char* s, const char* p) throw ()
{
    if ( !p )
	return 0;

    while ( *s )
    {
	if ( !*p || toupper(*p++) != *s++ )
	    return 0;
    }

    return p;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::sendMailFile(
    const char* filename, 
    char*       buf, 
    char*       extAddr,       // Buffer! Not an input param!
    const char* intAddr,
    const char* destFilename,
    const char* sessionId,
    const char* redirection,
    const char* requiredSender)
{
    const char* s = strrchr(filename, '/');
    if ( s )
	++s;
    else
	s = filename;

    switch ( *s )
    {
    default : throw Error(Msg(TXT("550 Invalid file name %0"), filename));
    case 'o': throw Error(TXT("550 Resending outbound mail prohibited"));
    case 'u':
    case 'i': break;
    }

    // Open mail file
    ifstream in(filename);
    if ( !in.good() )
    {
	SysLastErr e;
	throw SystemError(e, Msg(TXT("550 Cannot open %0"), filename));
    }
    
    // Connect to MTA
    MailAddrStatus mas = MailAddrInvalid;
    Socket sock;
    sock.bind(_iMailBindAddr);
    sock.connect(_iMailServerAddr);
    receiveReply(sock.get(), buf, _maxLineLength, sessionId);
    snprintf(buf, _maxLineLength, "EHLO %s", _hostname.c_str());
    transferCommand(sock.get(), buf, strlen(buf), sessionId);
    
    // Skip header of mail file
    while ( ReadLine(in, buf, _maxLineLength)>0 )
	/**/;

    // Now transfer all up to DATA, inclusively
    bool     redirected = false;
    unsigned len;  
    for(;;)
    {
	len = ReadLine(in, buf, _maxLineLength);
	if ( extAddr 
	     && ParseMailPath(ParseNext("FROM:",
					ParseWord("MAIL",buf)), extAddr) )
	{
	    bool exact;
	    if ( requiredSender && strcasecmp(requiredSender, extAddr)!=0 )
		throw Error(TXT("550 Sender mismatch"));
	    mas = statusExternal(extAddr, &exact);
	    if ( _auditStatus )
	    {
		Mutex::Lock x(_auditMutex);
		_audit << Now() << ' ' << sessionId
		       << " ?s " << String(mas) << ' ' << extAddr << endl;
	    }
	    if ( mas==MailAddrGrey )
		setExtern(extAddr, MailAddrWhite, intAddr, exact, sessionId);
	    extAddr = 0;
	}
	else if ( redirection && ParseWord("RCPT", buf) )
	{
	    if ( redirected )
		continue; // Ignore/Skip RCPT
	    snprintf(buf, _maxLineLength, "RCPT TO:<%s>", redirection);
	    len = strlen(buf);
	    redirected = true;
	}
	else if ( ParseNext("DATA", buf) )
	{
	    transferCommand(sock.get(), buf, len, sessionId);
	    break;
	}
	transferCommand(sock.get(), buf, len, sessionId);
    } 

    // Transfer mail data
    do
    {
	len = ReadLine(in, buf, _maxLineLength);
	transferLine(sock.get(), buf, len, sessionId);
    } while ( buf[0]!='.' || buf[1]!=0 );

    // Receive data transfer result
    receiveReply(sock.get(), buf, _maxLineLength, sessionId);

    if ( destFilename && mas>=MailAddrGrey )
	moveFile(filename, destFilename, sessionId, true);

    // Say bye (ignore answer)
    sock.send("QUIT"CRLF, 6);
    SmtpReadLine(sock.get(), buf, _maxLineLength, 5);
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::moveByCopy(
    const char* from,
    const char* to)
{
    const size_t BlkSize = 4096;
    char* buf = new char[BlkSize];
    int ifd = -1;
    int ofd = ::open(to, O_CREAT|O_EXCL|O_WRONLY, 0660);
    if ( ofd==-1 )
	goto fail0;
    ifd = ::open(from, O_RDONLY);
    if ( ifd==-1 )
	goto fail2;

    ssize_t rd, wr;
    for(;;)
    {
	rd = ::read(ifd, buf, BlkSize);
	if ( rd==0 )
	    break;
	if ( rd<0 )
	    goto fail3;
	wr = ::write(ofd, buf, rd);
	if ( wr<0 )
	    goto fail3;
    }

    ::close(ifd);
    if ( ::close(ofd)!=0 )
	goto fail1;
    ::unlink(from);
    return true;


 fail3: 
    {
	int e = errno;
	::close(ifd);
	errno = e;
    }

 fail2:
    {
	int e = errno;
	::close(ofd);
	errno = e;
    }

 fail1:
    {
	int e = errno;
	::unlink(to);
	errno = e;
    }
    
 fail0:
    delete[] buf;
    return false;
}

///////////////////////////////////////////////////////////////////////////////

bool SmtpServerBase::moveFile(
    const char* from, 
    const char* to, 
    const char* sessionId,
    bool        induced)
{
    if ( _auditActions )
    {
	Mutex::Lock x(_auditMutex);
	_audit << Now() << ' ' << sessionId
	       << (induced ? " mv " : " MV ") << from << " --> " << to << endl;
    }

    if ( ::rename(from, to)==0 )
	return true;

    int e = errno;
    if ( e==EXDEV )
	return moveByCopy(from, to);

    Format(LogError, TheLog, TXT("Cannot move file %0 to %1: %2"), 
	   from, to, 
	   SystemError::Message(e));

    errno = e;

    return false;
}

///////////////////////////////////////////////////////////////////////////////

unsigned SmtpServerBase::ReadLine(istream& in, char* buf, unsigned len)
{
    char* p    = buf;
    bool crMet = false;

    while ( !in.eof() )
    {
	if ( unsigned(p-buf)==len )
	    throw Error(TXT("500 File contains line which is too long"));
	int c = in.get();	
	if ( crMet && c==0xa )
	{
	    --p;
	    *p = 0;
	    return unsigned(p - buf);
	}

	crMet = (c==0x0d);
	*p++ = char(c);
    }

    throw Error(TXT("550 Unexpected end of file"));
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::transferCommand(
    SysSocket sock, char* buf, unsigned len, const char* sessionId)
{
    transferLine(sock, buf, len, sessionId);
    receiveReply(sock, buf, _maxLineLength, sessionId);
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::transferLine(SysSocket sock, char* buf, unsigned len, 
				  const char* sessionId)
{
    if ( _auditServerOut )
    {
	Mutex::Lock x(_auditMutex);
	_audit << Now() << ' ' << sessionId
	       << " so |" << buf << "| " << _iMailServerAddr << endl;
    }

    char* s = buf+len;
    s[0] = 0x0d;
    s[1] = 0x0a;

    if ( ::send(sock.value, buf, len+2, 0)==-1 )
    {
	SysLastErr e;
	SocketAddr addr;
	addr.setFromPeer(sock);
	throw SystemError(e, Msg(TXT("550 Transfer to %0 failed"), 
				 _iMailServerAddr));
    }

    *s = 0;
}

///////////////////////////////////////////////////////////////////////////////

void SmtpServerBase::receiveReply(
    positix::SysSocket sock, char* buf, unsigned len, const char* sessionId)
{
    int  rc;
    bool ok = true;

    for(;;)
    {
	rc = SmtpReadLine(sock, buf, len, _serverTimeout);

	if ( rc>=0 )
	{
	    if ( _auditServerIn )
	    {
		Mutex::Lock x(_auditMutex);
		_audit << Now() << ' ' << sessionId
		       << " si |" << buf << "| " << _iMailServerAddr << endl;
	    }

	    char*        ep;
	    unsigned long u = strtoul(buf, &ep, 10);
	    if ( u<100 || u>=1000 )
	    {
		throw Error(
		  Msg(TXT("Transfer to %0 failed due to invalid response: %1"),
		      _iMailServerAddr, buf));
	    }
	    ok &= (u>=100 && u<400);

	    if ( *ep!='-' )
	    {
		if ( !ok )
		    throw Error(Msg(TXT("Transfer to %0 failed: %1"),
				    _iMailServerAddr, buf));
		return;
	    }
	}
    }

    switch ( rc )
    {
    default: throw Error("Internal error");
    case -1: 
    case -2: throw Error(TXT("Line too long for remote server"));
    case -3: throw Error(TXT("Remote server timeout"));
    case -4: {SysLastErr e; throw SystemError(e, TXT("Read error"));}
    case -5: throw Error(TXT("Connection closed by remote server"));
    }
}

///////////////////////////////////////////////////////////////////////////////

// Return codes:
// OK                 : >=0, Length of line (excluding CRLF, excl. NUL)
// Line too long      : -1
// Line much too long : -2
// Timeout            : -3
// Read Error         : -4
// Connection shutdown: -5
// Hence, connection should be terminated on negative return values

int SmtpReadLine(SysSocket sock, char* buf, unsigned maxlen, unsigned timeout)
throw ()
{
    char*          b = buf;
    char*          c = buf;
    unsigned       L = maxlen;
    time_t         E = time(0)+timeout+1;
    fd_set         fds;
    struct timeval tv;

 skip:
    while ( L > 0 )
    {
	if ( timeout )
	{
	    time_t now = time(0);
	    if ( now >= E )
		return -3;
	    tv.tv_sec  = E - time(0);
	    tv.tv_usec = 0;

	    FD_ZERO(&fds);
	    FD_SET(sock.value, &fds);
	    int c = select(sock.value+1, &fds, 0, 0, &tv);
	    if ( c==0 )
		return -3;
	}
	
	ssize_t rc = recv(sock.value, b, L, MSG_PEEK);
	if ( rc==0 )
	    return -5;

	if ( rc==-1 )
	{
	    *b = 0;
	    return -4;
	}

	char* e    = b+rc;
	char* crlf = FindCRLF(c, e);
	if ( crlf )
	{
	    int len = int(crlf-b);
	    recv(sock.value, b, len, 0);
	    crlf[-2] = 0;
	    if ( maxlen==0 ) // Rest of a too long line?
		return -1;
	    return len-2;
	}

	recv(sock.value, b, rc, 0);
	L -= rc;
	b  = e;
	c  = b-1;
    }

    // Line is too long
    // Ignore lines which are a "bit" longer than allowed.
    // A bit longer means nearly twice as long as maxlen.
    // But even longer lines will not be accepted and the connection will
    // be shut down.
    // If a line has maxlen-1 characters including the terminating CRLF, 
    // the line chunks will be splitted exactly between CR and LF.
    // Hence we will retain the very last character of the first line part
    if ( maxlen==0 )
	return -2;
    L = maxlen-1;
    maxlen = 0; // Set maxlen=0 to indicate "line too long"
    buf[0] = buf[L];
    c = buf;
    b = buf+1;
    goto skip;
}

///////////////////////////////////////////////////////////////////////////////
