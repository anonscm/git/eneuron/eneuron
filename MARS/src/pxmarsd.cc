/*
*  eneuron, corporate solution for email archiving
*  Copyright (C) 2009 positix GmbH
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
*  eneuron, Copyright (C) 2009, positix GmbH
*  eneuron comes with ABSOLUTELY NO WARRANTY.
*  This is free software, and you are welcome to redistribute it
*  under certain conditions.
*/
///////////////////////////////////////////////////////////////////////////////
// Module: positix Mail And Relay Service (MARS)
// File  : pxmarsd.cc
///////////////////////////////////////////////////////////////////////////////
// $Id$
///////////////////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <syslog.h>
#include "SmtpServer.hh"
#include "msg.hh"

using namespace std;
using namespace positix;

///////////////////////////////////////////////////////////////////////////////

static volatile bool StopAsap = false;
static bool DaemonMode        = false;
static bool WatchDogMode      = true;

static SmtpServer TheSmtpServer;

LogStream TheLog;

///////////////////////////////////////////////////////////////////////////////

static void InstallTerminationHandler(void (*handler)(int))
{
    struct sigaction oldAction, newAction, ignAction;

    newAction.sa_handler = handler;
    newAction.sa_flags   = 0;
    sigemptyset(&newAction.sa_mask);

    ignAction.sa_handler = SIG_IGN;
    ignAction.sa_flags   = 0;
    sigemptyset(&ignAction.sa_mask);

    for (int i = 1; i <= NSIG; ++i)
    {
	switch ( i )
	{
	default:
	    sigaction(i, &ignAction, 0);
	    break;

	case SIGTERM  :
	case SIGINT   :
	case SIGQUIT  :
	case SIGPIPE  :
	    sigaction(i, 0, &oldAction);
	    if ( oldAction.sa_handler != SIG_IGN )
		sigaction(i, &newAction, 0);
	    break;

	case SIGCHLD:
	    signal(i, SIG_DFL);
	    break;

	case SIGSEGV  :
	case SIGBUS   :
	case SIGTRAP  :
	case SIGSYS   :
	case SIGKILL  :
	case SIGALRM  :
	case SIGVTALRM:
	case SIGPROF  :
	case SIGIO    :
	case SIGURG   :
	case SIGSTOP  :
	case SIGCONT  :
	case SIGTTIN  :
	case SIGTTOU  :
	case SIGXCPU  :
	case SIGXFSZ  :
	    // Don't change handler!
	    break;
	}
    }
}

///////////////////////////////////////////////////////////////////////////////

static void TerminationHandler(int /*signum*/)
{    
    TheSmtpServer.stop();
    StopAsap = true;
}

///////////////////////////////////////////////////////////////////////////////

static void SigChild(int /*signum*/)
{    
    // empty
}

///////////////////////////////////////////////////////////////////////////////

static void EnsureOpenNonTerminal(int fd)
{
    struct stat st;
    if ( fstat(fd, &st)==-1 || isatty(fd) )
    {
	int nfd = ::open("/dev/null", O_RDWR);
	if ( nfd==-1 || dup2(nfd, fd)==-1 )
	{
	    int e = errno;
	    syslog(LOG_MAIL|LOG_CRIT, 
		   "Cannot open /dev/null for file descriptor %d: %s", 
		   fd, strerror(e));
	    exit(1);
	}
	close(nfd);
    }
}

///////////////////////////////////////////////////////////////////////////////

static bool MakeProcessLeader(int& argc, char* argv[])
{
    PxOptionParser opt(argc, argv);

    while ( opt.next() )
    {
	if ( opt.iskey("daemon") )
	    DaemonMode = PxBoolOpt(opt.removeCurrent());
	else if ( opt.iskey("nowatchdog") )
	    WatchDogMode = PxBoolOpt(opt.removeCurrent());
    }

    if ( DaemonMode )
    {
	pid_t pid = fork();

	if ( pid_t(-1) == pid )
	{
	    int e = errno;
	    throw positix::SystemError(e, TXT("Cannot create child process"));
	}
	else if ( pid == 0 )
	{
	    // Child process
	    if ( -1==setsid() )
	    {
		int e = errno;
		cerr << "setsid: " << strerror(e) << endl;
	    }
	    Format(LogInfo, TheLog, TXT("Running as daemon"));
	    TheLog.syslog("pxmars", LogMail);
	    EnsureOpenNonTerminal(0);
	    EnsureOpenNonTerminal(1);
	    EnsureOpenNonTerminal(2);
	    chdir(POSITIX_DIR_SEP);
	    
	    return false;
	}
	else 
	{
	    // Parent process
	    Format(LogInfo, TheLog, TXT("Started daemon process %0"), pid);
	    _exit(0);
	    return true;
	}
    }
    else
    {
	Format(LogInfo, TheLog, TXT("Running as normal process"));
	return false;
    }
}

///////////////////////////////////////////////////////////////////////////////

static bool ForkWatchDog()
{
    for(;;)
    {
	InstallTerminationHandler(TerminationHandler);

	pid_t pid = fork();

	if ( pid_t(-1) == pid )
	{
	    int e = errno;
	    throw positix::SystemError(
		e, 
		TXT("Cannot create server process"));
	}
	else if ( pid==0 )
	{
	    // Child process
	    return false;
	}

	sigset_t sigset, old;

	signal(SIGCHLD, SigChild);
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGTERM);
	sigaddset(&sigset, SIGINT);
	sigaddset(&sigset, SIGQUIT);

	sigprocmask(SIG_BLOCK, &sigset, &old);
	sigsuspend(&old);
	sigprocmask(SIG_UNBLOCK, &sigset, 0);

	if ( StopAsap )
	{
	    kill(pid, SIGTERM);
	    return true;
	}
	
	waitpid(pid, 0, 0);
	Format(LogError, TheLog, "%0", TXT("Restarting server"));
	signal(SIGCHLD, SIG_IGN);
    }
}

///////////////////////////////////////////////////////////////////////////////

#include <sys/resource.h>

int main(int argc, char* argv[])
{
    try
    {
	if ( MakeProcessLeader(argc, argv) )
	    return 0;

// TODO:
//   	if ( DaemonMode && WatchDogMode && ForkWatchDog() )
//   	    return 0;

 	Format(LogInfo, TheLog, TXT("Installing signal handlers"));
 	InstallTerminationHandler(TerminationHandler);

	TheSmtpServer.configure(argc, argv);
	TheSmtpServer.switchLog();
	TheSmtpServer.run();

	return 0;
    }
    catch ( positix::Error& e )
    {
	Format(LogError, TheLog, e.msg());
    }
    catch ( std::exception& e )
    {
	Format(LogError, TheLog, TXT("A runtime exception was raised: %0"),
	       e.what());
    }
    catch ( ... )
    {
	Format(LogError, TheLog, TXT("An unknown exception was raised"));
    }

    return 2;
}


// Local Variables: ***
// mode:c++ ***
// End: ***

// end of positix MARS: pxmarsd.cc
