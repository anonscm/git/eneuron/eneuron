#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="a.conf"
export MAILRC=$(pwd)/mailrc.intern

echo Running $test

source defs.sh

echo "Testfälle intern"
echo "(1) Internes Ziel (bekannt bei MTA/MDA, unbekannt in Intern)"
echo "     --> $intN0"
echo "(2) Kein Eintrag für externes Ziel in Extern (bisher unbekannt)"
echo "    --> $extU1"
echo "(3) Exakter Eintrag für Ziel in Extern, erwünscht"
echo "     --> $extEW1"
echo "(4) Exakter Eintrag für Ziel in Extern, unerwünscht,"
echo "    (Muss: black-target-action=admit)"
echo "    --> $extEB1"
echo "(5) Übergeordneter Eintrag für Ziel in Extern, erwünscht"
echo "    --> extEAW1"
echo "(6) Übergeordneter Eintrag für Ziel in Extern, unerwünscht"
echo "    (Muss: black-target-action=admit)"
echo "     --> $extEDB1"
echo "(7) Unbekannter externer Empfänger"
echo "    --> nichtda$extAW1"

echo Sending test mail using $MAILRC
set -x
mail -n -s "positix MARS $test" \
     -r $intN0   \
     -c $extU1   \
     -c $extEW1  \
     -c $extEB1  \
     -c $extEAW1 \
     -c $extEDB1 \
     -c nichtda$extAW1 \
     $intN0 <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
Konf: $conf
EOF
set +x

echo "*** Ergebnis sollte sein:"
echo "(1) $intN0 erhält Mail"
echo "(2) $extU1 in Extern als erwünscht eingetragen (neu)"
echo "... $extU1 erhält Mail"
echo "(3) $extEW1 in Extern als erwünscht eingetragen (unverändert)"
echo "... $extEW1 erhält Mail"
echo "(4) $extEB1 in Extern als erwünscht eingetragen (geändert)"
echo "... $extEB1 erhält Mail"
echo "(5) Kein Eintrag für $extEAW1 in Extern"
echo "... $extAW1 weiterhin als erwünscht eingetragen"
echo "... $extEAW1 erhält Mail"
echo "(6) $extEDB1 in Extern als erwünscht eingetragen"
echo "... $extDB1 weiterhin als unerwünscht eingetragen"
echo "... $extEDB1 erhält Mail"
echo "(7) $intN0 erhält Benachrichtigung über Nicht-Zustellung an $nichtda$extAW1"
echo "(-) Mail wurde unter *ausgehend* gesichert"
echo "***"

echo Finished $0
