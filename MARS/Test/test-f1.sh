#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="act1.conf"
export MAILRC="mailrc.extern"

echo Running $test

source defs.sh

echo "Testfall Freischaltung"
echo "(f1) Sicherungsdatei nicht vorhanden"
echo "Muss root sein!"

set +x
/opt/positix/bin/pxmars -cfg="$conf" u_1980-01-02_03-04-05_123456

mail -n -s "positix MTA $test" \
     -r roland@white.test \
     u_1980-01-02_03-04-05_123456 <<EOF
Hello you!
EOF


echo "**** Ergebnis sollte sein:"
echo "(a) Cannot open ...."
echo "(b) Unknown recipient"
echo "****"


echo Finished $0
