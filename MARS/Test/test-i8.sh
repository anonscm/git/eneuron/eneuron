#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test="Test-i8"
conf="g.conf"
export MAILRC=$(pwd)/mailrc.intern

echo Running $test

source defs.sh

echo "Testfälle intern"
echo "(13) Privater Absender (privat.x.privat)"
echo "(14) Nicht-privater Absender (privat.privat) [nicht existent]"
# $conf muss enthalten:
#    private-allow  yes
#    private-strip  yes
#    private-prefix "privat."
#    private-suffix ".privat"

echo Sending test mail using $MAILRC
set -x
mail -v -n -s "positix MTA $test (13)" \
     -r roland@$here \
     privat.root.privat@$here <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
EOF
set +x

echo Sending test mail using $MAILRC
set -x
mail -v -n -s "positix MTA Testsuite $test (14)" \
     -r roland@$here \
     privat.privat@$here <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
EOF
set +x

echo "**** Ergebnis sollte sein:"
echo "(13) root@$here erhält Mail mit (13) im Betreff"
echo ".... Mail wurde *nicht* gesichert"
echo "(14) Mail an privat.privat wurde abgewiesen"
echo ".... Server klassifiziert privat.privat als MailAddrWhite"
echo ".... Es gibt keine zugehörige Sicherung in TMP (Fehlerfall)"
echo "****"

echo Finished $0

