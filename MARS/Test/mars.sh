#!/bin/sh
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

logdir=/var/opt/positix/mars/test
logname="$logdir/$(date '+%F_%T')_$1"

if [ -z "$1" ] ; then
    echo Bitte Konfigurationsdatei angeben
    exit 1
fi
cfg="$1"
shift

# Sicherstellen, dass kein MARS-Prozess läuft
if ps -C pxmarsd ; then
    echo Es läuft bereits ein MARS-Prozess
    exit 1
fi

# Datenbank initialisieren
sh init-db.sh

# Datenbank ausgeben
sh dump-db.sh 2>&1 | tee "$logname.db"

# Umgebungsvariable MAILRC für Benachrichtigungen setzen:
export MAILRC=$(pwd)/mailrc.mars

# MARS starten
mkdir -p $logdir
echo Log in "$logname"
set -x
/opt/positix/bin/pxmarsd -cfg="$cfg" $* 2>&1 | tee "$logname.log"
set +x

# Nachdem MARS beendet wurde
sh dump-db.sh >> "$logname.db"

# Datenbankinhalte (vorher, nachher) anzeigen
echo Datenbankinhalte:
cat "$logname.db"
