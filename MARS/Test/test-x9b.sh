#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="A.conf"

echo Running $test

source defs.sh

echo "Testfall extern"
echo "(9b) Leerer Absender, mit geeigneter Information in Mail"
echo ".... Absender bekannt und erwünscht (Fall E7b)"

echo Sending test mail using netcat
set -x
netcat -vv -n -s 192.168.1.50 192.168.0.50 25 <<EOF
ehlo localhost
mail from:<>
rcpt to:<$intN1>
data
Received: from [194.64.31.10] (helo=mail02.pironet-ndh.com) by
        mxeu5.kundenserver.de with ESMTP (Nemesis), id 1CfD8X1gvI-0007D0;
        Fri, 17 Dec 2004 09:06:53 +0100
Received: by mail02.pironet-ndh.com (Postfix) id 24B6D557D3; Fri, 17 Dec
        2004 09:06:53 +0100 (CET)
Date: Fri, 17 Dec 2004 09:06:53 +0100 (CET)
From: MAILER-DAEMON@mail02.pironet-ndh.com (Mail Delivery System)
Subject: Undelivered Mail Returned to Sender
To: roland.laznik@positix.de
MIME-Version: 1.0
Content-Type: multipart/report; report-type=delivery-status; boundary="911A355768.1103270813/mail02.pironet-ndh.com"
Message-Id: <20041217080653.24B6D557D3@mail02.pironet-ndh.com>
Envelope-To: roland.laznik@positix.de
X-SpamScore: 0.000
X-Evolution-Source: pop://m35142333-1@pop.1und1.com/

This is a MIME-encapsulated message.

--911A355768.1103270813/mail02.pironet-ndh.com
Content-Description: Notification
Content-Type: text/plain

This is the Postfix program at host mail02.pironet-ndh.com.

I'm sorry to have to inform you that your message could not be
be delivered to one or more recipients. It's attached below.

For further assistance, please send mail to <postmaster>

If you do so, please include this problem report. You can
delete your own text from the attached returned message.

                        The Postfix program

--911A355768.1103270813/mail02.pironet-ndh.com
Content-Description: Delivery report
Content-Type: message/delivery-status

Reporting-MTA: dns; mail02.pironet-ndh.com
X-Postfix-Queue-ID: 911A355768
X-Postfix-Sender: rfc822; roland.laznik@positix.de
Arrival-Date: Fri, 17 Dec 2004 09:06:52 +0100 (CET)

Final-Recipient: rfc822; $extEW1
Action: failed
Status: 5.0.0

--911A355768.1103270813/mail02.pironet-ndh.com
Content-Description: Undelivered Message
Content-Type: message/rfc822

Received: from moutng.kundenserver.de (moutng.kundenserver.de
        [212.227.126.177]) by mail02.pironet-ndh.com (Postfix) with ESMTP id
        911A355768 for <$extEW1>; Fri, 17 Dec 2004 09:06:52 +0100 (CET)
Received: from [212.227.126.205] (helo=mrelayng.kundenserver.de) by
        moutng.kundenserver.de with esmtp (Exim 3.35 #1) id 18W-0000YZ-00 for
        $extEW1; Fri, 17 Dec 2004 09:06:52 +0100
Received: from [80.133.116.108] (helo=[192.168.0.50]) by
        mrelayng.kundenserver.de with esmtp (Exim 3.35 #1)
        id fD8W-0005YP-00 for $extEW1; Fri, 17 Dec 2004 09:06:52 +0100
Subject: ?
From: Roland Laznik <roland.laznik@positix.de>
To: $extEW1
Content-Type: text/plain
Message-Id: <1103270779.3461.3.camel@pirx.localdomain>
Mime-Version: 1.0
X-Mailer: Ximian Evolution 1.4.6 
Date: Fri, 17 Dec 2004 09:08:02 +0100
Content-Transfer-Encoding: 7bit
X-Provags-ID: kundenserver.de abuse@kundenserver.de
        auth:6223fa4226069e40f1b82da0ec21cb57

?


--911A355768.1103270813/mail02.pironet-ndh.com--
.
quit
EOF
set +x

echo "*** Ergebnis sollte sein:"
echo "(x9b) Mail wird direkt akzeptiert (relayed)"
echo "..... $intN1 erhält Mail"
echo "***"

echo Finished $0
