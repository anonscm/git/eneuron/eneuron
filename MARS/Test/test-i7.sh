#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test="Test-i7"
conf="f.conf"
export MAILRC=$(pwd)/mailrc.intern

echo Running $test

source defs.sh

echo "Testfall intern"
echo "(13) Privater (x.privat) Absender per Muster und private-strip=yes"
# $conf muss enthalten:
#    private-allow  yes
#    private-strip  yes
#    private-prefix ""
#    private-suffix ".privat"

echo Sending test mail using $MAILRC
set -x
mail -v -n -s "positix MTA $test" -r roland@$here \
     root.privat@$here <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
EOF
set +x

echo "**** Ergebnis sollte sein:"
echo "(13) root@$here erhält Mail"
echo ".... Mail wurde *nicht* gesichert"
echo "... Insbesondere existiert keine Sicherung in TMP"
echo "****"

echo Finished $0

