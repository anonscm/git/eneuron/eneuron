#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="A.conf"

echo Running $test

source defs.sh

echo "Testfall extern"
echo "(9a) Leerer Absender, mit geeigneter Information in Mail"
echo ".... Ein Absender unbekannt"

echo Sending test mail using telnet
set -x
netcat -vv -n -s 192.168.1.50 192.168.0.50 25 <<EOF
ehlo localhost
mail from:<>
rcpt to:<$intN1>
data
Received: from [212.227.126.176] (helo=moutng.kundenserver.de) by
      mxng06.kundenserver.de with esmtp (Exim 3.35 #1) id 1Ch17i-0004uf-00
      for roland.laznik@positix.de; Wed, 22 Dec 2004 08:41:30 +0100
Received: from mail by moutng.kundenserver.de with local (Exim 3.35 #1) id
      1Ch17i-00 for roland.laznik@positix.de; Wed, 22 Dec 2004 08:41:30 +0100
X-Failed-Recipients: $extEW1, $extU1
From: Mail Delivery System <Mailer-Daemon@moutng.kundenserver.de>
To: $intN1
Subject: Mail delivery failed: returning message to sender
Message-Id: <E1Ch17i-0007Yz-00@moutng.kundenserver.de>
Date: Wed, 22 Dec 2004 08:41:30 +0100
Mime-Version: 1.0

This message was created automatically by mail delivery software (Exim).

A message that you sent could not be delivered to one or more of its
recipients. This is a permanent error. The following address(es) failed:

  nd2@positix.de
    SMTP error from remote mailer after RCPT TO:<nd2@positix.de>:
    host mx00.kundenserver.de [212.227.15.169]: 550 <nd2@positix.de>:
    invalid address
  nd1@positix.de
    SMTP error from remote mailer after RCPT TO:<nd1@positix.de>:
    host mx00.kundenserver.de [212.227.15.169]: 550 <nd1@positix.de>:
    invalid address

------ This is a copy of the message, including all the headers. ------

Return-path: <roland.laznik@positix.de>
Received: from [212.227.126.155] (helo=mrelayng.kundenserver.de)
        by moutng.kundenserver.de with esmtp (Exim 3.35 #1)
        id 1Ch17e-0007YV-00; Wed, 22 Dec 2004 08:41:26 +0100
Received: from [80.133.113.123] (helo=[192.168.0.50])
      by mrelayng.kundenserver.de with asmtp (TLSv1:EDH-RSA-DES-CBC3-SHA:168)
      (Exim 3.35 #1)
      id 1Ch17e-0001c5-00; Wed, 22 Dec 2004 08:41:26 +0100
Subject: Failure
From: Roland Laznik <roland.laznik@positix.de>
To: nd1@positix.de
Cc: nd2@positix.de
Content-Type: text/plain
Message-Id: <1103701375.3501.15.camel@pirx.localdomain>
Mime-Version: 1.0
X-Mailer: Ximian Evolution 1.4.6
Date: Wed, 22 Dec 2004 08:42:55 +0100
Content-Transfer-Encoding: 7bit

Hello,

this mail will be rejected
.
quit
EOF
set +x

echo "*** Ergebnis sollte sein:"
echo "(x9a) Mail wird nach kompletten DATA-Transfer abgewiesen"
echo "***"


echo Finished $0
