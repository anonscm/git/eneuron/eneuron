#!/bin/sh
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

here=positix.localdomain
db=pxmars

# Externe Adressen:
# Exakt Erwünscht Adresse   Bemerkung
# Ja    Ja        $extEW1
# Ja    Ja        $extEW2   Ausnahme zu $extDB1
# Ja    Nein      $extEB1   Ausnahme zu $extAW1
# Nein  Ja        $extDW1
# Nein  Ja        $extAW1
# Nein  Nein      $extDB1
# Nein  Nein      $extAB1
#
# Unbekannter Absender: $extU1
extAB1=@atblack.test
extAW1=@atwhite.test
extDB1=.dotblack.test
extDW1=.dotwhite.test
extEB1=root@black.test
extEW1=roland@white.test
extEW2=root@alsowhite.test
extU1=root@unknown.test

# Konten für Domains
extEAB1=root$extAB1
extEDB1=roland@${extDB1:1}
extEAW1=root$extAW1
extEDW1=roland@${extDW1:1}

# Interne Adressen:
# Status Code   Adresse   Alias
# normal 1      $intN1
# offen  2      $intO1
# offen  2      $intO2    $intO2a
# privat 3      $intP1    $intP1a
# privat 3      $intP2    $intP2a
intN0=sl@$here
intN1=rl@$here
intO1=root@$here
intO2=open@$here
intO2a=rl@$here
intP1=privat.rl@$here
intP1a=rl@$here
intP2=ppp@$here
intP2a=root@$here
intPM1=rl.privat@$here
