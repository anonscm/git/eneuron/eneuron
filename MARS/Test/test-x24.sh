#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="D.conf"

echo Running $test

source defs.sh

function generateSpaces () {
    local i
    for (( i=0; $i<$1; ++i )) ; do
	echo -n ' '
    done
}

echo "Testfall extern"
echo "(24) Ewige Zeile"

echo Sending test mail using netcat
netcat -vv -n -s 192.168.1.50 192.168.0.50 25 <<EOF
ehlo localhost
mail from:$(generateSpaces 10000)<$extEW1>
quit
EOF

echo "*** Ergebnis sollte sein:"
echo "(24) MARS trennt Verbindung"
echo "***"


echo Finished $0
