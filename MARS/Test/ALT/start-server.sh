#!/bin/sh
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.
echo Starting Server $1 using $2
export MAILRC=$(pwd)/mailrc.server
if [ -z "$1" ] ; then
    echo Missing argument 1: Test identifier
    exit
fi
if [ -z "$2" ] ; then
    echo Missing argument 2: Server binary
    exit
fi
if [ -z "$3" ] ; then
    echo Missing argument 3: configuration file
    exit
fi
$2 -cfg="$3" 2>&1 | tee $HOME/out/log/$(date '+%F_%T')_"$1".log
