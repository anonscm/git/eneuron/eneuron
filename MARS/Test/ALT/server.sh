#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.
echo Starting Server $1 using $2
if [ -z "$pxmsd" ] ; then
    echo Variable pxmsd not set
    exit
fi
if [ -z "$conf" ] ; then
    echo Variable conf not set
    exit
fi
if [ -z "$test" ] ; then
    echo Variable test not set
    exit
fi

echo Looking for running server
if pidof $(basename $pxmsd) ; then
    echo Server is already running. Test aborted.
    exit 1
fi
echo Invoking server
xterm -geometry 500x500 -T "Server $test" \
      -e sh start-server.sh "$test" "$pxmsd" "$conf" &
# Give server some time to start-up
sleep 1
if ! pidof $(basename $pxmsd) ; then 
    echo Server not started. Test aborted.
    exit 1
fi
