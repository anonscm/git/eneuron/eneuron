#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test="Test-i10"
conf="i.conf"

echo Running $test

source defs.sh
source server.sh

echo Creating Database
mysql -u root < ../mysql-create.sql

echo Initializing Database
mysql -u root $db <<EOF
USE $db;
DELETE FROM Intern;
DELETE FROM Extern;
INSERT Extern VALUES('info@positix.de', 0, NULL, NULL);
EOF

echo "Testfall intern"
echo "(16) Exakter Eintrag für Ziel in Extern, unerwünscht,"
echo "     black-target-action reject"

source dump-db.sh

echo Sending test mail using $MAILRC
set -x
mail -n -s "positix MTA $test" \
     -c support@positix.de  \
     -c info@positix.de     \
     -c license@positix.de  \
     -c nichtda@positix.com \
     roland@$here <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
EOF
set +x

source dump-db.sh

echo "**** Ergebnis sollte sein:"
echo "(16) Mail wurde abgewiesen (recipient is blacklisted)"
echo "****"


echo Finished $0
