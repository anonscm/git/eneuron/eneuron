#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test="Test-i6"
conf="e.conf"

echo Running $test

source defs.sh
source server.sh

echo Creating Database
mysql -u root < ../mysql-create.sql

echo Initializing Database
mysql -u root $db <<EOF
USE $db;
DELETE FROM Intern;
DELETE FROM Extern;
EOF

echo "Testfalle intern"
echo "(12) Nicht-Privater (PS) Absender"
# $conf muss enthalten:
#    private-allow  yes
#    private-strip  no
#    private-prefix "rol"
#    private-suffix "and"

source dump-db.sh

echo Sending test mail using $MAILRC
set -x
mail -n -s "positix MTA $test" \
     roland@$here <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
EOF
set +x

echo "**** Ergebnis sollte sein:"
echo "(12) roland erhält Mail"
echo ".... Mail wurde in OUTGOING gesichert"
echo "****"

echo Finished $0

