#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="A.conf"

echo Running $test

source defs.sh
source server.sh

echo Creating Database
mysql -u root < ../mysql-create.sql

source x-initdb.sh

echo "Testfall extern"
echo "(5) Bekannter Absender, übergeordnet, unerwünscht"

source dump-db.sh

echo Sending test mail using $MAILRC
set -x
mail -n -s "positix MTA $test" \
     -r at$extAB1 \
     $intN1 <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
Absender $extAB1
EOF
set +x

echo Sending test mail using $MAILRC
set -x
mail -n -s "positix MTA $test" \
     -r dot@sub$extDB1 \
     $intN1 <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
Absender $extDB1
EOF
set +x

source dump-db.sh

echo "*** Ergebnis sollte sein:"
echo "(5) Zwei Mails wurden abgewiesen (sender blacklisted)"
echo "... Zwei Mails um @X und .X -Mailbezeichner zu testen"
echo "***"

echo Finished $0
