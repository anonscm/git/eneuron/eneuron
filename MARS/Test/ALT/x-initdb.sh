#!/bin/sh
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

echo Creating Database
mysql -u root < ../mysql-create.sql

if [ -z "$db" ] ; then
    source defs.sh
fi

echo Initializing Database
mysql -u root $db <<EOF
USE $db;
DELETE FROM Extern;
INSERT Extern VALUES('$extEW1', 1, NULL, NULL);
INSERT Extern VALUES('$extEW2', 1, NULL, NULL);
INSERT Extern VALUES('$extEB1', 0, NULL, NULL);
INSERT Extern VALUES('$extDW1', 1, NULL, NULL);
INSERT Extern VALUES('$extAW1', 1, NULL, NULL);
INSERT Extern VALUES('$extDB1', 0, NULL, NULL);
INSERT Extern VALUES('$extAB1', 0, NULL, NULL);

DELETE FROM Intern;
INSERT Intern VALUES('$intN1', 1, DEFAULT);
INSERT Intern VALUES('$intO1', 2, DEFAULT);
INSERT Intern VALUES('$intO2', 2, '$intO2a');
INSERT Intern VALUES('$intP1', 3, '$intP1a');
INSERT Intern VALUES('$intP2', 3, '$intP2a');
EOF
