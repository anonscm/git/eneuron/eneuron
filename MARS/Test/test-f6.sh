#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="act1.conf"
export MAILRC="mailrc.extern"

echo Running $test

source defs.sh

echo "Testfall Freischaltung"
echo "(f6) Sicherungsdatei vorhanden, falscher Absender bei erwünschter Domain"
echo "Sicherstellen, dass $extU1 tatsächlich unbekannt ist"
echo "Muss auf externen MTA ausgeführt werden!"
echo "Zwischenzeitlich wurde die Domain von $extU1 als erwünscht eingetragen"


set +x

./activation-mail.sh "$test" "$extU1" "$intN1" "test-f3s.sh" "x"

echo "**** Ergebnis sollte sein:"
echo "(f5) Aktivierung wird abgewiesen"
echo "****"


echo Finished $0
