#!/bin/bash
#  eneuron, corporate solution for email archiving
#  Copyright (C) 2009 positix GmbH
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  eneuron, Copyright (C) 2009, positix GmbH
#  eneuron comes with ABSOLUTELY NO WARRANTY.
#  This is free software, and you are welcome to redistribute it
#  under certain conditions.

test=$(basename $0 .sh)
conf="act1.conf"
export MAILRC="mailrc.extern"

echo Running $test

source defs.sh

echo "Testfall Freischaltung"
echo "(f7) Sicherungsdatei vorhanden, Aktivierung mittels pxmars"

function printMails () {
    mail <<EOF
hold
p*
EOF
}

# Delete all mails!
mail <<EOF
d*
EOF

# Send mail
echo Sending test mail using $MAILRC
set -x
mail -n -s "positix MTA $test" -r "$extU1" "$intN1" <<EOF
Dies ist eine automatisch generierte Testmail $test.
Datum: $(date '+%F %T %N')
Absender: $extU1
Die Mail sollte nur vorbehaltlich akzeptiert worden sein!
EOF
set +x

user=${extU1%%@*}

# Wait for reply
while ! mail -e -u "$user"; do
    sleep 1
done

# Grep mailid
id=$(printMails | grep -o 'u_....-..-.._..-..-.._......')

echo ID="$id"

echo "Bitte mittels pxmars Mail $id freischalten!"
echo "Dann sollte $intN1 eine Mail erhalten"

echo Finished $0
